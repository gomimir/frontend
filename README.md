# Mimir frontend

Mimir frontend serves UI to the end-user.

For more information about how to use it see [documentation](https://gomimir.gitlab.io).

## Development

This project was bootstrapped with
[Create React App](https://facebook.github.io/create-react-app/docs/getting-started).

Quick list of commands, that you can run in the project directory:

### `yarn start`

Runs the app in the development mode.<br /> Open [http://localhost:3000](http://localhost:3000) to
view it in the browser.

The page will reload if you make edits.<br /> You will also see any lint errors in the console.

### `yarn gqlgen`

Run it after you change any `*.graphql` files. It regenerates the typings.

Expects running local Mimir server.
