FROM pierrezemb/gostatic AS gostatic

FROM node:16 AS build
ADD package.json /app/package.json
ADD yarn.lock /app/yarn.lock
WORKDIR /app
RUN yarn
ADD public /app/public
ADD tsconfig.json /app/tsconfig.json
ADD src /app/src
RUN yarn build

FROM alpine:latest
RUN apk add --no-cache gettext jq
COPY --from=gostatic /goStatic /goStatic
ADD support/dockerstart.sh /start.sh
COPY --from=build /app/build /app/build
CMD ["/start.sh"]