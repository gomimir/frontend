import React, { useEffect, useState } from 'react'
import { Box, Collapse, Stack, Wrap, WrapItem } from '@chakra-ui/react'
import { FacetDescriptor } from '../facets/FacetDescriptor'
import { toggleFacetFilter } from '../facets/FacetFilter'
import { createAssetListingLink } from '../hooks/useAssetListingScreen'
import { useScreen } from '../hooks/useScreen'
import { FacetValue } from './FacetValue'

interface FacetItem {
  value: string
  label: string
  numAssets: number
  active?: boolean
}

interface Props {
  title: string
  desc: FacetDescriptor
  items?: FacetItem[]
}

export const FacetGroup: React.FC<Props> = ({ title, desc, items }) => {
  const { navigate } = useScreen()

  const handleClick = ({ value }: { value: string }) => {
    navigate(
      createAssetListingLink(({ activeIndex, facets, ...params }) => ({
        ...params,
        facets: toggleFacetFilter(facets, desc, value),
      })),
    )
  }

  const [stableItems, setStableItems] = useState<FacetItem[]>()
  useEffect(() => {
    if (items) {
      setStableItems(items)
    }
  }, [items])

  return (
    <Collapse
      in={stableItems && (stableItems.length > 0 || stableItems.some((item) => item.active))}
    >
      <Stack pb={4}>
        <Box fontWeight="semibold" __css={{ ':first-letter': { textTransform: 'capitalize' } }}>
          {title}
        </Box>
        <Wrap>
          {stableItems?.map((item) => {
            return (
              <WrapItem key={item.label}>
                <FacetValue
                  label={item.label}
                  numAssets={item.numAssets}
                  active={item.active}
                  onClick={() => handleClick(item)}
                  loading={!items}
                />
              </WrapItem>
            )
          })}
        </Wrap>
      </Stack>
    </Collapse>
  )
}
