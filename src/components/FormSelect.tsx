import { FormErrorMessage, FormControl, FormLabel } from '@chakra-ui/react'
import { useEffect } from 'react'
import { Control, Path, useController } from 'react-hook-form'
import { OptionsOrGroups, GroupBase, MultiValue, SingleValue } from 'react-select'
import { Select, SelectProps } from './ChakraReactSelect'

interface Props<TFieldValues, TOption, TMulti extends boolean = false>
  extends Omit<SelectProps<TOption, TMulti>, 'noOptionsMessage'> {
  name: Path<TFieldValues>
  control: Control<TFieldValues>
  size?: 'sm' | 'md'
  label: string
  disabled?: boolean
  defaultToFirstOption?: boolean
  noOptionsMessage?: string
  mapValue?: (o: TOption | null) => any
  isRequired?: boolean
}

export function FormSelect<TFieldValues, TOption, TMulti extends boolean = false>({
  name,
  control,
  label,
  disabled,
  options,
  defaultToFirstOption,
  noOptionsMessage,
  mapValue = (_) => _,
  size = 'sm',
  isRequired,
  isMulti,
  ...selectProps
}: Props<TFieldValues, TOption, TMulti>) {
  const {
    field: { value, onChange, ...fieldProps },
    fieldState: { error, invalid },
  } = useController({ name, control })
  const firstOption = findFirstOption(options)
  const hasOptions = !!firstOption

  // Propagating the first option to the form data, so that the reality matches what user sees
  useEffect(() => {
    if (!value && defaultToFirstOption && firstOption) {
      onChange(isMulti ? [mapValue(firstOption)] : mapValue(firstOption))
    }
  }, [value, onChange, firstOption, defaultToFirstOption, mapValue, isMulti])

  return (
    <FormControl isInvalid={invalid || (!!noOptionsMessage && !hasOptions)} isRequired={isRequired}>
      {label && (
        <FormLabel fontSize={size === 'sm' ? 'xs' : undefined} fontWeight="semibold" mb={1}>
          {label}
        </FormLabel>
      )}
      {hasOptions || !noOptionsMessage ? (
        <>
          <Select<TOption, boolean>
            size={size}
            {...fieldProps}
            {...selectProps}
            isDisabled={disabled}
            options={options}
            value={
              isMulti
                ? ((value as MultiValue<TOption>)
                    .map((value) => findOption(options, mapValue, value))
                    .filter((_) => _) as MultiValue<TOption>)
                : findOption(options, mapValue, value)
            }
            onChange={(o) =>
              onChange(
                isMulti
                  ? (o as MultiValue<TOption>).map(mapValue)
                  : mapValue(o as SingleValue<TOption>),
              )
            }
            getOptionValue={(o) => mapValue(o)}
            isMulti={isMulti}
          />
          <FormErrorMessage fontSize="xs">{error?.message}</FormErrorMessage>
        </>
      ) : (
        <FormErrorMessage mt={0} lineHeight="32px" fontSize={size === 'sm' ? 'xs' : undefined}>
          {noOptionsMessage}
        </FormErrorMessage>
      )}
    </FormControl>
  )
}

function findFirstOption<TOption>(
  groupsOrOptions: OptionsOrGroups<TOption, GroupBase<TOption>> | undefined,
): TOption | null {
  if (!groupsOrOptions) {
    return null
  }

  if (isGroups(groupsOrOptions)) {
    for (const group of groupsOrOptions) {
      if (group.options.length > 0) {
        return group.options[0]
      }
    }

    return null
  }

  return (groupsOrOptions as readonly TOption[])[0] || null
}

function findOption<TOption, TValue>(
  groupsOrOptions: OptionsOrGroups<TOption, GroupBase<TOption>> | undefined,
  mapValue: (o: TOption) => TValue,
  value: TValue,
): TOption | null {
  if (!groupsOrOptions) {
    return null
  }

  if (isGroups(groupsOrOptions)) {
    for (const group of groupsOrOptions) {
      const option = group.options.find((o) => mapValue(o) === value)
      if (option) {
        return option
      }
    }

    return null
  }

  return (groupsOrOptions as readonly TOption[]).find((o) => mapValue(o) === value) || null
}

function isGroups<TOption, TGroup extends GroupBase<TOption>>(
  groupsOrOptions: OptionsOrGroups<TOption, TGroup>,
): groupsOrOptions is readonly TGroup[] {
  return !!(groupsOrOptions as readonly TGroup[]).find((o) => Array.isArray(o.options))
}
