import { Box, Button, Center, Radio, RadioGroup, Text, useToast, VStack } from '@chakra-ui/react'
import { useCallback, useState } from 'react'
import {
  ActionType,
  AssetType,
  FileLocationType,
  IndexInput,
  useCreateIndexMutation,
  useListRepositoriesQuery,
} from '../graphql'
import { useScreen } from '../hooks/useScreen'
import { ErrorContent, ErrorMessage, ErrorTitle } from './ErrorContent'
import { IndexForm } from './IndexForm'
import {
  createActionFormData,
  createFormData,
  createRuleFormData,
  createTagKindFormData,
  IndexFormData,
} from './IndexForm/data'
import { QueryError } from './QueryError'

enum IndexCreateMode {
  Documents = 'for-documents',
  Photos = 'for-photos',
  Custom = 'custom',
}

const INITIAL_DATA: { [mode in IndexCreateMode]: (repositoryId: string) => IndexFormData } = {
  [IndexCreateMode.Documents]: (repositoryId) => ({
    ...createFormData(),
    name: 'Documents',
    assetType: AssetType.Document,
    fileLocations: [
      {
        type: FileLocationType.Crawl,
        repository: repositoryId,
        prefix: 'documents-sorted/',
      },
      {
        type: FileLocationType.Upload,
        repository: repositoryId,
        prefix: 'documents-upload/',
      },
      {
        type: FileLocationType.Sidecar,
        repository: repositoryId,
        prefix: 'documents-sidecars/',
      },

      {
        type: FileLocationType.Temp,
        repository: repositoryId,
        prefix: 'documents-temp/',
      },
    ],
    tagKinds: [
      {
        ...createTagKindFormData(),
        name: 'tag',
      },
    ],
    rules: [
      {
        ...createRuleFormData(),
        description: 'Scanned documents',
        match: '(?i)\\.(jpg|jpeg|gif|png|webp|heic|tiff|pdf)$',
        actions: [
          {
            ...createActionFormData(),
            description: 'Generate thumbnails',
            type: ActionType.Process,
            process: {
              request: 'generateThumbnails',
              queue: 'thumbnails',
              parameters: JSON.stringify(
                {
                  thumbnails: [
                    { height: 450, format: 'webp' },
                    { height: 450, format: 'jpeg' },
                    { format: 'webp' },
                    { format: 'jpeg' },
                  ],
                },
                null,
                '  ',
              ),
            },
          },
          {
            ...createActionFormData(),
            description: 'Extract text',
            type: ActionType.Process,
            process: {
              request: 'extractText',
              queue: 'extractText',
              parameters: '',
            },
          },
        ],
      },
      {
        ...createRuleFormData(),
        description: 'Dot files',
        match: '/\\.[^/]+$',
        actions: [
          {
            ...createActionFormData(),
            description: 'Ignore them',
            type: ActionType.Ignore,
            ignore: true,
          },
        ],
      },
    ],
  }),
  [IndexCreateMode.Photos]: (repositoryId) => ({
    ...createFormData(),
    name: 'Photos',
    assetType: AssetType.Photo,
    fileLocations: [
      {
        type: FileLocationType.Crawl,
        repository: repositoryId,
        prefix: 'photos-sorted/',
      },
      {
        type: FileLocationType.Upload,
        repository: repositoryId,
        prefix: 'photos-upload/',
      },
      {
        type: FileLocationType.Sidecar,
        repository: repositoryId,
        prefix: 'photos-sidecars/',
      },

      {
        type: FileLocationType.Temp,
        repository: repositoryId,
        prefix: 'photos-temp/',
      },
    ],
    tagKinds: [
      {
        ...createTagKindFormData(),
        name: 'album',
        userAssignable: false,
      },
      {
        ...createTagKindFormData(),
        name: 'pick',
      },
    ],
    rules: [
      {
        ...createRuleFormData(),
        description: 'Match all non-media files',
        inverse: true,
        match: '(?i)\\.(jpg|jpeg|cr2|arw|heic|mov|mp4|m4v)$',
        actions: [
          {
            ...createActionFormData(),
            description: 'Ignore them',
            type: ActionType.Ignore,
            ignore: true,
          },
        ],
      },
      {
        ...createRuleFormData(),
        description: 'JPEGs and HEIC photos',
        match: '(?i)\\.(jpg|jpeg|heic)$',
        actions: [
          {
            ...createActionFormData(),
            description: 'Set highest thumbnail priority',
            type: ActionType.SetParams,
            setParams: '{ "priority": 1 }',
          },
          {
            ...createActionFormData(),
            description: 'Extract EXIF information',
            type: ActionType.Process,
            process: {
              request: 'extractExif',
              queue: 'extractExif',
              parameters: '',
            },
          },
          {
            ...createActionFormData(),
            description: 'Generate thumbnails',
            type: ActionType.Process,
            process: {
              request: 'generateThumbnails',
              queue: 'thumbnails',
              parameters: JSON.stringify(
                {
                  thumbnails: [
                    { height: 450, format: 'webp' },
                    { height: 450, format: 'jpeg' },
                    { format: 'webp' },
                    { format: 'jpeg' },
                  ],
                },
                null,
                '  ',
              ),
            },
          },
        ],
      },
      {
        ...createRuleFormData(),
        description: 'RAW images',
        match: '(?i)\\.(arw|cr2)$',
        actions: [
          {
            ...createActionFormData(),
            description: 'Extract EXIF information',
            type: ActionType.Process,
            process: {
              request: 'extractExif',
              queue: 'extractExif',
              parameters: '',
            },
          },
        ],
      },
      {
        ...createRuleFormData(),
        description: 'Video files',
        match: '(?i)\\.(mov|mp4|m4v)$',
        actions: [
          {
            ...createActionFormData(),
            description: 'Extract metadata and thumbnail',
            type: ActionType.Process,
            process: {
              request: 'processVideo',
              queue: 'ffmpeg',
              parameters: '',
            },
          },
        ],
      },
      {
        ...createRuleFormData(),
        description: 'Album folders',
        match:
          '^photos-sorted/(?P<album>[^/]+)\\/(?P<subdir>(?:[^/]+\\/)*?)(?P<basename>[^/]+?)\\.(?P<suffix>[^/.\\s]+)$',
        actions: [
          {
            ...createActionFormData(),
            description: 'Tag files with the album name',
            type: ActionType.AddTag,
            addTag: {
              kind: 'album',
              // eslint-disable-next-line no-template-curly-in-string
              name: '${album}',
            },
          },
          {
            ...createActionFormData(),
            description: 'Set grouping key to ignore the file suffixes',
            type: ActionType.SetKey,
            // eslint-disable-next-line no-template-curly-in-string
            setKey: '${album}/${subdir}${basename}',
          },
        ],
      },
      {
        ...createRuleFormData(),
        description: 'Album folders with RAW / JPEG sub-folders',
        match:
          '^photos-sorted/(?P<album>[^/]+)\\/(?P<subdir>(?:[^/]+\\/)*?)(?:RAW|JPEG|MOV|Images)/(?P<basename>[^/]+?)\\.(?P<suffix>[^/.\\s]+)$',
        actions: [
          {
            ...createActionFormData(),
            description: 'Set grouping key to ignore the RAW / JPEG sub-folders',
            type: ActionType.SetKey,
            // eslint-disable-next-line no-template-curly-in-string
            setKey: '${album}/${subdir}${basename}',
          },
        ],
      },
    ],
  }),
  [IndexCreateMode.Custom]: () => createFormData(),
}

const IndexCreateOptions: React.FC<{
  value: IndexCreateMode
  onChange: (value: IndexCreateMode) => void
}> = ({ value, onChange }) => {
  const fontSize1 = ['md', 'lg']
  const fontSize2 = ['sm', 'md']

  return (
    <RadioGroup value={value} onChange={(value) => onChange(value as IndexCreateMode)}>
      <VStack alignItems="start">
        <Radio value={IndexCreateMode.Documents} size="lg">
          <Box pl={3}>
            <Text fontWeight="semibold" fontSize={fontSize1}>
              Get me started for documents
            </Text>
            <Text fontSize={fontSize2} color="gray.500">
              We will create the index from pre-prepared template. You will be able to tweak it
            </Text>
          </Box>
        </Radio>
        <Radio value={IndexCreateMode.Photos} size="lg">
          <Box pl={3}>
            <Text fontWeight="semibold" fontSize={fontSize1}>
              Get me started for photos
            </Text>
            <Text fontSize={fontSize2} color="gray.500">
              We will create the index from pre-prepared template. You will be able to tweak it
            </Text>
          </Box>
        </Radio>
        <Radio value={IndexCreateMode.Custom} size="lg">
          <Box pl={3}>
            <Text fontWeight="semibold" fontSize={fontSize1}>
              I want something special
            </Text>
            <Text fontSize={fontSize2} color="gray.500">
              We will let you start from scratch
            </Text>
          </Box>
        </Radio>
      </VStack>
    </RadioGroup>
  )
}

const IndexCreateForm: React.FC<{
  initialData: IndexFormData
}> = ({ initialData }) => {
  const [createIndex] = useCreateIndexMutation()
  const toast = useToast()
  const { navigate } = useScreen()

  const handleSubmit = useCallback(
    (data: IndexInput) => {
      createIndex({
        variables: {
          index: data,
        },
        errorPolicy: 'all',
        update: (cache, _result) => {
          cache.evict({
            fieldName: 'indices',
          })
        },
      }).then((res) => {
        if (res.data?.indexCreate.ok) {
          toast({
            status: 'success',
            title: 'Index created',
          })
          navigate({
            name: 'IndexListing',
          })
        } else {
          toast({
            status: 'error',
            title: 'Operation failed',
          })
        }
      })
    },
    [createIndex, toast, navigate],
  )

  return (
    <Box p={50} w="100%" maxW="1024px" mx="auto">
      <IndexForm
        initialData={initialData}
        onSubmit={handleSubmit}
        submitButtonText="Create index"
      />
    </Box>
  )
}

export const CreateIndex: React.FC<{
  firstTime?: boolean
}> = ({ firstTime }) => {
  const { navigate, request } = useScreen()
  const [selection, setSelection] = useState(IndexCreateMode.Documents)
  const { data: repositories, error } = useListRepositoriesQuery({
    fetchPolicy: 'cache-first',
  })

  const template = request.match?.queryParams.tpl

  if (error) {
    return <QueryError error={error} />
  }

  if (!repositories) {
    return null
  }

  if (!repositories.repositories.length) {
    return (
      <ErrorContent>
        <ErrorTitle>No repositories</ErrorTitle>
        <ErrorMessage>
          Please setup your file repositories first before creating an index
        </ErrorMessage>
      </ErrorContent>
    )
  }

  if (INITIAL_DATA[template as IndexCreateMode]) {
    return (
      <IndexCreateForm
        initialData={INITIAL_DATA[template as IndexCreateMode](repositories.repositories[0].id)}
      />
    )
  }

  return (
    <Center minH="100vh">
      <Box maxW={600} mx="auto" p={5}>
        <Text textStyle="h1">{firstTime ? 'Setup your first index' : 'Setup new index'}</Text>
        <Text>Mimir uses indices to organize your assets. Start by creating one.</Text>
        <Box pt={5} pb={3}>
          <IndexCreateOptions value={selection} onChange={setSelection} />
        </Box>
        <Button
          colorScheme="blue"
          onClick={() => navigate({ name: 'IndexCreate', params: { tpl: selection } })}
          mt={4}
        >
          {firstTime ? 'Get started' : 'Create index'}
        </Button>
      </Box>
    </Center>
  )
}
