import { Button } from '@chakra-ui/react'
import { Empty } from './Empty'
import { Index } from '../hooks/useIndex'
import { useModalManager } from '../hooks/useModalManager'
import { SearchModal } from './SearchModal'

export const EmptySearch: React.FC<{ index: Index; fulltextQuery: string }> = ({
  index,
  fulltextQuery,
}) => {
  const mm = useModalManager()
  const handleClick = () => mm((onClose) => <SearchModal index={index} onClose={onClose} />)

  return (
    <Empty
      title={`No results for "${fulltextQuery}"`}
      description="Try searching using different keywords."
    >
      <Button onClick={handleClick}>Search again</Button>
    </Empty>
  )
}
