import { useEffect, useState } from 'react'
import { CurrentUserFragment, SystemInfoQuery, useRefreshAuthMutation } from '../graphql'
import { AUTH_SESSION_STORAGE_KEY, REFRESH_SESSION_STORAGE_KEY } from '../utils/auth'

function parseDate(str: string): Date | undefined {
  const d = new Date(str)
  return isNaN(d as any) ? undefined : d
}

export const AuthRefresher: React.FC<{
  auth: SystemInfoQuery['serverInfo']['auth']
  user: CurrentUserFragment['currentUser']
}> = ({ auth, user, children }) => {
  const [willLogoutAtMsec, setWillLogoutAtMsec] = useState(
    parseDate(user.willLogoutAt || '')?.getTime(),
  )
  const [refreshAuth] = useRefreshAuthMutation()

  useEffect(() => {
    if (!willLogoutAtMsec) {
      return
    }

    const nowMsec = new Date().getTime()

    const timeoutId = setTimeout(() => {
      const refreshToken = localStorage.getItem(REFRESH_SESSION_STORAGE_KEY)
      if (refreshToken) {
        refreshAuth({
          variables: {
            refreshToken,
          },
          errorPolicy: 'all',
        }).then((res) => {
          if (res.data?.refreshAuth) {
            const { token, refreshToken, validUntil } = res.data?.refreshAuth

            localStorage.setItem(AUTH_SESSION_STORAGE_KEY, token)

            if (refreshToken) {
              localStorage.setItem(REFRESH_SESSION_STORAGE_KEY, refreshToken)
              setWillLogoutAtMsec(parseDate(validUntil)?.getTime())
            }
          }
        })
      }
    }, Math.max(willLogoutAtMsec - nowMsec - 5000, 0))

    return () => {
      clearTimeout(timeoutId)
    }
  }, [willLogoutAtMsec, refreshAuth])

  return <>{children}</>
}
