import React from 'react'
import { Box, HStack, IconButton, Tag, Text, VStack, Wrap, WrapItem } from '@chakra-ui/react'
import { Asset } from '../hooks/useAssetListing'
import { useIndex } from '../hooks/useIndex'
import { AddTagButton } from './AddTagModal'
import { AppLink } from './AppLink'
import { AssetFiles } from './AssetFiles'
import { InspectorSection } from './InspectorSection'
import { AssetType } from '../graphql'
import { getFileProperty } from '../utils/properties'
import { BsArrowBarLeft } from 'react-icons/bs'
import { useSidebar } from './SidebarLayout'
import { AssetProperties } from './AssetProperties'

// const TABS: TabBarItem[] = [
//   {
//     value: 'allResults',
//     label: 'All results',
//     icon: FiGrid,
//   },
//   {
//     value: 'selectedAsset',
//     label: 'Selected asset',
//     icon: FiImage,
//   },
// ]

const AssetName: React.FC<{ asset: Asset }> = ({ asset }) => {
  const name =
    getFileProperty(asset.properties, 'name', 'TextProperty')?.textValue ||
    asset.key.replace(/.+?([^/]+)$/, '$1')

  return (
    <Text noOfLines={1} maxW="100%" fontSize="lg" fontWeight="md">
      {name}
    </Text>
  )
}

export const AssetInspector: React.FC<{ asset: Asset }> = ({ asset }) => {
  const { hide: hideSidebar } = useSidebar()
  const index = useIndex()
  const mainFile = asset.mainFile

  const canAssignTags = index.tagKinds.some((tk) => tk.userAssignable)

  return (
    <Box pt={2} px={2}>
      <HStack w="100%" justifyContent="flex-end">
        <IconButton
          variant="ghost"
          icon={<BsArrowBarLeft />}
          aria-label="Close sidebar"
          onClick={hideSidebar}
        />
      </HStack>
      <VStack alignItems="start" spacing={4} py={2}>
        {index.assetType !== AssetType.Photo && <AssetName asset={asset} />}

        {/* <InspectorTabBar tabs={TABS} value="selectedAsset" onChange={() => {}} /> */}

        {mainFile && mainFile.properties.length && (
          <AssetProperties
            assetId={asset.id}
            filename={mainFile.filename}
            properties={mainFile.properties}
          />
        )}
        <InspectorSection title="Tags">
          <Wrap spacing={1}>
            {asset.tags.map(({ kind, name }) => (
              <WrapItem key={`${kind}_${name}`}>
                <AppLink
                  to={{
                    name: 'AssetListing',
                    params: {
                      indexName: index.id,
                      tagKind: kind,
                      tagName: name,
                    },
                  }}
                >
                  <Tag size="sm">{name}</Tag>
                </AppLink>
              </WrapItem>
            ))}
            {canAssignTags && (
              <WrapItem>
                <AddTagButton assetId={asset.id} />
              </WrapItem>
            )}
          </Wrap>
        </InspectorSection>
        <InspectorSection
          title="Files"
          badge={asset.files.length}
          collapsible={true}
          defaultOpen={false}
        >
          <AssetFiles asset={asset} index={index} />
        </InspectorSection>
      </VStack>
    </Box>
  )
}
