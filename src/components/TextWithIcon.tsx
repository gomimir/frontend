import { ChakraProps, forwardRef, HStack, Icon, Text, Tooltip } from '@chakra-ui/react'

export const TextWithIcon: React.FC<{ icon: React.ComponentType } & ChakraProps> = forwardRef(
  ({ icon, children, ...props }, ref) => (
    <HStack alignItems="center" overflow="hidden" maxW="100%" {...props} ref={ref}>
      <Icon as={icon} flexShrink={0} />
      <Text fontSize="sm" noOfLines={1}>
        {children}
      </Text>
    </HStack>
  ),
)

export const TextWithIconAndTooltip: React.FC<
  { icon: React.ComponentType; label: string } & ChakraProps
> = ({ icon, label, children, ...props }) => (
  <Tooltip label={label} placement="top-start" fontSize="xs">
    <TextWithIcon icon={icon} {...props}>
      {children}
    </TextWithIcon>
  </Tooltip>
)
