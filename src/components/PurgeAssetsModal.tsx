import { useToast } from '@chakra-ui/react'
import { IndexFragment, useDeleteAssetsMutation } from '../graphql'
import { DeleteModal } from './DeleteModal'

export const PurgeAssetsModal: React.FC<{
  index: IndexFragment
  onClose: () => void
}> = ({ index, onClose }) => {
  const [purge] = useDeleteAssetsMutation()
  const toast = useToast()

  const handleAccept = () => {
    onClose()

    purge({
      variables: {
        index: index.id,
      },
    }).then((response) => {
      if (response.data?.deleteAssetByQuery?.ok) {
        toast({
          title: 'Purge in process',
          description: `Deleting all indexed assets in ${index.name}. This might take a while.`,
          status: 'info',
        })
      } else {
        toast({
          title: 'Operation failed',
          status: 'error',
        })
      }
    })
  }

  return (
    <DeleteModal
      titleContent="Purge assets"
      buttonContent={`Purge ${index.name}`}
      onAccept={handleAccept}
      onClose={onClose}
    >
      Purging assets will remove all indexed information and uploaded files. Files in Crawl
      locations will be kept in place. Are you sure you want to purge all assets of index{' '}
      <strong>{index.name}</strong>?
    </DeleteModal>
  )
}
