import { FiFile, FiFolder } from 'react-icons/fi'
import { VStack } from '@chakra-ui/react'
import { Asset } from '../hooks/useAssetListing'
import { Index } from '../hooks/useIndex'
import { getCommonFilenameLength, getCommonPrefix } from '../utils/files'
import { TextWithIcon } from './TextWithIcon'
import { FileLocationType } from '../graphql'

export const AssetFiles: React.FC<{ asset: Asset; index: Index }> = ({ asset, index }) => {
  if (!asset.files.length) {
    return null
  }

  const startIdx = getCommonFilenameLength(asset.files)
  const commonFilename = asset.files[0].filename.substr(0, startIdx)

  const commonIndexFilesPrefix = getCommonPrefix(index.fileLocations.filter(fl => fl.type === FileLocationType.Crawl || fl.type === FileLocationType.Upload).map((fs) => fs.prefix))
  const shortenedCommonFilename = commonFilename.startsWith(commonIndexFilesPrefix)
    ? commonFilename.substr(commonIndexFilesPrefix.length)
    : commonFilename

  return (
    <VStack alignItems="start">
      <TextWithIcon icon={FiFolder}>{shortenedCommonFilename.replace(/\/$/, '')}</TextWithIcon>
      {asset.files.map((f) => (
        <TextWithIcon key={f.filename} pl={6} icon={FiFile}>
          {f.filename.substr(startIdx)}
        </TextWithIcon>
      ))}
    </VStack>
  )
}
