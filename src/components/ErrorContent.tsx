import { Button, Center, Grid, HStack, Text, VStack } from '@chakra-ui/react'
import { useOAuthLogin } from '../hooks/useOAuthLogin'
import { VersionInfo } from './VersionInfo'

export const ErrorTitle: React.FC = ({ children }) => (
  <Text color="gray.500" fontSize="xl">
    {children}
  </Text>
)
export const ErrorMessage: React.FC = ({ children }) => <Text fontSize="md">{children}</Text>
export const ErrorActions: React.FC = ({ children }) => <HStack>{children}</HStack>

export const ErrorContent: React.FC = ({ children }) => (
  <Center fontSize="xl" minH="100vh">
    <Grid maxW={600} mx="auto" p={3}>
      <VStack spacing={4} justifyItems="center" fontSize="md">
        <Text fontSize="2xl">Error :-(</Text>
        {children}
        <VersionInfo />
      </VStack>
    </Grid>
  </Center>
)

export const ErrorIndexNotFound: React.FC = () => (
  <ErrorContent>
    <ErrorTitle>Index not found</ErrorTitle>
  </ErrorContent>
)

export const ErrorUnauthorized: React.FC = () => {
  const handleLoginClick = useOAuthLogin({
    path: window.location.pathname,
  })

  return (
    <ErrorContent>
      <ErrorTitle>Unauthorized</ErrorTitle>
      <ErrorMessage>You need to login to perform this action.</ErrorMessage>
      <ErrorActions>
        <Button onClick={handleLoginClick}>Login</Button>
      </ErrorActions>
    </ErrorContent>
  )
}
