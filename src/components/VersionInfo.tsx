import { Text, useColorModeValue } from '@chakra-ui/react'
import { useVersionInfoQuery } from '../graphql'
import { VERSION } from '../version'

export const VersionInfo: React.FC = () => {
  const versionColor = useColorModeValue('gray.400', 'gray.600')
  const { data } = useVersionInfoQuery({
    fetchPolicy: 'cache-only',
  })

  return (
    <Text fontSize="sm" color={versionColor}>
      FE {VERSION}
      {data && ' / BE ' + data.serverInfo.version.versionString}
    </Text>
  )
}
