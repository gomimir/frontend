import { useToast } from '@chakra-ui/react'
import { IndexFragment, useDeleteIndexMutation } from '../graphql'
import { DeleteModal } from './DeleteModal'

export const DeleteIndexModal: React.FC<{
  index: IndexFragment
  onClose: () => void
}> = ({ index, onClose }) => {
  const [deleteIndex] = useDeleteIndexMutation()
  const toast = useToast()

  const handleAccept = () => {
    onClose()

    deleteIndex({
      variables: {
        id: index.id,
      },
      // https://github.com/apollographql/apollo-client/issues/6451#issuecomment-775242381
      update: (cache) => {
        cache.modify({
          fields: {
            indices(indices, { readField }) {
              return indices.filter((node: any) => readField('id', node) !== index.id)
            },
          },
        })
      },
    }).then((response) => {
      if (response.data?.indexDelete?.ok) {
        toast({
          title: 'Delete in process',
          description: `Deleting index ${index.name} and all of its assets. This might take a while.`,
          status: 'info',
        })
      } else {
        toast({
          title: 'Operation failed',
          status: 'error',
        })
      }
    })
  }

  return (
    <DeleteModal
      titleContent="Delete index"
      buttonContent={`Delete ${index.name}`}
      onAccept={handleAccept}
      onClose={onClose}
    >
      Delete index and all of its assets. All files will be kept in place. Are you sure you want to
      delete index <strong>{index.name}</strong>?
    </DeleteModal>
  )
}
