import { useViewportScroll } from 'framer-motion'
import { flatten, slice, times } from 'lodash'
import React, { useCallback, useEffect, useRef, useState } from 'react'
import { useHotkeys } from 'react-hotkeys-hook'
import { chakra } from '@chakra-ui/react'
import { ActiveItemActionKind, ActiveItemActionUnion } from '../helpers/ActiveItem'
import {
  adjustGridLayout,
  calculateGridLayout,
  findRowEnd,
  findRowStart,
  findVisibleRange,
  getNumRowsIn,
  GridLayout,
  growRangeByRows,
  ItemDimension,
  LayoutRect,
} from '../utils/galleryLayout'
import { getGridOptions } from '../utils/galleryOptions'
import { useContentWidth } from './ContentWidthProvider'

export interface GalleryItem extends ItemDimension {
  key: string
}

interface GalleryState extends GridLayout {
  isStable: boolean
  startIndex: number
  endIndex: number
}

function calculateVisibleRect(
  y: number,
  layoutHeight: number,
): {
  offset: number
  visibleHeight: number
} {
  const offset = y >= 0 ? 0 : 0 - y
  const visibleHeight =
    y >= 0 ? Math.max(window.innerHeight - y, 0) : Math.min(window.innerHeight, layoutHeight)

  return { offset, visibleHeight }
}

function calculateVisibleRegion(rects: LayoutRect[], y: number, layoutHeight: number) {
  const { offset, visibleHeight } = calculateVisibleRect(y, layoutHeight)

  const range = findVisibleRange(rects, offset, visibleHeight)

  const numRows = getNumRowsIn(rects, range)
  const [startIndex, endIndex] = growRangeByRows(rects, range, 1, numRows)

  return { startIndex, endIndex }
}

function recalculateLayout(
  items: ItemDimension[],
  containerRect: { width: number; y: number },
  itemBottomPadding: number,
): GalleryState {
  const containerWidth = containerRect.width

  const newLayout = calculateGridLayout(items, {
    ...getGridOptions(containerWidth),
    leftPadding: 0,
    topPadding: 0,
    rightPadding: 0,
    bottomPadding: 0,
    itemBottomPadding,
  })

  return {
    ...newLayout,
    isStable: true,
    ...calculateVisibleRegion(newLayout.rects, containerRect.y, newLayout.height),
  }
}

const GalleryViewItem: React.FC<{
  item: any
  itemId: string
  itemIndex: number

  rect: LayoutRect
  layoutIsStable: boolean
  // Note: this is treated as a function instead of React component so that we can wrap it without any performance penalty
  itemRenderer: (props: GalleryViewItemProps<any>) => React.ReactElement
  active?: boolean
  onActiveItemAction: (action: ActiveItemActionUnion) => void
}> = ({
  itemIndex,
  item,
  rect,
  layoutIsStable,
  itemRenderer,
  active = false,
  onActiveItemAction,
}) => {
  const handleClick = () => {
    if (!active) {
      onActiveItemAction({ kind: ActiveItemActionKind.SetIndex, index: itemIndex })
    }
  }

  const handleDoubleClick = () => {
    onActiveItemAction({ kind: ActiveItemActionKind.Preview, index: itemIndex })
  }

  return (
    <chakra.div
      position="absolute"
      w={rect.width}
      h={rect.height}
      transform={`translate3d(${rect.x}px, ${rect.y}px, 0)`}
      transition={
        layoutIsStable ? 'transform 0.3s ease-out, width 0.3s ease-out, height 0.3s ease-out' : ''
      }
      willChange={layoutIsStable ? undefined : 'transform, width, height'}
      onClick={handleClick}
      onDoubleClick={handleDoubleClick}
    >
      {active && (
        <chakra.div
          position="absolute"
          left="-5px"
          right="-5px"
          top="-5px"
          bottom="-5px"
          borderWidth="2px"
          borderStyle="solid"
          borderColor="gray.500"
          zIndex="-1"
        />
      )}
      {itemRenderer({ item, active })}
    </chakra.div>
  )
}

export interface GalleryViewItemProps<T> {
  item: T
  active: boolean
}

export interface GalleryItemDelegate<T> {
  getId(item: T): string
  getDimensions(item: T): ItemDimension
}

// If any of these props changes, we need to relayout
interface GalleryViewLayoutProps<T> {
  items: T[]
  itemDelegate: GalleryItemDelegate<T>
  itemBottomPadding?: number
}
interface GalleryViewProps<T> extends GalleryViewLayoutProps<T> {
  itemRenderer: (props: GalleryViewItemProps<T>) => React.ReactElement
  onMore?: (sizeHint: number) => void

  activeItemIndex?: number
  onActiveItemAction: (action: ActiveItemActionUnion) => void
}

function GalleryViewContent<T>({
  layout,
  layoutIsStable,
  startIndex,
  endIndex,
  itemDelegate,
  items,
  itemRenderer,
  activeItemIndex,
  onActiveItemAction,
  containerScroll,
}: GalleryViewProps<T> & {
  containerScroll: ContainerScrollInfo
  layout: GridLayout
  layoutIsStable: boolean
  startIndex: number
  endIndex: number
}): React.ReactElement<any, any> | null {
  useHotkeys(
    'up,down,left,right,esc,space',
    (e) => {
      e.preventDefault()
      let nextIndex: number | undefined
      if (activeItemIndex !== undefined) {
        nextIndex = activeItemIndex
        if (e.key === 'ArrowRight') {
          if (activeItemIndex < layout.rects.length - 1) {
            nextIndex = activeItemIndex + 1
          }
        } else if (e.key === 'ArrowLeft') {
          if (activeItemIndex > 0) {
            nextIndex = activeItemIndex - 1
          }
        } else if (e.key === 'ArrowDown') {
          if (layout) {
            let bestMatch = -Infinity
            nextIndex = findRowEnd(layout.rects, activeItemIndex)
            if (nextIndex + 1 < layout.rects.length) {
              const y = layout.rects[nextIndex + 1].y
              for (let j = nextIndex + 1; j < layout.rects.length && layout.rects[j].y === y; j++) {
                const left = Math.max(layout.rects[activeItemIndex].x, layout.rects[j].x)
                const right = Math.min(
                  layout.rects[activeItemIndex].x + layout.rects[activeItemIndex].width,
                  layout.rects[j].x + layout.rects[j].width,
                )
                const match = right - left
                if (match > bestMatch) {
                  bestMatch = match
                  nextIndex = j
                } else if (match < bestMatch) {
                  break // it won't be any better as we progress
                }
              }
            }
          }
        } else if (e.key === 'ArrowUp') {
          if (layout) {
            let bestMatch = -Infinity
            nextIndex = findRowStart(layout.rects, activeItemIndex)

            if (nextIndex > 0) {
              const y = layout.rects[nextIndex - 1].y
              for (let j = nextIndex - 1; j >= 0 && layout.rects[j].y === y; j--) {
                const left = Math.max(layout.rects[activeItemIndex].x, layout.rects[j].x)
                const right = Math.min(
                  layout.rects[activeItemIndex].x + layout.rects[activeItemIndex].width,
                  layout.rects[j].x + layout.rects[j].width,
                )
                const match = right - left
                if (match > bestMatch) {
                  bestMatch = match
                  nextIndex = j
                } else if (match < bestMatch) {
                  break // it won't be any better as we progress
                }
              }
            }
          }
        }
      } else if (layout.rects.length) {
        nextIndex = 0
      }
      if (nextIndex !== undefined) {
        if (nextIndex !== activeItemIndex) {
          onActiveItemAction({ kind: ActiveItemActionKind.SetIndex, index: nextIndex })
        }
      } else if (activeItemIndex !== undefined) {
        onActiveItemAction({ kind: ActiveItemActionKind.UnsetIndex })
      }
    },
    {},
    [activeItemIndex, onActiveItemAction, layout.rects],
  )

  // Maintain scroll to active item on re-layouts
  const prevActiveItemY = useRef<number>()
  useEffect(() => {
    if (activeItemIndex !== undefined) {
      const activeItemY = layout.rects[activeItemIndex].y
      if (prevActiveItemY.current !== activeItemY) {
        prevActiveItemY.current = activeItemY

        const { offset, visibleHeight } = calculateVisibleRect(containerScroll.y, layout.height)
        if (layout.rects[activeItemIndex].y < offset) {
          const scrollTop = Math.max(0, layout.rects[activeItemIndex].y - layout.options.gap)
          window.scrollTo({ top: scrollTop })
        } else if (
          layout.rects[activeItemIndex].y + layout.rects[activeItemIndex].height >
          offset + visibleHeight
        ) {
          const bottom =
            containerScroll.offsetTop +
            layout.rects[activeItemIndex].y +
            layout.rects[activeItemIndex].height +
            layout.options.gap
          const scrollTop = Math.max(bottom - window.innerHeight, 0)
          window.scrollTo({ top: scrollTop })
        }
      }
    }
  }, [activeItemIndex, layout, prevActiveItemY, containerScroll])

  let contents: React.ReactElement[] | null = null

  contents = slice(layout.rects, startIndex, endIndex + 1).map((rect, i) => {
    const realIndex = i + startIndex
    const id = itemDelegate.getId(items[realIndex])
    return (
      <GalleryViewItem
        key={id}
        itemId={id}
        itemIndex={realIndex}
        item={items[realIndex]}
        rect={rect}
        layoutIsStable={layoutIsStable}
        itemRenderer={itemRenderer}
        active={realIndex === activeItemIndex}
        onActiveItemAction={onActiveItemAction}
      />
    )
  })

  return <chakra.div>{contents}</chakra.div>
}

function calculateFetchSize(
  existingRects: ItemDimension[],
  containerWidth: number,
  itemBottomPadding: number,
) {
  let sample: ItemDimension[] =
    existingRects.length === 0 ? [{ width: 3000, height: 2000 }] : existingRects

  if (sample.length < 100) {
    sample = flatten(times(Math.ceil(100 / sample.length), () => sample))
  }

  const { startIndex, endIndex } = recalculateLayout(
    sample,
    { width: containerWidth, y: 0 },
    itemBottomPadding,
  )

  return (endIndex - startIndex) * 2
}

interface ContainerScrollInfo {
  y: number
  offsetTop: number
}

function GalleryViewFetcher<T>(
  props: GalleryViewProps<T> & {
    layout: GridLayout
    layoutIsStable: boolean
    containerScroll: ContainerScrollInfo
  },
): React.ReactElement<any, any> | null {
  const { layout, itemBottomPadding, containerScroll, onMore } = props

  const { startIndex, endIndex } = calculateVisibleRegion(
    layout.rects,
    containerScroll.y,
    layout.height,
  )

  // TODO: this is getting called a lot, it should be a noop if repeated, but we could make this stateful to make this behave
  const shouldFetchMore = layout.rects.length - endIndex < endIndex - startIndex
  const fetchSize = calculateFetchSize(
    layout.rects,
    layout.options.containerWidth,
    itemBottomPadding!,
  )

  useEffect(() => {
    if (shouldFetchMore) {
      onMore?.(fetchSize)
    }
  }, [shouldFetchMore, onMore, fetchSize])

  return <GalleryViewContent {...props} startIndex={startIndex} endIndex={endIndex} />
}

function useContainerScroll() {
  const ref = useRef<HTMLDivElement>(null!)
  const [scroll, setScroll] = useState<{ y: number; offsetTop: number }>()

  const updateContainerScroll = useCallback(() => {
    if (ref.current) {
      setScroll({ y: ref.current.getBoundingClientRect().y, offsetTop: ref.current.offsetTop })
    }
  }, [])

  // Update state on scroll
  const { scrollY } = useViewportScroll()
  useEffect(() => {
    return scrollY.onChange(updateContainerScroll)
  }, [scrollY, updateContainerScroll])

  // Set initial scroll on mount
  useEffect(updateContainerScroll, [updateContainerScroll])

  return { scroll, ref }
}

export function GalleryView<T>(props: GalleryViewProps<T>): React.ReactElement<any, any> | null {
  const { items, itemDelegate, itemBottomPadding = 0 } = props

  const calculateLayout = useCallback(
    (containerWidth: number) =>
      calculateGridLayout(
        items.map((item) => itemDelegate.getDimensions(item)),
        {
          ...getGridOptions(containerWidth),
          leftPadding: 0,
          topPadding: 0,
          rightPadding: 0,
          bottomPadding: 0,
          itemBottomPadding,
        },
      ),
    [items, itemDelegate, itemBottomPadding],
  )

  const { contentWidth } = useContentWidth()
  const [viewportState, setViewportState] = useState({
    ...calculateLayout(contentWidth),
    ready: false,
    stable: false,
  })

  const { ready, stable, ...layout } = viewportState

  const updateItems = useCallback(() => {
    setViewportState({ ...calculateLayout(contentWidth), ready, stable: true })
  }, [contentWidth, calculateLayout, ready])

  // eslint-disable-next-line react-hooks/exhaustive-deps
  useEffect(updateItems, [items])

  const updateLayout = useCallback(
    (containerWidth: number) => {
      const prevContainerWidth = viewportState.options.containerWidth
      const { ready } = viewportState

      // 1. Initial render - doing quick debounce for scrollbar twitch
      // (we set the height => scrollbar might show => we recalculate before we proceed)
      if (!ready) {
        const timer = setTimeout(() => {
          if (prevContainerWidth !== containerWidth) {
            setViewportState({ ...calculateLayout(containerWidth), ready: true, stable: true })
          } else {
            setViewportState({ ...viewportState, ready: true })
          }
        }, 50)

        return () => {
          clearTimeout(timer)
        }
      }

      // 2. Any follow up width change after initial render
      else if (prevContainerWidth !== containerWidth) {
        setViewportState({
          ...adjustGridLayout(viewportState, containerWidth),
          ready: true,
          stable: false,
        })

        const timer = setTimeout(() => {
          setViewportState({ ...calculateLayout(containerWidth), ready: true, stable: true })
        }, 500)

        return () => {
          clearTimeout(timer)
        }
      }
    },
    [viewportState, calculateLayout],
  )

  // Note: we use combination of useEffect / useCallback to keep the list of dependencies to minimum
  // The timeouts would get cleared unintentionally if we would include more then contentWidth
  useEffect(() => {
    return updateLayout(contentWidth)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [contentWidth])

  const { scroll, ref } = useContainerScroll()

  return (
    <div
      style={{
        height: layout.height,
        position: 'relative',
        // Active item border compensation
        paddingTop: '6px',
      }}
      ref={ref}
    >
      {ready && scroll && (
        <GalleryViewFetcher
          layout={layout}
          layoutIsStable={stable}
          containerScroll={scroll}
          {...props}
        />
      )}
    </div>
  )
}
