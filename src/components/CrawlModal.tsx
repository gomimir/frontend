import { useCallback } from 'react'
import { Control, FormProvider, useController, useForm, useWatch } from 'react-hook-form'
import {
  Button,
  Checkbox,
  Collapse,
  HStack,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  Radio,
  RadioGroup,
  Stack,
  Text,
  useToast,
} from '@chakra-ui/react'
import { FileLocationType, useCrawlMutation } from '../graphql'
import { FormInput } from './FormInput'
import { FormSelect } from './FormSelect'

enum CrawlMode {
  AllFiles = 'ALL_FILES',
  FileSet = 'FILE_SET',
}

interface FileLocation {
  type: FileLocationType
  repository: { id: string; name: string }
  prefix: string
}

interface CrawlFormValues {
  mode: CrawlMode
  fileLocation: FileLocation
  prefix: string
  reprocess: boolean
}

const PrefixInput: React.FC<{
  control: Control<CrawlFormValues>
}> = ({ control }) => {
  const fl = useWatch({
    control,
    name: 'fileLocation',
  })

  return (
    <FormInput leftAddon={fl.prefix} name="prefix" control={control} label="Prefix" tabIndex={3} />
  )
}

export const CrawlModal: React.FC<{
  index: {
    id: string
    fileLocations: FileLocation[]
  }
  onClose: () => void
}> = ({ index, onClose }) => {
  const usableFileLocations = index.fileLocations.filter(
    (fl) => fl.type === FileLocationType.Crawl || fl.type === FileLocationType.Upload,
  )

  const form = useForm<CrawlFormValues>({
    defaultValues: {
      mode: CrawlMode.AllFiles,
      prefix: '',
      fileLocation: usableFileLocations[0],
      reprocess: false,
    },
  })

  const { handleSubmit, setFocus, control } = form

  const {
    field: { value: crawlMode, onChange: handleCrawlModeChange },
  } = useController({
    name: 'mode',
    control,
  })

  const [crawl] = useCrawlMutation()
  const toast = useToast()

  const onSubmit = useCallback(
    (values: CrawlFormValues) => {
      onClose()

      crawl({
        variables: {
          reqs: [
            {
              index: index.id,
              fileSets:
                values.mode === CrawlMode.FileSet
                  ? [
                      {
                        repository: values.fileLocation!.repository.id,
                        prefix: values.fileLocation!.prefix + values.prefix,
                      },
                    ]
                  : undefined,
              reprocess: values.reprocess ? ['*'] : undefined,
            },
          ],
        },
      }).then((response) => {
        if (response.data?.crawl?.ok) {
          toast({
            title: 'Crawling started',
            description: 'Crawling for changes. This might take a while.',
            status: 'info',
          })
        } else {
          toast({
            title: 'Operation failed',
            status: 'error',
          })
        }
      })
    },
    [index, toast, crawl, onClose],
  )

  return (
    <Modal size="lg" isOpen={true} onClose={onClose}>
      <ModalOverlay />
      <ModalContent>
        <ModalHeader>Crawl files</ModalHeader>
        <ModalCloseButton tabIndex={-1} />
        <ModalBody>
          {usableFileLocations.length === 0 ? (
            <Text>No file locations configured.</Text>
          ) : (
            <FormProvider {...form}>
              <form onSubmit={handleSubmit(onSubmit)}>
                <Stack spacing={6}>
                  <Text>
                    Crawling will discover any new files, index them and trigger processing when
                    necessary. It can take a long time.
                  </Text>

                  <RadioGroup
                    value={crawlMode}
                    onChange={(value) => {
                      handleCrawlModeChange(value)
                      if (value === CrawlMode.FileSet) {
                        setTimeout(() => setFocus('prefix'), 0)
                      }
                    }}
                  >
                    <Stack spacing={1}>
                      <Radio value={CrawlMode.AllFiles}>Crawl all the files</Radio>
                      <Radio value={CrawlMode.FileSet}>Crawl specific file set</Radio>
                    </Stack>
                  </RadioGroup>

                  <Collapse in={crawlMode === CrawlMode.FileSet}>
                    <Stack>
                      <FormSelect
                        control={control}
                        name="fileLocation"
                        label="File location"
                        options={usableFileLocations}
                        getOptionLabel={(o) => `${o.repository.name} at ${o.prefix}`}
                        isRequired={true}
                      />
                      <PrefixInput control={control} />
                    </Stack>
                  </Collapse>

                  <Checkbox {...form.register('reprocess')}>Force reprocessing</Checkbox>
                </Stack>
              </form>
            </FormProvider>
          )}
        </ModalBody>
        <ModalFooter>
          <HStack>
            <Button
              onClick={onClose}
              tabIndex={4}
              variant={usableFileLocations.length === 0 ? 'blue' : 'ghost'}
            >
              Cancel
            </Button>
            {usableFileLocations.length > 0 && (
              <Button
                tabIndex={3}
                colorScheme="blue"
                mr={3}
                onClick={() => handleSubmit(onSubmit)()}
              >
                Crawl files
              </Button>
            )}
          </HStack>
        </ModalFooter>
      </ModalContent>
    </Modal>
  )
}
