import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalCloseButton,
  ModalBody,
  Button,
  HStack,
  ModalFooter,
  useToast,
} from '@chakra-ui/react'
import { useForm } from 'react-hook-form'
import { useUploadArchiveFromUrlMutation } from '../graphql'
import { FormInput } from './FormInput'

interface UploadFormData {
  url: string
}

export const UploadArchiveFromUrlModal: React.FC<{
  indexId: string
  onClose: () => void
}> = ({ indexId, onClose }) => {
  const [upload] = useUploadArchiveFromUrlMutation()
  const toast = useToast()

  const {
    control,
    handleSubmit,
    formState: { isValid },
  } = useForm<UploadFormData>({
    mode: 'onChange',
    defaultValues: {
      url: '',
    },
  })

  const onSubmit = (data: UploadFormData) => {
    onClose()

    upload({
      variables: {
        index: indexId,
        url: data.url,
      },
      errorPolicy: 'all',
    }).then((response) => {
      if (response.data?.uploadArchiveFromUrl?.ok) {
        toast({
          title: 'Upload initiated',
          description: 'Loading files from remote archive. This might take a while.',
          status: 'info',
        })
      } else {
        toast({
          title: 'Operation failed',
          status: 'error',
        })
      }
    })
  }

  return (
    <Modal size="lg" isOpen={true} onClose={onClose}>
      <ModalOverlay />
      <form onSubmit={handleSubmit(onSubmit)}>
        <ModalContent>
          <ModalHeader>Upload archive from URL</ModalHeader>
          <ModalCloseButton tabIndex={-1} />
          <ModalBody>
            <FormInput
              control={control}
              name="url"
              tabIndex={1}
              label="URL"
              placeholder="http://"
              rules={{ required: 'The URL is required' }}
            />
          </ModalBody>
          <ModalFooter>
            <HStack>
              <Button onClick={onClose} tabIndex={3} variant="ghost">
                Cancel
              </Button>
              <Button tabIndex={2} colorScheme="blue" mr={4} type="submit" disabled={!isValid}>
                Upload from URL
              </Button>
            </HStack>
          </ModalFooter>
        </ModalContent>
      </form>
    </Modal>
  )
}
