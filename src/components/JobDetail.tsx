import { formatDistance } from 'date-fns'
import {
  chakra,
  Progress,
  SimpleGrid,
  Stack,
  Stat,
  StatGroup,
  StatHelpText,
  StatLabel,
  StatNumber,
  Text,
} from '@chakra-ui/react'
import { formatTimeTook } from '../utils/format'
import {
  getJobDisplayName,
  getJobProgress,
  getTaskDisplayName,
  Job,
  TaskSummary,
} from '../utils/jobs'
import { TimeAgo } from './TimeAgo'

const TaskInfo: React.FC<{
  task: TaskSummary
}> = ({ task }) => {
  return (
    <Stack>
      <Text fontSize="lg">{getTaskDisplayName(task.name)}</Text>
      <Progress
        size="sm"
        value={((task.total - task.pending) / task.total) * 100}
        colorScheme={task.failed > 0 ? 'orange' : task.pending > 0 ? 'blue' : 'green'}
      />
      <chakra.table fontSize="sm">
        <chakra.tbody>
          <chakra.tr>
            <chakra.td>Total tasks:</chakra.td>
            <chakra.td textAlign="right">{task.total}</chakra.td>
          </chakra.tr>
          <chakra.tr>
            <chakra.td>Pending:</chakra.td>
            <chakra.td textAlign="right">{task.pending}</chakra.td>
          </chakra.tr>
          <chakra.tr>
            <chakra.td>Failed:</chakra.td>
            <chakra.td textAlign="right">{task.failed}</chakra.td>
          </chakra.tr>
          {task.took > 0 && (
            <chakra.tr>
              <chakra.td>Average time per task:</chakra.td>
              <chakra.td textAlign="right">
                {formatTimeTook(Math.round(task.took / (task.total - task.pending - task.failed)))}
              </chakra.td>
            </chakra.tr>
          )}
        </chakra.tbody>
      </chakra.table>
    </Stack>
  )
}

const JobProgressState: React.FC<{
  job: Job
}> = ({ job }) => {
  const progress = getJobProgress(job)

  return (
    <Stat>
      <StatLabel>In progress</StatLabel>
      <StatNumber>{progress === undefined ? 'Calculating' : `${Math.round(progress)}%`}</StatNumber>
      <StatHelpText>
        <Progress mt="1" value={progress} isIndeterminate={progress === undefined} />
      </StatHelpText>
    </Stat>
  )
}

export const JobDetail: React.FC<{
  job: Job
}> = ({ job }) => {
  return (
    <Stack spacing="4">
      <Text fontSize="2xl" pl="4">
        {getJobDisplayName(job.name)}{' '}
        <Text as="span" color="gray.400">
          (id: {job.id})
        </Text>
      </Text>
      <StatGroup
        borderWidth="1px"
        borderStyle="solid"
        borderColor="gray.300"
        borderRadius="xl"
        p="4"
      >
        <Stat>
          <StatLabel>Queued at</StatLabel>
          <StatNumber>{job.queuedAt.toLocaleString()}</StatNumber>
          <StatHelpText>
            <TimeAgo date={job.queuedAt} />
          </StatHelpText>
        </Stat>
        {job.finishedAt ? (
          <Stat>
            <StatLabel>Finished at</StatLabel>
            <StatNumber>{job.finishedAt.toLocaleString()}</StatNumber>
            <StatHelpText>Took {formatDistance(job.queuedAt, job.finishedAt)}</StatHelpText>
          </Stat>
        ) : (
          <JobProgressState job={job} />
        )}
      </StatGroup>
      <SimpleGrid minChildWidth="230px" spacing="40px" p="4">
        {job.taskSummary.map((task) => (
          <TaskInfo key={task.name} task={task} />
        ))}
      </SimpleGrid>
    </Stack>
  )
}
