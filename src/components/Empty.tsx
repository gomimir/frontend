import { Box, Center, Text } from '@chakra-ui/react'

export const Empty: React.FC<{ title: string; description?: string }> = ({
  title,
  description,
  children,
}) => (
  <Center fontSize="xl" minH="calc(100vh - 4.5rem - 3rem)">
    <Box maxW={600} mx="auto" p={5}>
      <Text textStyle="h1">{title}</Text>
      {description && (
        <Text fontSize="md" mb={6}>
          {description}
        </Text>
      )}
      {children}
    </Box>
  </Center>
)
