import { Box, Text, Icon, Circle, useColorMode } from '@chakra-ui/react'
import { mapValues } from 'lodash'
import { FiX } from 'react-icons/fi'
import { RiArrowDropDownFill } from 'react-icons/ri'

const COLOR_SHADES = {
  bgColor: [100, 500] as const,
  bgColorHighlighted: [200, 600] as const,
  bgColorCircle: [200, 600] as const,
  bgColorCircleHighlighted: [100, 700] as const,
  textColor: [500, 100] as const,
  iconColor: [400, 300] as const,
  closableHover: [200, 600] as const,
}

function useColorObj<T extends { [k: string]: readonly [number, number] }>(
  obj: T,
  colorScheme: string,
): { [k in keyof T]: string } {
  const { colorMode } = useColorMode()
  return mapValues(obj, ([light, dark]) => `${colorScheme}.${colorMode === 'dark' ? dark : light}`)
}

export const IconPill: React.FC<{
  colorScheme?:
    | 'gray'
    | 'green'
    | 'blue'
    | 'purple'
    | 'cyan'
    | 'teal'
    | 'pink'
    | 'yellow'
    | 'orange'
    | 'red'
  highlighted?: boolean
  clickable?: boolean
  hasDropdown?: boolean
  closable?: boolean
  onClose?: () => void
  icon: React.ReactElement
  text: string
}> = ({
  colorScheme = 'gray',
  highlighted = false,
  clickable = false,
  closable,
  onClose,
  hasDropdown = false,
  icon,
  text,
}) => {
  const {
    bgColor,
    bgColorHighlighted,
    bgColorCircle,
    bgColorCircleHighlighted,
    textColor,
    iconColor,
    closableHover,
  } = useColorObj(COLOR_SHADES, colorScheme)

  closable = closable ?? !!onClose
  const heightPx = 22
  const borderRadius = `${heightPx}px`

  return (
    <Box
      display="flex"
      h={`${heightPx}px`}
      borderRadius={borderRadius}
      backgroundColor={highlighted ? bgColorHighlighted : bgColor}
      alignItems="center"
      transition="background-color 0.3s ease-out"
      className="btn"
      pointerEvents={clickable || closable ? 'all' : undefined}
      _hover={
        clickable
          ? {
              cursor: 'pointer',
              backgroundColor: bgColorHighlighted,
              '.circle': {
                backgroundColor: bgColorCircleHighlighted,
              },
            }
          : undefined
      }
    >
      <Box
        borderRadius={borderRadius}
        w="22px"
        h="22px"
        backgroundColor={highlighted ? bgColorCircleHighlighted : bgColorCircle}
        display="flex"
        alignItems="center"
        justifyContent="center"
        transition="background-color 0.3s ease-out"
        className="circle"
        color={iconColor}
      >
        {icon}
      </Box>
      <Text fontSize="xs" pl={1.5} pr={hasDropdown || closable ? 0 : 2} color={textColor}>
        {text}
      </Text>
      {hasDropdown && (
        <Icon as={RiArrowDropDownFill} w="20px" h="20px" pt="1px" mr="3px" color={iconColor} />
      )}
      {closable && (
        <Circle
          size="15px"
          ml="3px"
          mr={1.5}
          _hover={{ bgColor: closableHover, cursor: 'pointer' }}
          onClick={onClose}
        >
          <Icon as={FiX} w="12px" h="12px" color={iconColor} />
        </Circle>
      )}
    </Box>
  )
}
