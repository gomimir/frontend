import { Button, HStack } from '@chakra-ui/react'
import { Operation } from '../graphql'
import { Index } from '../hooks/useIndex'
import { useModalManager } from '../hooks/useModalManager'
import { CrawlModal } from './CrawlModal'
import { Empty } from './Empty'
import { UploadModal } from './Upload'

export const EmptyIndex: React.FC<{ index: Index }> = ({ index }) => {
  const mm = useModalManager()

  const canCrawl = index.permissions.indexOperations.includes(Operation.Crawl)
  const canUpload = index.permissions.assetCollectionOperations.includes(Operation.Upload)

  const handleCrawl = () => {
    mm((onClose) => <CrawlModal index={index} onClose={onClose} />)
  }

  const handleUpload = () => {
    mm((onClose) => <UploadModal indexId={index.id} onClose={onClose} />)
  }

  return (
    <Empty
      title="Index is empty"
      description={`Index does not contain any ${index.messages.assetName()} yet. You can start by uploading few files or crawling through the existing ones.`}
    >
      <HStack>
        {canUpload && (
          <Button onClick={handleUpload} colorScheme="blue">
            Upload new files
          </Button>
        )}
        {canCrawl && <Button onClick={handleCrawl}>Crawl existing files</Button>}
      </HStack>
    </Empty>
  )
}
