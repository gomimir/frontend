import { useCallback, useEffect, useState } from 'react'
import { IconType } from 'react-icons'
import { FaQuestionCircle } from 'react-icons/fa'
import { FiAlertCircle } from 'react-icons/fi'
import { chakra, Icon, Spinner } from '@chakra-ui/react'
import { CONFIG } from '../config'

type LoadingState = 'loading' | 'loaded' | 'error'

const Image: React.FC<{
  src: string
  state: LoadingState
  setState: (newState: LoadingState) => void
  preloading?: boolean
  iconColor: string
}> = ({ src, iconColor, state, setState, preloading }) => {
  const handleLoad: React.ReactEventHandler<HTMLImageElement> = useCallback(
    (_e) => {
      setState('loaded')
    },
    [setState],
  )

  const handleError: React.ReactEventHandler<HTMLImageElement> = useCallback(
    (_e) => {
      setState('error')
    },
    [setState],
  )

  const hidden = state !== 'loaded' || preloading

  return (
    <>
      <chakra.img
        src={src}
        onLoad={handleLoad}
        onError={handleError}
        maxW="100%"
        maxH="100%"
        opacity={hidden ? 0 : 1}
        position={hidden ? 'absolute' : undefined}
        zIndex={hidden ? -1 : undefined}
      />
      {!preloading &&
        (state === 'loading' ? (
          <Spinner thickness="2px" speed="0.65s" emptyColor="gray.300" color="blue.300" size="md" />
        ) : (
          state === 'error' && <Icon as={FiAlertCircle} color={iconColor} w={6} h={6} />
        ))}
    </>
  )
}

export interface AssetThumbnailData {
  file: { url: string }
}

export const AssetThumbnail: React.FC<{
  thumbnail?: AssetThumbnailData | null
  fallbackIcon?: IconType
  iconColor: string
  onFinished?: () => void
  preloading?: boolean
}> = ({ thumbnail, fallbackIcon = FaQuestionCircle, iconColor, onFinished, preloading }) => {
  const [loading, setLoading] = useState<'loading' | 'loaded' | 'error'>(
    thumbnail ? 'loading' : 'loaded',
  )

  useEffect(() => {
    if (loading !== 'loading') {
      onFinished?.()
    }
  }, [loading, onFinished])

  if (thumbnail) {
    const src = CONFIG.httpUrl + thumbnail.file.url
    return (
      <Image
        src={src}
        iconColor={iconColor}
        state={loading}
        setState={setLoading}
        preloading={preloading}
      />
    )
  } else {
    return preloading ? null : <Icon as={fallbackIcon} color={iconColor} w={6} h={6} />
  }
}
