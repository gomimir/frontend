import React, { useContext, useMemo, useState } from 'react'
import useResizeObserver from 'use-resize-observer'

export const ContentPaddingContext = React.createContext({ left: 0, right: 0 })
export const ContentWidthContext = React.createContext(0)

export interface ViewportState {
  /** Raw width of the whole viewport */
  viewportWidth: number

  /* Viewport width - paddings */
  contentWidth: number

  leftPadding: number
  rightPadding: number
}

/**
 * Returns usable content width.
 * Note: we use global viewport calculation over calculating the width locally to the component so that we can quicky react to
 * sidebar transitions. The animated hiding / showing of the sidebars would otherwise cause recalculation problems for the GalleryView.
 */
export function useContentWidth(): ViewportState {
  const viewportPadding = useContext(ContentPaddingContext)
  const viewportWidth = useContext(ContentWidthContext)

  return {
    viewportWidth,
    contentWidth: viewportWidth - viewportPadding.left - viewportPadding.right,
    leftPadding: viewportPadding.left,
    rightPadding: viewportPadding.right,
  }
}

export const ContentWidthProvider: React.FC = ({ children }) => {
  const [width, setWidth] = useState<number | undefined>()

  const { ref } = useResizeObserver({
    onResize: ({ width: newWidth }) => {
      if (newWidth && newWidth !== width) {
        setWidth(newWidth)
      }
    },
  })

  return (
    <div ref={ref}>
      {width !== undefined && (
        <ContentWidthContext.Provider value={width}>{children}</ContentWidthContext.Provider>
      )}
    </div>
  )
}

export const ContentPaddingProvider: React.FC<{ left?: number; right?: number }> = ({
  left = 0,
  right = 0,
  children,
}) => {
  const { left: parentLeft, right: parentRight } = useContext(ContentPaddingContext)
  const childCtx = useMemo(
    () => ({ left: left + parentLeft, right: right + parentRight }),
    [left, right, parentLeft, parentRight],
  )

  return (
    <ContentPaddingContext.Provider value={childCtx}>{children}</ContentPaddingContext.Provider>
  )
}
