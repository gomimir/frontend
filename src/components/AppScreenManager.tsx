import { LocationMatch } from 'chobot'
import { Location, parsePath } from 'history'
import { pickBy } from 'lodash'
import React, { useEffect, useMemo, useState } from 'react'
import { history } from '../history'
import { router } from '../router'
import { AppRoute } from '../utils/AppRoute'
import { ErrorContent, ErrorMessage, ErrorTitle } from './ErrorContent'

export interface AppRequest {
  location: Location
  match?: LocationMatch<AppRoute>
}

export interface MatchedAppRequest extends AppRequest {
  match: LocationMatch
}

function createAppRequest(location: Location): AppRequest {
  const match = router.rootRoute.match(location) || undefined
  return {
    location,
    match,
  }
}

const NotFoundScreen: React.FC = () => {
  return (
    <ErrorContent>
      <ErrorTitle>Page not found</ErrorTitle>
      <ErrorMessage>Content you are looking for was not found on this URL</ErrorMessage>
    </ErrorContent>
  )
}

export interface Link {
  name: string
  params?: { [k: string]: any }
  hash?: string
}

interface AppScreenContextPayload {
  request: AppRequest
  navigate(link: Link): void
  navigate(createLink: (current?: AppRequest) => Link | undefined): void
}

export const AppScreenContext = React.createContext(null! as AppScreenContextPayload)

const AppScreen: React.FC<{ request: AppRequest }> = ({ request }) => {
  const screenCtx: AppScreenContextPayload = useMemo(
    () => ({
      request,
      navigate: (linkOrFactory) => {
        const link = typeof linkOrFactory === 'function' ? linkOrFactory(request) : linkOrFactory
        if (link) {
          const params = link.params
            ? // eslint-disable-next-line eqeqeq
              pickBy(link.params, (v) => v != undefined && v !== '')
            : undefined

          const url = router.createUrl(link.name, params, link.hash)

          if (url) {
            // https://github.com/ReactTraining/history/issues/814
            history.push({ search: '', hash: '', ...parsePath(url) })
          } else {
            console.warn('Unable to generate link', link.name, params)
          }
        }
      },
    }),
    [request],
  )

  if (request.match) {
    return (
      <AppScreenContext.Provider value={screenCtx}>
        {request.match.routes.reduceRight(
          (children, route) =>
            route.component ? React.createElement(route.component, {}, children) : children,
          undefined as React.ReactNode | undefined,
        )}
      </AppScreenContext.Provider>
    )
  }

  return <NotFoundScreen />
}

export const AppScreenManager: React.FC = () => {
  const [request, setRequest] = useState(createAppRequest(history.location))

  useEffect(() => {
    return history.listen(({ location }) => {
      setRequest(createAppRequest(location))
    })
  }, [])

  return <AppScreen request={request} />
}
