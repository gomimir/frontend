import { useCallback, useEffect, useRef, useState } from 'react'
import { FiCalendar, FiChevronRight, FiFile } from 'react-icons/fi'
import {
  Box,
  Collapse,
  Fade,
  HStack,
  Icon,
  Spacer,
  Stack,
  Text,
  Tooltip,
  useColorModeValue,
  useDisclosure,
} from '@chakra-ui/react'
import { Asset } from '../hooks/useAssetListing'
import { AssetListingScreenParams } from '../hooks/useAssetListingScreen'
import { ResolvedFacetsWithData } from '../hooks/useFacets'
import { getTagKind, Index } from '../hooks/useIndex'
import { createTagListingLink } from '../hooks/useTagListingScreen'
import { AppLink } from './AppLink'
import { AssetListingFilter } from './AssetListingFilter'
import { useViewportScroll } from 'framer-motion'
import { getFileProperty } from '../utils/properties'
import { AssetListingActions } from './AssetListingActions'
import { AssetActions } from './AssetActions'
import { AssetCheckbox } from './AssetCheckbox'
import { IconPill } from './IconPill'
import { ActiveItemActionKind, ActiveItemActionUnion } from '../helpers/ActiveItem'
import { formatDate, formatDateRange } from '../utils/format'
import { useItemSelection } from '../hooks/useItemSelection'

export function assetListingScreenTitle(index: Index, params: AssetListingScreenParams) {
  if (params.fulltextQuery) {
    return `Search results for "${params.fulltextQuery}"`
  }

  if (params.tag) {
    return params.tag.name
  }

  return index.messages.allAssets
}

export const HEADER_HEIGHT_PX = 44

const BigHeader: React.FC<{
  index: Index
  params: AssetListingScreenParams
}> = ({ index, params }) => {
  if (params.tag) {
    const tk = getTagKind(params.tag.kind, index)

    return (
      <Stack pt="24px">
        <Text textTransform="capitalize">
          <AppLink
            to={createTagListingLink({
              indexName: params.indexName,
              kind: params.tag.kind,
            })}
          >
            {tk.messages.kindName()}
          </AppLink>
        </Text>
        <Text textStyle="h1" lineHeight="45px">
          {assetListingScreenTitle(index, params)}
        </Text>
      </Stack>
    )
  }

  return (
    <Text textStyle="h1" pt="24px">
      {assetListingScreenTitle(index, params)}
    </Text>
  )
}

const AssetNamePill: React.FC<{
  asset: Asset
  onClose?: () => void
}> = ({ asset, onClose }) => {
  const name =
    getFileProperty(asset.properties, 'name', 'TextProperty')?.textValue ||
    asset.key.replace(/.+?([^/]+)$/, '$1')

  return (
    <IconPill
      icon={<Icon as={FiFile} w="12px" h="12px" />}
      text={name}
      colorScheme="blue"
      onClose={onClose}
    />
  )
}

const DateRangePill: React.FC<{
  startDate: Date
  endDate: Date
}> = ({ startDate, endDate }) => {
  return (
    <Tooltip
      placement="top-start"
      fontSize="xs"
      label={`${formatDate(startDate)} - ${formatDate(endDate)}`}
    >
      <Box>
        <IconPill
          icon={<Icon as={FiCalendar} w="12px" h="12px" />}
          text={formatDateRange(startDate, endDate)}
        />
      </Box>
    </Tooltip>
  )
}

interface Props {
  index: Index
  params: AssetListingScreenParams
  facets: ResolvedFacetsWithData[]
  hasUsefulFacets: boolean
  showBigHeader: boolean
  activeItemIndex?: number
  assets: Asset[]
  numAssets: number
  startDate?: Date
  endDate?: Date
  onActiveItemAction: (action: ActiveItemActionUnion) => void
}

export const AssetListingHeader: React.FC<Props> = ({
  params,
  index,
  facets,
  hasUsefulFacets,
  showBigHeader: showHeader,
  activeItemIndex,
  assets,
  onActiveItemAction,
  numAssets,
  startDate,
  endDate,
}) => {
  const { hasSelection } = useItemSelection()
  const { isOpen: filterIsOpen, onToggle: toggleFilter } = useDisclosure({
    defaultIsOpen: params.facets.length > 0,
  })

  const bigHeaderRef = useRef<HTMLDivElement>(null)
  const [fixed, setFixed] = useState(false)
  const { scrollY } = useViewportScroll()
  useEffect(() => {
    return scrollY.onChange(() =>
      setFixed(
        !!(
          bigHeaderRef.current &&
          scrollY.get() > bigHeaderRef.current.getBoundingClientRect().height
        ),
      ),
    )
  }, [scrollY])

  const borderColor = useColorModeValue('gray.200', 'gray.700')
  const bgColor = useColorModeValue('white', 'gray.800')

  const handleFilterToggle = useCallback(() => {
    if (!filterIsOpen) {
      if (fixed) {
        window.scrollTo(0, 0)
      }
      toggleFilter()
    } else {
      toggleFilter()
    }
  }, [filterIsOpen, toggleFilter, fixed])

  const title = fixed || !showHeader ? assetListingScreenTitle(index, params) : undefined

  return (
    <>
      {showHeader && (
        <Box ref={bigHeaderRef}>
          <BigHeader index={index} params={params} />
        </Box>
      )}
      {numAssets !== 0 && (
        <Box
          bgColor={bgColor}
          position="sticky"
          top={0}
          // Compensating for negative selection border on listing items
          mx="-6px"
          pl="6px"
          zIndex={1}
          h={`${HEADER_HEIGHT_PX}px`}
          shadow={fixed ? '0 14px 16px -16px #bbb' : undefined}
          borderBottom={fixed ? `1px solid ${borderColor}` : undefined}
        >
          <Stack alignItems="center" direction="row" h={`${HEADER_HEIGHT_PX}px`}>
            <HStack spacing={2} alignItems="center">
              {title &&
                (activeItemIndex !== undefined ? (
                  <>
                    <span>{title}</span>
                    <Icon as={FiChevronRight} />
                  </>
                ) : (
                  <Text fontWeight="semibold" pr={2}>
                    {title}
                  </Text>
                ))}
              {activeItemIndex !== undefined && (
                <AssetNamePill
                  asset={assets[activeItemIndex]}
                  onClose={
                    params.viewMode !== 'preview'
                      ? () => {
                          onActiveItemAction({ kind: ActiveItemActionKind.UnsetIndex })
                        }
                      : undefined
                  }
                />
              )}
              {(hasSelection || activeItemIndex === undefined) && (
                <Fade in={true}>
                  <AssetCheckbox
                    activeItemId={
                      activeItemIndex === undefined ? undefined : assets[activeItemIndex].id
                    }
                    params={params}
                    facets={facets}
                    index={index}
                  />
                </Fade>
              )}
              {activeItemIndex === undefined && !hasSelection && startDate && endDate && (
                <DateRangePill startDate={startDate} endDate={endDate} />
              )}
            </HStack>
            <Spacer display={{ base: 'none', sm: 'block' }} />
            {activeItemIndex !== undefined ? (
              <AssetActions asset={assets[activeItemIndex]} />
            ) : (
              <AssetListingActions
                params={params}
                hasUsefulFacets={hasUsefulFacets}
                onFilterToggle={handleFilterToggle}
              />
            )}
          </Stack>
        </Box>
      )}
      {numAssets !== 0 && (
        <Collapse in={filterIsOpen}>
          <AssetListingFilter facets={facets} />
        </Collapse>
      )}
    </>
  )
}
