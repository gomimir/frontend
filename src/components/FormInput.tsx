import {
  ChakraProps,
  Input,
  FormErrorMessage,
  FormControl,
  FormLabel,
  useColorModeValue,
  InputGroup,
  InputLeftAddon,
  forwardRef,
} from '@chakra-ui/react'
import { Control, Path, useController, UseControllerProps } from 'react-hook-form'

export const INPUT_BG_COLOR = ['white', 'gray.700'] as const

interface Props<TFieldValues, TName extends Path<TFieldValues>> extends ChakraProps {
  name: TName
  control: Control<TFieldValues>
  size?: 'sm' | 'md'
  label?: string
  disabled?: boolean
  placeholder?: string
  isRequired?: boolean
  leftAddon?: React.ReactNode
  rules?: UseControllerProps<TFieldValues, TName>['rules']
  tabIndex?: number
  ref?: React.MutableRefObject<any>
}

export const FormInput = forwardRef(
  (
    {
      name,
      control,
      label,
      size = 'sm',
      leftAddon,
      isRequired,
      rules,
      ...inputProps
    }: Props<any, any>,
    ref,
  ) => {
    const {
      field: { value, ...field },
      fieldState: { error, invalid },
    } = useController({ name, control, rules })
    const bgColor = useColorModeValue(...INPUT_BG_COLOR)

    // We support both isRequired (ie. for forms validated by yup) and native rules
    const effectiveIsRequired = isRequired ?? !!rules?.required

    const input = (
      <Input
        {...(field as any)}
        value={value || ''}
        bgColor={bgColor}
        size={size}
        ref={(e) => {
          field.ref(e)

          if (typeof ref === 'function') {
            ref(e)
          } else if (ref) {
            ref.current = e
          }
        }}
        {...inputProps}
      />
    )

    return (
      <FormControl isInvalid={invalid} isRequired={effectiveIsRequired}>
        {label && (
          <FormLabel fontSize={size === 'sm' ? 'xs' : undefined} fontWeight="semibold" mb={1}>
            {label}
          </FormLabel>
        )}
        {leftAddon ? (
          <InputGroup size="sm">
            <InputLeftAddon children={leftAddon} />
            {input}
          </InputGroup>
        ) : (
          input
        )}
        <FormErrorMessage fontSize={size === 'sm' ? 'xs' : undefined}>
          {error?.message}
        </FormErrorMessage>
      </FormControl>
    )
  },
) as <TFieldValues, TName extends Path<TFieldValues>>(
  props: Props<TFieldValues, TName>,
) => React.ReactElement
