import { parsePath } from 'history'
import { pickBy } from 'lodash'
import { useEffect } from 'react'
import { ChakraProps, Link } from '@chakra-ui/react'
import { history } from '../history'
import { router } from '../router'

export const AppLink: React.FC<
  {
    to: { name: string; params?: { [k: string]: any } }
    onClick?: (e: React.MouseEvent<any>) => void
  } & ChakraProps
> = ({ to, children, onClick, ...props }) => {
  // eslint-disable-next-line eqeqeq
  const params = to.params ? pickBy(to.params, (v) => v != undefined && v !== '') : undefined
  const href = router.createUrl(to.name, params)

  useEffect(() => {
    if (!href) {
      console.log('Unable to create link to', to.name, params)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [href])

  const handleClick = (e: React.MouseEvent<any>) => {
    onClick && onClick(e)

    if (!e.isDefaultPrevented() && href) {
      e.preventDefault()

      // https://github.com/ReactTraining/history/issues/814
      history.push({ search: '', hash: '', ...parsePath(href) })
    }
  }

  return (
    <Link {...props} href={href || '#'} onClick={handleClick}>
      {children}
    </Link>
  )
}
