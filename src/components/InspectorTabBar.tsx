import { IconType } from 'react-icons'
import { HStack, IconButton, Spacer, Tooltip } from '@chakra-ui/react'

export interface TabBarItem {
  value: string
  label: string
  icon: IconType
}

export const InspectorTabBar: React.FC<{
  tabs: TabBarItem[]
  value: string
  onChange: (newValue: string) => void
}> = ({ tabs, value: currentValue, onChange }) => {
  return (
    <HStack w="100%">
      <Spacer />
      {tabs.map(({ value, label, icon: IconComponent }) => (
        <Tooltip key={value} label={label} placement="top" hasArrow>
          <IconButton
            icon={<IconComponent />}
            variant={value === currentValue ? undefined : 'ghost'}
            color="current"
            aria-label={label}
            onClick={value === currentValue ? undefined : () => onChange(value)}
          />
        </Tooltip>
      ))}
      <Spacer />
    </HStack>
  )
}
