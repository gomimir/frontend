import { useHotkeys } from 'react-hotkeys-hook'
import { Center } from '@chakra-ui/layout'
import { Flex, useColorModeValue } from '@chakra-ui/react'
import { ActiveItemActionKind, ActiveItemActionUnion } from '../helpers/ActiveItem'
import { Asset } from '../hooks/useAssetListing'
import { getFileIcon } from '../utils/fileIcons'
import { AssetThumbnail } from './AssetThumbnail'
import { useCallback, useEffect, useState } from 'react'
import { range } from 'lodash'
import { getFileProperty } from '../utils/properties'
import { VideoPreview } from './VideoPreview'

const PRELOAD_NEXT = 1
const PRELOAD_KEEP_PREV = 1

const PreviewItem: React.FC<{
  asset: Asset
  preloading: boolean
  onFinished: () => void
  iconColor: string
}> = ({ asset, preloading, onFinished, iconColor }) => {
  const mainFile = asset.mainFile
  const type = getFileProperty(mainFile?.properties || [], 'type', 'TextProperty')

  if (type?.textValue === 'video') {
    return <VideoPreview file={mainFile!} preloading={preloading} />
  }

  return (
    <AssetThumbnail
      thumbnail={asset.previewThumbnail}
      fallbackIcon={getFileIcon(mainFile?.filename)}
      iconColor={iconColor}
      onFinished={onFinished}
      preloading={preloading}
    />
  )
}

export const PreviewPhoto: React.FC<{
  assets: Asset[]
  activeItemIndex: number
  onActiveItemAction: (action: ActiveItemActionUnion) => void
}> = ({ assets, activeItemIndex, onActiveItemAction }) => {
  const iconColor = useColorModeValue('gray.500', 'gray.400')
  const bgColor = useColorModeValue('gray.100', 'gray.700')

  useHotkeys(
    'up,down,left,right',
    (e) => {
      if (e.key === 'ArrowRight' || e.key === 'ArrowDown') {
        if (activeItemIndex + 1 < assets.length) {
          onActiveItemAction({ kind: ActiveItemActionKind.SetIndex, index: activeItemIndex + 1 })
        }
      } else if (e.key === 'ArrowLeft' || e.key === 'ArrowUp') {
        if (activeItemIndex > 0) {
          onActiveItemAction({ kind: ActiveItemActionKind.SetIndex, index: activeItemIndex - 1 })
        }
      }
    },
    {},
    [activeItemIndex, assets, onActiveItemAction],
  )

  const [preloadStartIndex, setPreloadStartIndex] = useState(activeItemIndex)
  useEffect(() => {
    setPreloadStartIndex((curr) => (curr >= activeItemIndex ? activeItemIndex : curr))
  }, [activeItemIndex, setPreloadStartIndex])

  const [preloadEndIndex, setPreloadEndIndex] = useState(activeItemIndex)
  useEffect(() => {
    setPreloadEndIndex((curr) => (curr <= activeItemIndex ? activeItemIndex : curr))
  }, [activeItemIndex, setPreloadEndIndex])

  const handleFinished = useCallback(() => {
    setPreloadEndIndex((curr) => curr + 1)
  }, [setPreloadEndIndex])

  const indices = range(
    Math.max(preloadStartIndex, activeItemIndex - PRELOAD_KEEP_PREV, 0),
    Math.min(preloadEndIndex, activeItemIndex + PRELOAD_NEXT, assets.length - 1) + 1,
  )

  return (
    <Flex direction="column" h="100%" w="100%">
      <Center bgColor={bgColor} flex={1} overflow="hidden">
        {indices.map((i) => {
          const asset = assets[i]
          const preloading = i !== activeItemIndex

          return (
            <PreviewItem
              key={asset.id}
              asset={asset}
              preloading={preloading}
              iconColor={iconColor}
              onFinished={handleFinished}
            />
          )
        })}
      </Center>
    </Flex>
  )
}
