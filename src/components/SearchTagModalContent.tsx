import { useCombobox } from 'downshift'
import React, { useEffect, useState } from 'react'
import { IconType } from 'react-icons'
import { FiPlus, FiSearch, FiTag } from 'react-icons/fi'
import {
  Box,
  Collapse,
  forwardRef,
  HStack,
  Icon,
  IconProps,
  Input,
  InputGroup,
  InputLeftElement,
  ModalContent,
  Text,
  useColorModeValue,
} from '@chakra-ui/react'
import { TagSortRequest, useListTagsQuery } from '../graphql'
import { Index, TagKind } from '../hooks/useIndex'

const OptionWrapper = forwardRef<{ highlighted?: boolean; disabled?: boolean }, 'div'>(
  ({ highlighted, disabled, children, ...props }, ref) => {
    const highlightedBgColor = useColorModeValue('gray.100', 'gray.600')

    return (
      <Box
        bgColor={highlighted ? highlightedBgColor : undefined}
        userSelect="none"
        px={2}
        lineHeight={2}
        cursor={disabled ? undefined : 'pointer'}
        ref={ref}
        {...props}
      >
        {children}
      </Box>
    )
  },
)

export const OptionContent: React.FC<{ icon: IconType; iconProps?: IconProps }> = ({
  icon,
  iconProps,
  children,
}) => (
  <HStack alignItems="center" overflow="hidden" maxW="100%">
    <Icon as={icon} flexShrink={0} {...iconProps} />
    <Text fontSize="sm" noOfLines={1}>
      {children}
    </Text>
  </HStack>
)

const TagOptionRenderer: React.FC<{ option: TagOption }> = ({ option }) => (
  <OptionContent icon={FiTag}>{option.tag.name}</OptionContent>
)

export interface NoMatchRendererProps {
  option: NoMatchOption
}

export type NoMatchRendererType = React.ComponentType<NoMatchRendererProps>

const NoMatchRenderer: React.FC<NoMatchRendererProps> = ({ option }) => (
  <OptionContent icon={FiPlus} iconProps={{ mt: '1px' }}>
    Add{'\xA0'}
    <Text as="span" fontStyle="italic">
      {option.text}
    </Text>
  </OptionContent>
)

export interface OptionRenderers {
  noMatch?: NoMatchRendererType
}

const DEFAULT_RENDERERS: Required<OptionRenderers> = {
  noMatch: NoMatchRenderer,
}

const OptionRenderer: React.FC<{ option: Option; renderers?: OptionRenderers }> = ({
  option,
  renderers,
}) => {
  switch (option.kind) {
    case 'tag':
      return <TagOptionRenderer option={option} />
    case 'no_match':
      const Renderer = renderers?.noMatch || DEFAULT_RENDERERS.noMatch
      return <Renderer option={option} />
    default:
      return null
  }
}

export interface TagOption {
  kind: 'tag'
  tag: {
    kind: string
    name: string
  }
}

export interface NoMatchOption {
  kind: 'no_match'
  text: string
}

export type Option = TagOption | NoMatchOption

export const SearchTagModalContent: React.FC<{
  index: Index
  onAccept: (selection: Option) => void
  tagKinds?: TagKind[]
  renderers?: OptionRenderers
  defaultSort?: TagSortRequest
}> = ({ index, onAccept, tagKinds, renderers, defaultSort }) => {
  const [nameQuery, setNameQuery] = useState<string>('')
  const trimmedName = nameQuery.trim()

  const { data } = useListTagsQuery({
    fetchPolicy: 'cache-first',
    variables: {
      index: [index.id],
      nameQuery: trimmedName === '' ? undefined : trimmedName,
      kind: tagKinds ? tagKinds.map((tk) => tk.name) : undefined,
      sort: trimmedName === '' ? defaultSort : undefined,
    },
  })

  const [items, setItems] = useState<Option[]>([])
  useEffect(() => {
    if (data) {
      const items: Option[] = data?.tags.length
        ? data.tags.map(({ kind, name }) => ({
            kind: 'tag',
            tag: { kind, name },
          }))
        : []

      if (trimmedName !== '' && data?.tags[0]?.name !== trimmedName) {
        items.unshift({
          kind: 'no_match',
          text: trimmedName,
        })
      }

      setItems(items)
    }
  }, [data, trimmedName])

  const { getInputProps, getItemProps, getMenuProps, getComboboxProps, highlightedIndex } =
    useCombobox({
      inputValue: nameQuery,
      initialIsOpen: true,
      defaultIsOpen: true,
      initialHighlightedIndex: 0,
      items,

      stateReducer: (_state, { type, changes }) => {
        switch (type) {
          case useCombobox.stateChangeTypes.InputChange:
            if (changes.highlightedIndex === -1) {
              return {
                ...changes,
                highlightedIndex: 0,
              }
            } else {
              return changes
            }

          default:
            return changes
        }
      },

      itemToString: (item) => {
        switch (item?.kind) {
          case 'tag':
            return item.tag.name

          case 'no_match':
            return item.text

          default:
            return ''
        }
      },

      onInputValueChange: ({ inputValue }) => {
        setNameQuery(inputValue || '')
      },

      onSelectedItemChange: ({ selectedItem }) => {
        onAccept(selectedItem!)
      },
    })

  return (
    <ModalContent px={2} {...getComboboxProps()}>
      <Box px={1}>
        <InputGroup h="48px">
          <InputLeftElement
            h="48px"
            pointerEvents="none"
            children={<Icon as={FiSearch} w={6} h={6} color="gray.300" />}
          />
          <Input
            placeholder="Tag name"
            size="lg"
            h="48px"
            variant="unstyled"
            {...getInputProps()}
          />
        </InputGroup>
      </Box>
      <Box px="11px">
        <Collapse in={items.length > 0}>
          <Box
            borderTopWidth="1px"
            borderTopColor="gray.300"
            pt={3}
            mb={3}
            maxH={`${5 * 32}px`}
            overflowX="hidden"
            overflowY="auto"
            {...getMenuProps()}
          >
            {items.map((item, index) => {
              return (
                <OptionWrapper
                  key={`${item}${index}`}
                  highlighted={highlightedIndex === index}
                  {...getItemProps({ item, index })}
                >
                  <OptionRenderer option={item} renderers={renderers} />
                </OptionWrapper>
              )
            })}
          </Box>
        </Collapse>
      </Box>
    </ModalContent>
  )
}
