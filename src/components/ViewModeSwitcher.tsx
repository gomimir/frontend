import * as React from 'react'
import { FaTh } from 'react-icons/fa'
import {
  IconButton,
  Menu,
  MenuButton,
  MenuItemOption,
  MenuList,
  MenuOptionGroup,
} from '@chakra-ui/react'
import { AssetListingScreenParams, useAssetListingScreen } from '../hooks/useAssetListingScreen'

export const ViewModeSwitcher: React.FC = () => {
  const { params, navigate } = useAssetListingScreen()

  const handleChange = (viewMode: AssetListingScreenParams['viewMode']) => {
    if (params?.viewMode !== viewMode) {
      navigate({ ...params, viewMode })
    }
  }

  return (
    <Menu>
      <MenuButton
        as={IconButton}
        size="md"
        fontSize="lg"
        color="current"
        variant="ghost"
        icon={<FaTh />}
        aria-label="Switch view mode"
      />
      <MenuList>
        <MenuOptionGroup
          defaultValue={params?.viewMode}
          type="radio"
          onChange={handleChange as any}
        >
          <MenuItemOption value="list" closeOnSelect={true}>
            List
          </MenuItemOption>
          <MenuItemOption value="gallery" closeOnSelect={true}>
            Gallery wall
          </MenuItemOption>
        </MenuOptionGroup>
      </MenuList>
    </Menu>
  )
}
