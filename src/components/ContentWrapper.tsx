import { Box, ChakraProps, forwardRef, useBreakpointValue } from '@chakra-ui/react'
import { ContentPaddingProvider } from './ContentWidthProvider'

const CONTENT_PADDING_PX = {
  base: 12,
  xl: 18,
}

// Provides default padding around the main content (and propagates it to contentWidth context)
export const ContentWrapper = forwardRef<ChakraProps, 'div'>(({ children, ...props }, ref) => {
  const paddingPx = useBreakpointValue(CONTENT_PADDING_PX)

  return paddingPx !== undefined ? (
    <Box ref={ref} px={`${paddingPx}px`} py="24px" {...props}>
      <ContentPaddingProvider left={paddingPx} right={paddingPx}>
        {children}
      </ContentPaddingProvider>
    </Box>
  ) : null
})
