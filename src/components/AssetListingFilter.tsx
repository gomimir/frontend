import { SimpleGrid } from '@chakra-ui/react'
import { ResolvedFacetsWithData } from '../hooks/useFacets'
import { FacetGroup } from './FacetGroup'

export const AssetListingFilter: React.FC<{ facets: ResolvedFacetsWithData[] }> = ({ facets }) => {
  return (
    <SimpleGrid minChildWidth="232px" gap={6} py={6}>
      {facets.map(({ key, desc, config, data }) => (
        <FacetGroup key={key} desc={desc} title={config.displayName} items={data} />
      ))}
    </SimpleGrid>
  )
}
