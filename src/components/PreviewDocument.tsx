import { useColorModeValue, Flex, Center } from '@chakra-ui/react'
import { CONFIG } from '../config'
import { Asset } from '../hooks/useAssetListing'
import { getFileIcon } from '../utils/fileIcons'
import { AssetThumbnail } from './AssetThumbnail'

export const PreviewDocument: React.FC<{ asset: Asset }> = ({ asset }) => {
  const iconColor = useColorModeValue('gray.500', 'gray.400')

  const pdfFile = asset.files.find((f) => f.filename.endsWith('.pdf'))
  if (pdfFile) {
    return (
      <iframe
        src={CONFIG.httpUrl + pdfFile.url}
        width="100%"
        height="100%"
        title={pdfFile.filename}
      />
    )
  }

  return (
    <Flex direction="column" h="100%" w="100%">
      <Center bgColor="gray.100" flex={1} overflow="hidden">
        <AssetThumbnail
          key={asset.id}
          thumbnail={asset.previewThumbnail}
          fallbackIcon={getFileIcon(asset.files?.[0]?.filename)}
          iconColor={iconColor}
        />
      </Center>
    </Flex>
  )
}
