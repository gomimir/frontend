// Taken from chakra-react-select example
// https://codesandbox.io/s/648uv?file=/chakra-react-select.js:0-8946

import React, { cloneElement, forwardRef } from 'react'
import ReactSelect, { components as selectComponents, GroupBase, OptionProps } from 'react-select'
import AsyncReactSelect, { AsyncProps } from 'react-select/async'
import CreatableReactSelect, { CreatableProps } from 'react-select/creatable'
import { StateManagerProps } from 'react-select/dist/declarations/src/useStateManager'
import {
  Box,
  Center,
  CloseButton,
  createIcon,
  Divider,
  Flex,
  Portal,
  StylesProvider,
  Tag,
  TagCloseButton,
  TagLabel,
  Text,
  useColorModeValue,
  useFormControl,
  useMultiStyleConfig,
  useStyles,
  useTheme,
} from '@chakra-ui/react'
import { TODO } from '../utils/todo'
import { SelectComponentsConfig } from 'react-select/dist/declarations/src/components'
import { StylesConfig } from 'react-select/dist/declarations/src/styles'
import { ThemeConfig } from 'react-select/dist/declarations/src/theme'

// Taken from the @chakra-ui/icons package to prevent needing it as a dependency
// https://github.com/chakra-ui/chakra-ui/blob/main/packages/icons/src/ChevronDown.tsx
const ChevronDown = createIcon({
  displayName: 'ChevronDownIcon',
  d: 'M16.59 8.59L12 13.17 7.41 8.59 6 10l6 6 6-6z',
})

// Custom styles for components which do not have a chakra equivalent
const chakraStyles: StylesConfig<unknown, boolean, GroupBase<unknown>> = {
  // When disabled, react-select sets the pointer-state to none
  // which prevents the `not-allowed` cursor style from chakra
  // from getting applied to the Control
  container: (provided) => ({
    ...provided,
    pointerEvents: 'auto',
  }),
  input: (provided) => ({
    ...provided,
    color: 'inherit',
    lineHeight: 1,
  }),
  menu: (provided) => ({
    ...provided,
    boxShadow: 'none',
  }),
  valueContainer: (provided, { selectProps }) => {
    const { size } = selectProps as ChakraReactSelectProps
    const px: { [k in ChakraReactSelectSize]: string } = {
      sm: '0.75rem',
      md: '1rem',
      lg: '1rem',
    }

    return {
      ...provided,
      padding: `0.125rem ${px[size!]}`,
    }
  },
  loadingMessage: (provided, { selectProps }) => {
    const { size } = selectProps as ChakraReactSelectProps
    const fontSizes: { [k in ChakraReactSelectSize]: string } = {
      sm: '0.875rem',
      md: '1rem',
      lg: '1.125rem',
    }

    const paddings: { [k in ChakraReactSelectSize]: string } = {
      sm: '6px 9px',
      md: '8px 12px',
      lg: '10px 15px',
    }

    return {
      ...provided,
      fontSize: fontSizes[size!],
      padding: paddings[size!],
    }
  },
  // Add the chakra style for when a TagCloseButton has focus
  multiValueRemove: (/*_provided, { isFocused, selectProps }*/) => {
    // const { multiValueRemoveFocusStyle } = selectProps as ChakraReactSelectProps
    // return isFocused ? multiValueRemoveFocusStyle : {},
    return {}
  },
  control: () => ({}),
  menuList: () => ({}),
  option: () => ({}),
  multiValue: () => ({}),
  multiValueLabel: () => ({}),
  group: () => ({}),
}

const chakraComponents: SelectComponentsConfig<unknown, boolean, GroupBase<unknown>> = {
  // Control components
  Control: ({ children, innerRef, innerProps, isDisabled, isFocused, selectProps }) => {
    const { size, isInvalid } = selectProps as ChakraReactSelectProps
    const inputStyles = useMultiStyleConfig('Input', { size })
    const bgColor = useColorModeValue('white', 'gray.700')

    const chakraTheme = useTheme()

    const heights: { [k in ChakraReactSelectSize]: number } = {
      sm: 8,
      md: 10,
      lg: 12,
    }

    const borderRadii: { [k in ChakraReactSelectSize]: number } = {
      sm: chakraTheme.radii.sm,
      md: chakraTheme.radii.md,
      lg: chakraTheme.radii.md,
    }

    return (
      <StylesProvider value={inputStyles}>
        <Flex
          ref={innerRef}
          sx={{
            ...inputStyles.field,
            p: 0,
            overflow: 'hidden',
            h: 'auto',
            minH: heights[size as ChakraReactSelectSize],
            bg: bgColor,
            borderRadius: borderRadii[size!],
          }}
          {...innerProps}
          data-focus={isFocused ? true : undefined}
          data-invalid={isInvalid ? true : undefined}
          data-disabled={isDisabled ? true : undefined}
        >
          {children}
        </Flex>
      </StylesProvider>
    )
  },
  MultiValueContainer: ({ children, innerRef, innerProps, data, selectProps }: TODO) => (
    <Tag
      ref={innerRef}
      {...innerProps}
      m="0.125rem"
      // react-select Fixed Options example: https://react-select.com/home#fixed-options
      variant={data.isFixed ? 'solid' : 'subtle'}
      colorScheme={data.colorScheme || selectProps.colorScheme}
      size={selectProps.size}
    >
      {children}
    </Tag>
  ),
  MultiValueLabel: ({ children, innerRef, innerProps }: TODO) => (
    <TagLabel ref={innerRef} {...innerProps}>
      {children}
    </TagLabel>
  ),
  MultiValueRemove: ({ children, innerRef, innerProps, data: { isFixed } }: TODO) => {
    if (isFixed) {
      return null
    }

    return (
      <TagCloseButton ref={innerRef} {...innerProps} tabIndex={-1}>
        {children}
      </TagCloseButton>
    )
  },
  IndicatorSeparator: ({ innerProps }: TODO) => (
    <Divider {...innerProps} orientation="vertical" opacity="1" />
  ),
  ClearIndicator: ({ innerProps, selectProps: { size } }: TODO) => (
    <CloseButton {...innerProps} size={size} mx={2} tabIndex={-1} />
  ),
  DropdownIndicator: ({ innerProps, selectProps: { size } }: TODO) => {
    const { addon } = useStyles()
    const bgColor = useColorModeValue('white', 'gray.700')

    const iconSizes: { [k in ChakraReactSelectSize]: number } = {
      sm: 4,
      md: 5,
      lg: 6,
    }

    const iconSize = iconSizes[size as ChakraReactSelectSize]

    return (
      <Center
        {...innerProps}
        sx={{
          ...addon,
          h: '100%',
          borderRadius: 0,
          borderWidth: 0,
          cursor: 'pointer',
          bg: bgColor,
          px: 2,
        }}
      >
        <ChevronDown h={iconSize} w={iconSize} />
      </Center>
    )
  },
  // Menu components
  MenuPortal: ({ children }) => <Portal>{children}</Portal>,
  Menu: ({ children, ...props }) => {
    const menuStyles = useMultiStyleConfig('Menu', {})
    return (
      <selectComponents.Menu {...props}>
        <StylesProvider value={menuStyles}>{children}</StylesProvider>
      </selectComponents.Menu>
    )
  },
  MenuList: ({ innerRef, children, maxHeight, selectProps }) => {
    const { size } = selectProps as ChakraReactSelectProps
    const { list } = useStyles()
    const chakraTheme = useTheme()

    const borderRadii: { [k in ChakraReactSelectSize]: number } = {
      sm: chakraTheme.radii.sm,
      md: chakraTheme.radii.md,
      lg: chakraTheme.radii.md,
    }

    return (
      <Box
        sx={{
          ...list,
          maxH: `${maxHeight}px`,
          overflowY: 'auto',
          borderRadius: borderRadii[size!],
        }}
        ref={innerRef}
      >
        {children}
      </Box>
    )
  },
  GroupHeading: ({ innerProps, children }: TODO) => {
    const { groupTitle } = useStyles()
    return (
      <Box sx={groupTitle} {...innerProps}>
        {children}
      </Box>
    )
  },
  Option: ({ innerRef, innerProps, children, isFocused, isDisabled, selectProps }) => {
    const { size } = selectProps as ChakraReactSelectProps
    const { item } = useStyles()

    return (
      <Box
        role="button"
        sx={{
          ...item,
          w: '100%',
          textAlign: 'start',
          bg: isFocused ? (item as TODO)._focus.bg : 'transparent',
          fontSize: size,
          ...(isDisabled && (item as TODO)._disabled),
        }}
        ref={innerRef}
        {...innerProps}
        {...(isDisabled && { disabled: true })}
      >
        {children}
      </Box>
    )
  },
}

export type ChakraReactSelectSize = 'sm' | 'md' | 'lg'

interface ChakraReactSelectProps {
  isDisabled?: boolean
  isInvalid?: boolean
  colorScheme?: string
  size?: ChakraReactSelectSize

  styles?: TODO
  components?: TODO
  theme?: ThemeConfig
}

const ChakraReactSelect: React.FC<
  ChakraReactSelectProps & { children: React.ReactElement } & StateManagerProps
> = ({
  children,
  styles = {},
  components = {},
  theme = (t) => t,
  size = 'md',
  colorScheme = 'gray',
  isDisabled,
  isInvalid,
  ...props
}) => {
  const chakraTheme = useTheme()

  // Combine the props passed into the component with the props
  // that can be set on a surrounding form control to get
  // the values of isDisabled and isInvalid
  const inputProps = useFormControl({ isDisabled, isInvalid })

  // The chakra theme styles for TagCloseButton when focused
  const closeButtonFocus = chakraTheme.components.Tag.baseStyle.closeButton._focus
  const multiValueRemoveFocusStyle = {
    background: closeButtonFocus.bg,
    boxShadow: chakraTheme.shadows[closeButtonFocus.boxShadow],
  }

  // The chakra UI global placeholder color
  // https://github.com/chakra-ui/chakra-ui/blob/main/packages/theme/src/styles.ts#L13
  const placeholderColor = useColorModeValue(
    chakraTheme.colors.gray[400],
    chakraTheme.colors.whiteAlpha[400],
  )

  const textColor = useColorModeValue(
    chakraTheme.colors.gray[800],
    chakraTheme.colors.whiteAlpha[900],
  )

  // Ensure that the size used is one of the options, either `sm`, `md`, or `lg`
  let realSize = size
  const sizeOptions = ['sm', 'md', 'lg']
  if (!sizeOptions.includes(size)) {
    realSize = 'md'
  }

  // react-select theme
  const effectiveTheme: ThemeConfig = (baseTheme) => {
    return typeof theme == 'function'
      ? theme({
          ...baseTheme,
          borderRadius: 8,
          colors: {
            ...baseTheme.colors,
            neutral50: placeholderColor, // placeholder text color
            neutral40: placeholderColor, // noOptionsMessage color
            neutral80: textColor, // text color
          },
          spacing: { ...baseTheme.spacing, baseUnit: 0 },
        })
      : theme
  }

  const select = cloneElement(children, {
    components: {
      ...chakraComponents,
      ...components,
    },
    styles: {
      ...chakraStyles,
      ...styles,
    },
    theme: effectiveTheme,
    colorScheme,
    size: realSize,
    multiValueRemoveFocusStyle,
    // isDisabled and isInvalid can be set on the component
    // or on a surrounding form control
    isDisabled: inputProps.disabled,
    isInvalid: !!inputProps['aria-invalid'],
    ...props,
  })

  return select
}

export type SelectProps<TOption, TMulti extends boolean = false> = ChakraReactSelectProps &
  StateManagerProps<TOption, TMulti, GroupBase<TOption>>

export const Select = forwardRef<any, ChakraReactSelectProps & StateManagerProps>((props, ref) => (
  <ChakraReactSelect {...props}>
    <ReactSelect ref={ref} />
  </ChakraReactSelect>
)) as <TOption, TMulti extends boolean = false>(
  props: SelectProps<TOption, TMulti> & {
    ref?: React.ForwardedRef<ReactSelect>
  },
) => ReturnType<typeof ChakraReactSelect>

export const AsyncSelect = forwardRef<
  any,
  ChakraReactSelectProps & AsyncProps<unknown, boolean, GroupBase<unknown>>
>((props, ref) => (
  <ChakraReactSelect {...props}>
    <AsyncReactSelect ref={ref} />
  </ChakraReactSelect>
))

export const CreatableSelect = forwardRef<
  any,
  ChakraReactSelectProps & CreatableProps<unknown, boolean, GroupBase<unknown>>
>((props, ref) => (
  <ChakraReactSelect {...props}>
    <CreatableReactSelect ref={ref} />
  </ChakraReactSelect>
))

export function DescriptiveOptionRenderer<TOption extends { label: string; description: string }>({
  data,
  isFocused,
  innerRef,
  innerProps,
  isDisabled,
}: OptionProps<TOption, false>) {
  const { item } = useStyles()

  return (
    <Box
      role="button"
      py={1}
      px={4}
      sx={{ ...item, bg: isFocused ? (item as TODO)._focus.bg : 'transparent' }}
      ref={innerRef}
      {...innerProps}
      {...(isDisabled && { disabled: true })}
    >
      <Text fontSize="sm" fontWeight="semibold">
        {data.label}
      </Text>
      <Text fontSize="xs" color="gray.500">
        {data.description}
      </Text>
    </Box>
  )
}
