import { map } from 'lodash'
import React, { useEffect, useState } from 'react'
import { Box } from '@chakra-ui/layout'
import { Collapse, Progress, Stack } from '@chakra-ui/react'
import { TaskSummaryByName } from '../hooks/useJobProgress'
import { UploadProgress as UploadState } from '../hooks/useUpload'
import { getTaskDisplayName } from '../utils/jobs'

const TaskProgress: React.FC<{ progress: number }> = ({ progress, children }) => {
  const [isOpen, setOpen] = useState(false)

  useEffect(() => {
    setOpen(true)
  }, [])

  return (
    <Collapse in={isOpen} animateOpacity>
      <Stack>
        <Box>{children}</Box>
        <Progress value={progress} size="xs" />
      </Stack>
    </Collapse>
  )
}

export const UploadProgress: React.FC<{
  uploadProgress: UploadState
  processingProgress: TaskSummaryByName
}> = ({ uploadProgress, processingProgress }) => {
  const {
    index = {
      total: 0,
      pending: 0,
    },
    waitForAllFiles,
    ...otherTasks
  } = processingProgress

  const allChunksIndexed = index.total - index.pending === uploadProgress.totalChunks

  return (
    <Stack spacing="4">
      <Stack>
        <Box>Uploading files</Box>
        <Progress
          value={Math.floor((uploadProgress.finishedBytes / uploadProgress.totalBytes) * 100)}
          size="xs"
        />
      </Stack>
      {uploadProgress.finishedChunks > 0 && (
        <TaskProgress
          progress={Math.floor(((index.total - index.pending) / uploadProgress.totalChunks) * 100)}
        >
          Indexing files
        </TaskProgress>
      )}
      {allChunksIndexed &&
        uploadProgress.finishedChunks > 0 &&
        map(otherTasks, ({ total, pending }, name) => (
          <TaskProgress progress={Math.floor(((total - pending) / total) * 100)} key={name}>
            {getTaskDisplayName(name)}
          </TaskProgress>
        ))}
    </Stack>
  )
}
