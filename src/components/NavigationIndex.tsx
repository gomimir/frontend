import { Flex, IconButton, Spacer, Tooltip, VStack } from '@chakra-ui/react'
import { FiSearch, FiTag } from 'react-icons/fi'
import { IoAlbumsOutline } from 'react-icons/io5'
import { BiSelectMultiple } from 'react-icons/bi'
import { createAssetListingLink } from '../hooks/useAssetListingScreen'
import { Index, TagKind } from '../hooks/useIndex'
import { useModalManager } from '../hooks/useModalManager'
import { createTagListingLink } from '../hooks/useTagListingScreen'
import { indexIcon } from '../screens/IndexListingScreen'
import { AppLink } from './AppLink'
import { NavigationBottom } from './NavigationBottom'
import { SearchModal } from './SearchModal'
import { Upload } from './Upload'

const SearchButton: React.FC<{ index: Index }> = ({ index }) => {
  const mm = useModalManager()
  const handleClick = () => mm((onClose) => <SearchModal index={index} onClose={onClose} />)

  return (
    <Tooltip label="Search" placement="right">
      <IconButton
        icon={<FiSearch size="22" />}
        variant="ghost"
        color="current"
        onClick={handleClick}
        aria-label="Search"
      />
    </Tooltip>
  )
}

function tagKindIcon(tagKindName: string) {
  switch (tagKindName) {
    case 'album':
      return IoAlbumsOutline
    case 'pick':
      return BiSelectMultiple
    default:
      return FiTag
  }
}

const TagButton: React.FC<{ index: Index; tagKind: TagKind }> = ({ index, tagKind }) => {
  const TagIcon = tagKindIcon(tagKind.name)

  return (
    <AppLink to={createTagListingLink({ indexName: index.id, kind: tagKind.name })}>
      <Tooltip label={tagKind.messages.allTags} placement="right">
        <IconButton
          icon={<TagIcon size="20" />}
          variant="ghost"
          color="current"
          aria-label={tagKind.messages.allTags}
        />
      </Tooltip>
    </AppLink>
  )
}

const AllButton: React.FC<{ index: Index }> = ({ index }) => {
  const IndexIcon = indexIcon(index)

  return (
    <AppLink to={createAssetListingLink({ indexName: index.id })}>
      <Tooltip label={index.messages.allAssets} placement="right">
        <IconButton
          icon={<IndexIcon size="22" />}
          variant="ghost"
          color="current"
          aria-label={index.messages.allAssets}
        />
      </Tooltip>
    </AppLink>
  )
}

export const NavigationIndex: React.FC<{ index: Index }> = ({ index }) => {
  return (
    <Flex direction="column" w="100%" h="100%" alignItems="center" py={3} color="gray.400">
      <VStack>
        <SearchButton index={index} />
        <AllButton index={index} />
        {index.tagKinds.map((tk) => (
          <TagButton key={tk.name} index={index} tagKind={tk} />
        ))}
        <Upload />
      </VStack>
      <Spacer />
      <NavigationBottom />
    </Flex>
  )
}
