import React, { useCallback, useEffect, useRef, useState } from 'react'
import { useForm } from 'react-hook-form'
import { FiPlus } from 'react-icons/fi'
import {
  Button,
  HStack,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  useDisclosure,
} from '@chakra-ui/react'
import { SortMethod, TagOrder } from '../graphql'
import { useAssetUpdate } from '../hooks/useAssetUpdate'
import { TagKind, useIndex } from '../hooks/useIndex'
import { AddNewTagForm, AddNewTagFormValues } from './AddNewTagForm'
import { Option, SearchTagModalContent } from './SearchTagModalContent'

const CreateTagModalContent: React.FC<{
  name: string
  onSubmit: (data: AddNewTagFormValues) => void
  onClose: () => void
  tagKinds: TagKind[]
}> = ({ name, onClose, onSubmit, tagKinds }) => {
  const form = useForm<AddNewTagFormValues>({
    mode: 'onChange',
    defaultValues: { kind: tagKinds[0]?.name, name },
  })
  const {
    handleSubmit,
    formState: { isValid: isFormValid },
  } = form

  // This component first renders with empty form (no inputs are registered)
  // as such it renders form invalid, this causes submit button to be disabled
  // and we focus wrong target on render. This ensures that the initial state is valid
  // (we know that because we prefill the default values)
  const internalRef = useRef(false)
  const [isSubmitDisabled, setIsSubmitDisabled] = useState<boolean>(false)
  useEffect(() => {
    if (internalRef.current) {
      setIsSubmitDisabled(!isFormValid)
    } else {
      internalRef.current = true
    }
  }, [isFormValid])

  return (
    <ModalContent>
      <ModalHeader>Add new tag</ModalHeader>
      <ModalCloseButton tabIndex={-1} />
      <ModalBody>
        <AddNewTagForm form={form} onSubmit={onSubmit} tagKinds={tagKinds} />
      </ModalBody>
      <ModalFooter>
        <HStack>
          <Button size="sm" onClick={onClose} tabIndex={-1} variant="ghost">
            Cancel
          </Button>
          <Button
            size="sm"
            tabIndex={1}
            colorScheme="blue"
            mr={3}
            onClick={() => handleSubmit(onSubmit)()}
            disabled={isSubmitDisabled}
          >
            Assign a new tag
          </Button>
        </HStack>
      </ModalFooter>
    </ModalContent>
  )
}

const DEFAULT_SORT = {
  by: TagOrder.LastUserAssignedAt,
  method: SortMethod.Desc,
}

const AddTagModal: React.FC<{
  onAccept: (tag: { kind: string; name: string }) => void
  onClose: () => void
}> = ({ onClose, onAccept }) => {
  const index = useIndex()
  const assignableTagKinds = index.tagKinds.filter((tk) => tk.userAssignable)

  const [state, setState] = useState<{ mode: 'select' } | { mode: 'create'; name: string }>({
    mode: 'select',
  })

  const handleSelect = useCallback(
    (option: Option) => {
      if (option.kind === 'tag') {
        onAccept(option.tag)
        onClose()
      } else if (option.kind === 'no_match') {
        setState({ mode: 'create', name: option.text })
      }
    },
    [onClose, onAccept],
  )

  const handleCreate = useCallback(
    ({ kind, name }: { kind: string; name: string }) => {
      onAccept({ kind, name })
      onClose()
    },
    [onClose, onAccept],
  )

  return (
    <Modal size="lg" isOpen={true} onClose={onClose} closeOnOverlayClick={true} closeOnEsc={true}>
      <ModalOverlay />

      {state.mode === 'select' ? (
        <SearchTagModalContent
          index={index}
          onAccept={handleSelect}
          tagKinds={assignableTagKinds}
          defaultSort={DEFAULT_SORT}
        />
      ) : (
        <CreateTagModalContent
          name={state.name}
          onClose={onClose}
          onSubmit={handleCreate}
          tagKinds={assignableTagKinds}
        />
      )}
    </Modal>
  )
}

export const AddTagButton: React.FC<{ assetId: string }> = ({ assetId }) => {
  const { isOpen, onOpen, onClose } = useDisclosure()
  const { update: updateAsset } = useAssetUpdate()

  const handleTagAssignment = useCallback(
    ({ kind, name }: { kind: string; name: string }) => {
      updateAsset(assetId, {
        addTags: [
          {
            kind,
            name,
          },
        ],
      })
    },
    [assetId, updateAsset],
  )

  return (
    <>
      <Button
        variant="ghost"
        size="xs"
        fontWeight={500}
        h="20px"
        iconSpacing="3px"
        leftIcon={<FiPlus />}
        display="inline-flex"
        onClick={onOpen}
      >
        Add tag
      </Button>
      {isOpen && <AddTagModal onClose={onClose} onAccept={handleTagAssignment} />}
    </>
  )
}
