import { isApolloError, ServerError } from '@apollo/client'
import { ErrorContent, ErrorMessage, ErrorTitle, ErrorUnauthorized } from './ErrorContent'

export const QueryError: React.FC<{ error: Error }> = ({ error }) => {
  if (isApolloError(error)) {
    if (error.networkError) {
      const response = (error.networkError as ServerError).response
      if (response && response.status === 422) {
        return (
          <ErrorContent>
            <ErrorTitle>Server were unable to process request</ErrorTitle>
            <ErrorMessage>
              It seems that the request sent to the server was invalid. This might mean that you are
              running mismatched version of Mimir frontend and server. Please check you are running
              the latest version of both and report the error if you do.
            </ErrorMessage>
          </ErrorContent>
        )
      }

      return (
        <ErrorContent>
          <ErrorTitle>Connection failed</ErrorTitle>
          <ErrorMessage>
            There was an error while connecting to the Mimir server. This might mean you are having
            network issues or that the server is temporarily down. Please check your Internet
            connection and try again later.
          </ErrorMessage>
        </ErrorContent>
      )
    }

    if (error.graphQLErrors.some((e) => e.extensions?.['code'] === 'UNAUTHORIZED')) {
      return <ErrorUnauthorized />
    }

    if (error.graphQLErrors.some((e) => e.extensions?.['code'] === 'FORBIDDEN')) {
      return (
        <ErrorContent>
          <ErrorTitle>Forbidden</ErrorTitle>
          <ErrorMessage>You do not have enough permissions to perform this action.</ErrorMessage>
        </ErrorContent>
      )
    }
  }

  return (
    <ErrorContent>
      <ErrorTitle>Unknown error occurred</ErrorTitle>
      <ErrorMessage>
        There was an unexpected error while communicating to the Mimir server. Please reach out to
        the support to help you out. We are sorry for the inconvenience.
      </ErrorMessage>
    </ErrorContent>
  )
}
