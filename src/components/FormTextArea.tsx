import {
  ChakraProps,
  Textarea,
  FormErrorMessage,
  FormControl,
  FormLabel,
  useColorModeValue,
} from '@chakra-ui/react'
import { Control, Path, useController } from 'react-hook-form'
import { INPUT_BG_COLOR } from './FormInput'

export function FormTextArea<TFieldValues>({
  name,
  control,
  label,
  isRequired,
  ...inputProps
}: {
  name: Path<TFieldValues>
  control: Control<TFieldValues>
  label?: string
  disabled?: boolean
  placeholder?: string
  isRequired?: boolean
} & ChakraProps) {
  const bgColor = useColorModeValue(...INPUT_BG_COLOR)
  const {
    field,
    fieldState: { error, invalid },
  } = useController({ name, control })

  return (
    <FormControl isInvalid={invalid} isRequired={isRequired}>
      {label && (
        <FormLabel fontSize="xs" fontWeight="semibold" mb={1}>
          {label}
        </FormLabel>
      )}
      <Textarea {...(field as any)} size="sm" bg={bgColor} {...inputProps} />
      <FormErrorMessage fontSize="xs">{error?.message}</FormErrorMessage>
    </FormControl>
  )
}
