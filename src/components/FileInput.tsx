import React, { useRef } from 'react'
import { Control, useController } from 'react-hook-form'
import { FiFile } from 'react-icons/fi'
import {
  Button,
  FormControl,
  FormErrorMessage,
  FormLabel,
  HStack,
  Icon,
  Input,
  InputGroup,
  InputLeftElement,
} from '@chakra-ui/react'

export const FileInput: React.FC<{
  name: string
  control: Control
  isRequired?: boolean
  tabIndex?: number
}> = ({ name, control, children, isRequired = false, tabIndex }) => {
  const inputRef = useRef<any>()
  const {
    field: { ref, value, onChange, ...inputProps },
    fieldState: { invalid, error },
  } = useController({
    name,
    control,
    rules: { required: isRequired },
  })

  return (
    <FormControl isInvalid={invalid} isRequired={isRequired}>
      <FormLabel htmlFor="writeUpFile">{children}</FormLabel>
      <input
        type="file"
        ref={inputRef}
        {...inputProps}
        multiple
        style={{ display: 'none' }}
        onChange={(e) => onChange(e.target.files)}
      />
      <HStack width="100%">
        <InputGroup>
          <InputLeftElement pointerEvents="none" children={<Icon as={FiFile} />} />
          <Input
            placeholder={
              value?.length
                ? value.length === 1
                  ? value[0].name
                  : `${value.length} files selected`
                : 'No files selected'
            }
            onClick={() => inputRef.current.click()}
            readOnly
            tabIndex={-1}
          />
        </InputGroup>
        <Button tabIndex={tabIndex} onClick={() => inputRef.current.click()}>
          Select
        </Button>
      </HStack>
      <FormErrorMessage>{error?.message}</FormErrorMessage>
    </FormControl>
  )
}
