import { formatDistance } from 'date-fns'
import { useEffect, useState } from 'react'
import { ChakraProps, Text } from '@chakra-ui/react'

export const TimeAgo: React.FC<{ date: Date; message?: string } & ChakraProps> = ({
  date,
  message = '%v ago',
  ...props
}) => {
  const [now, updateNow] = useState(new Date())

  useEffect(() => {
    const intervalId = setInterval(() => {
      updateNow(new Date())
    }, 1000)

    return () => {
      clearInterval(intervalId)
    }
  }, [])

  return <Text {...props}>{message.replace('%v', formatDistance(date, now))}</Text>
}
