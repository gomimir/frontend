import {
  RadioGroup,
  VStack,
  Radio,
  Box,
  Text,
  Modal,
  Button,
  HStack,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
} from '@chakra-ui/react'
import { useCallback, useState } from 'react'
import { CONFIG } from '../config'
import { Asset } from '../hooks/useAssetListing'
import { getCommonFilenameLength } from '../utils/files'
import { formatBytes } from '../utils/format'

export function download(relativeFileUrl: string) {
  const url = new URL(CONFIG.httpUrl + relativeFileUrl)
  url.searchParams.append('download', '')

  window.location.replace(url.toString())
}

const FileSelection: React.FC<{
  asset: Asset
  value: string
  onChange: (newValue: string) => void
}> = ({ asset, value, onChange }) => {
  const startIdx = getCommonFilenameLength(asset.files)

  return (
    <RadioGroup value={value} onChange={onChange}>
      <VStack alignItems="start">
        {asset.files.map((f) => (
          <Radio key={f.url} value={f.url} size="md">
            <Box pl={3}>
              <Text fontWeight="semibold" fontSize="sm">
                {f.filename.substring(startIdx)}
              </Text>
              {f.fileInfo.size && (
                <Text color="gray.500" fontSize="xs">
                  {formatBytes(f.fileInfo.size, 2)}
                </Text>
              )}
            </Box>
          </Radio>
        ))}
      </VStack>
    </RadioGroup>
  )
}

export const DownloadModal: React.FC<{ asset: Asset; onClose: () => void }> = ({
  asset,
  onClose,
}) => {
  const [selectedFileRelUrl, setFileSelection] = useState(asset.files[0].url)
  const handleSubmit = useCallback(() => {
    onClose()
    download(selectedFileRelUrl)
  }, [onClose, selectedFileRelUrl])

  return (
    <Modal size="lg" isOpen={true} onClose={onClose} closeOnOverlayClick={true} closeOnEsc={true}>
      <ModalOverlay />
      <ModalContent>
        <ModalHeader>Download</ModalHeader>
        <ModalCloseButton tabIndex={-1} />
        <ModalBody>
          <FileSelection asset={asset} value={selectedFileRelUrl} onChange={setFileSelection} />
        </ModalBody>
        <ModalFooter>
          <HStack>
            <Button onClick={onClose} tabIndex={4} variant="ghost">
              Cancel
            </Button>
            <Button tabIndex={1} colorScheme="blue" mr={3} onClick={handleSubmit}>
              Download
            </Button>
          </HStack>
        </ModalFooter>
      </ModalContent>
    </Modal>
  )
}
