import React from 'react'
import { UseFormReturn } from 'react-hook-form'
import { FiCornerDownRight } from 'react-icons/fi'
import { Box } from '@chakra-ui/layout'
import { Checkbox, HStack, Icon, Stack } from '@chakra-ui/react'
import { FileInput } from './FileInput'

export const UploadForm: React.FC<{ form: UseFormReturn; onSubmit: (data: any) => void }> = ({
  form,
  onSubmit,
}) => {
  const { register, control, handleSubmit } = form

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <Stack spacing={2}>
        <FileInput name="files" control={control} tabIndex={1} isRequired>
          Files
        </FileInput>
        <HStack pl={15} align="center">
          <Icon as={FiCornerDownRight} color="GrayText" />
          <Box pt={1}>
            <Checkbox {...register('allFilesAreSingleAsset')}>
              All files belong to single asset
            </Checkbox>
          </Box>
        </HStack>
      </Stack>
    </form>
  )
}
