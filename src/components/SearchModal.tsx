import { FiSearch } from 'react-icons/fi'
import { Modal, ModalOverlay, Text } from '@chakra-ui/react'
import { SortMethod, TagOrder } from '../graphql'
import { useAssetListingScreen } from '../hooks/useAssetListingScreen'
import { Index } from '../hooks/useIndex'
import {
  NoMatchRendererProps,
  Option,
  OptionContent,
  OptionRenderers,
  SearchTagModalContent,
} from './SearchTagModalContent'

const DEFAULT_SORT = {
  by: TagOrder.LastAssignedAt,
  method: SortMethod.Desc,
}

const NoMatchRenderer: React.FC<NoMatchRendererProps> = ({ option }) => (
  <OptionContent icon={FiSearch}>
    Search{'\xA0'}for{'\xA0'}
    <Text as="span" fontStyle="italic">
      {option.text}
    </Text>
  </OptionContent>
)

const OPTION_RENDERERS: OptionRenderers = {
  noMatch: NoMatchRenderer,
}

export const SearchModal: React.FC<{
  index: Index
  onClose: () => void
}> = ({ index, onClose }) => {
  const { navigate } = useAssetListingScreen()
  const tagKinds = index.tagKinds

  const handleAccept = (option: Option) => {
    onClose()

    if (option.kind === 'tag') {
      navigate(({ indexName, viewMode }) => ({
        indexName,
        viewMode,
        tag: option.tag,
        facets: [],
      }))
    } else if (option.kind === 'no_match') {
      navigate(({ indexName, viewMode }) => ({
        indexName,
        viewMode,
        fulltextQuery: option.text,
        facets: [],
      }))
    }
  }

  return (
    <Modal size="lg" isOpen={true} onClose={onClose} closeOnOverlayClick={true} closeOnEsc={true}>
      <ModalOverlay />
      <SearchTagModalContent
        index={index}
        onAccept={handleAccept}
        defaultSort={DEFAULT_SORT}
        tagKinds={tagKinds}
        renderers={OPTION_RENDERERS}
      />
    </Modal>
  )
}
