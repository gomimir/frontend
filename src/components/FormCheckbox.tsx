import { ChakraProps, Checkbox, Text } from '@chakra-ui/react'
import { Control, Path, useController } from 'react-hook-form'

export function FormCheckbox<TFieldValues>({
  name,
  control,
  label,
  disabled,
  ...inputProps
}: {
  name: Path<TFieldValues>
  control: Control<TFieldValues>
  label: string
  disabled?: boolean
} & ChakraProps) {
  const {
    field: { value, ...rest },
  } = useController({ name, control })

  return (
    <Checkbox {...(rest as any)} isChecked={value} isDisabled={disabled} {...inputProps}>
      <Text fontSize="sm">{label}</Text>
    </Checkbox>
  )
}
