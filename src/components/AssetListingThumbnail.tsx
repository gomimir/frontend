import { AspectRatio, Box, chakra, Icon, useColorModeValue } from '@chakra-ui/react'
import { getFileIcon } from '../utils/fileIcons'
import { AssetThumbnail, AssetThumbnailData } from './AssetThumbnail'
import { RiCheckLine } from 'react-icons/ri'
import { primaryInput } from 'detect-it'

export const PLACEHOLDER_IMAGE_DIMENSIONS = {
  width: 3000,
  height: 2000,
}

interface AssetListingThumbnailData {
  files?: Array<{ filename: string }>
  galleryThumbnail: (AssetThumbnailData & { width: number; height: number }) | null
}

const GalleryItemCheckbox: React.FC<{
  bgColor?: string
  selected: boolean
  onToggle: () => void
}> = ({ selected, onToggle, bgColor = '#eeeeeeee' }) => {
  const handleClick = (e: React.MouseEvent<unknown>) => {
    e.stopPropagation()
    onToggle()
  }

  return (
    <Box
      position="absolute"
      left="12px"
      top="12px"
      borderRadius="22px"
      w="22px"
      h="22px"
      backgroundColor={selected ? 'green.400' : bgColor}
      display="flex"
      alignItems="center"
      justifyContent="center"
      visibility={selected ? 'visible' : 'hidden'}
      opacity={selected ? 1 : 0}
      transition="visibility 0.3s ease-out, opacity 0.3s ease-out, background-color 0.3s ease-out"
      _hover={{
        cursor: 'pointer',
        '.check': {
          color: selected ? 'green.100' : 'gray.600',
        },
      }}
      onClick={handleClick}
      className="checkbox"
    >
      <Icon
        as={RiCheckLine}
        w="18px"
        h="18px"
        className="check"
        color={selected ? '#fff' : '#00000022'}
      />
    </Box>
  )
}

export const AssetListingThumbnail: React.FC<{
  asset: AssetListingThumbnailData | null
  selected?: boolean
  onToggleSelect?: () => void
}> = ({ asset, selected = false, onToggleSelect }) => {
  const bgColor = useColorModeValue('gray.200', 'gray.700')
  const iconColor = useColorModeValue('gray.500', 'gray.400')

  const { width, height } = asset?.galleryThumbnail || PLACEHOLDER_IMAGE_DIMENSIONS

  return (
    <AspectRatio
      ratio={width / height}
      bg={bgColor}
      _hover={
        primaryInput === 'touch'
          ? undefined
          : {
              '.checkbox': {
                visibility: 'visible',
                opacity: '1',
              },
            }
      }
    >
      <chakra.div position="relative">
        {onToggleSelect && asset && (
          <GalleryItemCheckbox selected={selected} onToggle={onToggleSelect} />
        )}
        <AssetThumbnail
          thumbnail={asset?.galleryThumbnail}
          fallbackIcon={getFileIcon(asset?.files?.[0]?.filename)}
          iconColor={iconColor}
        />
      </chakra.div>
    </AspectRatio>
  )
}
