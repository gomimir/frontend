import { Flex, Spacer } from '@chakra-ui/react'
import { NavigationBottom } from './NavigationBottom'

export const NavigationHome: React.FC = () => {
  return (
    <Flex direction="column" w="100%" h="100%" alignItems="center" py={3} color="gray.400">
      <Spacer />
      <NavigationBottom />
    </Flex>
  )
}
