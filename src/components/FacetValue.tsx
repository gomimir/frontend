import React from 'react'
import { chakra, Fade, Text, useColorModeValue } from '@chakra-ui/react'

interface Props {
  active?: boolean
  onClick?: () => void
  label: string
  numAssets: number
  loading: boolean
}

export const FacetValue: React.FC<Props> = ({ active, onClick, label, numAssets, loading }) => {
  const bgColor = useColorModeValue(
    active ? 'blue.100' : 'gray.100',
    active ? 'blue.500' : 'whiteAlpha.200',
  )

  const bgColor2 = useColorModeValue(
    active ? 'blue.200' : 'gray.200',
    active ? 'blue.600' : 'whiteAlpha.300',
  )

  const bgColorSkeleton = useColorModeValue(
    active ? 'blue.300' : 'gray.300',
    active ? 'blue.600' : 'gray.600',
  )

  return (
    <chakra.span
      display="inline-flex"
      alignItems="center"
      fontSize="xs"
      fontWeight="medium"
      lineHeight={1.2}
      onClick={onClick}
      _hover={{
        cursor: 'pointer',
      }}
      __css={{
        '&:hover > *': {
          cursor: 'pointer',
          background: bgColor2,
        },
      }}
    >
      <chakra.span
        display="inline-flex"
        bg={bgColor}
        borderLeftRadius="md"
        px={2}
        minH="1.5rem"
        alignItems="center"
        position="relative"
        color={loading ? 'transparent' : 'inherit'}
      >
        <Text noOfLines={1} maxW="160px">
          {label}
        </Text>
        <Fade in={loading}>
          <chakra.div
            bg={bgColorSkeleton}
            position="absolute"
            top={2}
            bottom={2}
            left={2}
            right={2}
          ></chakra.div>
        </Fade>
      </chakra.span>
      <chakra.span
        display="inline-flex"
        bg={bgColor2}
        borderRightRadius="md"
        px={2}
        minH="1.5rem"
        alignItems="center"
        marginLeft="1px"
        fontSize="smaller"
        position="relative"
        color={loading ? 'transparent' : 'inherit'}
      >
        {numAssets}
      </chakra.span>
    </chakra.span>
  )
}
