import { Box, Grid, Icon, IconButton, Tooltip, useColorModeValue } from '@chakra-ui/react'
import { FiTrash } from 'react-icons/fi'
import { BOX_BG_COLOR, REMOVE_BUTTON_HOVER_BG_COLOR } from './constants'

export const RepeatingGroup: React.FC<{
  remove: () => void
}> = ({ children, remove }) => {
  const bgColor = useColorModeValue(...BOX_BG_COLOR)
  const hoverColor = useColorModeValue(...REMOVE_BUTTON_HOVER_BG_COLOR)

  return (
    <Box
      shadow="sm"
      borderLeftRadius="lg"
      borderTopRightRadius={'lg'}
      bgColor={bgColor}
      px={4}
      pt={6}
      pb={4}
      position="relative"
    >
      <Grid
        templateColumns={{
          md: 'repeat(2, 1fr)',
          lg: 'repeat(3, 1fr)',
        }}
        rowGap={3}
        columnGap={10}
        w="100%"
      >
        {children}
      </Grid>
      <Tooltip label="Remove the item" placement="top">
        <IconButton
          position="absolute"
          right="5px"
          top="5px"
          aria-label="remove"
          size="sm"
          color="gray.500"
          bgColor={bgColor}
          _hover={{ bgColor: hoverColor, color: 'gray.700' }}
          icon={<Icon as={FiTrash} />}
          onClick={remove}
        />
      </Tooltip>
    </Box>
  )
}
