import {
  ActionType,
  AssetType,
  FileLocationType,
  GetIndexDetailQuery,
  Operation,
} from '../../graphql'
import { array, mixed, object, string } from 'yup'

export interface ProcessActionConfig {
  request: string
  queue: string
  parameters: string
}

export interface AddTagActionConfig {
  kind: string
  name: string
}

export interface Action {
  description: string
  enabled: boolean
  type: ActionType

  setKey: string
  setParams: string
  addTag: AddTagActionConfig
  ignore: boolean
  process: ProcessActionConfig
}

export interface Rule {
  description: string
  enabled: boolean
  match: string
  inverse: boolean
  actions: Action[]
}

export interface TagKind {
  name: string
  displayName: string
  displayNamePlural: string
  userAssignable: boolean
}

export interface FileLocation {
  type: FileLocationType
  repository: string
  prefix: string
}

export interface Permissions {
  subjectId: string
  indexOperations: Set<Operation>
  assetCollectionOperations: Set<Operation>
  assetOperations: Set<Operation>
}

export interface IndexFormData {
  name: string
  assetType: AssetType
  fileLocations: FileLocation[]
  tagKinds: TagKind[]
  rules: Rule[]
  permissions: Permissions[]
}

export function createActionFormData(
  data?: GetIndexDetailQuery['index']['rules'][number]['actions'][number],
): Action {
  const res = {
    description: data?.description || '',
    enabled: data?.enabled ?? true,
    type: ActionType.Unknown,

    setKey: '',
    setParams: '',
    addTag: {
      kind: '',
      name: '',
    },
    ignore: true,
    process: {
      request: '',
      queue: '',
      parameters: '',
    },
  }

  if (data?.config?.__typename === 'IgnoreActionConfig') {
    res.type = ActionType.Ignore
    res.ignore = data.config.ignore
  } else if (data?.config?.__typename === 'SetKeyActionConfig') {
    res.type = ActionType.SetKey
    res.setKey = data.config.key
  } else if (data?.config?.__typename === 'SetParamsActionConfig') {
    res.type = ActionType.SetParams
    res.setParams =
      data.config.params !== null ? JSON.stringify(data.config.params, null, '  ') : ''
  } else if (data?.config?.__typename === 'AddTagActionConfig') {
    res.type = ActionType.AddTag
    res.addTag = {
      kind: data.config.tag.kind || '',
      name: data.config.tag.name || '',
    }
  } else if (data?.config?.__typename === 'ProcessActionConfig') {
    res.type = ActionType.Process
    res.process = {
      request: data.config.request || '',
      queue: data.config.queue || '',
      parameters: data.config.params !== null ? JSON.stringify(data.config.params, null, '  ') : '',
    }
  }

  return res
}

export function createFileLocationFormData(
  data?: GetIndexDetailQuery['index']['fileLocations'][number],
): FileLocation {
  return {
    type: data?.type || FileLocationType.Crawl,
    repository: data?.repository.id || '',
    prefix: data?.prefix || '',
  }
}

export function createTagKindFormData(
  data?: Partial<GetIndexDetailQuery['index']['tagKinds'][number]>,
): TagKind {
  return {
    name: data?.name || '',
    displayName: data?.displayName || '',
    displayNamePlural: data?.displayNamePlural || '',
    userAssignable: data?.userAssignable ?? true,
  }
}

export function createPermissionsFormData(
  data?: Partial<GetIndexDetailQuery['index']['allPermissions'][number]>,
): Permissions {
  return {
    subjectId: data?.subject?.id || '',
    indexOperations: new Set<Operation>(data?.permissions?.indexOperations || []),
    assetCollectionOperations: new Set<Operation>(
      data?.permissions?.assetCollectionOperations || [],
    ),
    assetOperations: new Set<Operation>(data?.permissions?.assetOperations || []),
  }
}

export function createRuleFormData(data?: GetIndexDetailQuery['index']['rules'][number]): Rule {
  return {
    description: data?.description || '',
    match: data?.match || '',
    inverse: data?.inverse || false,
    enabled: data?.enabled ?? true,
    actions: data?.actions.map(createActionFormData) || [],
  }
}

export function createFormData(data?: Partial<GetIndexDetailQuery['index']>): IndexFormData {
  return {
    name: data?.name || '',
    assetType: data?.assetType || AssetType.Unknown,
    fileLocations: data?.fileLocations?.map(createFileLocationFormData) || [],
    tagKinds: data?.tagKinds?.map(createTagKindFormData) || [],
    rules: data?.rules?.map(createRuleFormData) || [],
    permissions: data?.allPermissions?.map(createPermissionsFormData) || [],
  }
}

const jsonTest = (mustBeObj?: boolean) => (val: any) => {
  if (val) {
    try {
      const res = JSON.parse(val)
      return !mustBeObj || (typeof res === 'object' && !Array.isArray(res))
    } catch (err) {
      return false
    }
  }
  return true
}

export const indexSchema = object({
  name: string().required(),
  assetType: mixed().not([AssetType.Unknown], 'Please select asset type'),
  fileLocations: array(
    object({
      repository: string().required(),
      prefix: string()
        .required()
        .matches(/^[^#%&{}<>*?$!'":@\\]+$/, 'Prefix must not contain unsafe characters')
        .matches(/\/$/, 'Prefix should end with slash /')
        .matches(/^[^/]/, 'Prefix must not start with slash')
        .when('type', {
          is: (type: FileLocationType) => type === FileLocationType.Temp,
          then: (schema) =>
            schema.matches(
              /^[^/]+\/$/,
              'Temporary files can be stored only directly in top-level directories (buckets)',
            ),
        }),
    }),
  ),
  tagKinds: array(
    object({
      name: string()
        .required()
        .lowercase()
        .matches(
          /^[a-z0-9][a-z0-9._-]*$/,
          'Name should only contain characters a-z0-9._- and start with an alphanum character',
        ),
    }),
  ),
  rules: array(
    object({
      description: string().required(),
      actions: array(
        object({
          description: string().required(),
          type: mixed().not([ActionType.Unknown], 'Please select action type'),
          setKey: string().when('type', {
            is: (type: ActionType) => type === ActionType.SetKey,
            then: (schema) => schema.required(),
          }),
          setParams: string().when('type', {
            is: (type: ActionType) => type === ActionType.SetParams,
            then: (schema) =>
              schema.required().test('JSON', 'Must be valid JSON object', jsonTest(true)),
          }),
          addTag: object().when('type', {
            is: (type: ActionType) => type === ActionType.AddTag,
            then: (_) =>
              object({
                kind: string().required(),
                name: string().required(),
              }),
          }),
          process: object().when('type', {
            is: (type: ActionType) => type === ActionType.Process,
            then: (_) =>
              object({
                request: string().required(),
                queue: string().required(),
                parameters: string().test('JSON', 'Must be valid JSON document', jsonTest()),
              }),
          }),
        }),
      ),
    }),
  ),
  permissions: array(
    object({
      subjectId: string().required(),
    }),
  ),
})
