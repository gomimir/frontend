import {
  Box,
  Text,
  GridItem,
  Grid,
  Button,
  Flex,
  Icon,
  IconButton,
  Stack,
  StackDivider,
  Switch,
  Tooltip,
  useColorModeValue,
} from '@chakra-ui/react'
import { Control, useWatch, Controller, useFieldArray } from 'react-hook-form'
import { ActionType } from '../../graphql'
import { createActionFormData, IndexFormData } from './data'
import { FiTrash, FiPlus } from 'react-icons/fi'
import { DescriptiveOptionRenderer } from '../ChakraReactSelect'
import { ACTION_TYPE_OPTIONS, REMOVE_BUTTON_HOVER_BG_COLOR } from './constants'
import { FormInput } from '../FormInput'
import { FormSelect } from '../FormSelect'
import { FormTextArea } from '../FormTextArea'
import { FormCheckbox } from '../FormCheckbox'

const AddTagAction: React.FC<{
  actionIndex: number
  actionEnabled: boolean
  ruleIndex: number
  control: Control<IndexFormData>
}> = (props) => {
  const { actionIndex, actionEnabled, ruleIndex, control } = props
  const tagKinds = useWatch({ control, name: 'tagKinds' })

  return (
    <>
      <GridItem>
        <FormSelect
          control={control}
          name={`rules.${ruleIndex}.actions.${actionIndex}.addTag.kind`}
          label="Kind"
          disabled={!actionEnabled}
          defaultToFirstOption={true}
          noOptionsMessage="No tag kinds defined"
          options={tagKinds}
          getOptionLabel={(o) => o.displayName || o.name}
          mapValue={(o) => o?.name}
          isRequired={true}
        />
      </GridItem>
      {tagKinds.length > 0 && (
        <GridItem>
          <FormInput
            control={control}
            name={`rules.${ruleIndex}.actions.${actionIndex}.addTag.name`}
            label="Name"
            placeholder="Tag name"
            disabled={!actionEnabled}
            isRequired={true}
          />
        </GridItem>
      )}
    </>
  )
}

const ActionDetails: React.FC<{
  actionIndex: number
  actionEnabled: boolean
  ruleIndex: number
  control: Control<IndexFormData>
}> = (props) => {
  const { actionIndex, actionEnabled, ruleIndex, control } = props
  const actionType = useWatch({ control, name: `rules.${ruleIndex}.actions.${actionIndex}.type` })

  return (
    <Grid templateColumns="repeat(3, 1fr)" rowGap={3} columnGap={10} w="100%" pt={4}>
      <GridItem>
        <FormSelect
          control={control}
          name={`rules.${ruleIndex}.actions.${actionIndex}.type`}
          disabled={!actionEnabled}
          label="Type"
          components={{ Option: DescriptiveOptionRenderer }}
          options={ACTION_TYPE_OPTIONS}
          mapValue={(o) => o?.value}
          isRequired={true}
        />
      </GridItem>
      {actionType === ActionType.Ignore && (
        <GridItem h="28px" pt="27px" colSpan={2}>
          <FormCheckbox
            control={control}
            name={`rules.${ruleIndex}.actions.${actionIndex}.ignore`}
            disabled={!actionEnabled}
            label="Prevent this file from indexing"
          />
        </GridItem>
      )}
      {actionType === ActionType.SetKey && (
        <GridItem colSpan={2}>
          <FormInput
            control={control}
            name={`rules.${ruleIndex}.actions.${actionIndex}.setKey`}
            label="Grouping key"
            placeholder="Key"
            disabled={!actionEnabled}
            isRequired={true}
          />
        </GridItem>
      )}
      {actionType === ActionType.Process && (
        <>
          <GridItem>
            <FormInput
              control={control}
              name={`rules.${ruleIndex}.actions.${actionIndex}.process.request`}
              label="Request"
              placeholder="Key"
              disabled={!actionEnabled}
              isRequired={true}
            />
          </GridItem>
          <GridItem>
            <FormInput
              control={control}
              name={`rules.${ruleIndex}.actions.${actionIndex}.process.queue`}
              label="Queue"
              placeholder="Queue name"
              disabled={!actionEnabled}
              isRequired={true}
            />
          </GridItem>
          <GridItem colSpan={3}>
            <FormTextArea
              control={control}
              label="Parameters"
              name={`rules.${ruleIndex}.actions.${actionIndex}.process.parameters`}
              placeholder="JSON formatted parameters"
              disabled={!actionEnabled}
            />
          </GridItem>
        </>
      )}
      {actionType === ActionType.SetParams && (
        <GridItem colSpan={3}>
          <FormTextArea
            control={control}
            name={`rules.${ruleIndex}.actions.${actionIndex}.setParams`}
            label="Parameters"
            placeholder="JSON formatted parameters"
            disabled={!actionEnabled}
            isRequired={true}
          />
        </GridItem>
      )}
      {actionType === ActionType.AddTag && <AddTagAction {...props} />}
    </Grid>
  )
}

const Action: React.FC<{
  index: number
  ruleIndex: number
  ruleEnabled: boolean
  remove: (index: number) => void
  control: Control<IndexFormData>
}> = ({ ruleIndex, ruleEnabled, index, control, remove }) => {
  const hoverColor = useColorModeValue(...REMOVE_BUTTON_HOVER_BG_COLOR)
  const enabled =
    useWatch({ control, name: `rules.${ruleIndex}.actions.${index}.enabled` }) && ruleEnabled

  return (
    <Flex pl={4} pr={2} py={1}>
      <Text
        fontSize="sm"
        mr={3}
        minW="25px"
        textAlign="right"
        textDecoration={enabled ? undefined : 'line-through'}
        lineHeight="31px"
      >
        {ruleIndex + 1}.{index + 1}.
      </Text>
      <Box flex="1">
        <Flex alignItems="center">
          <FormInput
            control={control}
            name={`rules.${ruleIndex}.actions.${index}.description`}
            placeholder="Action description"
            disabled={!enabled}
            isRequired={true}
          />
        </Flex>
        <ActionDetails
          actionEnabled={enabled}
          actionIndex={index}
          ruleIndex={ruleIndex}
          control={control}
        />
      </Box>
      <Controller
        render={({ field }) => (
          <Tooltip label={field.value ? 'Disable the action' : 'Enable the action'} placement="top">
            <Box ml={4} mr={1} mt="3px">
              <Switch onChange={field.onChange} isChecked={field.value} isDisabled={!ruleEnabled} />
            </Box>
          </Tooltip>
        )}
        name={`rules.${ruleIndex}.actions.${index}.enabled`}
        control={control}
      />
      <Tooltip label="Remove the rule" placement="top">
        <IconButton
          aria-label="remove"
          size="sm"
          color="gray.500"
          bgColor="transparent"
          _hover={{ bgColor: hoverColor, color: 'gray.700' }}
          icon={<Icon as={FiTrash} />}
          onClick={() => remove(index)}
        />
      </Tooltip>
    </Flex>
  )
}

export const Actions: React.FC<{
  ruleIndex: number
  ruleEnabled: boolean
  control: Control<IndexFormData>
}> = ({ control, ruleIndex, ruleEnabled }) => {
  const { fields, append, remove } = useFieldArray({
    control,
    name: `rules.${ruleIndex}.actions`,
  })

  return (
    <Stack spacing={3} divider={<StackDivider borderColor="gray.300" />}>
      {fields.map((item, index) => (
        <Action
          key={item.id}
          index={index}
          control={control}
          remove={remove}
          ruleEnabled={ruleEnabled}
          ruleIndex={ruleIndex}
        />
      ))}
      <Box px={2}>
        <Button
          size="sm"
          leftIcon={<FiPlus />}
          onClick={() => append(createActionFormData())}
          disabled={!ruleEnabled}
        >
          Add action
        </Button>
      </Box>
    </Stack>
  )
}
