import { Stack, Box, Button, GridItem } from '@chakra-ui/react'
import { Control, useFieldArray, useWatch } from 'react-hook-form'
import { FiPlus } from 'react-icons/fi'
import { resolveEnglishPlural } from '../../utils/messages'
import { FormCheckbox } from '../FormCheckbox'
import { FormInput } from '../FormInput'
import { RepeatingGroup } from './common'
import { createTagKindFormData, IndexFormData } from './data'

const TagKind: React.FC<{
  index: number
  remove: (index: number) => void
  control: Control<IndexFormData>
}> = ({ index, remove, control }) => {
  const name = useWatch({ control, name: `tagKinds.${index}.name` })
  const displayName = useWatch({ control, name: `tagKinds.${index}.displayName` })
  const displayNamePlural = useWatch({ control, name: `tagKinds.${index}.displayNamePlural` })

  const baseName = displayName || displayNamePlural || name
  const displayNameDefaults = baseName ? resolveEnglishPlural(baseName) : []

  return (
    <RepeatingGroup remove={() => remove(index)}>
      <GridItem>
        <FormInput
          control={control}
          name={`tagKinds.${index}.name`}
          label="Name"
          placeholder="Name"
          isRequired={true}
        />
      </GridItem>
      <GridItem>
        <FormInput
          control={control}
          name={`tagKinds.${index}.displayName`}
          label="Display name"
          placeholder={displayNameDefaults[0] && `Default: ${displayNameDefaults[0]}`}
        />
      </GridItem>
      <GridItem>
        <FormInput
          control={control}
          name={`tagKinds.${index}.displayNamePlural`}
          label="Display name (plural)"
          placeholder={displayNameDefaults[0] && `Default: ${displayNameDefaults[1]}`}
        />
      </GridItem>
      <GridItem>
        <FormCheckbox
          control={control}
          name={`tagKinds.${index}.userAssignable`}
          label="User assignable"
        />
      </GridItem>
    </RepeatingGroup>
  )
}

export const TagKinds: React.FC<{ control: Control<IndexFormData> }> = ({ control }) => {
  const { fields, append, remove } = useFieldArray({
    control,
    name: 'tagKinds',
  })

  return (
    <Stack spacing={4}>
      {fields.map((item, index) => (
        <TagKind key={item.id} index={index} control={control} remove={remove} />
      ))}
      <Box>
        <Button size="sm" leftIcon={<FiPlus />} onClick={() => append(createTagKindFormData())}>
          Add tag kind
        </Button>
      </Box>
    </Stack>
  )
}
