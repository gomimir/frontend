import {
  Stack,
  Box,
  Button,
  GridItem,
  Icon,
  Text,
  Switch,
  FormControl,
  FormLabel,
  Wrap,
  WrapItem,
  Tooltip,
} from '@chakra-ui/react'
import { useCallback } from 'react'
import { Control, Path, useController, useFieldArray } from 'react-hook-form'
import { FiPlus, FiUser, FiUsers } from 'react-icons/fi'
import { GroupBase } from 'react-select'
import { FormatOptionLabelMeta } from 'react-select/dist/declarations/src/Select'
import { Operation, useGetUsersAndRolesQuery } from '../../graphql'
import { FormSelect } from '../FormSelect'
import { RepeatingGroup } from './common'
import { createPermissionsFormData, IndexFormData } from './data'

interface User {
  id: string
  username: string
  displayName: string
}

interface UserRole {
  id: string
  name: string
  displayName: string
}

function isUserSubject(option: User | UserRole) {
  return option.id.startsWith('UA')
}

function subjectDisplayName(option: User | UserRole) {
  return option.displayName || (option as User).username || (option as UserRole).name || option.id
}

function subjectOptionFormatter(
  option: User | UserRole,
  _formatOptionLabelMeta: FormatOptionLabelMeta<User | UserRole>,
) {
  return (
    <Box display="inline-flex" alignItems="center">
      <Icon as={option && isUserSubject(option) ? FiUser : FiUsers} mr={2} />
      {subjectDisplayName(option)}
    </Box>
  )
}

interface OperationDesc {
  operation: Operation
  label: string
  tooltip?: string
}

const INDEX_OPERATIONS: OperationDesc[] = [
  {
    operation: Operation.Read,
    label: 'Read',
    tooltip: 'Access the index and all its settings. This is necessary for common operation.',
  },
  {
    operation: Operation.Update,
    label: 'Update',
    tooltip: 'Update index settings',
  },
  {
    operation: Operation.Delete,
    label: 'Delete',
    tooltip: 'Delete the index and all its assets',
  },
  {
    operation: Operation.Crawl,
    label: 'Crawl',
    tooltip: 'Crawl index file locations to index new assets',
  },
]

const ASSET_COLLECTION_OPERATIONS: OperationDesc[] = [
  {
    operation: Operation.List,
    label: 'List',
    tooltip: 'List all indexed assets',
  },
  {
    operation: Operation.Upload,
    label: 'Upload',
    tooltip: 'Upload new assets to the index',
  },
]

const ASSET_OPERATIONS: OperationDesc[] = [
  {
    operation: Operation.Read,
    label: 'Read',
  },
  {
    operation: Operation.Update,
    label: 'Update',
  },
  {
    operation: Operation.Delete,
    label: 'Delete',
  },
  {
    operation: Operation.Process,
    label: 'Trigger reprocessing',
  },
]

const OperationToggle: React.FC<{
  operation: OperationDesc
  value: boolean
  onChange: (o: OperationDesc) => void
  idPrefix: string
}> = ({ operation, onChange, value, idPrefix }) => {
  const id = `${idPrefix}-${operation.operation}`
  const fc = (
    <FormControl display="flex" alignItems="center" minW="90px">
      <Switch id={id} onChange={() => onChange(operation)} isChecked={value} />
      <FormLabel htmlFor={id} mb={0} ml={2} fontSize="xs">
        {operation.label}
      </FormLabel>
    </FormControl>
  )

  return operation.tooltip ? (
    <Tooltip label={operation.tooltip} placement="top" size="xs">
      {fc}
    </Tooltip>
  ) : (
    fc
  )
}

const OperationGroup: React.FC<{
  name: Path<IndexFormData>
  control: Control<IndexFormData>
  title: string
  operations: OperationDesc[]
}> = ({ title, operations, name, control }) => {
  const {
    field: { value, onChange },
  } = useController<IndexFormData, `permissions.${number}.indexOperations`>({
    name: name as any,
    control,
  })

  const handleToggle = useCallback(
    (operation: OperationDesc) => {
      if (value.has(operation.operation)) {
        const newSet = new Set(value)
        newSet.delete(operation.operation)
        onChange(newSet)
      } else {
        const newSet = new Set(value)
        newSet.add(operation.operation)
        onChange(newSet)
      }
    },
    [value, onChange],
  )

  return (
    <>
      <Text fontSize="xs" fontWeight="semibold" mb={2}>
        {title}
      </Text>
      <Wrap>
        {operations.map((o) => (
          <WrapItem key={o.operation}>
            <OperationToggle
              idPrefix={name}
              operation={o}
              onChange={handleToggle}
              value={value.has(o.operation)}
            />
          </WrapItem>
        ))}
      </Wrap>
    </>
  )
}

const PermissionsItem: React.FC<{
  index: number
  remove: (index: number) => void
  control: Control<IndexFormData>
  data: { users: User[]; userRoles: UserRole[] }
}> = ({ index, remove, control, data }) => {
  const groupedOptions: Array<GroupBase<User | UserRole>> = [
    {
      label: 'Roles',
      options: data.userRoles,
    },
    {
      label: 'Users',
      options: data.users,
    },
  ]

  return (
    <RepeatingGroup remove={() => remove(index)}>
      <GridItem>
        <FormSelect
          control={control}
          name={`permissions.${index}.subjectId`}
          label="Subject"
          placeholder="User or role"
          options={groupedOptions}
          mapValue={(o) => o?.id || ''}
          formatOptionLabel={subjectOptionFormatter}
          getOptionLabel={subjectDisplayName}
          isRequired={true}
        />
      </GridItem>
      <GridItem colSpan={2}>
        <OperationGroup
          title="Index permissions"
          operations={INDEX_OPERATIONS}
          control={control}
          name={`permissions.${index}.indexOperations`}
        />
      </GridItem>
      <GridItem colStart={2} colSpan={2}>
        <OperationGroup
          title="Asset collection"
          operations={ASSET_COLLECTION_OPERATIONS}
          control={control}
          name={`permissions.${index}.assetCollectionOperations`}
        />
      </GridItem>
      <GridItem colStart={2} colSpan={2}>
        <OperationGroup
          title="Individual assets"
          operations={ASSET_OPERATIONS}
          control={control}
          name={`permissions.${index}.assetOperations`}
        />
      </GridItem>
    </RepeatingGroup>
  )
}

export const Permissions: React.FC<{ control: Control<IndexFormData> }> = ({ control }) => {
  const { data } = useGetUsersAndRolesQuery()

  const { fields, append, remove } = useFieldArray({
    control,
    name: 'permissions',
  })

  if (!data) {
    return null
  }

  return (
    <Stack spacing={4}>
      {fields.map((item, index) => (
        <PermissionsItem
          key={item.id}
          index={index}
          control={control}
          remove={remove}
          data={data}
        />
      ))}
      <Box>
        <Button
          size="sm"
          leftIcon={<FiPlus />}
          onClick={() =>
            append(
              createPermissionsFormData({
                permissions: {
                  indexOperations: [Operation.Read],
                  assetCollectionOperations: [Operation.List],
                  assetOperations: [Operation.Read],
                },
              }),
            )
          }
        >
          Add permission assignment
        </Button>
      </Box>
    </Stack>
  )
}
