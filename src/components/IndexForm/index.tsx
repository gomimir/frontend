import { Alert, AlertIcon, Box, Button, Stack, Text, VStack } from '@chakra-ui/react'
import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import { IndexFormData, indexSchema } from './data'
import { Rules } from './Rules'
import { TagKinds } from './TagKinds'
import { Permissions } from './Permissions'
import { FileLocations } from './FileLocations'
import {
  ActionType,
  AuthType,
  IndexActionInput,
  IndexInput,
  useSystemInfoQuery,
} from '../../graphql'
import { ASSET_TYPE_OPTIONS } from './constants'
import { FormInput } from '../FormInput'
import { FormSelect } from '../FormSelect'

export const IndexForm: React.FC<{
  initialData: IndexFormData
  onSubmit: (data: IndexInput) => void
  submitButtonText?: string
}> = ({ initialData, onSubmit, submitButtonText = 'Save' }) => {
  const { data } = useSystemInfoQuery({
    fetchPolicy: 'cache-first',
  })

  const supportsPermissions = data?.serverInfo.auth !== AuthType.None

  const { control, handleSubmit } = useForm<IndexFormData>({
    resolver: yupResolver(indexSchema),
    defaultValues: initialData,
  })

  const submit = (data: IndexFormData) => {
    const result: IndexInput = {
      name: data.name,
      assetType: data.assetType,
      fileLocations: data.fileLocations.map((fl) => ({
        type: fl.type,
        repositoryId: fl.repository,
        prefix: fl.prefix,
      })),
      tagKinds: data.tagKinds.map((tk) => ({
        name: tk.name,
        displayName: tk.displayName || undefined,
        displayNamePlural: tk.displayNamePlural || undefined,
        userAssignable: tk.userAssignable,
      })),
      rules: data.rules.map((r) => ({
        description: r.description,
        enabled: r.enabled,
        match: r.match || undefined,
        inverse: r.inverse,
        actions: r.actions.map((a) => {
          const res: IndexActionInput = {
            description: a.description,
            enabled: a.enabled,
          }

          if (a.type === ActionType.Ignore) {
            res.config = {
              ignore: a.ignore,
            }
          } else if (a.type === ActionType.SetKey) {
            res.config = {
              setKey: a.setKey,
            }
          } else if (a.type === ActionType.SetParams) {
            res.config = {
              setParams: JSON.parse(a.setParams),
            }
          } else if (a.type === ActionType.AddTag) {
            res.config = {
              addTag: a.addTag,
            }
          } else if (a.type === ActionType.Process) {
            res.config = {
              process: {
                request: a.process.request,
                queue: a.process.queue,
              },
            }

            if (a.process.parameters) {
              res.config.process!.params = JSON.parse(a.process.parameters)
            }
          }

          return res
        }),
      })),
    }

    if (supportsPermissions) {
      result.permissions = data.permissions.map((perm) => ({
        subjectId: perm.subjectId,
        permissions: {
          indexOperations: [...perm.indexOperations].sort(),
          assetCollectionOperations: [...perm.assetCollectionOperations].sort(),
          assetOperations: [...perm.assetOperations].sort(),
        },
      }))
    }

    onSubmit(result)
  }

  return (
    <form onSubmit={handleSubmit(submit)}>
      <Stack spacing={8}>
        <Box>
          <Text textStyle="h1">General information</Text>
          <VStack maxW="300px" mt={1} spacing={3}>
            <FormInput
              control={control}
              name="name"
              label="Index name"
              size="md"
              placeholder="Example: Documents"
              isRequired={true}
            />
            <FormSelect
              control={control}
              name="assetType"
              label="Asset type"
              placeholder="Asset type"
              size="md"
              options={ASSET_TYPE_OPTIONS}
              mapValue={(o) => o?.value}
              isRequired={true}
            />
          </VStack>
        </Box>
        <Box>
          <Text textStyle="h1">File locations</Text>
          <Text mb={6}>
            This is where we should look for the files to index / where we should store uploads and
            sidecar files.
          </Text>
          <FileLocations control={control} />
        </Box>
        <Box>
          <Text textStyle="h1">Tag kinds</Text>
          <Text mb={6}>
            Tags help you organize your assets. Each index can have specific tag kinds which model
            multiple dimensions - ie. for privacy classification. If you are unsure, we recommend
            starting with one generic tag kind.
          </Text>
          <TagKinds control={control} />
        </Box>
        <Box>
          <Text textStyle="h1">Rules</Text>
          <Text mb={6}>
            While evaluating the rules, they are applied in order. Each rule can override any values
            set by the previous rules. You should start from least to the most specific.
          </Text>
          <Rules control={control} />
        </Box>
        <Box>
          <Text textStyle="h1">Permissions</Text>
          <Text mb={6}>
            You can define who can get an access to your index and who can modify the data. By
            default only administrators will have access to the index.
          </Text>
          {supportsPermissions ? (
            <Permissions control={control} />
          ) : (
            <Alert status="warning">
              <AlertIcon />
              Server is not configured with auth. provider.
            </Alert>
          )}
        </Box>
        <Box>
          <Button type="submit" colorScheme="blue">
            {submitButtonText}
          </Button>
        </Box>
      </Stack>
    </form>
  )
}
