import {
  useColorModeValue,
  Box,
  Flex,
  Tooltip,
  Switch,
  IconButton,
  Icon,
  Stack,
  Button,
  HStack,
} from '@chakra-ui/react'
import { Control, useWatch, Controller, useFieldArray } from 'react-hook-form'
import { FiTrash, FiPlus } from 'react-icons/fi'
import { FormCheckbox } from '../FormCheckbox'
import { FormInput } from '../FormInput'
import { Actions } from './Actions'
import { BOX_BG_COLOR, REMOVE_BUTTON_HOVER_BG_COLOR } from './constants'
import { createRuleFormData, IndexFormData } from './data'

const Rule: React.FC<{
  index: number
  remove: (index: number) => void
  control: Control<IndexFormData>
}> = ({ index, remove, control }) => {
  const bgColor = useColorModeValue(...BOX_BG_COLOR)
  const hoverColor = useColorModeValue(...REMOVE_BUTTON_HOVER_BG_COLOR)
  const enabled = useWatch({ control, name: `rules.${index}.enabled` })

  return (
    <Box>
      <Box shadow="sm" borderLeftRadius="lg" borderTopRightRadius={'lg'} bgColor={bgColor}>
        <Flex py={2} pr={2}>
          <Box px="3" lineHeight="32px" textDecoration={enabled ? undefined : 'line-through'}>
            {index + 1}.
          </Box>

          <Box flex="1" mr={4}>
            <FormInput
              control={control}
              name={`rules.${index}.description`}
              mr={4}
              placeholder="Rule description"
              disabled={!enabled}
              isRequired={true}
            />
            <HStack mt={1}>
              {/* <Text fontSize="sm" fontWeight="semibold">
                Reg.{'\xA0'}exp:
              </Text> */}
              <FormInput
                control={control}
                name={`rules.${index}.match`}
                placeholder="Regular expression (Default: match all .*)"
                disabled={!enabled}
              />
              <FormCheckbox
                control={control}
                name={`rules.${index}.inverse`}
                disabled={!enabled}
                label="Invert"
              />
            </HStack>
          </Box>

          <Controller
            render={({ field }) => (
              <Tooltip label={field.value ? 'Disable the rule' : 'Enable the rule'} placement="top">
                <Box mr={1} mt="4px">
                  <Switch onChange={field.onChange} isChecked={field.value} />
                </Box>
              </Tooltip>
            )}
            name={`rules.${index}.enabled`}
            control={control}
          />

          <Tooltip label="Remove the rule" placement="top">
            <IconButton
              aria-label="remove"
              size="sm"
              color="gray.500"
              bgColor="transparent"
              _hover={{ bgColor: hoverColor, color: 'gray.700' }}
              icon={<Icon as={FiTrash} />}
              onClick={() => remove(index)}
            />
          </Tooltip>
        </Flex>
      </Box>
      <Box bgColor={bgColor} borderBottomRadius="lg" ml={10} pb={2}>
        <Actions control={control} ruleIndex={index} ruleEnabled={enabled} />
      </Box>
    </Box>
  )
}

export const Rules: React.FC<{ control: Control<IndexFormData> }> = ({ control }) => {
  const { fields, append, remove } = useFieldArray({
    control,
    name: 'rules',
  })

  return (
    <Stack spacing={4}>
      {fields.map((item, index) => (
        <Rule key={item.id} index={index} control={control} remove={remove} />
      ))}
      <Box>
        <Button size="sm" leftIcon={<FiPlus />} onClick={() => append(createRuleFormData())}>
          Add rule
        </Button>
      </Box>
    </Stack>
  )
}
