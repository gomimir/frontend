import { ActionType, AssetType, FileLocationType } from '../../graphql'

export interface ActionTypeOption {
  value: string
  label: string
  description: string
}

export const ACTION_TYPE_OPTIONS: ActionTypeOption[] = [
  {
    value: ActionType.Ignore,
    label: 'Ignore',
    description: 'Matched files will be marked to not be indexed',
  },
  {
    value: ActionType.SetKey,
    label: 'Set grouping key',
    description: 'Items with the same key will be groupped into single asset',
  },
  {
    value: ActionType.SetParams,
    label: 'Set parameters',
    description: 'Set additional parameters as file metadata',
  },
  {
    value: ActionType.AddTag,
    label: 'Add tag',
    description: 'Add tag to matched files',
  },
  {
    value: ActionType.Process,
    label: 'Process',
    description: 'Trigger file processing',
  },
]

export interface FileLocationTypeOption {
  value: FileLocationType
  label: string
  description: string
}

export const FILE_LOCATION_OPTIONS: FileLocationTypeOption[] = [
  {
    value: FileLocationType.Crawl,
    label: 'Crawl',
    description: 'Location used for crawling. Mimir is gonna leave all the files untouched.',
  },
  {
    value: FileLocationType.Upload,
    label: 'Upload',
    description: 'Used for newly upload files and crawling. Fully managed by Mimir.',
  },
  {
    value: FileLocationType.Sidecar,
    label: 'Sidecars',
    description: 'We will store files like thumbnails and previews there',
  },
  {
    value: FileLocationType.Temp,
    label: 'Temporary',
    description: 'We will store temporary files there (ie. archives being made)',
  },
]

export interface AssetTypeOption {
  value: AssetType
  label: string
}

export const ASSET_TYPE_OPTIONS: AssetTypeOption[] = [
  {
    value: AssetType.Document,
    label: 'Document',
  },
  {
    value: AssetType.Photo,
    label: 'Photo',
  },
]

export const BOX_BG_COLOR = ['gray.100', 'gray.700'] as const
export const REMOVE_BUTTON_HOVER_BG_COLOR = ['gray.200', 'gray.500'] as const
