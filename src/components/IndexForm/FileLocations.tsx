import { Stack, Box, Button, GridItem } from '@chakra-ui/react'
import { Control, useFieldArray } from 'react-hook-form'
import { FiPlus } from 'react-icons/fi'
import { ListRepositoriesQuery, useListRepositoriesQuery } from '../../graphql'
import { DescriptiveOptionRenderer } from '../ChakraReactSelect'
import { FormSelect } from '../FormSelect'
import { FormInput } from '../FormInput'
import { RepeatingGroup } from './common'
import { FILE_LOCATION_OPTIONS } from './constants'
import { createFileLocationFormData, IndexFormData } from './data'

const FileLocation: React.FC<{
  index: number
  remove: (index: number) => void
  control: Control<IndexFormData>
  repositories: ListRepositoriesQuery['repositories']
}> = ({ index, remove, control, repositories }) => {
  return (
    <RepeatingGroup remove={() => remove(index)}>
      <GridItem>
        <FormSelect
          control={control}
          name={`fileLocations.${index}.type`}
          label="Type"
          options={FILE_LOCATION_OPTIONS}
          mapValue={(o) => o?.value}
          components={{ Option: DescriptiveOptionRenderer }}
          defaultToFirstOption={repositories.length === 1}
          isRequired={true}
        />
      </GridItem>
      <GridItem>
        <FormSelect
          control={control}
          name={`fileLocations.${index}.repository`}
          label="Repository"
          options={repositories}
          mapValue={(o) => o?.id || ''}
          getOptionLabel={(o) => o.name}
          noOptionsMessage="No repositories available"
          isRequired={true}
        />
      </GridItem>
      <GridItem>
        <FormInput
          control={control}
          name={`fileLocations.${index}.prefix`}
          label="Prefix"
          placeholder="Prefix"
          isRequired={true}
        />
      </GridItem>
    </RepeatingGroup>
  )
}

export const FileLocations: React.FC<{ control: Control<IndexFormData> }> = ({ control }) => {
  const { data: repositories } = useListRepositoriesQuery()
  const { fields, append, remove } = useFieldArray({
    control,
    name: 'fileLocations',
  })

  if (!repositories?.repositories) {
    return null
  }

  return (
    <Stack spacing={4}>
      {fields.map((item, index) => (
        <FileLocation
          key={item.id}
          index={index}
          control={control}
          remove={remove}
          repositories={repositories?.repositories}
        />
      ))}
      <Box>
        <Button
          size="sm"
          leftIcon={<FiPlus />}
          onClick={() => append(createFileLocationFormData())}
        >
          Add file location
        </Button>
      </Box>
    </Stack>
  )
}
