import React from 'react'
import { FiCheck, FiEdit3, FiX } from 'react-icons/fi'
import { Editable } from '@chakra-ui/editable'
import {
  Box,
  ButtonGroup,
  EditableInput,
  EditablePreview,
  Flex,
  Icon,
  IconButton,
  useEditableControls,
} from '@chakra-ui/react'

const EditableControls: React.FC = () => {
  const { isEditing, getSubmitButtonProps, getCancelButtonProps, getEditButtonProps } =
    useEditableControls()

  return isEditing ? (
    <ButtonGroup justifyContent="center" size="sm" ml={2}>
      <IconButton icon={<FiCheck />} aria-label="confirm" {...getSubmitButtonProps()} />
      <IconButton icon={<FiX />} aria-label="cancel" {...getCancelButtonProps()} />
    </ButtonGroup>
  ) : (
    <Box
      display="inline-block"
      p="7px"
      h="18px"
      w="18px"
      ml="4px"
      boxSizing="content-box"
      _hover={{
        cursor: 'pointer',
        bg: 'gray.100',
        borderRadius: '100px',
        '.edit-action': {
          color: 'black',
        },
      }}
      {...getEditButtonProps()}
    >
      <Icon
        as={FiEdit3}
        display="none"
        w="18px"
        h="18px"
        className="edit-action"
        color="gray.500"
      />
    </Box>
  )
}

export const EditableTitle: React.FC<{
  defaultValue: string
  onSubmit?: (nextValue: string) => void
  isDisabled?: boolean
}> = ({ defaultValue, onSubmit, isDisabled }) => {
  return (
    <Editable
      defaultValue={defaultValue}
      placeholder={defaultValue}
      fontSize="2xl"
      isPreviewFocusable={false}
      _hover={
        !isDisabled
          ? {
              '.edit-action': {
                display: 'block',
              },
            }
          : {}
      }
      h="42px"
      onSubmit={onSubmit}
      isDisabled={isDisabled}
    >
      <Flex alignContent="center" alignItems="center">
        <EditablePreview py={0} noOfLines={1} />
        <EditableInput w="100%" py={0} />
        {!isDisabled && <EditableControls />}
      </Flex>
    </Editable>
  )
}
