import React from 'react'
import { FormProvider, UseFormReturn } from 'react-hook-form'
import { Stack } from '@chakra-ui/react'
import { TagKind } from '../hooks/useIndex'
import { FormInput } from './FormInput'
import { FormSelect } from './FormSelect'

export interface AddNewTagFormValues {
  kind: string
  name: string
}

export const AddNewTagForm: React.FC<{
  form: UseFormReturn<AddNewTagFormValues>
  tagKinds: TagKind[]
  onSubmit: (data: any) => void
}> = ({ form, onSubmit, tagKinds }) => {
  const { handleSubmit, control } = form

  return (
    <FormProvider {...form}>
      <form onSubmit={handleSubmit(onSubmit)}>
        <Stack spacing={2}>
          <FormSelect
            control={control}
            name="kind"
            label="Kind"
            options={tagKinds}
            getOptionLabel={(o) => o.messages.kindName(1)}
            mapValue={(o) => o?.name}
            isRequired
            tabIndex={2}
          />
          <FormInput
            name="name"
            control={control}
            label="Name"
            rules={{ required: true }}
            tabIndex={3}
          />
        </Stack>
      </form>
    </FormProvider>
  )
}
