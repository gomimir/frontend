import { FiChevronDown, FiChevronLeft } from 'react-icons/fi'
import {
  Badge,
  Box,
  Collapse,
  HStack,
  IconButton,
  Spacer,
  Text,
  useColorModeValue,
  useDisclosure,
} from '@chakra-ui/react'

export const InspectorSection: React.FC<{
  title: React.ReactNode
  collapsible?: boolean
  badge?: React.ReactNode
  defaultOpen?: boolean
}> = ({ title, badge, collapsible = false, defaultOpen = true, children }) => {
  const { isOpen, onToggle } = useDisclosure({
    defaultIsOpen: defaultOpen,
  })
  const badgeColor = useColorModeValue('blackAlpha', 'whiteAlpha')

  return (
    <Box w="100%">
      <HStack pb={2}>
        <Text fontSize="lg" fontWeight="md">
          {title}
        </Text>
        {badge && (
          <Badge size="sm" variant="outline" colorScheme={badgeColor}>
            {badge}
          </Badge>
        )}
        <Spacer />
        {collapsible && (
          <IconButton
            variant="ghost"
            size="xs"
            icon={isOpen ? <FiChevronDown /> : <FiChevronLeft />}
            aria-label={isOpen ? 'Fold' : 'Unfold'}
            onClick={onToggle}
          />
        )}
      </HStack>
      <Collapse in={isOpen} style={{ overflow: undefined }}>
        {children}
      </Collapse>
    </Box>
  )
}
