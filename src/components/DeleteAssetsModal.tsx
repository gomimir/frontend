import { useToast } from '@chakra-ui/react'
import { AssetFilter, useDeleteAssetsMutation } from '../graphql'
import { DeleteModal } from './DeleteModal'

export const DeleteAssetsModal: React.FC<{
  indexId: string
  numAssets: number
  // Note: the filter might be undefined, but I do not want to make it optional, to avoid forgetting passing it. It could be catastrofic.
  query: AssetFilter | undefined
  onClose: () => void
}> = ({ indexId, numAssets, query, onClose }) => {
  const str = numAssets === 1 ? `1\xA0asset` : `${numAssets}\xA0assets`
  const [purge] = useDeleteAssetsMutation()
  const toast = useToast()

  const handleAccept = () => {
    onClose()

    purge({
      variables: {
        index: indexId,
        query: query,
      },
    }).then((response) => {
      if (response.data?.deleteAssetByQuery?.ok) {
        toast({
          title: 'Delete in process',
          description: `Deleting ${str} assets`,
          status: 'info',
        })
      } else {
        toast({
          title: 'Operation failed',
          status: 'error',
        })
      }
    })
  }

  return (
    <DeleteModal
      titleContent="Delete assets"
      buttonContent={`Delete ${str}`}
      onAccept={handleAccept}
      onClose={onClose}
    >
      Delete assets will remove them from the index and remove any uploaded files. Files in Crawl
      locations will be kept in place. Are you sure you want to delete selected assets?
    </DeleteModal>
  )
}
