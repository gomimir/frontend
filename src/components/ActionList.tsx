import {
  Button,
  IconButton,
  MenuItem,
  useBreakpointValue,
  Menu,
  MenuButton,
  MenuList,
  HStack,
  Box,
  Portal,
} from '@chakra-ui/react'
import React, { useContext } from 'react'
import { FiMoreHorizontal } from 'react-icons/fi'

const ActionListContext = React.createContext<'menu' | 'button' | 'icon-button'>('button')

export const Action: React.FC<{
  label: string
  icon?: React.ReactElement
  onClick?: () => void
}> = ({ icon, onClick, label }) => {
  const viewType = useContext(ActionListContext)

  switch (viewType) {
    case 'button':
      return (
        <Button variant="ghost" size="sm" leftIcon={icon} onClick={onClick}>
          {label}
        </Button>
      )

    case 'icon-button':
      return (
        <IconButton variant="ghost" size="sm" icon={icon} onClick={onClick} aria-label={label} />
      )

    case 'menu':
      return (
        <MenuItem onClick={onClick} icon={icon} fontSize="sm">
          {label}
        </MenuItem>
      )
  }
}

export const ActionList: React.FC<{ numPrimaryActions?: number }> = ({
  numPrimaryActions = 1,
  children,
}) => {
  const viewType = useBreakpointValue<'icon-button' | 'button'>({
    base: 'icon-button',
    lg: 'button',
  })
  const childrenArr = React.Children.toArray(children)

  if (childrenArr.length === 0 || !viewType) {
    return null
  }

  return (
    <HStack spacing="2px">
      <ActionListContext.Provider value={viewType}>
        {childrenArr.slice(0, numPrimaryActions)}
      </ActionListContext.Provider>
      {childrenArr.length > numPrimaryActions && (
        <Box>
          <ActionListContext.Provider value="menu">
            <Menu>
              {viewType === 'button' ? (
                <MenuButton as={Button} leftIcon={<FiMoreHorizontal />} size="sm" variant="ghost">
                  More actions
                </MenuButton>
              ) : (
                <MenuButton
                  as={IconButton}
                  size="sm"
                  variant="ghost"
                  icon={<FiMoreHorizontal />}
                  aria-label="More actions"
                />
              )}
              <Portal>
                {React.createElement(MenuList, {}, ...childrenArr.slice(numPrimaryActions))}
              </Portal>
            </Menu>
          </ActionListContext.Provider>
        </Box>
      )}
    </HStack>
  )
}
