import React from 'react'
import { Flex, Text, Tooltip } from '@chakra-ui/react'
import { ActiveItemActionUnion } from '../helpers/ActiveItem'
import { createAssetListingLink } from '../hooks/useAssetListingScreen'
import { useIndex } from '../hooks/useIndex'
import { Tag } from '../hooks/useTagListing'
import { AppLink } from './AppLink'
import { AssetListingThumbnail, PLACEHOLDER_IMAGE_DIMENSIONS } from './AssetListingThumbnail'
import { GalleryItemDelegate, GalleryView, GalleryViewItemProps } from './GalleryView'

const tagItemDelegate: GalleryItemDelegate<Tag> = {
  getId: (tag) => `${tag.kind}_${tag.name}`,
  getDimensions: (tag) => {
    return tag.coverAsset?.galleryThumbnail
      ? {
          width: tag.coverAsset.galleryThumbnail.width,
          height: tag.coverAsset.galleryThumbnail.height,
        }
      : PLACEHOLDER_IMAGE_DIMENSIONS
  },
}

const TagGalleryItem = ({ item: tag }: GalleryViewItemProps<Tag>) => {
  const { kind, name } = tag

  const index = useIndex()

  const link = createAssetListingLink({
    indexName: index.id,
    tag: {
      kind,
      name,
    },
  })

  return (
    <>
      <AppLink to={link} _focus={{ boxShadow: 'none' }}>
        <AssetListingThumbnail asset={tag.coverAsset} />
      </AppLink>

      <Flex overflow="hidden" justifyContent="center" alignItems="center" pt={1}>
        <Tooltip label={tag.name} placement="top" hasArrow>
          <Text as="span" display="inline-block" fontSize="sm" noOfLines={1}>
            <AppLink to={link}>{tag.name}</AppLink>
          </Text>
        </Tooltip>
      </Flex>
    </>
  )
}

export const TagListingGallery: React.FC<{
  tags: Tag[]
  activeItemIndex?: number
  onActiveItemAction: (action: ActiveItemActionUnion) => void
}> = ({ tags, activeItemIndex, onActiveItemAction }) => {
  return (
    <GalleryView
      items={tags}
      itemDelegate={tagItemDelegate}
      itemRenderer={TagGalleryItem}
      itemBottomPadding={28}
      activeItemIndex={activeItemIndex}
      onActiveItemAction={onActiveItemAction}
    />
  )
}
