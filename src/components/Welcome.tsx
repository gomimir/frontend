import React from 'react'
import { Box, Button, Divider, Grid, HStack, Link, Text, VStack } from '@chakra-ui/react'
import { ColorModeSwitcher } from '../components/ColorModeSwitcher'
import { VersionInfo } from '../components/VersionInfo'
import { AppLink } from './AppLink'
import { Link as LinkDesc } from './AppScreenManager'

export const Welcome: React.FC<{
  goToAppLink: LinkDesc
}> = ({ goToAppLink }) => {
  return (
    <Box textAlign="center" fontSize="xl">
      <Grid maxW={600} mx="auto" minH="100vh" p={3}>
        <HStack justifySelf="flex-end">
          <ColorModeSwitcher />
        </HStack>

        <VStack spacing={4}>
          <Text>Hello.</Text>
          <Text color="gray.500">Welcome to Mimir.</Text>
          <Text fontSize="md">
            Mimir is a file indexing platform. It extracts valuable information from your files and
            allows you to search and organize them.
          </Text>
          <Text fontSize="md">
            We are currently working hard to bring you the best experience but the application is
            still under development and <strong>it is not ready for end-users yet</strong>.
          </Text>
          <Link
            color="teal.500"
            href="https://gomimir.gitlab.io/"
            target="_blank"
            rel="noopener noreferrer"
          >
            Learn about Mimir
          </Link>
          <Divider />
          <AppLink to={goToAppLink}>
            <Button>Go to the app</Button>
          </AppLink>
          <VersionInfo />
        </VStack>
      </Grid>
    </Box>
  )
}
