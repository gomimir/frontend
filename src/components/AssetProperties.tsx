import React, { useCallback, useContext, useMemo } from 'react'
import {
  Box,
  Button,
  ButtonGroup,
  forwardRef,
  HStack,
  Popover,
  PopoverBody,
  PopoverContent,
  PopoverAnchor,
  Text,
  useColorModeValue,
  useDisclosure,
  VStack,
} from '@chakra-ui/react'
import { AssetUpdate, FileUpdate, PropertyPairFragment } from '../graphql'
import { getFileProperty, isPropertyType } from '../utils/properties'
import { InspectorSection } from './InspectorSection'
import { formatFramerate, formatModelAndMake, formatTimeTook } from '../utils/format'
import { FormInput } from './FormInput'
import { SubmitHandler, useForm } from 'react-hook-form'
import { BsCheck, BsX } from 'react-icons/bs'
import { CSSObject } from '@emotion/react'
import { useAssetUpdate } from '../hooks/useAssetUpdate'
import { date, object } from 'yup'
import { yupResolver } from '@hookform/resolvers/yup'

const LabeledValue = forwardRef(
  (
    {
      label,
      editable,
      children,
    }: {
      label: string
      editable?: boolean
      children: React.ReactNode
    },
    ref,
  ) => {
    const { edit, isOpen } = useContext(PropertyEditorContext) || {}

    const highlightBgColor = useColorModeValue(
      '#EDF2F7', // gray.100
      'rgba(226, 232, 240, 0.16)',
    )

    const highlight: CSSObject = {
      content: '""',
      display: 'block',
      backgroundColor: highlightBgColor,
      position: 'absolute',
      top: '2px',
      bottom: '1px',
      left: '-6px',
      right: '0px',
      zIndex: -1,
      borderRadius: '4px',
    }

    return (
      <HStack
        ref={ref}
        maxW="100%"
        position="relative"
        css={
          isOpen
            ? {
                '&:after': highlight,
              }
            : editable
            ? {
                '&:hover': {
                  cursor: 'pointer',
                },
                '&:hover:after': highlight,
              }
            : undefined
        }
        lineHeight="24px"
        pr="6px"
        onClick={editable ? edit : undefined}
      >
        <Text as="span" fontSize="xs">
          {label}:
        </Text>
        <Text as="span" fontSize="xs" fontWeight="semibold" noOfLines={1}>
          {children}
        </Text>
      </HStack>
    )
  },
)

const PropertyEditorContext = React.createContext<{
  initialFocusRef: React.MutableRefObject<any>
  close: () => void
  edit: () => void
  isOpen: boolean
  isSaving: boolean
  save: (assetUpdate: AssetUpdate) => void
}>(null!)

const PropertyPair: React.FC<{ assetId: string; filename: string }> = ({
  assetId,
  filename,
  children,
}) => {
  const initialFocusRef = React.useRef(null)
  const { onOpen, onClose, isOpen } = useDisclosure()
  const { update: updateAsset, inProgress: isSaving } = useAssetUpdate()

  const save = useCallback(
    (update: Omit<FileUpdate, 'selector'>) => {
      updateAsset(assetId, {
        updateFiles: [
          {
            selector: {
              fileRef: {
                filename,
              },
            },
            ...update,
          },
        ],
      })
    },
    [assetId, filename, updateAsset],
  )

  const editorCtx = useMemo(
    () => ({ initialFocusRef, close: onClose, edit: onOpen, isOpen, isSaving, save }),
    [initialFocusRef, onClose, onOpen, isOpen, save, isSaving],
  )

  return (
    <PropertyEditorContext.Provider value={editorCtx}>{children}</PropertyEditorContext.Provider>
  )
}

const PropertyEditorPopover: React.FC = ({ children }) => {
  const { initialFocusRef, close, isOpen } = useContext(PropertyEditorContext)

  return (
    <Popover
      isOpen={isOpen}
      onClose={close}
      initialFocusRef={initialFocusRef}
      closeDelay={0}
      gutter={4}
      isLazy
      matchWidth
    >
      <Box w="100%" position="relative">
        <PopoverAnchor>
          <Box position="absolute" left="-6px" right="0px" />
        </PopoverAnchor>
      </Box>
      <PopoverContent width="inherit">
        <PopoverBody p={2}>{children}</PopoverBody>
      </PopoverContent>
    </Popover>
  )
}

const PropertyEditorButtons: React.FC = ({ children }) => {
  const { close, isSaving } = useContext(PropertyEditorContext)

  return (
    <ButtonGroup variant="outline" size="xs">
      <Button
        type="submit"
        colorScheme="blue"
        leftIcon={<BsCheck />}
        iconSpacing="1"
        isLoading={isSaving}
      >
        Save
      </Button>
      <Button leftIcon={<BsX />} iconSpacing="1" onClick={close}>
        Cancel
      </Button>
      {children}
    </ButtonGroup>
  )
}

const PropertyEditorForm: React.FC<{
  onSubmit?: React.FormEventHandler<HTMLFormElement>
}> = ({ onSubmit, children }) => {
  return (
    <PropertyEditorPopover>
      <form onSubmit={onSubmit}>
        <VStack>
          {children}
          <PropertyEditorButtons />
        </VStack>
      </form>
    </PropertyEditorPopover>
  )
}

interface TextPropertyForm {
  textValue: string
}

const TextPropertyEditor: React.FC<{ property: string; value: string }> = ({ property, value }) => {
  const { initialFocusRef, close, save } = useContext(PropertyEditorContext)

  const { control, handleSubmit } = useForm<{ textValue: string }>({
    defaultValues: {
      textValue: value,
    },
  })

  const submit: SubmitHandler<TextPropertyForm> = ({ textValue }) => {
    close()
    save({
      setProperties: [
        {
          name: property,
          textValue,
        },
      ],
    })
  }

  return (
    <PropertyEditorForm onSubmit={handleSubmit(submit)}>
      <FormInput name="textValue" control={control} ref={initialFocusRef} />
    </PropertyEditorForm>
  )
}

interface TimePropertyForm {
  timeValue: Date
}

const timePropertyFormSchema = object({
  timeValue: date().typeError('Must be ISO Date string').optional(),
})

const TimePropertyEditor: React.FC<{ property: string; value: Date }> = ({ property, value }) => {
  const { initialFocusRef, close, save } = useContext(PropertyEditorContext)

  const { control, handleSubmit } = useForm<TimePropertyForm>({
    resolver: yupResolver(timePropertyFormSchema),
    defaultValues: {
      timeValue: value,
    },
  })

  const submit: SubmitHandler<TimePropertyForm> = ({ timeValue }) => {
    close()
    save({
      setProperties: [
        {
          name: property,
          timeValue: timeValue.toISOString(),
        },
      ],
    })
  }

  return (
    <PropertyEditorForm onSubmit={handleSubmit(submit)}>
      <FormInput name="timeValue" control={control} ref={initialFocusRef} />
    </PropertyEditorForm>
  )
}

export const AssetProperties: React.FC<{
  assetId: string
  filename: string
  properties: PropertyPairFragment[]
}> = ({ assetId, filename, properties }) => {
  const propList: React.ReactNode[] = []
  const usedProperties: string[] = []

  const authoredAt = getFileProperty(properties, 'authoredAt', 'TimeProperty')
  usedProperties.push('authoredAt', 'date')
  if (authoredAt) {
    const d = new Date(authoredAt.timeValue)
    if (!isNaN(d as any)) {
      propList.push(
        <PropertyPair assetId={assetId} filename={filename}>
          <LabeledValue label="Authored at" editable>
            {d.toLocaleString()}
          </LabeledValue>
          <TimePropertyEditor property="authoredAt" value={d} />
        </PropertyPair>,
      )
    }
  } else {
    const date = getFileProperty(properties, 'date', 'TimeProperty')
    if (date) {
      const d = new Date(date.timeValue)
      if (!isNaN(d as any)) {
        propList.push(
          <PropertyPair assetId={assetId} filename={filename}>
            <LabeledValue label="Date" editable>
              {d.toLocaleString()}
            </LabeledValue>
            <TimePropertyEditor property="date" value={d} />
          </PropertyPair>,
        )
      }
    }
  }

  const videoDuration = getFileProperty(properties, 'video_duration', 'NumberProperty')
  usedProperties.push('video_duration')
  if (videoDuration) {
    propList.push(
      <LabeledValue label="Video duration">
        {formatTimeTook(videoDuration.numericValue * 1000)}
      </LabeledValue>,
    )
  }

  const imageWidth = getFileProperty(properties, 'image_width', 'NumberProperty')
  const imageHeight = getFileProperty(properties, 'image_height', 'NumberProperty')
  const videoFrameRate = getFileProperty(properties, 'video_frameRate', 'FractionProperty')
  const type = getFileProperty(properties, 'type', 'TextProperty')
  usedProperties.push('image_width', 'image_height', 'video_frameRate', 'type')

  if (imageWidth && imageHeight) {
    if (type?.textValue === 'video') {
      const resolution =
        imageWidth.numericValue === 3840 && imageHeight.numericValue === 2160
          ? '4k'
          : imageWidth.numericValue === 1920 && imageHeight.numericValue === 1080
          ? '1080p'
          : imageWidth.numericValue === 1280 && imageHeight.numericValue === 720
          ? '720p'
          : `${imageWidth.numericValue}x${imageHeight.numericValue}`

      if (videoFrameRate) {
        propList.push(
          <LabeledValue label="Video quality">{`${resolution}, ${formatFramerate(
            videoFrameRate.numerator,
            videoFrameRate.denominator,
          )}`}</LabeledValue>,
        )
      } else {
        propList.push(<LabeledValue label="Video resolution">{resolution}</LabeledValue>)
      }
    } else {
      propList.push(
        <LabeledValue label="Image resolution">{`${imageWidth.numericValue}x${imageHeight.numericValue}`}</LabeledValue>,
      )
    }
  } else if (videoFrameRate) {
    propList.push(
      <LabeledValue label="Video framerate">
        {formatFramerate(videoFrameRate.numerator, videoFrameRate.denominator)}
      </LabeledValue>,
    )
  }

  const videoCodec = getFileProperty(properties, 'video_codec', 'TextProperty')
  const audioCodec = getFileProperty(properties, 'audio_codec', 'TextProperty')
  usedProperties.push('video_codec', 'audio_codec')
  if (videoCodec && audioCodec) {
    propList.push(
      <LabeledValue label="Video / Audio codec">{`${videoCodec.textValue} / ${audioCodec.textValue}`}</LabeledValue>,
    )
  } else if (videoCodec) {
    propList.push(<LabeledValue label="Video codec">{videoCodec.textValue}</LabeledValue>)
  } else if (audioCodec) {
    propList.push(<LabeledValue label="Audio codec">{audioCodec.textValue}</LabeledValue>)
  }

  const shutterSpeed = getFileProperty(properties, 'exposure_time', 'FractionProperty')
  if (shutterSpeed) {
    propList.push(
      <LabeledValue label="Shutter speed">
        {shutterSpeed.numerator + '/' + shutterSpeed.denominator + ' s'}
      </LabeledValue>,
    )
  }

  const aperture = getFileProperty(properties, 'exposure_aperture', 'FractionProperty')
  usedProperties.push('exposure_aperture')
  if (aperture) {
    propList.push(
      <LabeledValue label="Aperture">
        {'f/' + aperture.numerator / aperture.denominator}
      </LabeledValue>,
    )
  }

  const iso = getFileProperty(properties, 'exposure_iso', 'NumberProperty')
  usedProperties.push('exposure_iso')
  if (iso) {
    propList.push(<LabeledValue label="ISO">{iso.numericValue.toString()}</LabeledValue>)
  }

  const cameraMake = getFileProperty(properties, 'camera_make', 'TextProperty')
  const cameraModel = getFileProperty(properties, 'camera_model', 'TextProperty')
  usedProperties.push('camera_make', 'camera_model')
  if (cameraMake || cameraModel) {
    propList.push(
      <LabeledValue label="Camera">
        {formatModelAndMake(cameraModel?.textValue, cameraMake?.textValue)}
      </LabeledValue>,
    )
  }

  const lensMake = getFileProperty(properties, 'lens_make', 'TextProperty')
  const lensModel = getFileProperty(properties, 'lens_model', 'TextProperty')
  usedProperties.push('lens_make', 'lens_model')
  if (lensMake || lensModel) {
    propList.push(
      <LabeledValue label="Lens">
        {[lensMake?.textValue, lensModel?.textValue].filter((_) => _).join(' ')}
      </LabeledValue>,
    )
  }

  const focalLength = getFileProperty(properties, 'lens_focalLength', 'FractionProperty')
  usedProperties.push('lens_focalLength')
  if (focalLength) {
    propList.push(
      <LabeledValue label="Focal length">
        {Math.round(focalLength.numerator / focalLength.denominator).toString() + ' mm'}
      </LabeledValue>,
    )
  }

  const usedPropertiesSet = new Set(usedProperties)
  for (const propPair of properties) {
    if (!usedPropertiesSet.has(propPair.name)) {
      if (isPropertyType(propPair, 'TextProperty')) {
        if (propPair.name === 'extractedText') {
          continue
        }

        propList.push(
          <PropertyPair assetId={assetId} filename={filename}>
            <LabeledValue label={propPair.name} editable>
              {propPair.textValue}
            </LabeledValue>
            <TextPropertyEditor property={propPair.name} value={propPair.textValue} />
          </PropertyPair>,
        )
      } else if (isPropertyType(propPair, 'TimeProperty')) {
        const d = new Date(propPair.timeValue)
        if (!isNaN(d as any)) {
          propList.push(
            <PropertyPair assetId={assetId} filename={filename}>
              <LabeledValue label={propPair.name} editable>
                {d.toLocaleString()}
              </LabeledValue>
              <TimePropertyEditor property={propPair.name} value={d} />
            </PropertyPair>,
          )
        }
      }
    }
  }

  return propList.length ? (
    <InspectorSection title="Properties">
      {React.createElement(VStack, { alignItems: 'start', spacing: 'sm' }, ...propList)}
    </InspectorSection>
  ) : null
}
