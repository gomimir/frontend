import { useToast } from '@chakra-ui/react'
import React, { useCallback } from 'react'
import { FiCpu, FiDownload, FiEye, FiInfo, FiTrash } from 'react-icons/fi'
import { MdFavoriteBorder } from 'react-icons/md'
import { Operation, useProcessAssetMutation, useUpdateAssetMutation } from '../graphql'
import { Asset } from '../hooks/useAssetListing'
import { AssetListingScreenParams, useAssetListingScreen } from '../hooks/useAssetListingScreen'
import { useModalManager } from '../hooks/useModalManager'
import { Navigate } from '../utils/link'
import { ActionList, Action } from './ActionList'
import { DeleteAssetModal } from './DeleteAssetModal'
import { download, DownloadModal } from './DownloadModal'
import { SidebarState, useSidebar } from './SidebarLayout'

const DownloadAction: React.FC<{ asset: Asset }> = ({ asset }) => {
  const openModal = useModalManager()

  const handleClick = useCallback(() => {
    if (asset.files.length === 1) {
      download(asset.files[0].url)
    } else {
      openModal((onClose) => <DownloadModal asset={asset} onClose={onClose} />)
    }
  }, [asset, openModal])

  return <Action label="Download" icon={<FiDownload />} onClick={handleClick} />
}

const ViewAction: React.FC<{ navigate: Navigate<AssetListingScreenParams> }> = ({ navigate }) => {
  const handleClick = useCallback(() => {
    navigate((params) => ({ ...params, viewMode: 'preview' }))
  }, [navigate])

  return <Action label="View" icon={<FiEye />} onClick={handleClick} />
}

const InspectAction: React.FC<{ setSidebarState: (newState: SidebarState) => void }> = ({
  setSidebarState,
}) => {
  const handleClick = useCallback(() => {
    setSidebarState('full')
  }, [setSidebarState])

  return <Action label="Inspect" icon={<FiInfo />} onClick={handleClick} />
}

const ReprocessAction: React.FC<{ assetId: string }> = ({ assetId }) => {
  const toast = useToast()
  const [processAsset] = useProcessAssetMutation()

  const handleClick = () => {
    processAsset({
      variables: {
        id: assetId,
        reprocess: ['*'],
      },
    }).then((response) => {
      if (response.data?.processAsset?.ok) {
        toast({
          title: 'Reprocessing started',
          description: 'Updating the asset information',
          status: 'info',
        })
      } else {
        toast({
          title: 'Operation failed',
          status: 'error',
        })
      }
    })
  }

  return <Action label="Reprocess" icon={<FiCpu />} onClick={handleClick} />
}

const DeleteAction: React.FC<{ asset: Asset }> = ({ asset }) => {
  const openModal = useModalManager()

  const handleClick = useCallback(() => {
    openModal((onClose) => <DeleteAssetModal asset={asset} onClose={onClose} />)
  }, [asset, openModal])

  return <Action label="Delete" icon={<FiTrash />} onClick={handleClick} />
}

const SetAsCoverAction: React.FC<{ asset: Asset }> = ({ asset }) => {
  const toast = useToast()
  const [updateAsset] = useUpdateAssetMutation()

  const assetId = asset.id

  const handleClick = useCallback(() => {
    updateAsset({
      variables: {
        id: assetId,
        update: {
          setProperties: [
            {
              name: 'priority',
              numericValue: new Date().getTime(),
            },
          ],
        },
      },
      errorPolicy: 'all',
    }).then((response) => {
      if (response.data?.updateAsset?.ok) {
        toast({
          title: 'Update finished',
          description: 'Cover image updated',
          status: 'success',
        })
      } else {
        toast({
          title: 'Operation failed',
          status: 'error',
        })
      }
    })
  }, [assetId, toast, updateAsset])

  return <Action label="Set as cover" icon={<MdFavoriteBorder />} onClick={handleClick} />
}

export const AssetActions: React.FC<{ asset: Asset }> = ({ asset }) => {
  const { params, navigate } = useAssetListingScreen()
  const { state: sidebarState, setState: setSidebarState } = useSidebar()

  const actions: React.ReactElement[] = []

  if (params?.viewMode !== 'preview') {
    actions.push(<ViewAction navigate={navigate} />)
  }

  if (sidebarState === 'hidden') {
    actions.push(<InspectAction setSidebarState={setSidebarState} />)
  }

  if (asset.files.length > 0) {
    actions.push(<DownloadAction asset={asset} />)
  }

  if (asset.allowedOperations.includes(Operation.Update) && params?.tag) {
    actions.push(<SetAsCoverAction asset={asset} />)
  }

  if (asset.allowedOperations.includes(Operation.Process)) {
    actions.push(<ReprocessAction assetId={asset.id} />)
  }

  if (asset.allowedOperations.includes(Operation.Delete)) {
    actions.push(<DeleteAction asset={asset} />)
  }

  return React.createElement(
    ActionList,
    { numPrimaryActions: params?.viewMode !== 'preview' ? 2 : 1 },
    ...actions,
  )
}
