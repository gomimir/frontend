import { Button, Center, Grid, Text, VStack } from '@chakra-ui/react'
import { SystemInfoQuery } from '../graphql'
import { useOAuthLogin } from '../hooks/useOAuthLogin'

export const Login: React.FC<{ auth: SystemInfoQuery['serverInfo']['auth'] }> = () => {
  const handleLoginClick = useOAuthLogin({
    path: window.location.pathname,
  })

  return (
    <Center fontSize="xl" minH="100vh">
      <Grid maxW={600} mx="auto" p={3}>
        <VStack spacing={4}>
          <Text>Mimir</Text>
          <Text color="gray.500">You need to login first</Text>

          <Button onClick={handleLoginClick}>Login</Button>
        </VStack>
      </Grid>
    </Center>
  )
}
