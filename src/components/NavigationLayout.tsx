import { Box, Flex, useBreakpointValue } from '@chakra-ui/react'
import { useState } from 'react'
import { ContentPaddingProvider, ContentWidthProvider } from './ContentWidthProvider'
import { SidebarWrapper } from './SidebarWrapper'

type NavigationState = 'hidden' | 'strip' | 'full'

interface ResponsiveNavigationState {
  base: NavigationState
  md: NavigationState
  lg: NavigationState
}

const WIDTH_PX: { [k in NavigationState]: number } = {
  hidden: 0,
  strip: 56,
  full: 280,
}

const DEFAULT_STATES: ResponsiveNavigationState = {
  base: 'hidden',
  md: 'strip',
  lg: 'strip', // TODO: full
}

export const NavigationLayout: React.FC<{
  navigationContent?: React.ReactNode | null
  animationEnabled?: boolean
}> = ({ navigationContent, animationEnabled = true, children }) => {
  const [responsiveState /*, setResponsiveState*/] =
    useState<ResponsiveNavigationState>(DEFAULT_STATES)

  const key = useBreakpointValue<keyof ResponsiveNavigationState>({
    base: 'base',
    md: 'md',
    lg: 'lg',
  })

  // const setState = useCallback(
  //   (newState: NavigationState) => setResponsiveState((s) => ({ ...s, [key]: newState })),
  //   [key],
  // )

  if (!key) {
    return null
  }

  const state = responsiveState[key]
  const width = navigationContent ? WIDTH_PX[state] : 0

  return (
    <ContentWidthProvider>
      <Flex flexDirection="row" minH={[['100vh', '-webkit-fill-available'] as any]}>
        <SidebarWrapper flexShrink={0} w={`${width}px`} animationEnabled={animationEnabled}>
          {navigationContent}
        </SidebarWrapper>
        <Box flex={1}>
          <ContentPaddingProvider left={width}>{children}</ContentPaddingProvider>
        </Box>
      </Flex>
    </ContentWidthProvider>
  )
}
