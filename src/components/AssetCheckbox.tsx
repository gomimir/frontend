import { Menu, MenuButton, MenuList, MenuItem, MenuDivider, Icon } from '@chakra-ui/react'
import React from 'react'
import { useCallback } from 'react'
import { FiDownload, FiFile, FiTrash, FiX } from 'react-icons/fi'
import { RiCheckDoubleLine, RiCheckLine } from 'react-icons/ri'
import { AssetFilter, Operation } from '../graphql'
import { translateNonFacetQuery, translateFacetQuery, buildQuery } from '../hooks/useAssetListing'
import { AssetListingScreenParams } from '../hooks/useAssetListingScreen'
import { ResolvedFacetsWithData } from '../hooks/useFacets'
import { Index, useIndex } from '../hooks/useIndex'
import { itemIsSelected, useItemSelection } from '../hooks/useItemSelection'
import { useModalManager } from '../hooks/useModalManager'
import { formatNumAssets } from '../utils/formatNumAssets'
import { ArchiveModal } from './ArchiveModal'
import { DeleteAssetsModal } from './DeleteAssetsModal'
import { IconPill } from './IconPill'

const DownloadAction: React.FC<{
  indexId: string
  buildQuery: () => AssetFilter | undefined
}> = ({ indexId, buildQuery }) => {
  const openModal = useModalManager()

  const handleDownload = useCallback(() => {
    openModal((onClose) => <ArchiveModal index={indexId} query={buildQuery()} onClose={onClose} />)
  }, [indexId, buildQuery, openModal])

  return (
    <MenuItem icon={<FiDownload />} onClick={handleDownload} fontSize="sm">
      Download assets
    </MenuItem>
  )
}

const DeleteAction: React.FC<{
  indexId: string
  numSelected: number
  buildQuery: () => AssetFilter | undefined
}> = ({ indexId, numSelected, buildQuery }) => {
  const openModal = useModalManager()

  const handleClick = useCallback(() => {
    openModal((onClose) => (
      <DeleteAssetsModal
        indexId={indexId}
        numAssets={numSelected}
        query={buildQuery()}
        onClose={onClose}
      />
    ))
  }, [indexId, buildQuery, numSelected, openModal])

  return (
    <MenuItem icon={<FiTrash />} onClick={handleClick} fontSize="sm">
      Delete assets
    </MenuItem>
  )
}

const AssetMenuList: React.FC<{
  params: AssetListingScreenParams
}> = ({ params }) => {
  const index = useIndex()
  const { selection, hasSelection, numSelected, totalCount, resetSelection, selectAll } =
    useItemSelection()
  const effectiveNumAssets = hasSelection ? numSelected : totalCount

  const actions: React.ReactElement[] = []

  const _buildQuery = useCallback(() => {
    const filters = [translateNonFacetQuery(params), translateFacetQuery(params)]
    const selectedIds = Object.keys(selection.selectedIds)

    if (hasSelection) {
      if (selection.mode === 'normal') {
        filters.push({
          id: {
            in: selectedIds,
          },
        })
      } else if (selection.mode === 'inverse' && selectedIds.length > 0) {
        filters.push({
          id: {
            notIn: selectedIds,
          },
        })
      }
    }

    return buildQuery(...filters)
  }, [hasSelection, selection, params])

  // TODO: Some visible warning or improve handling of large amount of files
  if (effectiveNumAssets < 2000) {
    actions.push(<DownloadAction indexId={params.indexName} buildQuery={_buildQuery} />)
  }

  if (index.permissions.assetOperations.includes(Operation.Delete)) {
    actions.push(
      <DeleteAction
        indexId={params.indexName}
        numSelected={effectiveNumAssets}
        buildQuery={_buildQuery}
      />,
    )
  }

  if (actions.length > 0) {
    actions.push(<MenuDivider />)
  }

  if (hasSelection) {
    actions.push(
      <MenuItem icon={<FiX />} onClick={resetSelection} fontSize="sm">
        Clear selection
      </MenuItem>,
    )
  } else {
    actions.push(
      <MenuItem icon={<RiCheckDoubleLine />} onClick={selectAll} fontSize="sm">
        Select all
      </MenuItem>,
    )
  }

  return React.createElement(MenuList, {}, ...actions)
}

export const AssetCheckbox: React.FC<{
  activeItemId: string | undefined
  params: AssetListingScreenParams
  facets: ResolvedFacetsWithData[]
  index: Index
}> = ({ activeItemId, params, facets, index }) => {
  const { selection, numSelected, totalCount, resetSelection } = useItemSelection()

  const currentSelected = activeItemId ? itemIsSelected(selection, activeItemId) : false
  const hasActiveItem = !!activeItemId

  const dropDown = numSelected > 1 || !currentSelected || (numSelected === 0 && totalCount > 0)
  const colorScheme = numSelected > 0 ? (currentSelected ? 'teal' : 'green') : 'gray'

  return (
    <Menu size="sm" isOpen={dropDown ? undefined : false}>
      {({ isOpen }) => (
        <>
          <MenuButton _hover={!dropDown ? { cursor: 'inherit' } : undefined} display="block">
            <IconPill
              clickable={dropDown}
              hasDropdown={dropDown}
              closable={!dropDown}
              highlighted={isOpen}
              colorScheme={colorScheme}
              onClose={resetSelection}
              icon={
                numSelected === 0 ? (
                  <Icon as={FiFile} w="12px" h="12px" />
                ) : numSelected === 1 && currentSelected ? (
                  <Icon as={RiCheckLine} w="14px" h="14px" />
                ) : (
                  <Icon as={RiCheckDoubleLine} w="14px" h="14px" />
                )
              }
              text={
                numSelected === 0
                  ? formatNumAssets({ numAssets: totalCount, facets, index })
                  : currentSelected
                  ? numSelected > 1
                    ? `This and ${numSelected - 1} other ${
                        numSelected > 2 ? 'items' : 'item'
                      } selected`
                    : 'Selected'
                  : `${numSelected} ${hasActiveItem ? 'other ' : ' '}${
                      numSelected > 1 ? 'items' : 'item'
                    } selected`
              }
            />
          </MenuButton>
          <AssetMenuList params={params} />
        </>
      )}
    </Menu>
  )
}
