import { useToast } from '@chakra-ui/react'
import { useDeleteAssetMutation } from '../graphql'
import { DeleteModal } from './DeleteModal'

export const DeleteAssetModal: React.FC<{
  asset: { id: string }
  onClose: () => void
}> = ({ asset, onClose }) => {
  const [deleteAsset] = useDeleteAssetMutation()
  const toast = useToast()

  const handleAccept = () => {
    onClose()

    deleteAsset({
      variables: {
        id: asset.id,
        deleteFiles: true,
      },
    }).then((response) => {
      if (response.data?.deleteAsset?.ok) {
        toast({
          title: 'Asset deleted',
          description: 'Asset was successfully deleted',
          status: 'success',
        })
      } else {
        toast({
          title: 'Operation failed',
          status: 'error',
        })
      }
    })
  }

  return (
    <DeleteModal
      titleContent="Delete asset"
      buttonContent="Delete asset"
      onAccept={handleAccept}
      onClose={onClose}
    >
      You are about to delete selected asset and all of its files. Are you sure you want to proceed?
    </DeleteModal>
  )
}
