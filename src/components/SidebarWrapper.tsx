import { Box, ChakraProps, forwardRef } from '@chakra-ui/react'

export const SidebarWrapper = forwardRef<
  { w: string; animationEnabled: boolean } & ChakraProps,
  'div'
>(({ animationEnabled, children, ...props }, ref) => (
  <Box
    transition={animationEnabled ? 'width .2s ease-in-out' : undefined}
    ref={ref}
    visibility={props.w === '0px' ? 'hidden' : undefined}
    {...props}
  >
    <Box
      position="fixed"
      zIndex={1}
      overflow="visible"
      w={props.w}
      h={[['100vh', '-webkit-fill-available'] as any]}
    >
      {children}
    </Box>
  </Box>
))
