import React from 'react'
import { FaChevronRight, FaTasks } from 'react-icons/fa'
import { FiAlertCircle, FiCheck } from 'react-icons/fi'
import {
  Box,
  Circle,
  HStack,
  Icon,
  IconButton,
  Popover,
  PopoverArrow,
  PopoverBody,
  PopoverContent,
  PopoverTrigger,
  Progress,
  Spinner,
  Stack,
  Text,
  Tooltip,
  useColorModeValue,
} from '@chakra-ui/react'
import { JobsInfo } from '../hooks/useJobs'
import { useScreen } from '../hooks/useScreen'
import { getJobDisplayName, getJobProgress, Job } from '../utils/jobs'
import { TimeAgo } from './TimeAgo'

const JobListItem: React.FC<{ job: Job }> = ({ job }) => {
  const { navigate } = useScreen()

  const textColor = useColorModeValue('gray.600', 'gray.200')

  const percentage = job.finished ? 100 : getJobProgress(job)
  const numFailed = job.taskSummary.reduce((sum, ts) => sum + ts.failed, 0)

  return (
    <HStack w="100%" spacing="3">
      <Circle
        size="24px"
        bg={job.finished ? (numFailed > 0 ? 'orange.300' : 'green.300') : 'blue.400'}
        color="white"
      >
        {job.finished ? (
          <Icon as={numFailed > 0 ? FiAlertCircle : FiCheck} />
        ) : (
          <Spinner size="sm" />
        )}
      </Circle>
      <Stack spacing="0.25" flex="1">
        <Text fontSize="sm" textColor={textColor}>
          {getJobDisplayName(job.name)}
        </Text>
        {!job.finished && (
          <Box pt="1" pr="1">
            <Progress
              size="xs"
              isIndeterminate={percentage === undefined}
              value={percentage}
              w="100%"
              colorScheme="blue"
            />
          </Box>
        )}
        {job.finished && <TimeAgo date={job.finishedAt!} message="Finished %v ago" fontSize="xs" />}
      </Stack>
      <IconButton
        size="sm"
        icon={<FaChevronRight />}
        variant="ghost"
        color="current"
        aria-label="Show job detail"
        onClick={() =>
          navigate({
            name: 'JobDetail',
            params: {
              jobId: job.id,
            },
          })
        }
      />
    </HStack>
  )
}

const JobList: React.FC<{
  jobsInfo: JobsInfo
}> = ({ jobsInfo }) => {
  const { jobs } = jobsInfo

  if (jobs.length === 0) {
    return <Text>No jobs</Text>
  }

  return (
    <Stack w="100%" spacing="3" p="2">
      {jobs.slice(0, 6).map((job) => (
        <JobListItem key={job.id} job={job} />
      ))}
    </Stack>
  )
}

export const JobListTrigger: React.FC<{
  jobsInfo: JobsInfo
}> = ({ jobsInfo }) => {
  return (
    <Box>
      <Popover isLazy autoFocus={false} placement="right-end">
        <Tooltip label="Show job list" placement="right">
          <Box display="inline-block">
            <PopoverTrigger>
              <IconButton
                icon={<FaTasks />}
                variant="ghost"
                color="current"
                aria-label="Show job list"
              />
            </PopoverTrigger>
          </Box>
        </Tooltip>
        <PopoverContent>
          <PopoverArrow />
          <PopoverBody>
            <JobList jobsInfo={jobsInfo} />
          </PopoverBody>
        </PopoverContent>
      </Popover>
    </Box>
  )
}
