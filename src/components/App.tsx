import { ApolloProvider } from '@apollo/client'
import { ChakraProvider, ColorModeScript } from '@chakra-ui/react'
import { client } from '../client'
import { theme } from '../theme'
import { AppScreenManager } from './AppScreenManager'

export const App = () => (
  <>
    <ColorModeScript />
    <ChakraProvider theme={theme}>
      <ApolloProvider client={client}>
        <AppScreenManager />
      </ApolloProvider>
    </ChakraProvider>
  </>
)
