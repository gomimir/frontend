import { PropertyPairFragment } from '../../graphql'

export interface BasePropertyRendererProps {
  assetId: string
  filename: string
}

export type PropertyRenderer = (allProperties: PropertyPairFragment[]) => {
  render: (props: BasePropertyRendererProps) => React.ReactNode
  usedKeys: string[]
}
