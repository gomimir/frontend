import { LabeledValue } from '../components/LabeledValue'
import { createPropertyRenderer } from '../factories'
import { matchRequiredProperty } from '../matchers'

export default createPropertyRenderer(
  matchRequiredProperty('lens_focalLength', 'FractionProperty'),
  ({ property }) => {
    return (
      <LabeledValue label="Focal length">
        {Math.round(property.numerator / property.denominator).toString() + ' mm'}
      </LabeledValue>
    )
  },
)
