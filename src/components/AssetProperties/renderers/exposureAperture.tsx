import { LabeledValue } from '../components/LabeledValue'
import { createPropertyRenderer } from '../factories'
import { matchRequiredProperty } from '../matchers'

export default createPropertyRenderer(
  matchRequiredProperty('exposure_aperture', 'FractionProperty'),
  ({ property }) => {
    return (
      <LabeledValue label="Aperture">
        {'f/' + property.numerator / property.denominator}
      </LabeledValue>
    )
  },
)
