import { LabeledValue } from '../components/LabeledValue'
import { PropertyPair } from '../components/PropertyPair'
import { TimePropertyEditor } from '../components/TimeEditorForm'
import { createMultiPropertyRenderer } from '../factories'
import { matchOptionalProperties } from '../matchers'

export default createMultiPropertyRenderer(
  matchOptionalProperties({
    authoredAt: 'TimeProperty',
    date: 'TimeProperty',
  }),
  ({ properties: { authoredAt, date }, ...props }) => {
    if (authoredAt) {
      const d = new Date(authoredAt.timeValue)
      if (!isNaN(d as any)) {
        return (
          <PropertyPair {...props}>
            <LabeledValue label="Authored at" editable>
              {d.toLocaleString()}
            </LabeledValue>
            <TimePropertyEditor property="authoredAt" value={d} />
          </PropertyPair>
        )
      }
    } else if (date) {
      const d = new Date(date.timeValue)
      if (!isNaN(d as any)) {
        return (
          <PropertyPair {...props}>
            <LabeledValue label="Date" editable>
              {d.toLocaleString()}
            </LabeledValue>
            <TimePropertyEditor property="date" value={d} />
          </PropertyPair>
        )
      }
    }

    return null
  },
)
