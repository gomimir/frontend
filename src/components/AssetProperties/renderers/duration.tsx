import { formatTimeTook } from '../../../utils/format'
import { LabeledValue } from '../components/LabeledValue'
import { createPropertyRenderer } from '../factories'
import { matchRequiredProperty } from '../matchers'

export default createPropertyRenderer(
  matchRequiredProperty('video_duration', 'NumberProperty'),
  ({ property }) => {
    return (
      <LabeledValue label="Video duration">
        {formatTimeTook(property.numericValue * 1000)}
      </LabeledValue>
    )
  },
)
