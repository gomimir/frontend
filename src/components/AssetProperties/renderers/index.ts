import dateRenderer from './date'
import durationRenderer from './duration'
import resolutionRenderer from './resolution'
import codecRenderer from './codec'
import exposureTimeRenderer from './exposureTime'
import exposureApertureRenderer from './exposureAperture'
import exposureISORenderer from './exposureISO'
import cameraRenderer from './camera'
import lensModelRenderer from './lensModel'
import lensFocalLengthRenderer from './lensFocalLength'

// Listed properties will be skipped by wildcard renderers
export const BLACKLISTED_PROPERTIES = ['extractedText']

// Ordered list of property renderers
export const PROPERTY_RENDERERS = [
  dateRenderer,
  durationRenderer,
  resolutionRenderer,
  codecRenderer,
  exposureTimeRenderer,
  exposureApertureRenderer,
  exposureISORenderer,
  cameraRenderer,
  lensModelRenderer,
  lensFocalLengthRenderer,
]
