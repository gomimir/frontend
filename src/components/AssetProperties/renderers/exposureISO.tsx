import { LabeledValue } from '../components/LabeledValue'
import { createPropertyRenderer } from '../factories'
import { matchRequiredProperty } from '../matchers'

export default createPropertyRenderer(
  matchRequiredProperty('exposure_iso', 'NumberProperty'),
  ({ property }) => {
    return <LabeledValue label="ISO">{property.numericValue.toString()}</LabeledValue>
  },
)
