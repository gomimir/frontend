import { formatFramerate } from '../../../utils/format'
import { LabeledValue } from '../components/LabeledValue'
import { createMultiPropertyRenderer } from '../factories'
import { matchOptionalProperties } from '../matchers'

export default createMultiPropertyRenderer(
  matchOptionalProperties({
    image_width: 'NumberProperty',
    image_height: 'NumberProperty',
    video_frameRate: 'FractionProperty',
    type: 'TextProperty',
  }),
  ({
    properties: {
      image_width: imageWidth,
      image_height: imageHeight,
      video_frameRate: videoFrameRate,
      type,
    },
  }) => {
    if (imageWidth && imageHeight) {
      if (type?.textValue === 'video') {
        const resolution =
          imageWidth.numericValue === 3840 && imageHeight.numericValue === 2160
            ? '4k'
            : imageWidth.numericValue === 1920 && imageHeight.numericValue === 1080
            ? '1080p'
            : imageWidth.numericValue === 1280 && imageHeight.numericValue === 720
            ? '720p'
            : `${imageWidth.numericValue}x${imageHeight.numericValue}`

        if (videoFrameRate) {
          return (
            <LabeledValue label="Video quality">{`${resolution}, ${formatFramerate(
              videoFrameRate.numerator,
              videoFrameRate.denominator,
            )}`}</LabeledValue>
          )
        } else {
          return <LabeledValue label="Video resolution">{resolution}</LabeledValue>
        }
      } else {
        return (
          <LabeledValue label="Image resolution">{`${imageWidth.numericValue}x${imageHeight.numericValue}`}</LabeledValue>
        )
      }
    } else if (videoFrameRate) {
      return (
        <LabeledValue label="Video framerate">
          {formatFramerate(videoFrameRate.numerator, videoFrameRate.denominator)}
        </LabeledValue>
      )
    }

    return null
  },
)
