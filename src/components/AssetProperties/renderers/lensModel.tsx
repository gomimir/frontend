import { formatModelAndMake } from '../../../utils/format'
import { LabeledValue } from '../components/LabeledValue'
import { createMultiPropertyRenderer } from '../factories'
import { matchOptionalProperties } from '../matchers'

export default createMultiPropertyRenderer(
  matchOptionalProperties({
    lens_make: 'TextProperty',
    lens_model: 'TextProperty',
  }),
  ({ properties: { lens_make: lensMake, lens_model: lensModel } }) => {
    if (lensMake || lensModel) {
      return (
        <LabeledValue label="Lens">
          {formatModelAndMake(lensModel?.textValue, lensMake?.textValue)}
        </LabeledValue>
      )
    }

    return null
  },
)
