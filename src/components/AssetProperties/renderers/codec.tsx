import { LabeledValue } from '../components/LabeledValue'
import { createMultiPropertyRenderer } from '../factories'
import { matchOptionalProperties } from '../matchers'

export default createMultiPropertyRenderer(
  matchOptionalProperties({
    video_codec: 'TextProperty',
    audio_codec: 'TextProperty',
  }),
  ({ properties: { video_codec: videoCodec, audio_codec: audioCodec } }) => {
    if (videoCodec && audioCodec) {
      return (
        <LabeledValue label="Video / Audio codec">{`${videoCodec.textValue} / ${audioCodec.textValue}`}</LabeledValue>
      )
    } else if (videoCodec) {
      return <LabeledValue label="Video codec">{videoCodec.textValue}</LabeledValue>
    } else if (audioCodec) {
      return <LabeledValue label="Audio codec">{audioCodec.textValue}</LabeledValue>
    }

    return null
  },
)
