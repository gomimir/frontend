import { formatModelAndMake } from '../../../utils/format'
import { LabeledValue } from '../components/LabeledValue'
import { createMultiPropertyRenderer } from '../factories'
import { matchOptionalProperties } from '../matchers'

export default createMultiPropertyRenderer(
  matchOptionalProperties({
    camera_make: 'TextProperty',
    camera_model: 'TextProperty',
  }),
  ({ properties: { camera_make: cameraMake, camera_model: cameraModel } }) => {
    if (cameraMake || cameraModel) {
      return (
        <LabeledValue label="Camera">
          {formatModelAndMake(cameraModel?.textValue, cameraMake?.textValue)}
        </LabeledValue>
      )
    }

    return null
  },
)
