import { LabeledValue } from '../components/LabeledValue'
import { createPropertyRenderer } from '../factories'
import { matchRequiredProperty } from '../matchers'

export default createPropertyRenderer(
  matchRequiredProperty('exposure_time', 'FractionProperty'),
  ({ property }) => {
    return (
      <LabeledValue label="Shutter speed">
        {property.numerator + '/' + property.denominator + ' s'}
      </LabeledValue>
    )
  },
)
