import { PropertyPairFragment } from '../../graphql'

export type SinglePropertyMatcher<T extends PropertyPairFragment | undefined> = (
  allProperties: PropertyPairFragment[],
) => { property?: T; usedKeys: string[] }

export function matchRequiredProperty<T extends PropertyPairFragment['__typename']>(
  name: string,
  typename: T,
): SinglePropertyMatcher<Extract<PropertyPairFragment, { __typename?: T }>> {
  return (allProperties) => {
    for (const property of allProperties) {
      if (property.name === name && property.__typename === typename) {
        return {
          property: property as Extract<PropertyPairFragment, { __typename?: T }>,
          usedKeys: [name],
        }
      }
    }

    return { usedKeys: [name] }
  }
}

export type MultiPropertyMatcher<
  T extends { [propName: string]: PropertyPairFragment | undefined },
> = (allProperties: PropertyPairFragment[]) => T

export function matchOptionalProperties<
  T extends { [propName: string]: PropertyPairFragment['__typename'] },
>(
  descriptors: T,
): MultiPropertyMatcher<{
  [propName in keyof T]: Extract<PropertyPairFragment, { __typename?: T[propName] }> | undefined
}> {
  return (allProperties) => {
    const result: { [k: string]: PropertyPairFragment } = {}
    for (const prop of allProperties) {
      if (descriptors[prop.name] === prop.__typename) {
        result[prop.name] = prop
      }
    }

    return result as {
      [propName in keyof T]: Extract<PropertyPairFragment, { __typename?: T[propName] | undefined }>
    }
  }
}
