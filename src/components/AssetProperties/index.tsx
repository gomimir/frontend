import { VStack } from '@chakra-ui/react'
import React from 'react'
import { PropertyPairFragment } from '../../graphql'
import { InspectorSection } from '../InspectorSection'
import { LabeledValue } from './components/LabeledValue'
import { PropertyPair } from './components/PropertyPair'
import { TextPropertyEditor } from './components/TextPropertyEditor'
import { TimePropertyEditor } from './components/TimeEditorForm'
import { BLACKLISTED_PROPERTIES, PROPERTY_RENDERERS } from './renderers'
import { BasePropertyRendererProps } from './types'

interface AssetPropertiesProps extends BasePropertyRendererProps {
  allProperties: PropertyPairFragment[]
}

export const AssetProperties: React.FC<AssetPropertiesProps> = ({ allProperties, ...props }) => {
  const { renderedProperties, usedKeys } = PROPERTY_RENDERERS.reduce(
    (res, renderer) => {
      const { render, usedKeys: currentUsedKeys } = renderer(allProperties)
      const content = render(props)

      if (content !== null) {
        res.renderedProperties.push(content)
      }

      res.renderedProperties.push(...currentUsedKeys)

      return res
    },
    {
      renderedProperties: [] as React.ReactNode[],
      usedKeys: [...BLACKLISTED_PROPERTIES] as string[],
    },
  )

  const usedKeysSet = new Set(usedKeys)
  const sortedProperties = [...allProperties].sort((a, b) => a.name.localeCompare(b.name))

  for (const prop of sortedProperties) {
    if (!usedKeysSet.has(prop.name)) {
      if (prop.__typename === 'TextProperty') {
        renderedProperties.push(
          <PropertyPair {...props}>
            <LabeledValue label={prop.name} editable>
              {prop.textValue}
            </LabeledValue>
            <TextPropertyEditor property={prop.name} value={prop.textValue} />
          </PropertyPair>,
        )
      } else if (prop.__typename === 'TimeProperty') {
        const d = new Date(prop.timeValue)
        if (!isNaN(d as any)) {
          renderedProperties.push(
            <PropertyPair {...props}>
              <LabeledValue label={prop.name} editable>
                {d.toLocaleString()}
              </LabeledValue>
              <TimePropertyEditor property={prop.name} value={d} />
            </PropertyPair>,
          )
        }
      }
    }
  }

  return renderedProperties.length ? (
    <InspectorSection title="Properties">
      {React.createElement(VStack, { alignItems: 'start', spacing: 'sm' }, ...renderedProperties)}
    </InspectorSection>
  ) : null
}
