import React, { useContext } from 'react'
import { forwardRef, HStack, Text, useColorModeValue } from '@chakra-ui/react'
import { CSSObject } from '@emotion/react'
import { PropertyEditorContext } from './PropertyEditorContext'

export const LabeledValue = forwardRef(
  (
    {
      label,
      editable,
      children,
    }: {
      label: string
      editable?: boolean
      children: React.ReactNode
    },
    ref,
  ) => {
    const { edit, isOpen } = useContext(PropertyEditorContext) || {}

    const highlightBgColor = useColorModeValue(
      '#EDF2F7', // gray.100
      'rgba(226, 232, 240, 0.16)',
    )

    const highlight: CSSObject = {
      content: '""',
      display: 'block',
      backgroundColor: highlightBgColor,
      position: 'absolute',
      top: '2px',
      bottom: '1px',
      left: '-6px',
      right: '0px',
      zIndex: -1,
      borderRadius: '4px',
    }

    return (
      <HStack
        ref={ref}
        maxW="100%"
        position="relative"
        css={
          isOpen
            ? {
                '&:after': highlight,
              }
            : editable
            ? {
                '&:hover': {
                  cursor: 'pointer',
                },
                '&:hover:after': highlight,
              }
            : undefined
        }
        lineHeight="24px"
        pr="6px"
        onClick={editable ? edit : undefined}
      >
        <Text as="span" fontSize="xs">
          {label}:
        </Text>
        <Text as="span" fontSize="xs" fontWeight="semibold" noOfLines={1}>
          {children}
        </Text>
      </HStack>
    )
  },
)
