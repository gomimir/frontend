import React from 'react'
import { AssetUpdate } from '../../../graphql'

export const PropertyEditorContext = React.createContext<{
  initialFocusRef: React.MutableRefObject<any>
  close: () => void
  edit: () => void
  isOpen: boolean
  isSaving: boolean
  save: (assetUpdate: AssetUpdate) => void
}>(null!)
