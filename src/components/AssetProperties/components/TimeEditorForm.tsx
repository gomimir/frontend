import React, { useContext } from 'react'
import { FormInput } from '../../FormInput'
import { SubmitHandler, useForm } from 'react-hook-form'
import { date, object } from 'yup'
import { yupResolver } from '@hookform/resolvers/yup'
import { PropertyEditorContext } from './PropertyEditorContext'
import { PropertyEditorForm } from './PropertyEditorForm'

interface TimePropertyForm {
  timeValue: Date
}

const timePropertyFormSchema = object({
  timeValue: date().typeError('Must be ISO Date string').optional(),
})

export const TimePropertyEditor: React.FC<{ property: string; value: Date }> = ({
  property,
  value,
}) => {
  const { initialFocusRef, close, save } = useContext(PropertyEditorContext)

  const { control, handleSubmit } = useForm<TimePropertyForm>({
    resolver: yupResolver(timePropertyFormSchema),
    defaultValues: {
      timeValue: value,
    },
  })

  const submit: SubmitHandler<TimePropertyForm> = ({ timeValue }) => {
    close()
    save({
      setProperties: [
        {
          name: property,
          timeValue: timeValue.toISOString(),
        },
      ],
    })
  }

  return (
    <PropertyEditorForm onSubmit={handleSubmit(submit)}>
      <FormInput name="timeValue" control={control} ref={initialFocusRef} />
    </PropertyEditorForm>
  )
}
