import React, { useCallback, useMemo } from 'react'
import { useDisclosure } from '@chakra-ui/react'
import { FileUpdate } from '../../../graphql'
import { useAssetUpdate } from '../../../hooks/useAssetUpdate'
import { PropertyEditorContext } from './PropertyEditorContext'

export const PropertyPair: React.FC<{ assetId: string; filename: string }> = ({
  assetId,
  filename,
  children,
}) => {
  const initialFocusRef = React.useRef(null)
  const { onOpen, onClose, isOpen } = useDisclosure()
  const { update: updateAsset, inProgress: isSaving } = useAssetUpdate()

  const save = useCallback(
    (update: Omit<FileUpdate, 'selector'>) => {
      updateAsset(assetId, {
        updateFiles: [
          {
            selector: {
              fileRef: {
                filename,
              },
            },
            ...update,
          },
        ],
      })
    },
    [assetId, filename, updateAsset],
  )

  const editorCtx = useMemo(
    () => ({ initialFocusRef, close: onClose, edit: onOpen, isOpen, isSaving, save }),
    [initialFocusRef, onClose, onOpen, isOpen, save, isSaving],
  )

  return (
    <PropertyEditorContext.Provider value={editorCtx}>{children}</PropertyEditorContext.Provider>
  )
}
