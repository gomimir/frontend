import React, { useContext } from 'react'
import { Box, Popover, PopoverBody, PopoverContent, PopoverAnchor } from '@chakra-ui/react'
import { PropertyEditorContext } from './PropertyEditorContext'

export const PropertyEditorPopover: React.FC = ({ children }) => {
  const { initialFocusRef, close, isOpen } = useContext(PropertyEditorContext)

  return (
    <Popover
      isOpen={isOpen}
      onClose={close}
      initialFocusRef={initialFocusRef}
      closeDelay={0}
      gutter={4}
      isLazy
      matchWidth
    >
      <Box w="100%" position="relative">
        <PopoverAnchor>
          <Box position="absolute" left="-6px" right="0px" />
        </PopoverAnchor>
      </Box>
      <PopoverContent width="inherit">
        <PopoverBody p={2}>{children}</PopoverBody>
      </PopoverContent>
    </Popover>
  )
}
