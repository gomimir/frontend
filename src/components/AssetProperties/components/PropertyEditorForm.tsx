import React from 'react'
import { VStack } from '@chakra-ui/react'
import { PropertyEditorPopover } from './PropertyEditorPopover'
import { PropertyEditorButtons } from './PropertyEditorButtons'

export const PropertyEditorForm: React.FC<{
  onSubmit?: React.FormEventHandler<HTMLFormElement>
}> = ({ onSubmit, children }) => {
  return (
    <PropertyEditorPopover>
      <form onSubmit={onSubmit}>
        <VStack>
          {children}
          <PropertyEditorButtons />
        </VStack>
      </form>
    </PropertyEditorPopover>
  )
}
