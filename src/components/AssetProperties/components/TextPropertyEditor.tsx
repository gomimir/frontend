import React, { useContext } from 'react'
import { FormInput } from '../../FormInput'
import { SubmitHandler, useForm } from 'react-hook-form'
import { PropertyEditorForm } from './PropertyEditorForm'
import { PropertyEditorContext } from './PropertyEditorContext'

interface TextPropertyForm {
  textValue: string
}

export const TextPropertyEditor: React.FC<{ property: string; value: string }> = ({
  property,
  value,
}) => {
  const { initialFocusRef, close, save } = useContext(PropertyEditorContext)

  const { control, handleSubmit } = useForm<{ textValue: string }>({
    defaultValues: {
      textValue: value,
    },
  })

  const submit: SubmitHandler<TextPropertyForm> = ({ textValue }) => {
    close()
    save({
      setProperties: [
        {
          name: property,
          textValue,
        },
      ],
    })
  }

  return (
    <PropertyEditorForm onSubmit={handleSubmit(submit)}>
      <FormInput name="textValue" control={control} ref={initialFocusRef} />
    </PropertyEditorForm>
  )
}
