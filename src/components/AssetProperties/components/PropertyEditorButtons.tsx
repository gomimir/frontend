import React, { useContext } from 'react'
import { Button, ButtonGroup } from '@chakra-ui/react'
import { BsCheck, BsX } from 'react-icons/bs'
import { PropertyEditorContext } from './PropertyEditorContext'

export const PropertyEditorButtons: React.FC = ({ children }) => {
  const { close, isSaving } = useContext(PropertyEditorContext)

  return (
    <ButtonGroup variant="outline" size="xs">
      <Button
        type="submit"
        colorScheme="blue"
        leftIcon={<BsCheck />}
        iconSpacing="1"
        isLoading={isSaving}
      >
        Save
      </Button>
      <Button leftIcon={<BsX />} iconSpacing="1" onClick={close}>
        Cancel
      </Button>
      {children}
    </ButtonGroup>
  )
}
