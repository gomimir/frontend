import { PropertyPairFragment } from '../../graphql'
import { MultiPropertyMatcher, SinglePropertyMatcher } from './matchers'
import { BasePropertyRendererProps, PropertyRenderer } from './types'

export interface SinglePropertyRendererProps<T> extends BasePropertyRendererProps {
  property: T
}

export interface MultiPropertyRendererProps<T> extends BasePropertyRendererProps {
  properties: T
}

export function createPropertyRenderer<T extends PropertyPairFragment | undefined>(
  matcher: SinglePropertyMatcher<T>,
  renderer: (props: SinglePropertyRendererProps<T>) => React.ReactNode,
): PropertyRenderer {
  return (allProperties) => {
    const { property, usedKeys } = matcher(allProperties)

    return {
      render: (baseProps) => (property ? renderer({ property, ...baseProps }) : null),
      usedKeys,
    }
  }
}

export function createMultiPropertyRenderer<
  T extends { [propName: string]: PropertyPairFragment | undefined },
>(
  matcher: MultiPropertyMatcher<T>,
  renderer: (props: MultiPropertyRendererProps<T>) => React.ReactNode,
): PropertyRenderer {
  return (allProperties) => {
    const properties = matcher(allProperties)

    return {
      render: (baseProps) => renderer({ properties, ...baseProps }),
      usedKeys: Object.keys(properties),
    }
  }
}
