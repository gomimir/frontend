import { CONFIG } from '../config'
import { VideoFormat, VideoSidecarFragment } from '../graphql'
import { Asset } from '../hooks/useAssetListing'

// https://github.com/zzarcon/react-video-renderer/blob/master/src/video.tsx
export const VideoPreview: React.FC<{
  file: Exclude<Asset['mainFile'], null>
  preloading: boolean
}> = ({ file, preloading }) => {
  // TODO: consider all supported formats by the user browser
  const transcodedVideos = file.sidecars.filter(
    (s) => s.__typename === 'Video' && s.videoFormat === VideoFormat.Mp4 && s.videoCodec === 'h264',
  ) as VideoSidecarFragment[]

  // TODO: proper resolution pick
  transcodedVideos.sort((a, b) => b.width * b.height - a.width * a.height)

  return (
    <video
      src={CONFIG.httpUrl + (transcodedVideos[0]?.file.url || file.url)}
      controls
      style={{
        maxHeight: '100%',
        maxWidth: '100%',
        opacity: preloading ? 0 : undefined,
        position: preloading ? 'absolute' : undefined,
      }}
    ></video>
  )
}
