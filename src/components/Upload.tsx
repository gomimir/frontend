import React from 'react'
import { useForm } from 'react-hook-form'
import { FaUpload } from 'react-icons/fa'
import { Box } from '@chakra-ui/layout'
import { Modal } from '@chakra-ui/modal'
import {
  Alert,
  AlertDescription,
  AlertIcon,
  AlertTitle,
  Button,
  HStack,
  IconButton,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  Tooltip,
  useDisclosure,
} from '@chakra-ui/react'
import { useIndex } from '../hooks/useIndex'
import { useJobProgress } from '../hooks/useJobProgress'
import { useUpload } from '../hooks/useUpload'
import { UploadForm } from './UploadForm'
import { UploadProgress } from './UploadProgress'
import { Operation } from '../graphql'

export const UploadModal: React.FC<{
  indexId: string
  onClose: () => void
}> = ({ indexId, onClose }) => {
  const { upload, progress: uploadProgress, jobId, errors: uploadErrors } = useUpload()
  const processingProgress = useJobProgress(jobId)

  const form = useForm({ mode: 'onChange' })
  const {
    handleSubmit,
    formState: { isSubmitted, isValid },
  } = form

  const onSubmit = ({
    files,
    allFilesAreSingleAsset,
  }: {
    allFilesAreSingleAsset: boolean
    files: File[]
  }) => {
    upload({
      index: indexId,
      files,
      allFilesAreSingleAsset,
    })
  }

  const uploadFailed = !!uploadErrors?.length
  const uploadInProgress = uploadProgress.totalChunks > uploadProgress.finishedChunks
  const canClose = !uploadInProgress || uploadFailed

  return (
    <Modal
      size="lg"
      isOpen={true}
      onClose={onClose}
      closeOnOverlayClick={canClose}
      closeOnEsc={canClose}
    >
      <ModalOverlay />
      <ModalContent>
        <ModalHeader>Upload files</ModalHeader>
        {canClose && <ModalCloseButton tabIndex={-1} />}
        <ModalBody>
          {uploadFailed ? (
            <Alert
              status="error"
              variant="subtle"
              flexDirection="column"
              alignItems="center"
              justifyContent="center"
              textAlign="center"
              height="190px"
            >
              <AlertIcon boxSize="32px" mr={0} />
              <AlertTitle mt={4} mb={1} fontSize="lg">
                Upload failed
              </AlertTitle>
              <AlertDescription maxWidth="sm">
                Error occurred while uploading the files. Check the log for more information.
              </AlertDescription>
            </Alert>
          ) : isSubmitted ? (
            <UploadProgress
              uploadProgress={uploadProgress}
              processingProgress={processingProgress}
            />
          ) : (
            <UploadForm form={form} onSubmit={onSubmit} />
          )}
        </ModalBody>
        <ModalFooter>
          <HStack spacing="4">
            <Box fontSize="xs" color="GrayText">
              {isSubmitted &&
                !uploadFailed &&
                (uploadInProgress
                  ? 'Wait until files finish uploading'
                  : 'All files uploaded, processing will finish in the background')}
            </Box>

            {isSubmitted || uploadFailed ? (
              <Button
                colorScheme="blue"
                mr={3}
                onClick={onClose}
                disabled={uploadInProgress && !uploadFailed}
              >
                Close
              </Button>
            ) : (
              <HStack>
                <Button onClick={onClose} tabIndex={4} variant="ghost">
                  Cancel
                </Button>
                <Button
                  tabIndex={3}
                  colorScheme="blue"
                  mr={3}
                  onClick={() => handleSubmit(onSubmit)()}
                  disabled={!isValid}
                >
                  Upload files
                </Button>
              </HStack>
            )}
          </HStack>
        </ModalFooter>
      </ModalContent>
    </Modal>
  )
}

export const Upload: React.FC = () => {
  const index = useIndex()
  const { isOpen, onOpen, onClose } = useDisclosure()

  if (!index.permissions.assetCollectionOperations.includes(Operation.Upload)) {
    return null
  }

  return (
    <>
      <Tooltip label="Upload files" placement="right">
        <IconButton
          icon={<FaUpload />}
          variant="ghost"
          color="current"
          onClick={onOpen}
          aria-label="Upload files"
        />
      </Tooltip>
      {isOpen && <UploadModal indexId={index.id} onClose={onClose} />}
    </>
  )
}
