import { random, sample, times } from 'lodash'
import React, { useMemo } from 'react'
import { chakra } from '@chakra-ui/react'
import { GalleryItem, GalleryItemDelegate, GalleryViewItemProps } from './GalleryView'
import { GalleryViewInfinite } from './GalleryViewInfinite'

const ASPECT_RATIOS = [
  // Landscape is more common
  3 / 2,
  3 / 2,
  3 / 2,
  3 / 2,
  3 / 2,
  3 / 2,
  // Portrait
  2 / 3,
  2 / 3,
  2 / 3,
  // Square
  1,
]

interface MockedGalleryItem extends GalleryItem {
  index: number
}

function generateItems(startIndex: number, numItems: number): MockedGalleryItem[] {
  return times(numItems, () => {
    const width = random(100, 4000)
    const ratio = sample(ASPECT_RATIOS)!
    const height = Math.round(width * ratio)

    const index = startIndex++

    return {
      index,
      key: `mockItem${index}`,
      width,
      height,
    }
  })
}

const GalleryViewItemMock = ({ item }: GalleryViewItemProps<MockedGalleryItem>) => {
  return (
    <chakra.div h="100%" bg="gray.200">
      {item.index + 1}
    </chakra.div>
  )
}

const MockDelegate: GalleryItemDelegate<MockedGalleryItem> = {
  getId: (item) => item.key,
  getDimensions: ({ width, height }) => ({ width, height }),
}

export const GalleryViewInfiniteMock: React.FC<{ initialNumItems?: number }> = ({
  initialNumItems = 100,
}) => {
  const initialItems = useMemo(() => generateItems(0, initialNumItems), [initialNumItems])

  const fetchMore = async (offset: number) => {
    await new Promise((resolve) => setTimeout(resolve, 1000))
    return {
      items: generateItems(offset, 50),
      hasMore: true,
    }
  }

  return (
    <GalleryViewInfinite
      initialItems={initialItems}
      itemDelegate={MockDelegate}
      itemRenderer={GalleryViewItemMock}
      hasMore={true}
      fetchMore={fetchMore}
    />
  )
}
