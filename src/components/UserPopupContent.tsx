import { Box, Text, useColorModeValue } from '@chakra-ui/react'
import { useSystemInfoQuery } from '../graphql'

export const UserPopupContent: React.FC = () => {
  const textColor = useColorModeValue('gray.600', 'gray.200')
  const { data } = useSystemInfoQuery({
    fetchPolicy: 'cache-only',
  })

  const user = data?.currentUser.user

  if (!user) {
    return null
  }

  const userRoles =
    user.roles.length > 1 ? user.roles.filter((ur) => ur.name !== 'user') : user.roles

  const userRoleNames =
    userRoles.length === 0 ? ['User'] : userRoles.map((ur) => ur.displayName || ur.name)

  return (
    <Box>
      <Text color={textColor}>{user.displayName || user.username}</Text>
      <Text>{userRoleNames.join(', ')}</Text>
    </Box>
  )
}
