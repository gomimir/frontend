import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalCloseButton,
  ModalBody,
  ModalFooter,
  HStack,
  Button,
} from '@chakra-ui/react'

export const DeleteModal: React.FC<{
  titleContent: React.ReactNode
  buttonContent: React.ReactNode
  onClose: () => void
  onAccept: () => void
}> = ({ onClose, onAccept, titleContent, buttonContent, children }) => {
  return (
    <Modal size="lg" isOpen={true} onClose={onClose}>
      <ModalOverlay />
      <ModalContent>
        <ModalHeader>{titleContent}</ModalHeader>
        <ModalCloseButton tabIndex={-1} />
        <ModalBody>{children}</ModalBody>
        <ModalFooter>
          <HStack>
            <Button onClick={onClose} tabIndex={1} variant="ghost">
              Cancel
            </Button>
            <Button tabIndex={2} colorScheme="red" mr={3} onClick={onAccept}>
              {buttonContent}
            </Button>
          </HStack>
        </ModalFooter>
      </ModalContent>
    </Modal>
  )
}
