import React, { useCallback } from 'react'
import { ActiveItemActionUnion } from '../helpers/ActiveItem'
import { Asset } from '../hooks/useAssetListing'
import { itemIsSelected, useItemSelection } from '../hooks/useItemSelection'
import { AssetListingThumbnail, PLACEHOLDER_IMAGE_DIMENSIONS } from './AssetListingThumbnail'
import { GalleryItemDelegate, GalleryView, GalleryViewItemProps } from './GalleryView'

const assetItemDelegate: GalleryItemDelegate<Asset> = {
  getId: (asset) => asset.id,
  getDimensions: (asset) =>
    asset.galleryThumbnail
      ? {
          width: asset.galleryThumbnail.width,
          height: asset.galleryThumbnail.height,
        }
      : PLACEHOLDER_IMAGE_DIMENSIONS,
}

export const AssetListingGallery: React.FC<{
  assets: Asset[]
  fetchMore: (num?: number) => Promise<void>
  activeItemIndex?: number
  onActiveItemAction: (action: ActiveItemActionUnion) => void
}> = ({ assets, fetchMore, activeItemIndex, onActiveItemAction }) => {
  const { selection, toggleItemSelection } = useItemSelection()

  const itemRenderer = useCallback(
    ({ item: asset }: GalleryViewItemProps<Asset>) => (
      <AssetListingThumbnail
        asset={asset}
        selected={itemIsSelected(selection, asset.id)}
        onToggleSelect={() => toggleItemSelection(asset.id)}
      />
    ),
    [selection, toggleItemSelection],
  )

  return (
    <GalleryView
      items={assets}
      itemDelegate={assetItemDelegate}
      itemRenderer={itemRenderer}
      onMore={fetchMore}
      activeItemIndex={activeItemIndex}
      onActiveItemAction={onActiveItemAction}
    />
  )
}
