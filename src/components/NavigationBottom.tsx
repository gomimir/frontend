import {
  Tooltip,
  IconButton,
  VStack,
  Box,
  Popover,
  PopoverArrow,
  PopoverBody,
  PopoverContent,
  PopoverTrigger,
} from '@chakra-ui/react'
import { FiHome, FiUser } from 'react-icons/fi'
import { AuthType, useSystemInfoQuery } from '../graphql'
import { useJobs } from '../hooks/useJobs'
import { AppLink } from './AppLink'
import { ColorModeSwitcher } from './ColorModeSwitcher'
import { JobListTrigger } from './JobListTrigger'
import { UserPopupContent } from './UserPopupContent'

const HomeButton: React.FC = () => {
  return (
    <AppLink to={{ name: 'Homepage' }}>
      <Tooltip label="Home" placement="right">
        <IconButton icon={<FiHome size="22" />} variant="ghost" color="current" aria-label="Home" />
      </Tooltip>
    </AppLink>
  )
}

const UserButton: React.FC = () => {
  return (
    <Box>
      <Popover isLazy autoFocus={false} placement="right-start">
        <Tooltip label="User" placement="right">
          <Box display="inline-block">
            <PopoverTrigger>
              <IconButton
                icon={<FiUser size="22" />}
                variant="ghost"
                color="current"
                aria-label="User"
              />
            </PopoverTrigger>
          </Box>
        </Tooltip>
        <PopoverContent>
          <PopoverArrow />
          <PopoverBody>
            <UserPopupContent />
          </PopoverBody>
        </PopoverContent>
      </Popover>
    </Box>
  )
}

export const NavigationBottom: React.FC = () => {
  const { jobsInfo } = useJobs()
  const { data } = useSystemInfoQuery({
    fetchPolicy: 'cache-only',
  })

  const showUser = data?.serverInfo.auth && data?.serverInfo.auth !== AuthType.None

  return (
    <VStack spacing={1}>
      <HomeButton />
      <ColorModeSwitcher />
      {jobsInfo && <JobListTrigger jobsInfo={jobsInfo} />}
      {showUser && <UserButton />}
    </VStack>
  )
}
