import { FiFilter } from 'react-icons/fi'
import { AssetListingScreenParams } from '../hooks/useAssetListingScreen'
import { Action, ActionList } from './ActionList'

export const AssetListingActions: React.FC<{
  params: AssetListingScreenParams
  hasUsefulFacets: boolean
  onFilterToggle: () => void
}> = ({ hasUsefulFacets, onFilterToggle }) => {
  return (
    <ActionList numPrimaryActions={2}>
      {hasUsefulFacets && <Action label="Filter" icon={<FiFilter />} onClick={onFilterToggle} />}
    </ActionList>
  )
}
