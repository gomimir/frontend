import { useCallback, useContext, useState } from 'react'
import { FormProvider, useForm } from 'react-hook-form'
import {
  Box,
  Button,
  HStack,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  Text,
  useToast,
} from '@chakra-ui/react'
import { CONFIG } from '../config'
import { ArchiveFiles, AssetFilter, useCreateArchiveMutation } from '../graphql'
import { FormSelect } from './FormSelect'
import { JobManagerContext } from '../hooks/useJobs'

const FILES_FILTER_OPTIONS = [
  {
    label: 'All originals',
    value: ArchiveFiles.AllOriginals,
  },
  {
    label: 'Cover thumbnails (full-res)',
    value: ArchiveFiles.CoverThumbnails,
  },
]

interface ArchiveFormValues {
  filesFilter: ArchiveFiles
}

export const ArchiveModal: React.FC<{
  index: string
  query?: AssetFilter
  onClose: () => void
}> = ({ index, query, onClose }) => {
  const form = useForm<ArchiveFormValues>({
    mode: 'onChange',
    defaultValues: { filesFilter: ArchiveFiles.CoverThumbnails },
  })

  const { handleSubmit, control } = form

  const [createArchive] = useCreateArchiveMutation()
  const jobManager = useContext(JobManagerContext)
  const toast = useToast()
  const [state, setState] = useState<{
    inProgress: boolean
  }>({
    inProgress: false,
  })

  const onSubmit = useCallback(
    (values: ArchiveFormValues) => {
      setState({ inProgress: true })
      createArchive({
        variables: {
          index: index,
          query: query,
          files: values.filesFilter,
        },
      })
        .then((response) => {
          if (response.data?.archiveAssets?.ok) {
            if (response.data.archiveAssets.archive) {
              const { jobId, url } = response.data.archiveAssets.archive
              jobManager.onJobFinished(jobId, () => {
                window.location.replace(CONFIG.httpUrl + url)
                onClose()
              })
            } else {
              onClose()
              toast({
                title: 'No files has been archived',
                description: 'The provided filter resulted in empty selection',
                status: 'info',
              })
            }
          } else {
            onClose()
            toast({
              title: 'Operation failed',
              status: 'error',
            })
          }
        })
        .catch((err) => {
          onClose()
          toast({
            title: 'Operation failed',
            description: err.message,
            status: 'error',
          })
        })
    },
    [createArchive, index, query, jobManager, onClose, toast],
  )

  return (
    <Modal size="lg" isOpen={true} onClose={onClose} closeOnOverlayClick={true} closeOnEsc={true}>
      <ModalOverlay />
      <ModalContent>
        <ModalHeader>Download archive</ModalHeader>
        <ModalCloseButton tabIndex={-1} />
        <ModalBody>
          {state.inProgress ? (
            <Box>
              <Text>Download is being prepared. It will start automatically in few seconds</Text>
            </Box>
          ) : (
            <FormProvider {...form}>
              <form onSubmit={handleSubmit(onSubmit)}>
                <FormSelect
                  control={control}
                  name="filesFilter"
                  label="Files"
                  options={FILES_FILTER_OPTIONS}
                  mapValue={(o) => o?.value}
                  isRequired
                  tabIndex={2}
                />
              </form>
            </FormProvider>
          )}
        </ModalBody>
        <ModalFooter>
          <HStack>
            <Button onClick={onClose} tabIndex={4} variant="ghost">
              Cancel
            </Button>
            <Button
              tabIndex={1}
              colorScheme="blue"
              mr={3}
              onClick={() => handleSubmit(onSubmit)()}
              isLoading={state.inProgress}
            >
              Download archive
            </Button>
          </HStack>
        </ModalFooter>
      </ModalContent>
    </Modal>
  )
}
