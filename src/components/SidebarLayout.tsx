import { Box, Flex, useBreakpointValue } from '@chakra-ui/react'
import React, { useContext, useMemo } from 'react'
import { useState } from 'react'
import { ContentPaddingProvider } from './ContentWidthProvider'
import { SidebarWrapper } from './SidebarWrapper'

export type SidebarState = 'hidden' | 'full'

interface ResponsiveSidebarState {
  base: SidebarState
  lg: SidebarState
}

const WIDTH_PX: { [k in SidebarState]: number } = {
  hidden: 0,
  full: 280,
}

const DEFAULT_STATES: ResponsiveSidebarState = {
  base: 'hidden',
  lg: 'hidden',
}

interface SidebarHookResult {
  state: SidebarState
  setState: (newState: SidebarState) => void
  hide: () => void
}

const SidebarContext = React.createContext<SidebarHookResult>(null!)

export function useSidebar() {
  return useContext(SidebarContext)
}

export const SidebarLayout: React.FC<{ sidebarContent?: React.ReactNode | null }> = ({
  sidebarContent,
  children,
}) => {
  const [responsiveState, setResponsiveState] = useState<ResponsiveSidebarState>(DEFAULT_STATES)

  const key = useBreakpointValue<keyof ResponsiveSidebarState>({
    base: 'base',
    lg: 'lg',
  })

  const ctx = useMemo(
    () =>
      key
        ? {
            state: responsiveState[key],
            setState: (newState: SidebarState) =>
              setResponsiveState((s) => ({ ...s, [key]: newState })),
            hide: () => setResponsiveState((s) => ({ ...s, [key]: 'hidden' })),
          }
        : undefined,
    [responsiveState, setResponsiveState, key],
  )

  if (!ctx?.state) {
    return null
  }

  const width = sidebarContent ? WIDTH_PX[ctx.state] : 0

  return (
    <SidebarContext.Provider value={ctx}>
      <Flex flexDirection="row" minH={[['100vh', '-webkit-fill-available'] as any]}>
        <Box flex={1}>
          <ContentPaddingProvider right={width}>{children}</ContentPaddingProvider>
        </Box>
        <SidebarWrapper flexShrink={0} w={`${width}px`} animationEnabled={true}>
          {sidebarContent}
        </SidebarWrapper>
      </Flex>
    </SidebarContext.Provider>
  )
}
