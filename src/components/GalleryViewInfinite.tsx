import React, { useState } from 'react'
import { Stack } from '@chakra-ui/react'
import { GalleryItemDelegate, GalleryView, GalleryViewItemProps } from './GalleryView'

interface GalleryViewInfiniteProps<T> {
  initialItems: T[]
  itemDelegate: GalleryItemDelegate<T>
  itemRenderer: (props: GalleryViewItemProps<T>) => React.ReactElement
  hasMore: boolean
  fetchMore: (offset: number) => Promise<{ items: T[]; hasMore: boolean }>
}

export function GalleryViewInfinite<T>({
  initialItems,
  hasMore: initialHasMore,
  fetchMore,
  itemDelegate,
  itemRenderer,
}: GalleryViewInfiniteProps<T>): React.ReactElement<any, any> | null {
  const [{ items, hasMore }, setData] = useState({ items: initialItems, hasMore: initialHasMore })
  const [isLoading, setLoading] = useState(false)

  const load = async () => {
    setLoading(true)

    // TODO: handle errors
    const { items: freshItems, hasMore } = await fetchMore(items.length)

    setData({ items: [...items, ...freshItems], hasMore })
    setLoading(false)
  }

  const handleLoadMore = (_sizeHint: number) => {
    if (hasMore) {
      load()
    }
  }

  return (
    <Stack>
      <GalleryView
        items={items}
        itemDelegate={itemDelegate}
        itemRenderer={itemRenderer}
        onMore={handleLoadMore}
        onActiveItemAction={() => {}}
      />
      {isLoading && <div>Loading</div>}
    </Stack>
  )
}
