import { defaults } from 'lodash'

interface BootstrapConfig {
  httpUrl: string
  wsUrl: string
}

const DEFAULTS: BootstrapConfig = {
  httpUrl: 'http://localhost:3000',
  wsUrl: 'ws://localhost:3000',
}

export const CONFIG: BootstrapConfig = defaults({}, (window as any).CONFIG, DEFAULTS)
