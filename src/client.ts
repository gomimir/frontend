import { createUploadLink } from 'apollo-upload-client'
import { PartialDeep } from 'type-fest'
import { ApolloClient, InMemoryCache, split } from '@apollo/client'
import { getMainDefinition, mergeDeep } from '@apollo/client/utilities'
import { CONFIG } from './config'
import { AssetListing } from './graphql'
import possibleTypes from './graphql/possibleTypes'
import { AUTH_SESSION_STORAGE_KEY } from './utils/auth'
import { customFetch } from './utils/fetch'
import { WebSocketLink } from './utils/WebSocketLink'

const httpLink = createUploadLink({
  uri: CONFIG.httpUrl + '/graphql',
  fetch: customFetch,
})

const wsLink = new WebSocketLink({
  url: CONFIG.wsUrl + '/graphql',
  connectionParams: () => {
    const sessionKey = localStorage.getItem(AUTH_SESSION_STORAGE_KEY)
    if (sessionKey) {
      return {
        authToken: sessionKey,
      }
    }
  },
})

// The split function takes three parameters:
//
// * A function that's called for each operation to execute
// * The Link to use for an operation if the function returns a "truthy" value
// * The Link to use for an operation if the function returns a "falsy" value
const splitLink = split(
  ({ query }) => {
    const definition = getMainDefinition(query)
    return definition.kind === 'OperationDefinition' && definition.operation === 'subscription'
  },
  wsLink,
  httpLink as any,
)

export const client = new ApolloClient({
  link: splitLink,
  cache: new InMemoryCache({
    possibleTypes: possibleTypes.possibleTypes,
    typePolicies: {
      Query: {
        fields: {
          assets: {
            // FIXME: merging items with different sortBy
            keyArgs: ['index', 'query' /* , 'sortBy' */],
            merge(
              existing: PartialDeep<AssetListing> = { items: [] },
              incoming: PartialDeep<AssetListing>,
              { args },
            ) {
              if (incoming.items) {
                const items = existing?.items ? [...existing.items] : []
                items.splice(args?.skip || 0, incoming.items.length, ...incoming.items)

                return mergeDeep(existing, { ...incoming, items })
              } else {
                return mergeDeep(existing, incoming)
              }
            },
          },
          index: {
            keyArgs: ['id'],
            read(_, { args, toReference }) {
              return toReference({
                __typename: 'Index',
                id: args?.id,
              })
            },
          },
          tags: {
            keyArgs: ['nameQuery', 'kind', 'index'],
          },
        },
      },
      Index: {
        keyFields: ['id'],
      },
      Repository: {
        keyFields: ['id'],
      },
      Asset: {
        keyFields: ['id'],
        fields: {
          mainFile: {
            // https://www.apollographql.com/docs/react/caching/cache-field-behavior/#merging-non-normalized-objects
            merge(existing, incoming, { mergeObjects }) {
              return mergeObjects(existing, incoming)
            },
          },
        },
      },
    },
  }),
})
