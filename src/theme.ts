import { extendTheme } from '@chakra-ui/react'

export const theme = extendTheme({
  textStyles: {
    h1: {
      fontSize: ['32px', '42px'],
      fontWeight: 'semibold',
      pb: 2,
    },
  },
})
