
      export interface PossibleTypesResultData {
        possibleTypes: {
          [key: string]: string[]
        }
      }
      const result: PossibleTypesResultData = {
  "possibleTypes": {
    "ActionConfig": [
      "AddTagActionConfig",
      "IgnoreActionConfig",
      "ProcessActionConfig",
      "SetKeyActionConfig",
      "SetParamsActionConfig",
      "UnknownActionConfig"
    ],
    "JobEvent": [
      "FinishedJobEvent",
      "NewJobEvent",
      "ProgressJobEvent"
    ],
    "PermissionSubject": [
      "User",
      "UserRole"
    ],
    "Property": [
      "FractionProperty",
      "GPSProperty",
      "NumberProperty",
      "TextProperty",
      "TimeProperty"
    ],
    "PropertyHolder": [
      "AssetListingFacets",
      "FileFacets"
    ],
    "Repository": [
      "LocalRepository",
      "S3Repository",
      "UnknownRepository"
    ],
    "Sidecar": [
      "Thumbnail",
      "Video"
    ],
    "TagAssignmentReason": [
      "FilenameRuleTagAssignment",
      "UserTagAssignment"
    ]
  }
};
      export default result;
    