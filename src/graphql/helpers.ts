import { NameNode, SelectionNode, SelectionSetNode, VariableNode } from 'graphql'

export function gqlName(name: string): NameNode {
  return { kind: 'Name', value: name }
}

export function gqlVar(name: string): VariableNode {
  return {
    kind: 'Variable',
    name: gqlName(name),
  }
}

export function gqlSelSet(selections: SelectionNode[]): SelectionSetNode {
  return { kind: 'SelectionSet', selections }
}
