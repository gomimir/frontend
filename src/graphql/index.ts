import { gql } from '@apollo/client';
import * as Apollo from '@apollo/client';
export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
const defaultOptions =  {}
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  AssetID: string;
  JsonMap: any;
  /** Time formatted according to RFC3339 */
  Time: string;
  /**
   * Date / Time expression as defined https://www.elastic.co/guide/en/elasticsearch/reference/current/common-options.html#date-math
   * Some examples:
   * - now/d: start of today
   * - now/d-1d: start of yesterday
   */
  TimeExpression: any;
  /** Time zones may either be specified as an ISO 8601 UTC offset (e.g. +01:00 or -08:00) or as one of the time zone ids from the TZ database */
  TimeZone: any;
  /** File upload according to https://github.com/jaydenseric/graphql-multipart-request-spec */
  Upload: any;
};

export type ActionConfig = {
  action: ActionType;
};

export type ActionConfigInput = {
  ignore?: Maybe<Scalars['Boolean']>;
  setKey?: Maybe<Scalars['String']>;
  setParams?: Maybe<Scalars['JsonMap']>;
  addTag?: Maybe<TagInput>;
  process?: Maybe<ProcessActionConfigInput>;
};

export type ActionMutationResult = {
  __typename?: 'ActionMutationResult';
  ok: Scalars['Boolean'];
  action: Maybe<IndexAction>;
};

export enum ActionType {
  Unknown = 'UNKNOWN',
  Ignore = 'IGNORE',
  SetKey = 'SET_KEY',
  SetParams = 'SET_PARAMS',
  AddTag = 'ADD_TAG',
  Process = 'PROCESS'
}

export type AddTagActionConfig = ActionConfig & {
  __typename?: 'AddTagActionConfig';
  action: ActionType;
  tag: Tag;
};

export type ArchiveAssetsResult = {
  __typename?: 'ArchiveAssetsResult';
  /** True, if action has been successfully executed */
  ok: Scalars['Boolean'];
  /** Archive job info, can be null if the query returned no results */
  archive: Maybe<ArchiveJob>;
};

export enum ArchiveFiles {
  /** Export only single cover full-res thumbnail for each asset */
  CoverThumbnails = 'COVER_THUMBNAILS',
  /** Export all original asset files (no sidecars) */
  AllOriginals = 'ALL_ORIGINALS'
}

export type ArchiveJob = {
  __typename?: 'ArchiveJob';
  /**
   * Job ID for the scheduled action
   * Use Subscription.jobEvents to listen to the job state changes.
   */
  jobId: Scalars['String'];
  /** URL where you can download the archive once the job finishes */
  url: Scalars['String'];
};

export type Asset = {
  __typename?: 'Asset';
  /** Globally unique asset ID */
  id: Scalars['String'];
  /** Information about how the asset is indexed */
  indexInfo: Maybe<IndexInfo>;
  /** Unique asset identifier which is used to group the multiple files together */
  key: Scalars['String'];
  /** List of assets files */
  files: Array<AssetFile>;
  /** List of assigned tags */
  tags: Array<TagAssignment>;
  /** List of assigned properties */
  properties: Array<Property>;
  /** Main file of the asset (file with the highest priority) */
  mainFile: Maybe<AssetFile>;
  /**
   * Thumbnail which closest to the given dimensions.
   * If file contains multiple files, it chooses file with the highest priority (number property)
   */
  thumbnail: Maybe<Thumbnail>;
  /** List of operations available to the user of current session */
  allowedOperations: Array<Operation>;
};


export type AssetThumbnailArgs = {
  width?: Maybe<Scalars['Int']>;
  height?: Maybe<Scalars['Int']>;
  acceptableFormats?: Maybe<Array<ThumbnailFormat>>;
};

export type AssetFile = {
  __typename?: 'AssetFile';
  /** Name of the repository where the file is stored */
  repository: Scalars['String'];
  /** Filename, in case of S3 the first directory is a bucket name */
  filename: Scalars['String'];
  /** Additional information about the contents of the file */
  fileInfo: FileInfo;
  /** URL address which can be used to download the file */
  url: Scalars['String'];
  /** List of all sidecar files */
  sidecars: Array<Sidecar>;
  /** Thumbnail which closest to the given dimensions */
  thumbnail: Maybe<Thumbnail>;
  /** List of assigned properties */
  properties: Array<Property>;
  /** List of processings that have been run */
  processings: Array<FileProcessing>;
};


export type AssetFileThumbnailArgs = {
  width?: Maybe<Scalars['Int']>;
  height?: Maybe<Scalars['Int']>;
  acceptableFormats?: Maybe<Array<ThumbnailFormat>>;
};

export type AssetFileFilter = {
  repository?: Maybe<Array<TextFilter>>;
  filename?: Maybe<Array<TextFilter>>;
  withProperty?: Maybe<Array<PropertyFilter>>;
  withProcessing?: Maybe<Array<ProcessingFilter>>;
};

export type AssetFileStats = {
  __typename?: 'AssetFileStats';
  /** Number of files */
  count: Scalars['Int'];
  fileSize: NumericStats;
  sidecars: SidecarStats;
};

export type AssetFilter = {
  id?: Maybe<IdFilter>;
  fulltext?: Maybe<Scalars['String']>;
  withFile?: Maybe<AssetFileFilter>;
  withProperty?: Maybe<Array<PropertyFilter>>;
  withTag?: Maybe<Array<TagFilter>>;
};


export type AssetListing = {
  __typename?: 'AssetListing';
  /** Listed items */
  items: Array<Asset>;
  /** Number of records */
  totalCount: Scalars['Int'];
  /** Facets for narrowing down the search */
  facets: AssetListingFacets;
  /** Aggregated statistics about queried results */
  stats: AssetListingStats;
};

export type AssetListingFacets = PropertyHolder & {
  __typename?: 'AssetListingFacets';
  /** Aggregated statistics about assigned tags to resulting items */
  tags: Array<TagBucket>;
  /** List of available tag kinds */
  tagKinds: Array<TagKindBucket>;
  /** Returns commonly used values of given property */
  property: PropertyFacets;
  /** Returns list of properties and their types */
  propertyIntrospection: Array<PropertyIntrospection>;
  /** Aggregated statistics about files */
  files: FileFacets;
};


export type AssetListingFacetsTagsArgs = {
  kind: Scalars['String'];
  size?: Maybe<Scalars['Int']>;
};


export type AssetListingFacetsPropertyArgs = {
  name: Scalars['String'];
};

export type AssetListingStats = {
  __typename?: 'AssetListingStats';
  files: FileStats;
};

export type AssetMutationResult = {
  __typename?: 'AssetMutationResult';
  /** True if action has been successfully performed */
  ok: Scalars['Boolean'];
  /** Updated asset */
  asset: Asset;
};

export type AssetSortRequest = {
  property?: Maybe<PropertySortRequest>;
  file?: Maybe<FileSortRequest>;
  method?: Maybe<SortMethod>;
};

export enum AssetType {
  Document = 'DOCUMENT',
  Photo = 'PHOTO',
  Unknown = 'UNKNOWN'
}

export type AssetUpdate = {
  addTags?: Maybe<Array<TagInput>>;
  removeTags?: Maybe<Array<TagInput>>;
  setProperties?: Maybe<Array<PropertyInput>>;
  unsetProperties?: Maybe<Array<Scalars['String']>>;
  updateFiles?: Maybe<Array<FileUpdate>>;
};

export type AsyncMutationResult = {
  __typename?: 'AsyncMutationResult';
  /** True, if action has been successfully scheduled */
  ok: Scalars['Boolean'];
  /**
   * Job ID for the scheduled action
   * Use Subscription.jobEvents to listen to the job state changes.
   */
  jobId: Scalars['String'];
};

export type AuthRefresh = {
  __typename?: 'AuthRefresh';
  token: Scalars['String'];
  validUntil: Scalars['Time'];
  refreshToken: Maybe<Scalars['String']>;
};

export enum AuthType {
  None = 'NONE',
  Proxy = 'PROXY',
  Oidc = 'OIDC'
}

export type CrawlRequest = {
  /** Index ID */
  index: Scalars['ID'];
  /** File sets to crawl. If omitted all file locations registered for the index will be crawled. */
  fileSets?: Maybe<Array<FileSetInput>>;
  /**
   * Ignore previous results of processors with given name to force them to run again.
   * Use '*' to ignore all processor results.
   */
  reprocess?: Maybe<Array<Scalars['String']>>;
};

export type CurrentUser = {
  __typename?: 'CurrentUser';
  /** Logged in user, if authenticated */
  user: Maybe<User>;
  /** True, if current session has authenticated user */
  authenticated: Scalars['Boolean'];
  /** Time when the user session will end */
  willLogoutAt: Maybe<Scalars['Time']>;
  /** List of global operations available to the user of current session */
  collectionPermissions: Array<ResourceOperations>;
};

export type ErrorFilter = {
  code?: Maybe<Array<TextFilter>>;
  message?: Maybe<Array<TextFilter>>;
};

export type File = {
  __typename?: 'File';
  /** Name of the repository where the file is stored */
  repository: Scalars['String'];
  /** Filename, in case of S3 the first directory is a bucket name */
  filename: Scalars['String'];
  /** Additional information about the contents of the file */
  fileInfo: FileInfo;
  /** URL address which can be used to download the file */
  url: Scalars['String'];
};

export type FileFacets = PropertyHolder & {
  __typename?: 'FileFacets';
  /** Returns commonly used values of given property */
  property: PropertyFacets;
  /** Returns list of properties and their types */
  propertyIntrospection: Array<PropertyIntrospection>;
};


export type FileFacetsPropertyArgs = {
  name: Scalars['String'];
};

export type FileInfo = {
  __typename?: 'FileInfo';
  /** Date of the last modification of the file contents */
  lastModified: Scalars['Time'];
  /** File size in bytes */
  size: Maybe<Scalars['Int']>;
};

export type FileLocation = {
  __typename?: 'FileLocation';
  type: FileLocationType;
  repository: Repository;
  prefix: Scalars['String'];
};

export type FileLocationInput = {
  type: FileLocationType;
  repositoryId: Scalars['ID'];
  prefix: Scalars['String'];
};

export enum FileLocationType {
  Crawl = 'CRAWL',
  Upload = 'UPLOAD',
  Sidecar = 'SIDECAR',
  Temp = 'TEMP'
}

export type FileProcessing = {
  __typename?: 'FileProcessing';
  /** Name of the processor */
  name: Scalars['String'];
  /** ID of the job which triggered this processing */
  jobId: Scalars['String'];
  /** Time of the last run */
  processedAt: Scalars['Time'];
  /** Hash of the configuration options */
  configHash: Maybe<Scalars['String']>;
  /** Error, if occurred during processing */
  error: Maybe<FileProcessingError>;
  /** Number of milliseconds it took to process the file. Null for failed processings */
  took: Maybe<Scalars['Int']>;
};

export type FileProcessingError = {
  __typename?: 'FileProcessingError';
  code: Scalars['String'];
  message: Scalars['String'];
};

export type FileRefSelector = {
  /** Name of the repository where the file is stored */
  repository?: Maybe<Scalars['String']>;
  /** Filename, in case of S3 the first directory is a bucket name */
  filename?: Maybe<Scalars['String']>;
};

export type FileSetInput = {
  /** ID of the repository where to look for files */
  repository: Scalars['ID'];
  /**
   * Optional prefix. All file paths needs to start with it in order to be matched.
   * Use strings ending with / in order to match everything within a directory.
   */
  prefix?: Maybe<Scalars['String']>;
};

export type FileSortRequest = {
  property?: Maybe<PropertySortRequest>;
  processing?: Maybe<ProcessingOrder>;
};

export type FileStats = {
  __typename?: 'FileStats';
  property: PropertyStats;
};


export type FileStatsPropertyArgs = {
  name: Scalars['String'];
};

export type FileUpdate = {
  /** Which files to update? */
  selector?: Maybe<FileUpdateSelector>;
  setProperties?: Maybe<Array<PropertyInput>>;
  unsetProperties?: Maybe<Array<Scalars['String']>>;
};

export type FileUpdateSelector = {
  /** Select files by reference */
  fileRef?: Maybe<FileRefSelector>;
};

export type FileUpload = {
  file: Scalars['Upload'];
  properties?: Maybe<Array<PropertyInput>>;
};

export type FilenameRuleTagAssignment = TagAssignmentReason & {
  __typename?: 'FilenameRuleTagAssignment';
  kind: TagAssignmentReasonKind;
  createdAt: Scalars['Time'];
  repository: Scalars['String'];
  filename: Scalars['String'];
};

export type FinishedJobEvent = JobEvent & {
  __typename?: 'FinishedJobEvent';
  /** Job info */
  job: Job;
};

export type FractionProperty = Property & {
  __typename?: 'FractionProperty';
  name: Scalars['String'];
  numerator: Scalars['Int'];
  denominator: Scalars['Int'];
};

export type FractionPropertyInput = {
  numerator: Scalars['Int'];
  denominator: Scalars['Int'];
};

export type GpsProperty = Property & {
  __typename?: 'GPSProperty';
  name: Scalars['String'];
  latitude: Scalars['Float'];
  longitude: Scalars['Float'];
};

export type GpsPropertyInput = {
  latitude: Scalars['Float'];
  longitude: Scalars['Float'];
};

export type IdFilter = {
  in?: Maybe<Array<Scalars['AssetID']>>;
  notIn?: Maybe<Array<Scalars['AssetID']>>;
};

export type IgnoreActionConfig = ActionConfig & {
  __typename?: 'IgnoreActionConfig';
  action: ActionType;
  ignore: Scalars['Boolean'];
};

export type Index = {
  __typename?: 'Index';
  id: Scalars['ID'];
  name: Scalars['String'];
  assetType: AssetType;
  rules: Array<IndexRule>;
  fileLocations: Array<FileLocation>;
  tagKinds: Array<TagKind>;
  allPermissions: Array<IndexPermissionsAssignment>;
  permissions: IndexPermissions;
  stats: IndexStats;
};

export type IndexAction = {
  __typename?: 'IndexAction';
  id: Scalars['ID'];
  description: Maybe<Scalars['String']>;
  enabled: Scalars['Boolean'];
  config: Maybe<ActionConfig>;
};

export type IndexActionInput = {
  description?: Maybe<Scalars['String']>;
  enabled?: Maybe<Scalars['Boolean']>;
  config?: Maybe<ActionConfigInput>;
};

export type IndexInfo = {
  __typename?: 'IndexInfo';
  /** Name of the index in which the document is stored */
  indexName: Scalars['String'];
  /** ID of the stored document, unique within an index */
  documentId: Scalars['String'];
};

export type IndexInput = {
  name?: Maybe<Scalars['String']>;
  assetType?: Maybe<AssetType>;
  rules?: Maybe<Array<IndexRuleInput>>;
  fileLocations?: Maybe<Array<FileLocationInput>>;
  tagKinds?: Maybe<Array<TagKindInput>>;
  permissions?: Maybe<Array<IndexPermissionsAssignmentInput>>;
};

export type IndexMutationResult = {
  __typename?: 'IndexMutationResult';
  ok: Scalars['Boolean'];
  index: Maybe<Index>;
};

export type IndexPermissions = {
  __typename?: 'IndexPermissions';
  indexOperations: Array<Operation>;
  assetOperations: Array<Operation>;
  assetCollectionOperations: Array<Operation>;
};

export type IndexPermissionsAssignment = {
  __typename?: 'IndexPermissionsAssignment';
  subject: PermissionSubject;
  permissions: IndexPermissions;
};

export type IndexPermissionsAssignmentInput = {
  subjectId: Scalars['ID'];
  permissions: IndexPermissionsInput;
};

export type IndexPermissionsInput = {
  indexOperations?: Maybe<Array<Operation>>;
  assetOperations?: Maybe<Array<Operation>>;
  assetCollectionOperations?: Maybe<Array<Operation>>;
};

export type IndexRule = {
  __typename?: 'IndexRule';
  id: Scalars['ID'];
  description: Maybe<Scalars['String']>;
  match: Maybe<Scalars['String']>;
  inverse: Scalars['Boolean'];
  enabled: Scalars['Boolean'];
  actions: Array<IndexAction>;
};

export type IndexRuleInput = {
  description?: Maybe<Scalars['String']>;
  match?: Maybe<Scalars['String']>;
  inverse?: Maybe<Scalars['Boolean']>;
  enabled?: Maybe<Scalars['Boolean']>;
  actions?: Maybe<Array<IndexActionInput>>;
};

export type IndexStats = {
  __typename?: 'IndexStats';
  /** Number of assets within the index */
  count: Scalars['Int'];
  files: AssetFileStats;
};

export type Job = {
  __typename?: 'Job';
  /** Job ID */
  id: Scalars['String'];
  /** Name of the job */
  name: Scalars['String'];
  /** Time when the job was put into queue */
  queuedAt: Scalars['Time'];
  /** Time when job finished (null if it has not finished yet) */
  finishedAt: Maybe<Scalars['Time']>;
  /** Has job finished? */
  finished: Scalars['Boolean'];
  /** Task summary / progress */
  taskSummary: Array<TaskSummary>;
};

/**
 * Base job event
 * Note: we are modeling JobEvents through interfaces so that client can choose what kind of data
 * it needs for individual event types. That way we can limit the amount of data we send.
 */
export type JobEvent = {
  /** Job info */
  job: Job;
};


export type LocalRepository = Repository & {
  __typename?: 'LocalRepository';
  id: Scalars['ID'];
  type: RepositoryType;
  name: Scalars['String'];
  path: Scalars['String'];
};

export type LocalRepositoryInput = {
  path?: Maybe<Scalars['String']>;
};

export type MaybeAsyncMutationResult = {
  __typename?: 'MaybeAsyncMutationResult';
  /** True, if action has been successfully scheduled */
  ok: Scalars['Boolean'];
  /**
   * Job ID for the scheduled action
   * Use Subscription.jobEvents to listen to the job state changes.
   * It can be null if request did not result into additionally scheduled action.
   */
  jobId: Maybe<Scalars['String']>;
};

export type Mutation = {
  __typename?: 'Mutation';
  actionCreate: ActionMutationResult;
  actionUpdate: ActionMutationResult;
  actionDelete: MutationResult;
  /** Update single asset */
  updateAsset: AssetMutationResult;
  /** Updates asset properties in bulk */
  updateAssetByQuery: MutationByQueryResult;
  /** Delete single asset */
  deleteAsset: MutationResult;
  /** Delete assets in bulk */
  deleteAssetByQuery: MutationResult;
  /** Triggers processing on all assets files */
  processAsset: MaybeAsyncMutationResult;
  /** Creates an archive to download resulting assets at bulk */
  archiveAssets: ArchiveAssetsResult;
  refreshAuth: AuthRefresh;
  oauthLogin: OAuthLogin;
  oauthExchange: OAuthExchange;
  indexCreate: IndexMutationResult;
  indexUpdate: IndexMutationResult;
  indexDelete: MutationResult;
  /** Crawl file repositories to index new files */
  crawl: AsyncMutationResult;
  repositoryCreate: RepositoryMutationResult;
  repositoryUpdate: RepositoryMutationResult;
  repositoryDelete: MutationResult;
  ruleCreate: RuleMutationResult;
  ruleUpdate: RuleMutationResult;
  ruleDelete: MutationResult;
  ruleMoveBefore: MutationResult;
  /**
   * Upload new files to store. After files are stored, server starts crawling newly uploaded files.
   * If this mutation is present, server expects multi-part request as described here: https://github.com/jaydenseric/graphql-multipart-request-spec.
   */
  upload: UploadResult;
  /** Appends more files to existing upload session */
  uploadMore: MutationResult;
  /**
   * Reads archive (tgz) from given URL, extracts it and stores extracted files in the file repository as if they were uploaded themselves.
   * This is useful for initial provisioning with test data.
   */
  uploadArchiveFromUrl: AsyncMutationResult;
};


export type MutationActionCreateArgs = {
  ruleId: Scalars['ID'];
  action?: Maybe<IndexActionInput>;
};


export type MutationActionUpdateArgs = {
  id: Scalars['ID'];
  action: IndexActionInput;
};


export type MutationActionDeleteArgs = {
  id: Scalars['ID'];
};


export type MutationUpdateAssetArgs = {
  id: Scalars['ID'];
  update: AssetUpdate;
};


export type MutationUpdateAssetByQueryArgs = {
  index: Scalars['ID'];
  query?: Maybe<AssetFilter>;
  update: AssetUpdate;
};


export type MutationDeleteAssetArgs = {
  id: Scalars['AssetID'];
  deleteFiles?: Maybe<Scalars['Boolean']>;
};


export type MutationDeleteAssetByQueryArgs = {
  index: Scalars['ID'];
  query?: Maybe<AssetFilter>;
  deleteFiles?: Maybe<Scalars['Boolean']>;
};


export type MutationProcessAssetArgs = {
  id: Scalars['AssetID'];
  reprocess?: Maybe<Array<Scalars['String']>>;
};


export type MutationArchiveAssetsArgs = {
  index: Scalars['ID'];
  query?: Maybe<AssetFilter>;
  files?: Maybe<ArchiveFiles>;
};


export type MutationRefreshAuthArgs = {
  refreshToken: Scalars['String'];
};


export type MutationOauthLoginArgs = {
  state?: Maybe<OAuthStateInput>;
  callbackUrl: Scalars['String'];
};


export type MutationOauthExchangeArgs = {
  code: Scalars['String'];
  callbackUrl: Scalars['String'];
  serializedState: Scalars['String'];
};


export type MutationIndexCreateArgs = {
  index?: Maybe<IndexInput>;
};


export type MutationIndexUpdateArgs = {
  id: Scalars['ID'];
  index: IndexInput;
};


export type MutationIndexDeleteArgs = {
  id: Scalars['ID'];
};


export type MutationCrawlArgs = {
  reqs: Array<CrawlRequest>;
};


export type MutationRepositoryCreateArgs = {
  repository?: Maybe<RepositoryInput>;
};


export type MutationRepositoryUpdateArgs = {
  id: Scalars['ID'];
  repository: RepositoryInput;
};


export type MutationRepositoryDeleteArgs = {
  id: Scalars['ID'];
};


export type MutationRuleCreateArgs = {
  indexId: Scalars['ID'];
  rule?: Maybe<IndexRuleInput>;
};


export type MutationRuleUpdateArgs = {
  id: Scalars['ID'];
  rule: IndexRuleInput;
};


export type MutationRuleDeleteArgs = {
  id: Scalars['ID'];
};


export type MutationRuleMoveBeforeArgs = {
  id: Scalars['ID'];
  before?: Maybe<Scalars['ID']>;
};


export type MutationUploadArgs = {
  files: Array<FileUpload>;
  index: Scalars['ID'];
  allFilesAreSingleAsset?: Maybe<Scalars['Boolean']>;
  uploadMore?: Maybe<Scalars['Boolean']>;
};


export type MutationUploadMoreArgs = {
  files: Array<FileUpload>;
  session: Scalars['String'];
  finished: Scalars['Boolean'];
};


export type MutationUploadArchiveFromUrlArgs = {
  url: Scalars['String'];
  index: Scalars['ID'];
};

export type MutationByQueryResult = {
  __typename?: 'MutationByQueryResult';
  ok: Scalars['Boolean'];
  numMatched: Scalars['Int'];
  numChanged: Scalars['Int'];
  numFailed: Scalars['Int'];
};

export type MutationResult = {
  __typename?: 'MutationResult';
  /** True if action has been successfully performed */
  ok: Scalars['Boolean'];
};

export type NewJobEvent = JobEvent & {
  __typename?: 'NewJobEvent';
  /** Job info */
  job: Job;
};

export type NumberFilter = {
  eq?: Maybe<Scalars['Float']>;
  gte?: Maybe<Scalars['Float']>;
  gt?: Maybe<Scalars['Float']>;
  lte?: Maybe<Scalars['Float']>;
  lt?: Maybe<Scalars['Float']>;
};

export type NumberProperty = Property & {
  __typename?: 'NumberProperty';
  name: Scalars['String'];
  numericValue: Scalars['Float'];
};

export type NumericStats = {
  __typename?: 'NumericStats';
  sum: Scalars['Int'];
};

export type OAuthExchange = {
  __typename?: 'OAuthExchange';
  token: Scalars['String'];
  refreshToken: Maybe<Scalars['String']>;
  state: OAuthState;
};

export type OAuthLogin = {
  __typename?: 'OAuthLogin';
  url: Scalars['String'];
};

export type OAuthState = {
  __typename?: 'OAuthState';
  path: Scalars['String'];
};

export type OAuthStateInput = {
  path: Scalars['String'];
};

/** Operation - used for expressing user permissions */
export enum Operation {
  List = 'LIST',
  Read = 'READ',
  Update = 'UPDATE',
  Delete = 'DELETE',
  Crawl = 'CRAWL',
  Process = 'PROCESS',
  Upload = 'UPLOAD'
}

export type PermissionSubject = {
  id: Scalars['ID'];
};

export type ProcessActionConfig = ActionConfig & {
  __typename?: 'ProcessActionConfig';
  action: ActionType;
  request: Scalars['String'];
  queue: Maybe<Scalars['String']>;
  params: Maybe<Scalars['JsonMap']>;
};

export type ProcessActionConfigInput = {
  request: Scalars['String'];
  queue?: Maybe<Scalars['String']>;
  params?: Maybe<Scalars['JsonMap']>;
};

export type ProcessingFilter = {
  name?: Maybe<Array<TextFilter>>;
  jobId?: Maybe<Scalars['String']>;
  error?: Maybe<ErrorFilter>;
};

export enum ProcessingOrder {
  LastFailedProcessedAt = 'LAST_FAILED_PROCESSED_AT'
}

export type ProgressJobEvent = JobEvent & {
  __typename?: 'ProgressJobEvent';
  /** Job info */
  job: Job;
};

export type Property = {
  name: Scalars['String'];
};

export type PropertyFacets = {
  __typename?: 'PropertyFacets';
  textValues: Array<TextPropertyBucket>;
  timeRanges: Array<TimeRangeBucket>;
};


export type PropertyFacetsTimeRangesArgs = {
  ranges?: Maybe<Array<TimeRangeInput>>;
  timezone?: Maybe<Scalars['TimeZone']>;
};

export type PropertyFilter = {
  name: Scalars['String'];
  textValue?: Maybe<Array<TextFilter>>;
  numericValue?: Maybe<Array<NumberFilter>>;
  timeValue?: Maybe<Array<TimeFilter>>;
};

export type PropertyHolder = {
  /** Returns commonly used values of given property */
  property: PropertyFacets;
  /** Returns list of properties and their types */
  propertyIntrospection: Array<PropertyIntrospection>;
};


export type PropertyHolderPropertyArgs = {
  name: Scalars['String'];
};

export type PropertyInput = {
  name: Scalars['String'];
  textValue?: Maybe<Scalars['String']>;
  numericValue?: Maybe<Scalars['Float']>;
  timeValue?: Maybe<Scalars['Time']>;
  fractionValue?: Maybe<FractionPropertyInput>;
  gpsValue?: Maybe<GpsPropertyInput>;
};

export type PropertyIntrospection = {
  __typename?: 'PropertyIntrospection';
  /** Property name */
  name: Scalars['String'];
  /** Property type */
  type: PropertyType;
  /** Number how many documents have this property in the current search scope */
  count: Scalars['Int'];
};

export type PropertySortRequest = {
  name: Scalars['String'];
  type: PropertyType;
};

export type PropertyStats = {
  __typename?: 'PropertyStats';
  timeValue: Maybe<TimeValueStats>;
};

export enum PropertyType {
  Text = 'TEXT',
  Number = 'NUMBER',
  Time = 'TIME',
  Unknown = 'UNKNOWN'
}

export type Query = {
  __typename?: 'Query';
  currentUser: CurrentUser;
  /** Retrieve single asset by ID. Will return error if it does not exist. */
  asset: Asset;
  /** Asset listing */
  assets: AssetListing;
  index: Index;
  indices: Array<Index>;
  jobs: Array<Job>;
  repository: Repository;
  repositories: Array<Repository>;
  serverInfo: ServerInfo;
  /** Listing of all used tags */
  tags: Array<TagListingItem>;
  user: User;
  users: Array<User>;
  userRoles: Array<UserRole>;
};


export type QueryAssetArgs = {
  id: Scalars['ID'];
};


export type QueryAssetsArgs = {
  index?: Maybe<Array<Scalars['ID']>>;
  skip?: Maybe<Scalars['Int']>;
  size?: Maybe<Scalars['Int']>;
  query?: Maybe<AssetFilter>;
  sortBy?: Maybe<Array<AssetSortRequest>>;
};


export type QueryIndexArgs = {
  id: Scalars['ID'];
};


export type QueryRepositoryArgs = {
  id: Scalars['ID'];
};


export type QueryTagsArgs = {
  kind?: Maybe<Array<Scalars['String']>>;
  index?: Maybe<Array<Scalars['ID']>>;
  nameQuery?: Maybe<Scalars['String']>;
  sort?: Maybe<TagSortRequest>;
};


export type QueryUserArgs = {
  id: Scalars['ID'];
};

export type Repository = {
  id: Scalars['ID'];
  type: RepositoryType;
  name: Scalars['String'];
};

export type RepositoryInput = {
  name?: Maybe<Scalars['String']>;
  s3?: Maybe<S3RepositoryInput>;
  local?: Maybe<LocalRepositoryInput>;
};

export type RepositoryMutationResult = {
  __typename?: 'RepositoryMutationResult';
  ok: Scalars['Boolean'];
  repository: Maybe<Repository>;
};

export enum RepositoryType {
  Unknown = 'UNKNOWN',
  S3 = 'S3',
  Local = 'LOCAL'
}

export type ResolvedTimeExpression = {
  __typename?: 'ResolvedTimeExpression';
  expression: Scalars['String'];
  value: Scalars['Time'];
};

export type ResourceOperations = {
  __typename?: 'ResourceOperations';
  resource: ResourceType;
  allowedOperations: Array<Operation>;
};

export enum ResourceType {
  Index = 'INDEX',
  Asset = 'ASSET',
  Repository = 'REPOSITORY',
  Job = 'JOB',
  User = 'USER'
}

export type RuleMutationResult = {
  __typename?: 'RuleMutationResult';
  ok: Scalars['Boolean'];
  rule: Maybe<IndexRule>;
};

export type S3Repository = Repository & {
  __typename?: 'S3Repository';
  id: Scalars['ID'];
  type: RepositoryType;
  name: Scalars['String'];
  endpoint: Scalars['String'];
  useSSL: Scalars['Boolean'];
  caCert: Scalars['String'];
  accessKey: Scalars['String'];
};

export type S3RepositoryInput = {
  endpoint?: Maybe<Scalars['String']>;
  useSSL?: Maybe<Scalars['Boolean']>;
  caCert?: Maybe<Scalars['String']>;
  accessKey?: Maybe<Scalars['String']>;
  secretKey?: Maybe<Scalars['String']>;
};

export type ServerInfo = {
  __typename?: 'ServerInfo';
  version: VersionInfo;
  auth: AuthType;
};

export type SetKeyActionConfig = ActionConfig & {
  __typename?: 'SetKeyActionConfig';
  action: ActionType;
  key: Scalars['String'];
};

export type SetParamsActionConfig = ActionConfig & {
  __typename?: 'SetParamsActionConfig';
  action: ActionType;
  params: Maybe<Scalars['JsonMap']>;
};

export type Sidecar = {
  file: File;
};

export type SidecarStats = {
  __typename?: 'SidecarStats';
  /** Number of sidecar files */
  count: Scalars['Int'];
  fileSize: NumericStats;
};

/** How should the results be sorted? */
export enum SortMethod {
  /** Sorting in ascending order */
  Asc = 'ASC',
  /** Sorting in descending order */
  Desc = 'DESC'
}

export type Subscription = {
  __typename?: 'Subscription';
  /** Subscribe to status changes of jobs */
  jobEvents: JobEvent;
};

export type Tag = {
  __typename?: 'Tag';
  kind: Scalars['String'];
  name: Scalars['String'];
};

export type TagAssignment = {
  __typename?: 'TagAssignment';
  /** Tag kind */
  kind: Scalars['String'];
  /** Name of the tag, unique within the same tag kind */
  name: Scalars['String'];
  /** Reasons for this tag assignment */
  reasons: Array<TagAssignmentReason>;
};

export type TagAssignmentReason = {
  kind: TagAssignmentReasonKind;
  createdAt: Scalars['Time'];
};

export enum TagAssignmentReasonKind {
  User = 'USER',
  FilenameRule = 'FILENAME_RULE'
}

export type TagBucket = {
  __typename?: 'TagBucket';
  kind: Scalars['String'];
  name: Scalars['String'];
  numAssets: Scalars['Int'];
};

export type TagFilter = {
  kind: Scalars['String'];
  name: Array<Scalars['String']>;
};

export type TagInput = {
  kind: Scalars['String'];
  name: Scalars['String'];
};

export type TagKind = {
  __typename?: 'TagKind';
  name: Scalars['String'];
  displayName: Scalars['String'];
  displayNamePlural: Scalars['String'];
  userAssignable: Scalars['Boolean'];
};

export type TagKindBucket = {
  __typename?: 'TagKindBucket';
  kind: Scalars['String'];
  numTags: Scalars['Int'];
};

export type TagKindInput = {
  name: Scalars['String'];
  displayName?: Maybe<Scalars['String']>;
  displayNamePlural?: Maybe<Scalars['String']>;
  userAssignable?: Maybe<Scalars['Boolean']>;
};

export type TagListingItem = {
  __typename?: 'TagListingItem';
  /** Tag kind */
  kind: Scalars['String'];
  /** Name of the tag, unique within the same tag kind */
  name: Scalars['String'];
  /** Asset which can be used as cover thumbnail */
  coverAsset: Maybe<Asset>;
};

export enum TagOrder {
  LastFileDate = 'LAST_FILE_DATE',
  LastAssignedAt = 'LAST_ASSIGNED_AT',
  LastUserAssignedAt = 'LAST_USER_ASSIGNED_AT'
}

export type TagSortRequest = {
  by?: Maybe<TagOrder>;
  method?: Maybe<SortMethod>;
};

/** Task summary / progress */
export type TaskSummary = {
  __typename?: 'TaskSummary';
  /** Name of the task */
  name: Scalars['String'];
  /** Has all the task finished? */
  finished: Scalars['Boolean'];
  /** Total number of tasks of this kind in the job */
  total: Scalars['Int'];
  /** Number of still unresolved tasks of this kind in the job */
  pending: Scalars['Int'];
  /** Number of tasks which finished with error */
  failed: Scalars['Int'];
  /**
   * How much milliseconds did it take to process the tasks (only successful processings are counted)
   * Note: This is real processing time as reported by the individual processors. If you are running multiple processors in parallel
   * this can sum up into larger number than real time it took to finish the job.
   */
  took: Scalars['Int'];
};

export type TextFilter = {
  /** Exact match with given string */
  eq?: Maybe<Scalars['String']>;
  /** Loose match with given string (note: ensure you use the default sorting to leverage the scoring) */
  matches?: Maybe<Scalars['String']>;
  /** Must start with given string */
  startsWith?: Maybe<Scalars['String']>;
};

export type TextProperty = Property & {
  __typename?: 'TextProperty';
  name: Scalars['String'];
  textValue: Scalars['String'];
};

export type TextPropertyBucket = {
  __typename?: 'TextPropertyBucket';
  value: Scalars['String'];
  numAssets: Scalars['Int'];
};

export type Thumbnail = Sidecar & {
  __typename?: 'Thumbnail';
  /** Thumbnail file */
  file: File;
  /** Thumbnail file format */
  format: ThumbnailFormat;
  /** Width of the thumbnail */
  width: Scalars['Int'];
  /** Height of the thumbnail */
  height: Scalars['Int'];
};

export enum ThumbnailFormat {
  Jpeg = 'JPEG',
  Webp = 'WEBP'
}



export type TimeFilter = {
  eq?: Maybe<Scalars['TimeExpression']>;
  gte?: Maybe<Scalars['TimeExpression']>;
  gt?: Maybe<Scalars['TimeExpression']>;
  lte?: Maybe<Scalars['TimeExpression']>;
  lt?: Maybe<Scalars['TimeExpression']>;
};

export type TimeProperty = Property & {
  __typename?: 'TimeProperty';
  name: Scalars['String'];
  timeValue: Scalars['Time'];
};

export type TimeRange = {
  __typename?: 'TimeRange';
  name: Scalars['String'];
  from: Maybe<ResolvedTimeExpression>;
  to: Maybe<ResolvedTimeExpression>;
};

export type TimeRangeBucket = {
  __typename?: 'TimeRangeBucket';
  range: Maybe<TimeRange>;
  numAssets: Scalars['Int'];
};

export type TimeRangeInput = {
  name: Scalars['String'];
  /** Start of the range as a date expression. Range will include this value (>= operator) */
  from?: Maybe<Scalars['TimeExpression']>;
  /** End of the range as a date expression. Range will exclude this value (< operator) */
  to?: Maybe<Scalars['TimeExpression']>;
};

export type TimeValueStats = {
  __typename?: 'TimeValueStats';
  min: Maybe<Scalars['Time']>;
  max: Maybe<Scalars['Time']>;
};


export type UnknownActionConfig = ActionConfig & {
  __typename?: 'UnknownActionConfig';
  action: ActionType;
};

export type UnknownRepository = Repository & {
  __typename?: 'UnknownRepository';
  id: Scalars['ID'];
  type: RepositoryType;
  name: Scalars['String'];
};


export type UploadResult = {
  __typename?: 'UploadResult';
  ok: Scalars['Boolean'];
  /**
   * Job ID for the processing job of freshly uploaded files.
   * It can be null if no files required processing and there was no request for continuing upload session.
   */
  jobId: Maybe<Scalars['String']>;
  /** Upload session key if more file uploads has been requested */
  uploadSession: Maybe<Scalars['String']>;
};

export type User = PermissionSubject & {
  __typename?: 'User';
  id: Scalars['ID'];
  username: Scalars['String'];
  displayName: Scalars['String'];
  roles: Array<UserRole>;
};

export type UserRole = PermissionSubject & {
  __typename?: 'UserRole';
  id: Scalars['ID'];
  name: Scalars['String'];
  displayName: Scalars['String'];
};

export type UserTagAssignment = TagAssignmentReason & {
  __typename?: 'UserTagAssignment';
  kind: TagAssignmentReasonKind;
  createdAt: Scalars['Time'];
  uid: Scalars['String'];
};

export type VersionInfo = {
  __typename?: 'VersionInfo';
  versionString: Scalars['String'];
  major: Scalars['Int'];
  minor: Scalars['Int'];
  patch: Scalars['Int'];
};

export type Video = Sidecar & {
  __typename?: 'Video';
  /** Video file */
  file: File;
  /** Video file format */
  format: VideoFormat;
  /** Width of the video */
  width: Scalars['Int'];
  /** Height of the video */
  height: Scalars['Int'];
  /** Audio codec */
  audioCodec: Scalars['String'];
  /** Video codec */
  videoCodec: Scalars['String'];
};

export enum VideoFormat {
  Mp4 = 'MP4'
}

export type CrawlMutationVariables = Exact<{
  reqs: Array<CrawlRequest> | CrawlRequest;
}>;


export type CrawlMutation = { __typename?: 'Mutation', crawl: { __typename?: 'AsyncMutationResult', ok: boolean, jobId: string } };

export type CreateArchiveMutationVariables = Exact<{
  index: Scalars['ID'];
  query?: Maybe<AssetFilter>;
  files?: Maybe<ArchiveFiles>;
}>;


export type CreateArchiveMutation = { __typename?: 'Mutation', archiveAssets: { __typename?: 'ArchiveAssetsResult', ok: boolean, archive: Maybe<{ __typename?: 'ArchiveJob', jobId: string, url: string }> } };

export type CreateIndexMutationVariables = Exact<{
  index?: Maybe<IndexInput>;
}>;


export type CreateIndexMutation = { __typename?: 'Mutation', indexCreate: { __typename?: 'IndexMutationResult', ok: boolean, index: Maybe<{ __typename?: 'Index', id: string }> } };

export type CurrentUserFragment = { __typename?: 'Query', currentUser: { __typename?: 'CurrentUser', authenticated: boolean, willLogoutAt: Maybe<string>, user: Maybe<{ __typename?: 'User', id: string, username: string, displayName: string, roles: Array<{ __typename?: 'UserRole', id: string, name: string, displayName: string }> }>, collectionPermissions: Array<{ __typename?: 'ResourceOperations', resource: ResourceType, allowedOperations: Array<Operation> }> } };

export type CurrentUserQueryVariables = Exact<{ [key: string]: never; }>;


export type CurrentUserQuery = { __typename?: 'Query', currentUser: { __typename?: 'CurrentUser', authenticated: boolean, willLogoutAt: Maybe<string>, user: Maybe<{ __typename?: 'User', id: string, username: string, displayName: string, roles: Array<{ __typename?: 'UserRole', id: string, name: string, displayName: string }> }>, collectionPermissions: Array<{ __typename?: 'ResourceOperations', resource: ResourceType, allowedOperations: Array<Operation> }> } };

export type DeleteAssetMutationVariables = Exact<{
  id: Scalars['AssetID'];
  deleteFiles?: Maybe<Scalars['Boolean']>;
}>;


export type DeleteAssetMutation = { __typename?: 'Mutation', deleteAsset: { __typename?: 'MutationResult', ok: boolean } };

export type DeleteAssetsMutationVariables = Exact<{
  index: Scalars['ID'];
  query?: Maybe<AssetFilter>;
}>;


export type DeleteAssetsMutation = { __typename?: 'Mutation', deleteAssetByQuery: { __typename?: 'MutationResult', ok: boolean } };

export type DeleteIndexMutationVariables = Exact<{
  id: Scalars['ID'];
}>;


export type DeleteIndexMutation = { __typename?: 'Mutation', indexDelete: { __typename?: 'MutationResult', ok: boolean } };

export type GetIndexQueryVariables = Exact<{
  id: Scalars['ID'];
}>;


export type GetIndexQuery = { __typename?: 'Query', index: { __typename?: 'Index', id: string, name: string, assetType: AssetType, tagKinds: Array<{ __typename?: 'TagKind', name: string, displayName: string, displayNamePlural: string, userAssignable: boolean }>, fileLocations: Array<{ __typename?: 'FileLocation', type: FileLocationType, prefix: string, repository: { __typename?: 'LocalRepository', id: string, type: RepositoryType, name: string } | { __typename?: 'S3Repository', id: string, type: RepositoryType, name: string } | { __typename?: 'UnknownRepository', id: string, type: RepositoryType, name: string } }>, permissions: { __typename?: 'IndexPermissions', indexOperations: Array<Operation>, assetOperations: Array<Operation>, assetCollectionOperations: Array<Operation> }, stats: { __typename?: 'IndexStats', count: number, files: { __typename?: 'AssetFileStats', count: number, fileSize: { __typename?: 'NumericStats', sum: number }, sidecars: { __typename?: 'SidecarStats', fileSize: { __typename?: 'NumericStats', sum: number } } } } } };

export type RuleFragment = { __typename?: 'IndexRule', id: string, description: Maybe<string>, match: Maybe<string>, inverse: boolean, enabled: boolean, actions: Array<{ __typename?: 'IndexAction', id: string, description: Maybe<string>, enabled: boolean, config: Maybe<{ __typename?: 'AddTagActionConfig', action: ActionType, tag: { __typename?: 'Tag', kind: string, name: string } } | { __typename?: 'IgnoreActionConfig', ignore: boolean, action: ActionType } | { __typename?: 'ProcessActionConfig', request: string, queue: Maybe<string>, params: Maybe<any>, action: ActionType } | { __typename?: 'SetKeyActionConfig', key: string, action: ActionType } | { __typename?: 'SetParamsActionConfig', params: Maybe<any>, action: ActionType } | { __typename?: 'UnknownActionConfig', action: ActionType }> }> };

export type GetIndexDetailQueryVariables = Exact<{
  id: Scalars['ID'];
}>;


export type GetIndexDetailQuery = { __typename?: 'Query', index: { __typename?: 'Index', id: string, name: string, assetType: AssetType, rules: Array<{ __typename?: 'IndexRule', id: string, description: Maybe<string>, match: Maybe<string>, inverse: boolean, enabled: boolean, actions: Array<{ __typename?: 'IndexAction', id: string, description: Maybe<string>, enabled: boolean, config: Maybe<{ __typename?: 'AddTagActionConfig', action: ActionType, tag: { __typename?: 'Tag', kind: string, name: string } } | { __typename?: 'IgnoreActionConfig', ignore: boolean, action: ActionType } | { __typename?: 'ProcessActionConfig', request: string, queue: Maybe<string>, params: Maybe<any>, action: ActionType } | { __typename?: 'SetKeyActionConfig', key: string, action: ActionType } | { __typename?: 'SetParamsActionConfig', params: Maybe<any>, action: ActionType } | { __typename?: 'UnknownActionConfig', action: ActionType }> }> }>, allPermissions: Array<{ __typename?: 'IndexPermissionsAssignment', subject: { __typename?: 'User', id: string } | { __typename?: 'UserRole', id: string }, permissions: { __typename?: 'IndexPermissions', indexOperations: Array<Operation>, assetOperations: Array<Operation>, assetCollectionOperations: Array<Operation> } }>, tagKinds: Array<{ __typename?: 'TagKind', name: string, displayName: string, displayNamePlural: string, userAssignable: boolean }>, fileLocations: Array<{ __typename?: 'FileLocation', type: FileLocationType, prefix: string, repository: { __typename?: 'LocalRepository', id: string, type: RepositoryType, name: string } | { __typename?: 'S3Repository', id: string, type: RepositoryType, name: string } | { __typename?: 'UnknownRepository', id: string, type: RepositoryType, name: string } }>, permissions: { __typename?: 'IndexPermissions', indexOperations: Array<Operation>, assetOperations: Array<Operation>, assetCollectionOperations: Array<Operation> }, stats: { __typename?: 'IndexStats', count: number, files: { __typename?: 'AssetFileStats', count: number, fileSize: { __typename?: 'NumericStats', sum: number }, sidecars: { __typename?: 'SidecarStats', fileSize: { __typename?: 'NumericStats', sum: number } } } } } };

export type GetUsersAndRolesQueryVariables = Exact<{ [key: string]: never; }>;


export type GetUsersAndRolesQuery = { __typename?: 'Query', userRoles: Array<{ __typename?: 'UserRole', id: string, name: string, displayName: string }>, users: Array<{ __typename?: 'User', id: string, username: string, displayName: string }> };

export type TaskSummaryFragment = { __typename?: 'Job', taskSummary: Array<{ __typename?: 'TaskSummary', name: string, total: number, pending: number, failed: number, took: number }> };

export type JobEventsSubscriptionVariables = Exact<{ [key: string]: never; }>;


export type JobEventsSubscription = { __typename?: 'Subscription', jobEvents: { __typename?: 'FinishedJobEvent', job: { __typename?: 'Job', id: string, finishedAt: Maybe<string>, taskSummary: Array<{ __typename?: 'TaskSummary', name: string, total: number, pending: number, failed: number, took: number }> } } | { __typename?: 'NewJobEvent', job: { __typename?: 'Job', id: string, name: string, queuedAt: string, taskSummary: Array<{ __typename?: 'TaskSummary', name: string, total: number, pending: number, failed: number, took: number }> } } | { __typename?: 'ProgressJobEvent', job: { __typename?: 'Job', id: string, taskSummary: Array<{ __typename?: 'TaskSummary', name: string, total: number, pending: number, failed: number, took: number }> } } };

type PropertyPair_FractionProperty_Fragment = { __typename?: 'FractionProperty', numerator: number, denominator: number, name: string };

type PropertyPair_GpsProperty_Fragment = { __typename?: 'GPSProperty', name: string };

type PropertyPair_NumberProperty_Fragment = { __typename?: 'NumberProperty', numericValue: number, name: string };

type PropertyPair_TextProperty_Fragment = { __typename?: 'TextProperty', textValue: string, name: string };

type PropertyPair_TimeProperty_Fragment = { __typename?: 'TimeProperty', timeValue: string, name: string };

export type PropertyPairFragment = PropertyPair_FractionProperty_Fragment | PropertyPair_GpsProperty_Fragment | PropertyPair_NumberProperty_Fragment | PropertyPair_TextProperty_Fragment | PropertyPair_TimeProperty_Fragment;

export type AssetFileFragment = { __typename?: 'AssetFile', filename: string, url: string, fileInfo: { __typename?: 'FileInfo', size: Maybe<number> } };

export type VideoSidecarFragment = { __typename?: 'Video', width: number, height: number, videoCodec: string, audioCodec: string, videoFormat: VideoFormat, file: { __typename?: 'File', url: string } };

export type AssetFragment = { __typename?: 'Asset', id: string, key: string, allowedOperations: Array<Operation>, mainFile: Maybe<{ __typename?: 'AssetFile', filename: string, url: string, properties: Array<{ __typename?: 'FractionProperty', numerator: number, denominator: number, name: string } | { __typename?: 'GPSProperty', name: string } | { __typename?: 'NumberProperty', numericValue: number, name: string } | { __typename?: 'TextProperty', textValue: string, name: string } | { __typename?: 'TimeProperty', timeValue: string, name: string }>, sidecars: Array<{ __typename?: 'Thumbnail' } | { __typename?: 'Video', width: number, height: number, videoCodec: string, audioCodec: string, videoFormat: VideoFormat, file: { __typename?: 'File', url: string } }>, fileInfo: { __typename?: 'FileInfo', size: Maybe<number> } }>, files: Array<{ __typename?: 'AssetFile', filename: string, url: string, fileInfo: { __typename?: 'FileInfo', size: Maybe<number> } }>, previewThumbnail: Maybe<{ __typename?: 'Thumbnail', width: number, height: number, file: { __typename?: 'File', url: string } }>, tags: Array<{ __typename?: 'TagAssignment', kind: string, name: string }>, properties: Array<{ __typename?: 'FractionProperty', numerator: number, denominator: number, name: string } | { __typename?: 'GPSProperty', name: string } | { __typename?: 'NumberProperty', numericValue: number, name: string } | { __typename?: 'TextProperty', textValue: string, name: string } | { __typename?: 'TimeProperty', timeValue: string, name: string }>, galleryThumbnail: Maybe<{ __typename?: 'Thumbnail', width: number, height: number, file: { __typename?: 'File', url: string } }> };

export type AssetMutableFragment = { __typename?: 'Asset', tags: Array<{ __typename?: 'TagAssignment', kind: string, name: string }>, properties: Array<{ __typename?: 'FractionProperty', numerator: number, denominator: number, name: string } | { __typename?: 'GPSProperty', name: string } | { __typename?: 'NumberProperty', numericValue: number, name: string } | { __typename?: 'TextProperty', textValue: string, name: string } | { __typename?: 'TimeProperty', timeValue: string, name: string }>, mainFile: Maybe<{ __typename?: 'AssetFile', properties: Array<{ __typename?: 'FractionProperty', numerator: number, denominator: number, name: string } | { __typename?: 'GPSProperty', name: string } | { __typename?: 'NumberProperty', numericValue: number, name: string } | { __typename?: 'TextProperty', textValue: string, name: string } | { __typename?: 'TimeProperty', timeValue: string, name: string }> }> };

export type ThumbnailsFragment = { __typename?: 'Asset', previewThumbnail: Maybe<{ __typename?: 'Thumbnail', width: number, height: number, file: { __typename?: 'File', url: string } }>, galleryThumbnail: Maybe<{ __typename?: 'Thumbnail', width: number, height: number, file: { __typename?: 'File', url: string } }> };

export type GalleryThumbnailFragment = { __typename?: 'Asset', galleryThumbnail: Maybe<{ __typename?: 'Thumbnail', width: number, height: number, file: { __typename?: 'File', url: string } }> };

export type TextPropertyValuesFragment = { __typename?: 'TextPropertyBucket', value: string, numAssets: number };

export type TimePropertyValuesFragment = { __typename?: 'TimeRangeBucket', numAssets: number, range: Maybe<{ __typename?: 'TimeRange', name: string, from: Maybe<{ __typename?: 'ResolvedTimeExpression', value: string }>, to: Maybe<{ __typename?: 'ResolvedTimeExpression', value: string }> }> };

export type TagFacetValuesFragment = { __typename?: 'TagBucket', name: string, numAssets: number };

export type PropertyIntrospectionFragment = { __typename?: 'PropertyIntrospection', name: string, type: PropertyType, count: number };

export type ListAssetsQueryVariables = Exact<{
  query?: Maybe<AssetFilter>;
  sortBy?: Maybe<Array<AssetSortRequest> | AssetSortRequest>;
  galleryThumbnailHeight?: Maybe<Scalars['Int']>;
  previewThumbnailWidth?: Maybe<Scalars['Int']>;
  previewThumbnailHeight?: Maybe<Scalars['Int']>;
  acceptableThumbnailFormats?: Maybe<Array<ThumbnailFormat> | ThumbnailFormat>;
  size?: Maybe<Scalars['Int']>;
  index?: Maybe<Array<Scalars['ID']> | Scalars['ID']>;
}>;


export type ListAssetsQuery = { __typename?: 'Query', assets: { __typename?: 'AssetListing', totalCount: number, items: Array<{ __typename?: 'Asset', id: string, key: string, allowedOperations: Array<Operation>, mainFile: Maybe<{ __typename?: 'AssetFile', filename: string, url: string, properties: Array<{ __typename?: 'FractionProperty', numerator: number, denominator: number, name: string } | { __typename?: 'GPSProperty', name: string } | { __typename?: 'NumberProperty', numericValue: number, name: string } | { __typename?: 'TextProperty', textValue: string, name: string } | { __typename?: 'TimeProperty', timeValue: string, name: string }>, sidecars: Array<{ __typename?: 'Thumbnail' } | { __typename?: 'Video', width: number, height: number, videoCodec: string, audioCodec: string, videoFormat: VideoFormat, file: { __typename?: 'File', url: string } }>, fileInfo: { __typename?: 'FileInfo', size: Maybe<number> } }>, files: Array<{ __typename?: 'AssetFile', filename: string, url: string, fileInfo: { __typename?: 'FileInfo', size: Maybe<number> } }>, previewThumbnail: Maybe<{ __typename?: 'Thumbnail', width: number, height: number, file: { __typename?: 'File', url: string } }>, tags: Array<{ __typename?: 'TagAssignment', kind: string, name: string }>, properties: Array<{ __typename?: 'FractionProperty', numerator: number, denominator: number, name: string } | { __typename?: 'GPSProperty', name: string } | { __typename?: 'NumberProperty', numericValue: number, name: string } | { __typename?: 'TextProperty', textValue: string, name: string } | { __typename?: 'TimeProperty', timeValue: string, name: string }>, galleryThumbnail: Maybe<{ __typename?: 'Thumbnail', width: number, height: number, file: { __typename?: 'File', url: string } }> }>, facets: { __typename?: 'AssetListingFacets', tagKinds: Array<{ __typename?: 'TagKindBucket', kind: string, numTags: number }>, propertyIntrospection: Array<{ __typename?: 'PropertyIntrospection', name: string, type: PropertyType, count: number }>, files: { __typename?: 'FileFacets', propertyIntrospection: Array<{ __typename?: 'PropertyIntrospection', name: string, type: PropertyType, count: number }> } }, stats: { __typename?: 'AssetListingStats', files: { __typename?: 'FileStats', property_date: { __typename?: 'PropertyStats', timeValue: Maybe<{ __typename?: 'TimeValueStats', min: Maybe<string>, max: Maybe<string> }> } } } } };

export type ListMoreAssetsQueryVariables = Exact<{
  query?: Maybe<AssetFilter>;
  sortBy?: Maybe<Array<AssetSortRequest> | AssetSortRequest>;
  galleryThumbnailHeight?: Maybe<Scalars['Int']>;
  previewThumbnailWidth?: Maybe<Scalars['Int']>;
  previewThumbnailHeight?: Maybe<Scalars['Int']>;
  acceptableThumbnailFormats?: Maybe<Array<ThumbnailFormat> | ThumbnailFormat>;
  skip?: Maybe<Scalars['Int']>;
  size?: Maybe<Scalars['Int']>;
  index?: Maybe<Array<Scalars['ID']> | Scalars['ID']>;
}>;


export type ListMoreAssetsQuery = { __typename?: 'Query', assets: { __typename?: 'AssetListing', items: Array<{ __typename?: 'Asset', id: string, key: string, allowedOperations: Array<Operation>, mainFile: Maybe<{ __typename?: 'AssetFile', filename: string, url: string, properties: Array<{ __typename?: 'FractionProperty', numerator: number, denominator: number, name: string } | { __typename?: 'GPSProperty', name: string } | { __typename?: 'NumberProperty', numericValue: number, name: string } | { __typename?: 'TextProperty', textValue: string, name: string } | { __typename?: 'TimeProperty', timeValue: string, name: string }>, sidecars: Array<{ __typename?: 'Thumbnail' } | { __typename?: 'Video', width: number, height: number, videoCodec: string, audioCodec: string, videoFormat: VideoFormat, file: { __typename?: 'File', url: string } }>, fileInfo: { __typename?: 'FileInfo', size: Maybe<number> } }>, files: Array<{ __typename?: 'AssetFile', filename: string, url: string, fileInfo: { __typename?: 'FileInfo', size: Maybe<number> } }>, previewThumbnail: Maybe<{ __typename?: 'Thumbnail', width: number, height: number, file: { __typename?: 'File', url: string } }>, tags: Array<{ __typename?: 'TagAssignment', kind: string, name: string }>, properties: Array<{ __typename?: 'FractionProperty', numerator: number, denominator: number, name: string } | { __typename?: 'GPSProperty', name: string } | { __typename?: 'NumberProperty', numericValue: number, name: string } | { __typename?: 'TextProperty', textValue: string, name: string } | { __typename?: 'TimeProperty', timeValue: string, name: string }>, galleryThumbnail: Maybe<{ __typename?: 'Thumbnail', width: number, height: number, file: { __typename?: 'File', url: string } }> }> } };

export type ListFailedAssetsQueryVariables = Exact<{
  query?: Maybe<AssetFilter>;
  index?: Maybe<Array<Scalars['ID']> | Scalars['ID']>;
}>;


export type ListFailedAssetsQuery = { __typename?: 'Query', assets: { __typename?: 'AssetListing', items: Array<{ __typename?: 'Asset', id: string, files: Array<{ __typename?: 'AssetFile', filename: string, repository: string, url: string, processings: Array<{ __typename?: 'FileProcessing', name: string, jobId: string, processedAt: string, error: Maybe<{ __typename?: 'FileProcessingError', code: string, message: string }> }> }> }> } };

export type ListFailedProcessingsQueryVariables = Exact<{
  index?: Maybe<Array<Scalars['ID']> | Scalars['ID']>;
}>;


export type ListFailedProcessingsQuery = { __typename?: 'Query', assets: { __typename?: 'AssetListing', items: Array<{ __typename?: 'Asset', id: string, files: Array<{ __typename?: 'AssetFile', repository: string, filename: string, processings: Array<{ __typename?: 'FileProcessing', name: string, processedAt: string, error: Maybe<{ __typename?: 'FileProcessingError', message: string }> }> }> }> } };

export type IndexFragment = { __typename?: 'Index', id: string, name: string, assetType: AssetType, tagKinds: Array<{ __typename?: 'TagKind', name: string, displayName: string, displayNamePlural: string, userAssignable: boolean }>, fileLocations: Array<{ __typename?: 'FileLocation', type: FileLocationType, prefix: string, repository: { __typename?: 'LocalRepository', id: string, type: RepositoryType, name: string } | { __typename?: 'S3Repository', id: string, type: RepositoryType, name: string } | { __typename?: 'UnknownRepository', id: string, type: RepositoryType, name: string } }>, permissions: { __typename?: 'IndexPermissions', indexOperations: Array<Operation>, assetOperations: Array<Operation>, assetCollectionOperations: Array<Operation> }, stats: { __typename?: 'IndexStats', count: number, files: { __typename?: 'AssetFileStats', count: number, fileSize: { __typename?: 'NumericStats', sum: number }, sidecars: { __typename?: 'SidecarStats', fileSize: { __typename?: 'NumericStats', sum: number } } } } };

export type ListIndicesQueryVariables = Exact<{ [key: string]: never; }>;


export type ListIndicesQuery = { __typename?: 'Query', indices: Array<{ __typename?: 'Index', id: string, name: string, assetType: AssetType, tagKinds: Array<{ __typename?: 'TagKind', name: string, displayName: string, displayNamePlural: string, userAssignable: boolean }>, fileLocations: Array<{ __typename?: 'FileLocation', type: FileLocationType, prefix: string, repository: { __typename?: 'LocalRepository', id: string, type: RepositoryType, name: string } | { __typename?: 'S3Repository', id: string, type: RepositoryType, name: string } | { __typename?: 'UnknownRepository', id: string, type: RepositoryType, name: string } }>, permissions: { __typename?: 'IndexPermissions', indexOperations: Array<Operation>, assetOperations: Array<Operation>, assetCollectionOperations: Array<Operation> }, stats: { __typename?: 'IndexStats', count: number, files: { __typename?: 'AssetFileStats', count: number, fileSize: { __typename?: 'NumericStats', sum: number }, sidecars: { __typename?: 'SidecarStats', fileSize: { __typename?: 'NumericStats', sum: number } } } } }> };

export type ListJobsQueryVariables = Exact<{ [key: string]: never; }>;


export type ListJobsQuery = { __typename?: 'Query', jobs: Array<{ __typename?: 'Job', id: string, name: string, queuedAt: string, finishedAt: Maybe<string>, finished: boolean, taskSummary: Array<{ __typename?: 'TaskSummary', name: string, total: number, pending: number, failed: number, took: number }> }> };

export type ListRepositoriesQueryVariables = Exact<{ [key: string]: never; }>;


export type ListRepositoriesQuery = { __typename?: 'Query', repositories: Array<{ __typename?: 'LocalRepository', id: string, name: string } | { __typename?: 'S3Repository', id: string, name: string } | { __typename?: 'UnknownRepository', id: string, name: string }> };

export type ListTagsQueryVariables = Exact<{
  index?: Maybe<Array<Scalars['ID']> | Scalars['ID']>;
  kind?: Maybe<Array<Scalars['String']> | Scalars['String']>;
  galleryThumbnailHeight?: Maybe<Scalars['Int']>;
  acceptableThumbnailFormats?: Maybe<Array<ThumbnailFormat> | ThumbnailFormat>;
  nameQuery?: Maybe<Scalars['String']>;
  sort?: Maybe<TagSortRequest>;
}>;


export type ListTagsQuery = { __typename?: 'Query', tags: Array<{ __typename?: 'TagListingItem', kind: string, name: string, coverAsset: Maybe<{ __typename?: 'Asset', id: string, galleryThumbnail: Maybe<{ __typename?: 'Thumbnail', width: number, height: number, file: { __typename?: 'File', url: string } }> }> }> };

export type OAuthExchangeMutationVariables = Exact<{
  callbackUrl: Scalars['String'];
  code: Scalars['String'];
  serializedState: Scalars['String'];
}>;


export type OAuthExchangeMutation = { __typename?: 'Mutation', oauthExchange: { __typename?: 'OAuthExchange', token: string, refreshToken: Maybe<string>, state: { __typename?: 'OAuthState', path: string } } };

export type OAuthLoginMutationVariables = Exact<{
  callbackUrl: Scalars['String'];
  state?: Maybe<OAuthStateInput>;
}>;


export type OAuthLoginMutation = { __typename?: 'Mutation', oauthLogin: { __typename?: 'OAuthLogin', url: string } };

export type ProcessAssetMutationVariables = Exact<{
  id: Scalars['AssetID'];
  reprocess?: Maybe<Array<Scalars['String']> | Scalars['String']>;
}>;


export type ProcessAssetMutation = { __typename?: 'Mutation', processAsset: { __typename?: 'MaybeAsyncMutationResult', ok: boolean, jobId: Maybe<string> } };

export type RefreshAuthMutationVariables = Exact<{
  refreshToken: Scalars['String'];
}>;


export type RefreshAuthMutation = { __typename?: 'Mutation', refreshAuth: { __typename?: 'AuthRefresh', token: string, refreshToken: Maybe<string>, validUntil: string } };

export type SystemInfoQueryVariables = Exact<{ [key: string]: never; }>;


export type SystemInfoQuery = { __typename?: 'Query', serverInfo: { __typename?: 'ServerInfo', auth: AuthType, version: { __typename?: 'VersionInfo', versionString: string } }, currentUser: { __typename?: 'CurrentUser', authenticated: boolean, willLogoutAt: Maybe<string>, user: Maybe<{ __typename?: 'User', id: string, username: string, displayName: string, roles: Array<{ __typename?: 'UserRole', id: string, name: string, displayName: string }> }>, collectionPermissions: Array<{ __typename?: 'ResourceOperations', resource: ResourceType, allowedOperations: Array<Operation> }> } };

export type UpdateAssetMutationVariables = Exact<{
  id: Scalars['ID'];
  update: AssetUpdate;
}>;


export type UpdateAssetMutation = { __typename?: 'Mutation', updateAsset: { __typename?: 'AssetMutationResult', ok: boolean, asset: { __typename?: 'Asset', id: string, tags: Array<{ __typename?: 'TagAssignment', kind: string, name: string }>, properties: Array<{ __typename?: 'FractionProperty', numerator: number, denominator: number, name: string } | { __typename?: 'GPSProperty', name: string } | { __typename?: 'NumberProperty', numericValue: number, name: string } | { __typename?: 'TextProperty', textValue: string, name: string } | { __typename?: 'TimeProperty', timeValue: string, name: string }>, mainFile: Maybe<{ __typename?: 'AssetFile', properties: Array<{ __typename?: 'FractionProperty', numerator: number, denominator: number, name: string } | { __typename?: 'GPSProperty', name: string } | { __typename?: 'NumberProperty', numericValue: number, name: string } | { __typename?: 'TextProperty', textValue: string, name: string } | { __typename?: 'TimeProperty', timeValue: string, name: string }> }> } } };

export type UpdateIndexMutationVariables = Exact<{
  id: Scalars['ID'];
  index: IndexInput;
}>;


export type UpdateIndexMutation = { __typename?: 'Mutation', indexUpdate: { __typename?: 'IndexMutationResult', ok: boolean } };

export type UploadMutationVariables = Exact<{
  index: Scalars['ID'];
  files: Array<FileUpload> | FileUpload;
  allFilesAreSingleAsset?: Maybe<Scalars['Boolean']>;
  uploadMore?: Maybe<Scalars['Boolean']>;
}>;


export type UploadMutation = { __typename?: 'Mutation', upload: { __typename?: 'UploadResult', ok: boolean, jobId: Maybe<string>, uploadSession: Maybe<string> } };

export type UploadArchiveFromUrlMutationVariables = Exact<{
  index: Scalars['ID'];
  url: Scalars['String'];
}>;


export type UploadArchiveFromUrlMutation = { __typename?: 'Mutation', uploadArchiveFromUrl: { __typename?: 'AsyncMutationResult', ok: boolean, jobId: string } };

export type UploadMoreMutationVariables = Exact<{
  files: Array<FileUpload> | FileUpload;
  session: Scalars['String'];
  finished: Scalars['Boolean'];
}>;


export type UploadMoreMutation = { __typename?: 'Mutation', uploadMore: { __typename?: 'MutationResult', ok: boolean } };

export type VersionInfoFragment = { __typename?: 'ServerInfo', version: { __typename?: 'VersionInfo', versionString: string } };

export type VersionInfoQueryVariables = Exact<{ [key: string]: never; }>;


export type VersionInfoQuery = { __typename?: 'Query', serverInfo: { __typename?: 'ServerInfo', version: { __typename?: 'VersionInfo', versionString: string } } };

export const CurrentUserFragmentDoc = gql`
    fragment CurrentUser on Query {
  currentUser {
    user {
      id
      username
      displayName
      roles {
        id
        name
        displayName
      }
    }
    authenticated
    willLogoutAt
    collectionPermissions {
      resource
      allowedOperations
    }
  }
}
    `;
export const RuleFragmentDoc = gql`
    fragment Rule on IndexRule {
  id
  description
  match
  inverse
  enabled
  actions {
    id
    description
    enabled
    config {
      action
      ... on IgnoreActionConfig {
        ignore
      }
      ... on SetKeyActionConfig {
        key
      }
      ... on SetParamsActionConfig {
        params
      }
      ... on AddTagActionConfig {
        tag {
          kind
          name
        }
      }
      ... on ProcessActionConfig {
        request
        queue
        params
      }
    }
  }
}
    `;
export const TaskSummaryFragmentDoc = gql`
    fragment TaskSummary on Job {
  taskSummary {
    name
    total
    pending
    failed
    took
  }
}
    `;
export const AssetFileFragmentDoc = gql`
    fragment AssetFile on AssetFile {
  filename
  fileInfo {
    size
  }
  url
}
    `;
export const PropertyPairFragmentDoc = gql`
    fragment PropertyPair on Property {
  name
  ... on TextProperty {
    textValue
  }
  ... on TimeProperty {
    timeValue
  }
  ... on NumberProperty {
    numericValue
  }
  ... on FractionProperty {
    numerator
    denominator
  }
}
    `;
export const VideoSidecarFragmentDoc = gql`
    fragment VideoSidecar on Video {
  file {
    url
  }
  videoFormat: format
  width
  height
  videoCodec
  audioCodec
}
    `;
export const GalleryThumbnailFragmentDoc = gql`
    fragment GalleryThumbnail on Asset {
  galleryThumbnail: thumbnail(
    height: $galleryThumbnailHeight
    acceptableFormats: $acceptableThumbnailFormats
  ) {
    file {
      url
    }
    width
    height
  }
}
    `;
export const ThumbnailsFragmentDoc = gql`
    fragment Thumbnails on Asset {
  ...GalleryThumbnail
  previewThumbnail: thumbnail(
    width: $previewThumbnailWidth
    height: $previewThumbnailHeight
    acceptableFormats: $acceptableThumbnailFormats
  ) {
    file {
      url
    }
    width
    height
  }
}
    ${GalleryThumbnailFragmentDoc}`;
export const AssetMutableFragmentDoc = gql`
    fragment AssetMutable on Asset {
  tags {
    kind
    name
  }
  properties {
    ...PropertyPair
  }
  mainFile {
    properties {
      ...PropertyPair
    }
  }
}
    ${PropertyPairFragmentDoc}`;
export const AssetFragmentDoc = gql`
    fragment Asset on Asset {
  id
  key
  mainFile {
    ...AssetFile
    properties {
      ...PropertyPair
    }
    sidecars {
      ...VideoSidecar
    }
  }
  files {
    ...AssetFile
  }
  ...Thumbnails
  ...AssetMutable
  allowedOperations
}
    ${AssetFileFragmentDoc}
${PropertyPairFragmentDoc}
${VideoSidecarFragmentDoc}
${ThumbnailsFragmentDoc}
${AssetMutableFragmentDoc}`;
export const TextPropertyValuesFragmentDoc = gql`
    fragment TextPropertyValues on TextPropertyBucket {
  value
  numAssets
}
    `;
export const TimePropertyValuesFragmentDoc = gql`
    fragment TimePropertyValues on TimeRangeBucket {
  range {
    name
    from {
      value
    }
    to {
      value
    }
  }
  numAssets
}
    `;
export const TagFacetValuesFragmentDoc = gql`
    fragment TagFacetValues on TagBucket {
  name
  numAssets
}
    `;
export const PropertyIntrospectionFragmentDoc = gql`
    fragment PropertyIntrospection on PropertyIntrospection {
  name
  type
  count
}
    `;
export const IndexFragmentDoc = gql`
    fragment Index on Index {
  id
  name
  assetType
  tagKinds {
    name
    displayName
    displayNamePlural
    userAssignable
  }
  fileLocations {
    type
    repository {
      id
      type
      name
    }
    prefix
  }
  permissions {
    indexOperations
    assetOperations
    assetCollectionOperations
  }
  stats {
    count
    files {
      count
      fileSize {
        sum
      }
      sidecars {
        fileSize {
          sum
        }
      }
    }
  }
}
    `;
export const VersionInfoFragmentDoc = gql`
    fragment VersionInfo on ServerInfo {
  version {
    versionString
  }
}
    `;
export const CrawlDocument = gql`
    mutation Crawl($reqs: [CrawlRequest!]!) {
  crawl(reqs: $reqs) {
    ok
    jobId
  }
}
    `;
export type CrawlMutationFn = Apollo.MutationFunction<CrawlMutation, CrawlMutationVariables>;

/**
 * __useCrawlMutation__
 *
 * To run a mutation, you first call `useCrawlMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCrawlMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [crawlMutation, { data, loading, error }] = useCrawlMutation({
 *   variables: {
 *      reqs: // value for 'reqs'
 *   },
 * });
 */
export function useCrawlMutation(baseOptions?: Apollo.MutationHookOptions<CrawlMutation, CrawlMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<CrawlMutation, CrawlMutationVariables>(CrawlDocument, options);
      }
export type CrawlMutationHookResult = ReturnType<typeof useCrawlMutation>;
export type CrawlMutationResult = Apollo.MutationResult<CrawlMutation>;
export type CrawlMutationOptions = Apollo.BaseMutationOptions<CrawlMutation, CrawlMutationVariables>;
export const CreateArchiveDocument = gql`
    mutation CreateArchive($index: ID!, $query: AssetFilter, $files: ArchiveFiles) {
  archiveAssets(index: $index, query: $query, files: $files) {
    ok
    archive {
      jobId
      url
    }
  }
}
    `;
export type CreateArchiveMutationFn = Apollo.MutationFunction<CreateArchiveMutation, CreateArchiveMutationVariables>;

/**
 * __useCreateArchiveMutation__
 *
 * To run a mutation, you first call `useCreateArchiveMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateArchiveMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createArchiveMutation, { data, loading, error }] = useCreateArchiveMutation({
 *   variables: {
 *      index: // value for 'index'
 *      query: // value for 'query'
 *      files: // value for 'files'
 *   },
 * });
 */
export function useCreateArchiveMutation(baseOptions?: Apollo.MutationHookOptions<CreateArchiveMutation, CreateArchiveMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<CreateArchiveMutation, CreateArchiveMutationVariables>(CreateArchiveDocument, options);
      }
export type CreateArchiveMutationHookResult = ReturnType<typeof useCreateArchiveMutation>;
export type CreateArchiveMutationResult = Apollo.MutationResult<CreateArchiveMutation>;
export type CreateArchiveMutationOptions = Apollo.BaseMutationOptions<CreateArchiveMutation, CreateArchiveMutationVariables>;
export const CreateIndexDocument = gql`
    mutation CreateIndex($index: IndexInput) {
  indexCreate(index: $index) {
    ok
    index {
      id
    }
  }
}
    `;
export type CreateIndexMutationFn = Apollo.MutationFunction<CreateIndexMutation, CreateIndexMutationVariables>;

/**
 * __useCreateIndexMutation__
 *
 * To run a mutation, you first call `useCreateIndexMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateIndexMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createIndexMutation, { data, loading, error }] = useCreateIndexMutation({
 *   variables: {
 *      index: // value for 'index'
 *   },
 * });
 */
export function useCreateIndexMutation(baseOptions?: Apollo.MutationHookOptions<CreateIndexMutation, CreateIndexMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<CreateIndexMutation, CreateIndexMutationVariables>(CreateIndexDocument, options);
      }
export type CreateIndexMutationHookResult = ReturnType<typeof useCreateIndexMutation>;
export type CreateIndexMutationResult = Apollo.MutationResult<CreateIndexMutation>;
export type CreateIndexMutationOptions = Apollo.BaseMutationOptions<CreateIndexMutation, CreateIndexMutationVariables>;
export const CurrentUserDocument = gql`
    query CurrentUser {
  ...CurrentUser
}
    ${CurrentUserFragmentDoc}`;

/**
 * __useCurrentUserQuery__
 *
 * To run a query within a React component, call `useCurrentUserQuery` and pass it any options that fit your needs.
 * When your component renders, `useCurrentUserQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useCurrentUserQuery({
 *   variables: {
 *   },
 * });
 */
export function useCurrentUserQuery(baseOptions?: Apollo.QueryHookOptions<CurrentUserQuery, CurrentUserQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<CurrentUserQuery, CurrentUserQueryVariables>(CurrentUserDocument, options);
      }
export function useCurrentUserLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<CurrentUserQuery, CurrentUserQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<CurrentUserQuery, CurrentUserQueryVariables>(CurrentUserDocument, options);
        }
export type CurrentUserQueryHookResult = ReturnType<typeof useCurrentUserQuery>;
export type CurrentUserLazyQueryHookResult = ReturnType<typeof useCurrentUserLazyQuery>;
export type CurrentUserQueryResult = Apollo.QueryResult<CurrentUserQuery, CurrentUserQueryVariables>;
export const DeleteAssetDocument = gql`
    mutation DeleteAsset($id: AssetID!, $deleteFiles: Boolean) {
  deleteAsset(id: $id, deleteFiles: $deleteFiles) {
    ok
  }
}
    `;
export type DeleteAssetMutationFn = Apollo.MutationFunction<DeleteAssetMutation, DeleteAssetMutationVariables>;

/**
 * __useDeleteAssetMutation__
 *
 * To run a mutation, you first call `useDeleteAssetMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteAssetMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteAssetMutation, { data, loading, error }] = useDeleteAssetMutation({
 *   variables: {
 *      id: // value for 'id'
 *      deleteFiles: // value for 'deleteFiles'
 *   },
 * });
 */
export function useDeleteAssetMutation(baseOptions?: Apollo.MutationHookOptions<DeleteAssetMutation, DeleteAssetMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<DeleteAssetMutation, DeleteAssetMutationVariables>(DeleteAssetDocument, options);
      }
export type DeleteAssetMutationHookResult = ReturnType<typeof useDeleteAssetMutation>;
export type DeleteAssetMutationResult = Apollo.MutationResult<DeleteAssetMutation>;
export type DeleteAssetMutationOptions = Apollo.BaseMutationOptions<DeleteAssetMutation, DeleteAssetMutationVariables>;
export const DeleteAssetsDocument = gql`
    mutation DeleteAssets($index: ID!, $query: AssetFilter) {
  deleteAssetByQuery(index: $index, query: $query, deleteFiles: true) {
    ok
  }
}
    `;
export type DeleteAssetsMutationFn = Apollo.MutationFunction<DeleteAssetsMutation, DeleteAssetsMutationVariables>;

/**
 * __useDeleteAssetsMutation__
 *
 * To run a mutation, you first call `useDeleteAssetsMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteAssetsMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteAssetsMutation, { data, loading, error }] = useDeleteAssetsMutation({
 *   variables: {
 *      index: // value for 'index'
 *      query: // value for 'query'
 *   },
 * });
 */
export function useDeleteAssetsMutation(baseOptions?: Apollo.MutationHookOptions<DeleteAssetsMutation, DeleteAssetsMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<DeleteAssetsMutation, DeleteAssetsMutationVariables>(DeleteAssetsDocument, options);
      }
export type DeleteAssetsMutationHookResult = ReturnType<typeof useDeleteAssetsMutation>;
export type DeleteAssetsMutationResult = Apollo.MutationResult<DeleteAssetsMutation>;
export type DeleteAssetsMutationOptions = Apollo.BaseMutationOptions<DeleteAssetsMutation, DeleteAssetsMutationVariables>;
export const DeleteIndexDocument = gql`
    mutation DeleteIndex($id: ID!) {
  indexDelete(id: $id) {
    ok
  }
}
    `;
export type DeleteIndexMutationFn = Apollo.MutationFunction<DeleteIndexMutation, DeleteIndexMutationVariables>;

/**
 * __useDeleteIndexMutation__
 *
 * To run a mutation, you first call `useDeleteIndexMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteIndexMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteIndexMutation, { data, loading, error }] = useDeleteIndexMutation({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useDeleteIndexMutation(baseOptions?: Apollo.MutationHookOptions<DeleteIndexMutation, DeleteIndexMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<DeleteIndexMutation, DeleteIndexMutationVariables>(DeleteIndexDocument, options);
      }
export type DeleteIndexMutationHookResult = ReturnType<typeof useDeleteIndexMutation>;
export type DeleteIndexMutationResult = Apollo.MutationResult<DeleteIndexMutation>;
export type DeleteIndexMutationOptions = Apollo.BaseMutationOptions<DeleteIndexMutation, DeleteIndexMutationVariables>;
export const GetIndexDocument = gql`
    query GetIndex($id: ID!) {
  index(id: $id) {
    ...Index
  }
}
    ${IndexFragmentDoc}`;

/**
 * __useGetIndexQuery__
 *
 * To run a query within a React component, call `useGetIndexQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetIndexQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetIndexQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useGetIndexQuery(baseOptions: Apollo.QueryHookOptions<GetIndexQuery, GetIndexQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetIndexQuery, GetIndexQueryVariables>(GetIndexDocument, options);
      }
export function useGetIndexLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetIndexQuery, GetIndexQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetIndexQuery, GetIndexQueryVariables>(GetIndexDocument, options);
        }
export type GetIndexQueryHookResult = ReturnType<typeof useGetIndexQuery>;
export type GetIndexLazyQueryHookResult = ReturnType<typeof useGetIndexLazyQuery>;
export type GetIndexQueryResult = Apollo.QueryResult<GetIndexQuery, GetIndexQueryVariables>;
export const GetIndexDetailDocument = gql`
    query GetIndexDetail($id: ID!) {
  index(id: $id) {
    ...Index
    rules {
      ...Rule
    }
    allPermissions {
      subject {
        id
      }
      permissions {
        indexOperations
        assetOperations
        assetCollectionOperations
      }
    }
  }
}
    ${IndexFragmentDoc}
${RuleFragmentDoc}`;

/**
 * __useGetIndexDetailQuery__
 *
 * To run a query within a React component, call `useGetIndexDetailQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetIndexDetailQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetIndexDetailQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useGetIndexDetailQuery(baseOptions: Apollo.QueryHookOptions<GetIndexDetailQuery, GetIndexDetailQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetIndexDetailQuery, GetIndexDetailQueryVariables>(GetIndexDetailDocument, options);
      }
export function useGetIndexDetailLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetIndexDetailQuery, GetIndexDetailQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetIndexDetailQuery, GetIndexDetailQueryVariables>(GetIndexDetailDocument, options);
        }
export type GetIndexDetailQueryHookResult = ReturnType<typeof useGetIndexDetailQuery>;
export type GetIndexDetailLazyQueryHookResult = ReturnType<typeof useGetIndexDetailLazyQuery>;
export type GetIndexDetailQueryResult = Apollo.QueryResult<GetIndexDetailQuery, GetIndexDetailQueryVariables>;
export const GetUsersAndRolesDocument = gql`
    query GetUsersAndRoles {
  userRoles {
    id
    name
    displayName
  }
  users {
    id
    username
    displayName
  }
}
    `;

/**
 * __useGetUsersAndRolesQuery__
 *
 * To run a query within a React component, call `useGetUsersAndRolesQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetUsersAndRolesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetUsersAndRolesQuery({
 *   variables: {
 *   },
 * });
 */
export function useGetUsersAndRolesQuery(baseOptions?: Apollo.QueryHookOptions<GetUsersAndRolesQuery, GetUsersAndRolesQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetUsersAndRolesQuery, GetUsersAndRolesQueryVariables>(GetUsersAndRolesDocument, options);
      }
export function useGetUsersAndRolesLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetUsersAndRolesQuery, GetUsersAndRolesQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetUsersAndRolesQuery, GetUsersAndRolesQueryVariables>(GetUsersAndRolesDocument, options);
        }
export type GetUsersAndRolesQueryHookResult = ReturnType<typeof useGetUsersAndRolesQuery>;
export type GetUsersAndRolesLazyQueryHookResult = ReturnType<typeof useGetUsersAndRolesLazyQuery>;
export type GetUsersAndRolesQueryResult = Apollo.QueryResult<GetUsersAndRolesQuery, GetUsersAndRolesQueryVariables>;
export const JobEventsDocument = gql`
    subscription JobEvents {
  jobEvents {
    ... on NewJobEvent {
      job {
        id
        name
        queuedAt
        ...TaskSummary
      }
    }
    ... on ProgressJobEvent {
      job {
        id
        ...TaskSummary
      }
    }
    ... on FinishedJobEvent {
      job {
        id
        finishedAt
        ...TaskSummary
      }
    }
  }
}
    ${TaskSummaryFragmentDoc}`;

/**
 * __useJobEventsSubscription__
 *
 * To run a query within a React component, call `useJobEventsSubscription` and pass it any options that fit your needs.
 * When your component renders, `useJobEventsSubscription` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the subscription, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useJobEventsSubscription({
 *   variables: {
 *   },
 * });
 */
export function useJobEventsSubscription(baseOptions?: Apollo.SubscriptionHookOptions<JobEventsSubscription, JobEventsSubscriptionVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useSubscription<JobEventsSubscription, JobEventsSubscriptionVariables>(JobEventsDocument, options);
      }
export type JobEventsSubscriptionHookResult = ReturnType<typeof useJobEventsSubscription>;
export type JobEventsSubscriptionResult = Apollo.SubscriptionResult<JobEventsSubscription>;
export const ListAssetsDocument = gql`
    query ListAssets($query: AssetFilter, $sortBy: [AssetSortRequest!], $galleryThumbnailHeight: Int, $previewThumbnailWidth: Int, $previewThumbnailHeight: Int, $acceptableThumbnailFormats: [ThumbnailFormat!], $size: Int, $index: [ID!]) {
  assets(query: $query, sortBy: $sortBy, size: $size, index: $index) {
    items {
      ...Asset
    }
    facets {
      tagKinds {
        kind
        numTags
      }
      propertyIntrospection {
        ...PropertyIntrospection
      }
      files {
        propertyIntrospection {
          ...PropertyIntrospection
        }
      }
    }
    stats {
      files {
        property_date: property(name: "date") {
          timeValue {
            min
            max
          }
        }
      }
    }
    totalCount
  }
}
    ${AssetFragmentDoc}
${PropertyIntrospectionFragmentDoc}`;

/**
 * __useListAssetsQuery__
 *
 * To run a query within a React component, call `useListAssetsQuery` and pass it any options that fit your needs.
 * When your component renders, `useListAssetsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useListAssetsQuery({
 *   variables: {
 *      query: // value for 'query'
 *      sortBy: // value for 'sortBy'
 *      galleryThumbnailHeight: // value for 'galleryThumbnailHeight'
 *      previewThumbnailWidth: // value for 'previewThumbnailWidth'
 *      previewThumbnailHeight: // value for 'previewThumbnailHeight'
 *      acceptableThumbnailFormats: // value for 'acceptableThumbnailFormats'
 *      size: // value for 'size'
 *      index: // value for 'index'
 *   },
 * });
 */
export function useListAssetsQuery(baseOptions?: Apollo.QueryHookOptions<ListAssetsQuery, ListAssetsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<ListAssetsQuery, ListAssetsQueryVariables>(ListAssetsDocument, options);
      }
export function useListAssetsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<ListAssetsQuery, ListAssetsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<ListAssetsQuery, ListAssetsQueryVariables>(ListAssetsDocument, options);
        }
export type ListAssetsQueryHookResult = ReturnType<typeof useListAssetsQuery>;
export type ListAssetsLazyQueryHookResult = ReturnType<typeof useListAssetsLazyQuery>;
export type ListAssetsQueryResult = Apollo.QueryResult<ListAssetsQuery, ListAssetsQueryVariables>;
export const ListMoreAssetsDocument = gql`
    query ListMoreAssets($query: AssetFilter, $sortBy: [AssetSortRequest!], $galleryThumbnailHeight: Int, $previewThumbnailWidth: Int, $previewThumbnailHeight: Int, $acceptableThumbnailFormats: [ThumbnailFormat!], $skip: Int, $size: Int, $index: [ID!]) {
  assets(query: $query, sortBy: $sortBy, skip: $skip, size: $size, index: $index) {
    items {
      ...Asset
    }
  }
}
    ${AssetFragmentDoc}`;

/**
 * __useListMoreAssetsQuery__
 *
 * To run a query within a React component, call `useListMoreAssetsQuery` and pass it any options that fit your needs.
 * When your component renders, `useListMoreAssetsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useListMoreAssetsQuery({
 *   variables: {
 *      query: // value for 'query'
 *      sortBy: // value for 'sortBy'
 *      galleryThumbnailHeight: // value for 'galleryThumbnailHeight'
 *      previewThumbnailWidth: // value for 'previewThumbnailWidth'
 *      previewThumbnailHeight: // value for 'previewThumbnailHeight'
 *      acceptableThumbnailFormats: // value for 'acceptableThumbnailFormats'
 *      skip: // value for 'skip'
 *      size: // value for 'size'
 *      index: // value for 'index'
 *   },
 * });
 */
export function useListMoreAssetsQuery(baseOptions?: Apollo.QueryHookOptions<ListMoreAssetsQuery, ListMoreAssetsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<ListMoreAssetsQuery, ListMoreAssetsQueryVariables>(ListMoreAssetsDocument, options);
      }
export function useListMoreAssetsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<ListMoreAssetsQuery, ListMoreAssetsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<ListMoreAssetsQuery, ListMoreAssetsQueryVariables>(ListMoreAssetsDocument, options);
        }
export type ListMoreAssetsQueryHookResult = ReturnType<typeof useListMoreAssetsQuery>;
export type ListMoreAssetsLazyQueryHookResult = ReturnType<typeof useListMoreAssetsLazyQuery>;
export type ListMoreAssetsQueryResult = Apollo.QueryResult<ListMoreAssetsQuery, ListMoreAssetsQueryVariables>;
export const ListFailedAssetsDocument = gql`
    query ListFailedAssets($query: AssetFilter, $index: [ID!]) {
  assets(
    query: $query
    sortBy: {file: {processing: LAST_FAILED_PROCESSED_AT}, method: DESC}
    index: $index
  ) {
    items {
      id
      files {
        filename
        repository
        url
        processings {
          name
          jobId
          processedAt
          error {
            code
            message
          }
        }
      }
    }
  }
}
    `;

/**
 * __useListFailedAssetsQuery__
 *
 * To run a query within a React component, call `useListFailedAssetsQuery` and pass it any options that fit your needs.
 * When your component renders, `useListFailedAssetsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useListFailedAssetsQuery({
 *   variables: {
 *      query: // value for 'query'
 *      index: // value for 'index'
 *   },
 * });
 */
export function useListFailedAssetsQuery(baseOptions?: Apollo.QueryHookOptions<ListFailedAssetsQuery, ListFailedAssetsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<ListFailedAssetsQuery, ListFailedAssetsQueryVariables>(ListFailedAssetsDocument, options);
      }
export function useListFailedAssetsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<ListFailedAssetsQuery, ListFailedAssetsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<ListFailedAssetsQuery, ListFailedAssetsQueryVariables>(ListFailedAssetsDocument, options);
        }
export type ListFailedAssetsQueryHookResult = ReturnType<typeof useListFailedAssetsQuery>;
export type ListFailedAssetsLazyQueryHookResult = ReturnType<typeof useListFailedAssetsLazyQuery>;
export type ListFailedAssetsQueryResult = Apollo.QueryResult<ListFailedAssetsQuery, ListFailedAssetsQueryVariables>;
export const ListFailedProcessingsDocument = gql`
    query ListFailedProcessings($index: [ID!]) {
  assets(
    index: $index
    query: {withFile: {withProcessing: {error: {}}}}
    sortBy: {file: {processing: LAST_FAILED_PROCESSED_AT}, method: DESC}
  ) {
    items {
      id
      files {
        repository
        filename
        processings {
          name
          processedAt
          error {
            message
          }
        }
      }
    }
  }
}
    `;

/**
 * __useListFailedProcessingsQuery__
 *
 * To run a query within a React component, call `useListFailedProcessingsQuery` and pass it any options that fit your needs.
 * When your component renders, `useListFailedProcessingsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useListFailedProcessingsQuery({
 *   variables: {
 *      index: // value for 'index'
 *   },
 * });
 */
export function useListFailedProcessingsQuery(baseOptions?: Apollo.QueryHookOptions<ListFailedProcessingsQuery, ListFailedProcessingsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<ListFailedProcessingsQuery, ListFailedProcessingsQueryVariables>(ListFailedProcessingsDocument, options);
      }
export function useListFailedProcessingsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<ListFailedProcessingsQuery, ListFailedProcessingsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<ListFailedProcessingsQuery, ListFailedProcessingsQueryVariables>(ListFailedProcessingsDocument, options);
        }
export type ListFailedProcessingsQueryHookResult = ReturnType<typeof useListFailedProcessingsQuery>;
export type ListFailedProcessingsLazyQueryHookResult = ReturnType<typeof useListFailedProcessingsLazyQuery>;
export type ListFailedProcessingsQueryResult = Apollo.QueryResult<ListFailedProcessingsQuery, ListFailedProcessingsQueryVariables>;
export const ListIndicesDocument = gql`
    query ListIndices {
  indices {
    ...Index
  }
}
    ${IndexFragmentDoc}`;

/**
 * __useListIndicesQuery__
 *
 * To run a query within a React component, call `useListIndicesQuery` and pass it any options that fit your needs.
 * When your component renders, `useListIndicesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useListIndicesQuery({
 *   variables: {
 *   },
 * });
 */
export function useListIndicesQuery(baseOptions?: Apollo.QueryHookOptions<ListIndicesQuery, ListIndicesQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<ListIndicesQuery, ListIndicesQueryVariables>(ListIndicesDocument, options);
      }
export function useListIndicesLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<ListIndicesQuery, ListIndicesQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<ListIndicesQuery, ListIndicesQueryVariables>(ListIndicesDocument, options);
        }
export type ListIndicesQueryHookResult = ReturnType<typeof useListIndicesQuery>;
export type ListIndicesLazyQueryHookResult = ReturnType<typeof useListIndicesLazyQuery>;
export type ListIndicesQueryResult = Apollo.QueryResult<ListIndicesQuery, ListIndicesQueryVariables>;
export const ListJobsDocument = gql`
    query ListJobs {
  jobs {
    id
    name
    queuedAt
    finishedAt
    finished
    ...TaskSummary
  }
}
    ${TaskSummaryFragmentDoc}`;

/**
 * __useListJobsQuery__
 *
 * To run a query within a React component, call `useListJobsQuery` and pass it any options that fit your needs.
 * When your component renders, `useListJobsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useListJobsQuery({
 *   variables: {
 *   },
 * });
 */
export function useListJobsQuery(baseOptions?: Apollo.QueryHookOptions<ListJobsQuery, ListJobsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<ListJobsQuery, ListJobsQueryVariables>(ListJobsDocument, options);
      }
export function useListJobsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<ListJobsQuery, ListJobsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<ListJobsQuery, ListJobsQueryVariables>(ListJobsDocument, options);
        }
export type ListJobsQueryHookResult = ReturnType<typeof useListJobsQuery>;
export type ListJobsLazyQueryHookResult = ReturnType<typeof useListJobsLazyQuery>;
export type ListJobsQueryResult = Apollo.QueryResult<ListJobsQuery, ListJobsQueryVariables>;
export const ListRepositoriesDocument = gql`
    query ListRepositories {
  repositories {
    id
    name
  }
}
    `;

/**
 * __useListRepositoriesQuery__
 *
 * To run a query within a React component, call `useListRepositoriesQuery` and pass it any options that fit your needs.
 * When your component renders, `useListRepositoriesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useListRepositoriesQuery({
 *   variables: {
 *   },
 * });
 */
export function useListRepositoriesQuery(baseOptions?: Apollo.QueryHookOptions<ListRepositoriesQuery, ListRepositoriesQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<ListRepositoriesQuery, ListRepositoriesQueryVariables>(ListRepositoriesDocument, options);
      }
export function useListRepositoriesLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<ListRepositoriesQuery, ListRepositoriesQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<ListRepositoriesQuery, ListRepositoriesQueryVariables>(ListRepositoriesDocument, options);
        }
export type ListRepositoriesQueryHookResult = ReturnType<typeof useListRepositoriesQuery>;
export type ListRepositoriesLazyQueryHookResult = ReturnType<typeof useListRepositoriesLazyQuery>;
export type ListRepositoriesQueryResult = Apollo.QueryResult<ListRepositoriesQuery, ListRepositoriesQueryVariables>;
export const ListTagsDocument = gql`
    query ListTags($index: [ID!], $kind: [String!], $galleryThumbnailHeight: Int, $acceptableThumbnailFormats: [ThumbnailFormat!], $nameQuery: String, $sort: TagSortRequest) {
  tags(index: $index, kind: $kind, nameQuery: $nameQuery, sort: $sort) {
    kind
    name
    coverAsset {
      id
      ...GalleryThumbnail
    }
  }
}
    ${GalleryThumbnailFragmentDoc}`;

/**
 * __useListTagsQuery__
 *
 * To run a query within a React component, call `useListTagsQuery` and pass it any options that fit your needs.
 * When your component renders, `useListTagsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useListTagsQuery({
 *   variables: {
 *      index: // value for 'index'
 *      kind: // value for 'kind'
 *      galleryThumbnailHeight: // value for 'galleryThumbnailHeight'
 *      acceptableThumbnailFormats: // value for 'acceptableThumbnailFormats'
 *      nameQuery: // value for 'nameQuery'
 *      sort: // value for 'sort'
 *   },
 * });
 */
export function useListTagsQuery(baseOptions?: Apollo.QueryHookOptions<ListTagsQuery, ListTagsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<ListTagsQuery, ListTagsQueryVariables>(ListTagsDocument, options);
      }
export function useListTagsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<ListTagsQuery, ListTagsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<ListTagsQuery, ListTagsQueryVariables>(ListTagsDocument, options);
        }
export type ListTagsQueryHookResult = ReturnType<typeof useListTagsQuery>;
export type ListTagsLazyQueryHookResult = ReturnType<typeof useListTagsLazyQuery>;
export type ListTagsQueryResult = Apollo.QueryResult<ListTagsQuery, ListTagsQueryVariables>;
export const OAuthExchangeDocument = gql`
    mutation OAuthExchange($callbackUrl: String!, $code: String!, $serializedState: String!) {
  oauthExchange(
    callbackUrl: $callbackUrl
    code: $code
    serializedState: $serializedState
  ) {
    token
    refreshToken
    state {
      path
    }
  }
}
    `;
export type OAuthExchangeMutationFn = Apollo.MutationFunction<OAuthExchangeMutation, OAuthExchangeMutationVariables>;

/**
 * __useOAuthExchangeMutation__
 *
 * To run a mutation, you first call `useOAuthExchangeMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useOAuthExchangeMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [oAuthExchangeMutation, { data, loading, error }] = useOAuthExchangeMutation({
 *   variables: {
 *      callbackUrl: // value for 'callbackUrl'
 *      code: // value for 'code'
 *      serializedState: // value for 'serializedState'
 *   },
 * });
 */
export function useOAuthExchangeMutation(baseOptions?: Apollo.MutationHookOptions<OAuthExchangeMutation, OAuthExchangeMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<OAuthExchangeMutation, OAuthExchangeMutationVariables>(OAuthExchangeDocument, options);
      }
export type OAuthExchangeMutationHookResult = ReturnType<typeof useOAuthExchangeMutation>;
export type OAuthExchangeMutationResult = Apollo.MutationResult<OAuthExchangeMutation>;
export type OAuthExchangeMutationOptions = Apollo.BaseMutationOptions<OAuthExchangeMutation, OAuthExchangeMutationVariables>;
export const OAuthLoginDocument = gql`
    mutation OAuthLogin($callbackUrl: String!, $state: OAuthStateInput) {
  oauthLogin(callbackUrl: $callbackUrl, state: $state) {
    url
  }
}
    `;
export type OAuthLoginMutationFn = Apollo.MutationFunction<OAuthLoginMutation, OAuthLoginMutationVariables>;

/**
 * __useOAuthLoginMutation__
 *
 * To run a mutation, you first call `useOAuthLoginMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useOAuthLoginMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [oAuthLoginMutation, { data, loading, error }] = useOAuthLoginMutation({
 *   variables: {
 *      callbackUrl: // value for 'callbackUrl'
 *      state: // value for 'state'
 *   },
 * });
 */
export function useOAuthLoginMutation(baseOptions?: Apollo.MutationHookOptions<OAuthLoginMutation, OAuthLoginMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<OAuthLoginMutation, OAuthLoginMutationVariables>(OAuthLoginDocument, options);
      }
export type OAuthLoginMutationHookResult = ReturnType<typeof useOAuthLoginMutation>;
export type OAuthLoginMutationResult = Apollo.MutationResult<OAuthLoginMutation>;
export type OAuthLoginMutationOptions = Apollo.BaseMutationOptions<OAuthLoginMutation, OAuthLoginMutationVariables>;
export const ProcessAssetDocument = gql`
    mutation ProcessAsset($id: AssetID!, $reprocess: [String!]) {
  processAsset(id: $id, reprocess: $reprocess) {
    ok
    jobId
  }
}
    `;
export type ProcessAssetMutationFn = Apollo.MutationFunction<ProcessAssetMutation, ProcessAssetMutationVariables>;

/**
 * __useProcessAssetMutation__
 *
 * To run a mutation, you first call `useProcessAssetMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useProcessAssetMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [processAssetMutation, { data, loading, error }] = useProcessAssetMutation({
 *   variables: {
 *      id: // value for 'id'
 *      reprocess: // value for 'reprocess'
 *   },
 * });
 */
export function useProcessAssetMutation(baseOptions?: Apollo.MutationHookOptions<ProcessAssetMutation, ProcessAssetMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<ProcessAssetMutation, ProcessAssetMutationVariables>(ProcessAssetDocument, options);
      }
export type ProcessAssetMutationHookResult = ReturnType<typeof useProcessAssetMutation>;
export type ProcessAssetMutationResult = Apollo.MutationResult<ProcessAssetMutation>;
export type ProcessAssetMutationOptions = Apollo.BaseMutationOptions<ProcessAssetMutation, ProcessAssetMutationVariables>;
export const RefreshAuthDocument = gql`
    mutation RefreshAuth($refreshToken: String!) {
  refreshAuth(refreshToken: $refreshToken) {
    token
    refreshToken
    validUntil
  }
}
    `;
export type RefreshAuthMutationFn = Apollo.MutationFunction<RefreshAuthMutation, RefreshAuthMutationVariables>;

/**
 * __useRefreshAuthMutation__
 *
 * To run a mutation, you first call `useRefreshAuthMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useRefreshAuthMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [refreshAuthMutation, { data, loading, error }] = useRefreshAuthMutation({
 *   variables: {
 *      refreshToken: // value for 'refreshToken'
 *   },
 * });
 */
export function useRefreshAuthMutation(baseOptions?: Apollo.MutationHookOptions<RefreshAuthMutation, RefreshAuthMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<RefreshAuthMutation, RefreshAuthMutationVariables>(RefreshAuthDocument, options);
      }
export type RefreshAuthMutationHookResult = ReturnType<typeof useRefreshAuthMutation>;
export type RefreshAuthMutationResult = Apollo.MutationResult<RefreshAuthMutation>;
export type RefreshAuthMutationOptions = Apollo.BaseMutationOptions<RefreshAuthMutation, RefreshAuthMutationVariables>;
export const SystemInfoDocument = gql`
    query SystemInfo {
  serverInfo {
    ...VersionInfo
    auth
  }
  ...CurrentUser
}
    ${VersionInfoFragmentDoc}
${CurrentUserFragmentDoc}`;

/**
 * __useSystemInfoQuery__
 *
 * To run a query within a React component, call `useSystemInfoQuery` and pass it any options that fit your needs.
 * When your component renders, `useSystemInfoQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useSystemInfoQuery({
 *   variables: {
 *   },
 * });
 */
export function useSystemInfoQuery(baseOptions?: Apollo.QueryHookOptions<SystemInfoQuery, SystemInfoQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<SystemInfoQuery, SystemInfoQueryVariables>(SystemInfoDocument, options);
      }
export function useSystemInfoLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<SystemInfoQuery, SystemInfoQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<SystemInfoQuery, SystemInfoQueryVariables>(SystemInfoDocument, options);
        }
export type SystemInfoQueryHookResult = ReturnType<typeof useSystemInfoQuery>;
export type SystemInfoLazyQueryHookResult = ReturnType<typeof useSystemInfoLazyQuery>;
export type SystemInfoQueryResult = Apollo.QueryResult<SystemInfoQuery, SystemInfoQueryVariables>;
export const UpdateAssetDocument = gql`
    mutation UpdateAsset($id: ID!, $update: AssetUpdate!) {
  updateAsset(id: $id, update: $update) {
    ok
    asset {
      id
      ...AssetMutable
    }
  }
}
    ${AssetMutableFragmentDoc}`;
export type UpdateAssetMutationFn = Apollo.MutationFunction<UpdateAssetMutation, UpdateAssetMutationVariables>;

/**
 * __useUpdateAssetMutation__
 *
 * To run a mutation, you first call `useUpdateAssetMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateAssetMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateAssetMutation, { data, loading, error }] = useUpdateAssetMutation({
 *   variables: {
 *      id: // value for 'id'
 *      update: // value for 'update'
 *   },
 * });
 */
export function useUpdateAssetMutation(baseOptions?: Apollo.MutationHookOptions<UpdateAssetMutation, UpdateAssetMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<UpdateAssetMutation, UpdateAssetMutationVariables>(UpdateAssetDocument, options);
      }
export type UpdateAssetMutationHookResult = ReturnType<typeof useUpdateAssetMutation>;
export type UpdateAssetMutationResult = Apollo.MutationResult<UpdateAssetMutation>;
export type UpdateAssetMutationOptions = Apollo.BaseMutationOptions<UpdateAssetMutation, UpdateAssetMutationVariables>;
export const UpdateIndexDocument = gql`
    mutation UpdateIndex($id: ID!, $index: IndexInput!) {
  indexUpdate(id: $id, index: $index) {
    ok
  }
}
    `;
export type UpdateIndexMutationFn = Apollo.MutationFunction<UpdateIndexMutation, UpdateIndexMutationVariables>;

/**
 * __useUpdateIndexMutation__
 *
 * To run a mutation, you first call `useUpdateIndexMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateIndexMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateIndexMutation, { data, loading, error }] = useUpdateIndexMutation({
 *   variables: {
 *      id: // value for 'id'
 *      index: // value for 'index'
 *   },
 * });
 */
export function useUpdateIndexMutation(baseOptions?: Apollo.MutationHookOptions<UpdateIndexMutation, UpdateIndexMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<UpdateIndexMutation, UpdateIndexMutationVariables>(UpdateIndexDocument, options);
      }
export type UpdateIndexMutationHookResult = ReturnType<typeof useUpdateIndexMutation>;
export type UpdateIndexMutationResult = Apollo.MutationResult<UpdateIndexMutation>;
export type UpdateIndexMutationOptions = Apollo.BaseMutationOptions<UpdateIndexMutation, UpdateIndexMutationVariables>;
export const UploadDocument = gql`
    mutation Upload($index: ID!, $files: [FileUpload!]!, $allFilesAreSingleAsset: Boolean, $uploadMore: Boolean) {
  upload(
    index: $index
    files: $files
    allFilesAreSingleAsset: $allFilesAreSingleAsset
    uploadMore: $uploadMore
  ) {
    ok
    jobId
    uploadSession
  }
}
    `;
export type UploadMutationFn = Apollo.MutationFunction<UploadMutation, UploadMutationVariables>;

/**
 * __useUploadMutation__
 *
 * To run a mutation, you first call `useUploadMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUploadMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [uploadMutation, { data, loading, error }] = useUploadMutation({
 *   variables: {
 *      index: // value for 'index'
 *      files: // value for 'files'
 *      allFilesAreSingleAsset: // value for 'allFilesAreSingleAsset'
 *      uploadMore: // value for 'uploadMore'
 *   },
 * });
 */
export function useUploadMutation(baseOptions?: Apollo.MutationHookOptions<UploadMutation, UploadMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<UploadMutation, UploadMutationVariables>(UploadDocument, options);
      }
export type UploadMutationHookResult = ReturnType<typeof useUploadMutation>;
export type UploadMutationResult = Apollo.MutationResult<UploadMutation>;
export type UploadMutationOptions = Apollo.BaseMutationOptions<UploadMutation, UploadMutationVariables>;
export const UploadArchiveFromUrlDocument = gql`
    mutation UploadArchiveFromUrl($index: ID!, $url: String!) {
  uploadArchiveFromUrl(index: $index, url: $url) {
    ok
    jobId
  }
}
    `;
export type UploadArchiveFromUrlMutationFn = Apollo.MutationFunction<UploadArchiveFromUrlMutation, UploadArchiveFromUrlMutationVariables>;

/**
 * __useUploadArchiveFromUrlMutation__
 *
 * To run a mutation, you first call `useUploadArchiveFromUrlMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUploadArchiveFromUrlMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [uploadArchiveFromUrlMutation, { data, loading, error }] = useUploadArchiveFromUrlMutation({
 *   variables: {
 *      index: // value for 'index'
 *      url: // value for 'url'
 *   },
 * });
 */
export function useUploadArchiveFromUrlMutation(baseOptions?: Apollo.MutationHookOptions<UploadArchiveFromUrlMutation, UploadArchiveFromUrlMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<UploadArchiveFromUrlMutation, UploadArchiveFromUrlMutationVariables>(UploadArchiveFromUrlDocument, options);
      }
export type UploadArchiveFromUrlMutationHookResult = ReturnType<typeof useUploadArchiveFromUrlMutation>;
export type UploadArchiveFromUrlMutationResult = Apollo.MutationResult<UploadArchiveFromUrlMutation>;
export type UploadArchiveFromUrlMutationOptions = Apollo.BaseMutationOptions<UploadArchiveFromUrlMutation, UploadArchiveFromUrlMutationVariables>;
export const UploadMoreDocument = gql`
    mutation UploadMore($files: [FileUpload!]!, $session: String!, $finished: Boolean!) {
  uploadMore(files: $files, session: $session, finished: $finished) {
    ok
  }
}
    `;
export type UploadMoreMutationFn = Apollo.MutationFunction<UploadMoreMutation, UploadMoreMutationVariables>;

/**
 * __useUploadMoreMutation__
 *
 * To run a mutation, you first call `useUploadMoreMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUploadMoreMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [uploadMoreMutation, { data, loading, error }] = useUploadMoreMutation({
 *   variables: {
 *      files: // value for 'files'
 *      session: // value for 'session'
 *      finished: // value for 'finished'
 *   },
 * });
 */
export function useUploadMoreMutation(baseOptions?: Apollo.MutationHookOptions<UploadMoreMutation, UploadMoreMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<UploadMoreMutation, UploadMoreMutationVariables>(UploadMoreDocument, options);
      }
export type UploadMoreMutationHookResult = ReturnType<typeof useUploadMoreMutation>;
export type UploadMoreMutationResult = Apollo.MutationResult<UploadMoreMutation>;
export type UploadMoreMutationOptions = Apollo.BaseMutationOptions<UploadMoreMutation, UploadMoreMutationVariables>;
export const VersionInfoDocument = gql`
    query VersionInfo {
  serverInfo {
    ...VersionInfo
  }
}
    ${VersionInfoFragmentDoc}`;

/**
 * __useVersionInfoQuery__
 *
 * To run a query within a React component, call `useVersionInfoQuery` and pass it any options that fit your needs.
 * When your component renders, `useVersionInfoQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useVersionInfoQuery({
 *   variables: {
 *   },
 * });
 */
export function useVersionInfoQuery(baseOptions?: Apollo.QueryHookOptions<VersionInfoQuery, VersionInfoQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<VersionInfoQuery, VersionInfoQueryVariables>(VersionInfoDocument, options);
      }
export function useVersionInfoLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<VersionInfoQuery, VersionInfoQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<VersionInfoQuery, VersionInfoQueryVariables>(VersionInfoDocument, options);
        }
export type VersionInfoQueryHookResult = ReturnType<typeof useVersionInfoQuery>;
export type VersionInfoLazyQueryHookResult = ReturnType<typeof useVersionInfoLazyQuery>;
export type VersionInfoQueryResult = Apollo.QueryResult<VersionInfoQuery, VersionInfoQueryVariables>;