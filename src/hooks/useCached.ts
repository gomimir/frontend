import { isEqual } from 'lodash'
import { DependencyList, useMemo, useState } from 'react'

// Similar to useMemo, but maintains referentially stable result
export function useCached<T>(factory: () => T, deps: DependencyList | undefined) {
  const [cached, cache] = useState<T>(null!)

  return useMemo(() => {
    const newValue = factory()
    if (isEqual(newValue, cached)) {
      return cached
    } else {
      cache(newValue)
      return newValue
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, deps)
}
