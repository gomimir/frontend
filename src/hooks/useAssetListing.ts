import { groupBy, isEqual, pickBy } from 'lodash'
import { useEffect, useState } from 'react'
import { ApolloError } from '@apollo/client'
import { mergeDeep } from '@apollo/client/utilities'
import { DEFAULT_TIME_RANGES } from '../facets/constants'
import { FacetDescriptor, FacetKind, FacetSubject } from '../facets/FacetDescriptor'
import { FacetFilter } from '../facets/FacetFilter'
import {
  AssetFileFilter,
  AssetFilter,
  AssetSortRequest,
  ListAssetsQuery,
  ListMoreAssetsDocument,
  PropertyFilter,
  PropertyIntrospectionFragment,
  PropertyType,
  SortMethod,
  useListAssetsQuery,
} from '../graphql'
import { getMaxRowHeightForUserScreen } from '../utils/galleryOptions'
import { getAcceptableThumbnailFormats } from '../utils/thumbnail'
import { AssetListingScreenParams } from './useAssetListingScreen'
import { useCached } from './useCached'

export type Asset = ListAssetsQuery['assets']['items'][number]

export interface AssetListingStats {
  startDate?: Date
  endDate?: Date
}

interface AssetListingHookResult {
  assets?: Asset[]
  facets?: FacetDescriptor[]
  stats?: AssetListingStats
  totalCount?: number
  fetchMore(num?: number): Promise<void>
  error?: ApolloError
}

interface ResolvedFacets {
  nonFacetFilter: AssetFilter
  facets: FacetDescriptor[]
}

function buildPropertyFacets(
  subject: FacetSubject,
  propertyIntrospection: PropertyIntrospectionFragment[],
): FacetDescriptor[] {
  const result: FacetDescriptor[] = []

  for (const { name, type } of propertyIntrospection) {
    switch (type) {
      case PropertyType.Text:
        result.push({
          subject,
          kind: FacetKind.TextProperty,
          name,
        })
        break

      case PropertyType.Time:
        result.push({
          subject,
          kind: FacetKind.TimeProperty,
          name,
        })
        break
    }
  }

  return result
}

function buildFacets(facets: ListAssetsQuery['assets']['facets']): FacetDescriptor[] {
  const result: FacetDescriptor[] = []

  for (const { kind } of facets.tagKinds) {
    result.push({
      subject: FacetSubject.Asset,
      kind: FacetKind.Tag,
      name: kind,
    })
  }

  result.push(...buildPropertyFacets(FacetSubject.Asset, facets.propertyIntrospection))
  result.push(...buildPropertyFacets(FacetSubject.File, facets.files.propertyIntrospection))

  return result
}

function translatePropertyFacets(facets: FacetFilter[]): PropertyFilter[] {
  const result: PropertyFilter[] = []

  for (const facet of facets) {
    switch (facet.kind) {
      case FacetKind.TextProperty:
        result.push({ name: facet.name, textValue: facet.values.map((value) => ({ eq: value })) })
        break

      case FacetKind.TimeProperty:
        result.push({
          name: facet.name,
          timeValue: facet.values.map((value) => {
            const range = DEFAULT_TIME_RANGES.find((tr) => tr.name === value)
            return range ? { lt: range.to, gte: range.from } : {}
          }),
        })
        break
    }
  }

  return result
}

export function translateFacetQuery(params: AssetListingScreenParams): AssetFilter {
  const filter: AssetFilter = {}

  if (params.facets.length) {
    const { [FacetSubject.Asset]: assetFacets = [], [FacetSubject.File]: fileFacets = [] } =
      groupBy(params.facets, (f) => f.subject)

    const tagFacets = assetFacets.filter((f) => f.kind === FacetKind.Tag)
    if (tagFacets.length) {
      filter.withTag = tagFacets.map(({ name: kind, values: name }) => ({ kind, name }))
    }

    const withProperties = translatePropertyFacets(assetFacets)
    if (withProperties.length) {
      filter.withProperty = withProperties
    }

    if (fileFacets.length) {
      const fileFilter: AssetFileFilter = {}

      const withProperties = translatePropertyFacets(fileFacets)
      if (withProperties.length) {
        fileFilter.withProperty = withProperties
      }

      if (Object.keys(fileFilter).length) {
        filter.withFile = fileFilter
      }
    }
  }

  return filter
}

export function translateNonFacetQuery(params: AssetListingScreenParams): AssetFilter {
  const filter: AssetFilter = {}

  if (params.fulltextQuery) {
    filter.fulltext = params.fulltextQuery
  }

  if (params.tag) {
    filter.withTag = [
      {
        kind: params.tag.kind,
        name: [params.tag.name],
      },
    ]
  }

  return filter
}

function mergeFilters(...filters: AssetFilter[]): AssetFilter {
  return mergeDeep({}, ...filters)
}

export function buildQuery(...filters: AssetFilter[]): AssetFilter | undefined {
  return pickBy(mergeFilters(...filters), (v) => v && Object.keys(v).length > 0)
}

export function buildSortReq(
  params: AssetListingScreenParams,
): Array<AssetSortRequest> | undefined {
  if (!params.fulltextQuery) {
    return [
      {
        file: {
          property: {
            name: 'date',
            type: PropertyType.Time,
          },
        },
        method: params.tag ? SortMethod.Asc : SortMethod.Desc,
      },
    ]
  }
}

function getThumbnailDimensions() {
  return {
    galleryThumbnailHeight: getMaxRowHeightForUserScreen(),
    previewThumbnailWidth: window.screen.width * window.devicePixelRatio,
    previewThumbnailHeight: window.screen.height * window.devicePixelRatio,
  }
}

export function useAssetListing(params: AssetListingScreenParams): AssetListingHookResult {
  const [fetchingMorePromise, setFetchingMorePromise] = useState<Promise<void>>()

  const nonFacetFilter = useCached(() => translateNonFacetQuery(params), [params])
  const facetFilter = useCached(() => translateFacetQuery(params), [params])

  const { data, fetchMore, error } = useListAssetsQuery({
    variables: {
      query: buildQuery(nonFacetFilter, facetFilter),
      sortBy: buildSortReq(params),
      size: params.activeIndex ? params.activeIndex + 20 : 30,
      index: [params.indexName],
      ...getThumbnailDimensions(),
      acceptableThumbnailFormats: getAcceptableThumbnailFormats(),
    },
  })

  const [facets, setFacets] = useState<ResolvedFacets>()
  const [fetchMoreError, setFetchMoreError] = useState()

  useEffect(() => {
    if (data?.assets.facets && (!facets || !isEqual(facets.nonFacetFilter, nonFacetFilter))) {
      setFacets({
        nonFacetFilter,
        facets: buildFacets(data.assets.facets),
      })
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [nonFacetFilter, data?.assets.facets])

  const result: AssetListingHookResult = {
    error: error || fetchMoreError,
    facets: facets?.facets,
    fetchMore: (size?: number) => {
      if (fetchingMorePromise) {
        return Promise.resolve(fetchingMorePromise)
      } else {
        if (data!.assets.totalCount > data!.assets.items.length) {
          const skip = data?.assets.items.length

          const promise = fetchMore({
            query: ListMoreAssetsDocument,
            variables: {
              index: [params.indexName],
              query: buildQuery(nonFacetFilter, facetFilter),
              sortBy: buildSortReq(params),
              skip,
              size,
              ...getThumbnailDimensions(),
              acceptableThumbnailFormats: getAcceptableThumbnailFormats(),
            },
          })
            .then(() => {
              setFetchingMorePromise(undefined)
            })
            .catch((err) => {
              setFetchMoreError(err)
            })

          setFetchingMorePromise(promise)
          return promise
        }

        return Promise.resolve()
      }
    },
  }

  if (data?.assets) {
    result.assets = data.assets.items
    result.totalCount = data.assets.totalCount
    result.stats = {
      startDate: data.assets.stats.files.property_date.timeValue?.min
        ? new Date(data.assets.stats.files.property_date.timeValue?.min!)
        : undefined,
      endDate: data.assets.stats.files.property_date.timeValue
        ? new Date(data.assets.stats.files.property_date.timeValue.max!)
        : undefined,
    }
  }

  return result
}
