import { useListFailedAssetsQuery } from '../graphql'

export interface FailedAssetListingParams {
  indexNames?: string[]
}

export function useFailedAssets(params: FailedAssetListingParams) {
  const { data, error } = useListFailedAssetsQuery({
    variables: {
      query: {
        withFile: {
          withProcessing: [
            {
              error: {},
            },
          ],
        },
      },
      index: params.indexNames,
    },
  })

  return {
    assets: data?.assets.items,
    error,
  }
}
