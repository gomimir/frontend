import { useContext } from 'react'
import { ModalContext } from '../screens/RootScreen'

export function useModalManager() {
  return useContext(ModalContext)
}
