import { omit } from 'lodash'
import React, { useContext, useMemo, useState } from 'react'
import { assert } from '../utils/assert'

export interface ItemSelection {
  mode: 'normal' | 'inverse'
  selectedIds: { readonly [assetId: string]: boolean }
}

export function itemIsSelected(selection: ItemSelection, itemId: string): boolean {
  return selection.mode === 'normal'
    ? !!selection.selectedIds[itemId]
    : !selection.selectedIds[itemId]
}

function resolveNumSelected(selection: ItemSelection, totalCount: number): number {
  const numSelectedIds = Object.keys(selection.selectedIds).length
  return selection.mode === 'normal' ? numSelectedIds : totalCount - numSelectedIds
}

function toggleItemSelection(itemId: string): (prev: ItemSelection) => ItemSelection {
  return (prev) => {
    if (prev.selectedIds[itemId]) {
      return {
        ...prev,
        selectedIds: omit(prev.selectedIds, itemId),
      }
    } else {
      return {
        ...prev,
        selectedIds: { ...prev.selectedIds, [itemId]: true },
      }
    }
  }
}

function resetSelection(): ItemSelection {
  return { mode: 'normal', selectedIds: {} }
}

function selectAll(): ItemSelection {
  return { mode: 'inverse', selectedIds: {} }
}

const ItemSelectionContext = React.createContext<{
  selection: ItemSelection
  hasSelection: boolean
  numSelected: number
  totalCount: number
  setSelection: (newSelection: ItemSelection | ((prev: ItemSelection) => ItemSelection)) => void
  selectAll: () => void
  resetSelection: () => void
  toggleItemSelection: (itemId: string) => void
} | null>(null)

export const ItemSelectionProvider: React.FC<{ totalCount: number }> = ({
  totalCount,
  children,
}) => {
  const [selection, setSelection] = useState<ItemSelection>(resetSelection())

  const handlers = useMemo(
    () => ({
      setSelection,
      selectAll: () => {
        setSelection(selectAll())
      },
      resetSelection: () => {
        setSelection(resetSelection())
      },
      toggleItemSelection: (itemId: string) => {
        setSelection(toggleItemSelection(itemId))
      },
    }),
    [setSelection],
  )

  const ctx = useMemo(
    () => ({
      selection,
      hasSelection: selection.mode !== 'normal' || Object.keys(selection.selectedIds).length > 0,
      numSelected: resolveNumSelected(selection, totalCount),
      totalCount,
      ...handlers,
    }),
    [selection, totalCount, handlers],
  )

  return <ItemSelectionContext.Provider value={ctx}>{children}</ItemSelectionContext.Provider>
}

export function useItemSelection() {
  const ctx = useContext(ItemSelectionContext)
  assert(
    ctx,
    'Missing item selection context. Did you forget to wrap your component in ItemSelectionProvider?',
  )

  return ctx
}
