import { useContext } from 'react'
import { AppScreenContext } from '../components/AppScreenManager'

export function useScreen() {
  const ctx = useContext(AppScreenContext)
  return ctx
}
