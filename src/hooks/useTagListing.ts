import { ListTagsQuery, SortMethod, TagOrder, useListTagsQuery } from '../graphql'
import { getMaxRowHeightForUserScreen } from '../utils/galleryOptions'
import { getAcceptableThumbnailFormats } from '../utils/thumbnail'
import { TagListingParams } from './useTagListingScreen'

export type Tag = ListTagsQuery['tags'][number]

export function useTagListing(params: TagListingParams) {
  const { data, error } = useListTagsQuery({
    variables: {
      index: params.indexName,
      kind: [params.kind],
      sort: {
        by: TagOrder.LastFileDate,
        method: SortMethod.Desc,
      },
      galleryThumbnailHeight: getMaxRowHeightForUserScreen(),
      acceptableThumbnailFormats: getAcceptableThumbnailFormats(),
    },
  })

  return { data: data?.tags, error }
}
