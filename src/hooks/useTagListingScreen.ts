import { useCallback, useMemo } from 'react'
import { AppRequest, Link } from '../components/AppScreenManager'
import { createLinkFactory, Navigate } from '../utils/link'
import { useScreen } from './useScreen'

export interface TagListingParams {
  indexName: string
  kind: string
}

function paramsToLink(params: Partial<TagListingParams>): Link {
  return {
    name: 'TagListing',
    params: {
      indexName: params.indexName,
      tagKind: params.kind,
    } as { [k: string]: any },
  }
}

function reqToParams({ match }: AppRequest): TagListingParams {
  const params: TagListingParams = {
    indexName: match?.params.indexName,
    kind: match?.params.tagKind,
  }

  return params
}

export const createTagListingLink = createLinkFactory(reqToParams, paramsToLink)

export function useTagListingScreen() {
  const { request, navigate } = useScreen()
  let params = useMemo(() => reqToParams(request), [request])

  const listingNavigate: Navigate<TagListingParams> = useCallback(
    (mergeParams) => navigate(createTagListingLink(mergeParams as any)) as any,
    [navigate],
  )

  return { params, navigate: listingNavigate }
}
