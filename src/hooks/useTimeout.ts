import { useEffect, useState } from 'react'

export function useTimeout(delay: number) {
  const [fired, setFired] = useState(false)

  useEffect(() => {
    const timeoutId = setTimeout(() => {
      setFired(true)
    }, delay)

    return () => {
      if (timeoutId) {
        clearTimeout(timeoutId)
      }
    }
  }, [delay])

  return fired
}
