import React, { useContext } from 'react'
import { AssetType, FileLocation, GetIndexQuery } from '../graphql'
import { assert } from '../utils/assert'
import { IndexMessages, messagesForTagKind, TagKindMessages } from '../utils/messages'

export interface TagKind {
  name: string
  userAssignable: boolean
  messages: TagKindMessages
}

export function getTagKind(kindName: string, index: Index): TagKind {
  const config = index.tagKinds.find((k) => k.name === kindName)
  if (config) {
    return config
  }

  return {
    name: kindName,
    userAssignable: false,
    messages: messagesForTagKind(kindName),
  }
}

export interface Index {
  id: string
  name: string
  messages: IndexMessages
  assetType: AssetType

  permissions: GetIndexQuery['index']['permissions']
  stats: GetIndexQuery['index']['stats']
  tagKinds: TagKind[]
  fileLocations: FileLocation[]
}

export const IndexContext = React.createContext<Index | undefined>(undefined)

export function useIndex() {
  const ctx = useContext(IndexContext)
  assert(ctx, 'Expected Index context')
  return ctx
}
