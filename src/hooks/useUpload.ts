import { GraphQLError } from 'graphql'
import { useState } from 'react'
import { useApolloClient } from '@apollo/client'
import {
  UploadDocument,
  UploadMoreDocument,
  UploadMoreMutation,
  UploadMoreMutationVariables,
  UploadMutation,
  UploadMutationVariables,
} from '../graphql'

interface UploadChunk {
  files: File[]
  size: number
}

export interface UploadProgress {
  totalChunks: number
  finishedChunks: number
  totalBytes: number
  finishedBytes: number
}

export function useUpload() {
  // TODO: make this based on server settings
  const chunkSizeThreshold = 31 << 20 // 31 MB

  const [errors, setErrors] = useState(undefined as ReadonlyArray<GraphQLError> | undefined)
  const [jobId, setJobId] = useState(null as string | null)
  const [progress, setProgress] = useState({
    totalChunks: 0,
    finishedChunks: 0,
    totalBytes: 0,
    finishedBytes: 0,
  } as UploadProgress)

  const apolloClient = useApolloClient()

  const upload = async ({
    index,
    files,
    allFilesAreSingleAsset,
  }: {
    index: string
    allFilesAreSingleAsset: boolean
    files: File[]
  }) => {
    const chunks: UploadChunk[] = []
    let totalSize = 0

    let chunkIdx = -1

    for (const fli of files) {
      if (chunkIdx === -1 || chunks[chunkIdx].size + fli.size > chunkSizeThreshold) {
        chunks.push({
          files: [],
          size: 0,
        })

        chunkIdx = chunks.length - 1
      }

      chunks[chunkIdx].files.push(fli)
      chunks[chunkIdx].size += fli.size
      totalSize += fli.size
    }

    for (let i = 0, uploadSession: string | null = null, uploadedSize = 0; i < chunks.length; i++) {
      const chunk = chunks[i]

      const fetchOptions = {
        useUpload: true,
        onProgress: (e: ProgressEvent) => {
          setProgress({
            totalChunks: chunks.length,
            finishedChunks: i,
            totalBytes: totalSize,
            finishedBytes: uploadedSize + (e.loaded / e.total) * chunk.size,
          })
        },
      }

      if (i === 0) {
        const res = await apolloClient.mutate<UploadMutation, UploadMutationVariables>({
          mutation: UploadDocument,
          variables: {
            index,
            files: chunk.files.map((file) => ({
              file,
              properties: [{ name: 'date', timeValue: new Date(file.lastModified).toISOString() }],
            })),
            uploadMore: chunks.length > 1,
            allFilesAreSingleAsset,
          },
          context: { fetchOptions },
        })

        if (res.data?.upload?.ok === true) {
          uploadSession = res.data.upload.uploadSession
          setJobId(res.data.upload.jobId)
        } else {
          setErrors(res.errors)
          break
        }
      } else {
        const res = await apolloClient.mutate<UploadMoreMutation, UploadMoreMutationVariables>({
          mutation: UploadMoreDocument,
          variables: {
            files: chunk.files.map((file) => ({ file })),
            session: uploadSession!,
            finished: i + 1 === chunks.length,
          },
          context: { fetchOptions },
        })

        if (res.data?.uploadMore?.ok !== true) {
          setErrors(res.errors)
          break
        }
      }

      uploadedSize += chunk.size
      setProgress({
        totalChunks: chunks.length,
        finishedChunks: i + 1,
        totalBytes: totalSize,
        finishedBytes: uploadedSize,
      })
    }
  }

  return {
    errors,
    progress,
    upload,
    jobId,
  }
}
