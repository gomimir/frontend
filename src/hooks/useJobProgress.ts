import { useEffect, useState } from 'react'
import { useApolloClient } from '@apollo/client'
import {
  JobEventsDocument,
  JobEventsSubscription,
  JobEventsSubscriptionVariables,
} from '../graphql'

export type TaskSummaryByName = {
  [taskName: string]: { total: number; pending: number }
}

// TODO: replace with JobManager (see useJobs)
export function useJobProgress(jobId: string | null) {
  const apolloClient = useApolloClient()
  const [progress, setProgress] = useState({} as TaskSummaryByName)

  useEffect(() => {
    if (jobId) {
      const sub = apolloClient
        .subscribe<JobEventsSubscription, JobEventsSubscriptionVariables>({
          query: JobEventsDocument,
        })
        .subscribe({
          next: (payload) => {
            if (payload.data?.jobEvents) {
              if (payload.data.jobEvents.job.id !== jobId) {
                return
              }

              if (payload.data.jobEvents.job.taskSummary) {
                setProgress(
                  payload.data.jobEvents.job.taskSummary.reduce(
                    (byTaskName, { name, total, pending }) => {
                      byTaskName[name] = {
                        total,
                        pending,
                      }

                      return byTaskName
                    },
                    {} as TaskSummaryByName,
                  ),
                )
              }

              // if (payload.data.jobEvents.__typename === 'ProgressJobEvent') {
              //   if (payload.data.jobEvents.job.taskSummary) {
              //     const progress = payload.data.jobEvents.job.taskSummary.reduce(
              //       (totalProgress, taskSummary) => {
              //         totalProgress.totalTasks += taskSummary.total
              //         totalProgress.pendingTasks += taskSummary.pending
              //         return totalProgress
              //       },
              //       { totalTasks: 0, pendingTasks: 0 },
              //     )

              //     setProgress(progress.pendingTasks / progress.totalTasks)
              //   }
              // } else if (payload.data.jobEvents.__typename === 'FinishedJobEvent') {
              //   setProgress(1)
              //   sub.unsubscribe()
              // }
            }
          },
        })

      return () => {
        if (!sub.closed) {
          sub.unsubscribe()
        }
      }
    }
  }, [jobId, apolloClient])

  return progress
}
