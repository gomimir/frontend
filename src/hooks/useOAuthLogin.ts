import { useCallback } from 'react'
import { useToast } from '@chakra-ui/react'
import { OAuthState, useOAuthLoginMutation } from '../graphql'
import { authCallbackUrl, AUTH_SESSION_STORAGE_KEY } from '../utils/auth'

export function useOAuthLogin(state?: OAuthState) {
  const [login] = useOAuthLoginMutation()
  const toast = useToast()

  const handleLoginClick = useCallback(async () => {
    localStorage.removeItem(AUTH_SESSION_STORAGE_KEY)

    const res = await login({
      variables: {
        callbackUrl: authCallbackUrl(),
        state,
      },
    })

    if (res.data?.oauthLogin?.url) {
      window.location.href = res.data.oauthLogin.url
    } else {
      toast({
        title: 'Operation failed',
        status: 'error',
      })
    }
  }, [toast, login, state])

  return handleLoginClick
}
