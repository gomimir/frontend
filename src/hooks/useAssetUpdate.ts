import { useMemo } from 'react'
import { useToast } from '@chakra-ui/react'
import { AssetUpdate, useUpdateAssetMutation } from '../graphql'

export function useAssetUpdate() {
  const toast = useToast()
  const [executeUpdate, { loading: inProgress }] = useUpdateAssetMutation()

  const update = useMemo(() => {
    return (id: string, update: AssetUpdate) => {
      return executeUpdate({
        variables: { id, update },
        update: (cache, _result) => {
          if (update.addTags?.length) {
            // TODO: we might want to do it only for specific arguments (ie. no nameQuery)
            cache.evict({
              fieldName: 'tags',
            })
          }
        },
      })
        .then(() => {
          toast({
            title: 'Changes saved',
            description: 'Asset has been updated',
            status: 'success',
          })
        })
        .catch((err) => {
          toast({
            title: 'Update failed',
            description: err.message,
            status: 'error',
          })
        }) as Promise<void>
    }
  }, [executeUpdate, toast])

  return { update, inProgress }
}
