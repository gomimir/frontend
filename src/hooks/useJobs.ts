import { assignWith, isEqual } from 'lodash'
import { useEffect, useMemo, useState } from 'react'
import { useApolloClient } from '@apollo/client'
import {
  JobEventsDocument,
  JobEventsSubscription,
  JobEventsSubscriptionVariables,
  ListJobsDocument,
  ListJobsQuery,
  ListJobsQueryVariables,
} from '../graphql'
import { Job, TaskSummary } from '../utils/jobs'
import React from 'react'

export interface JobsInfo {
  jobs: Job[]
  numUnfinished: number
}

interface JobUpdate {
  id: string
  name?: string
  queuedAt?: string
  finishedAt?: string | null
  finished?: boolean
  taskSummary?: TaskSummary[] | null
}

function updateJob(prevState: Job | undefined, update: JobUpdate): Job {
  const prevJob: Job = prevState || ({ id: update.id, finished: false } as Job)
  const newJob = assignWith({ ...prevJob }, update, (sourceValue, newValue, key) => {
    if (newValue === undefined) {
      return sourceValue
    }

    switch (key) {
      case 'queuedAt':
      case 'finishedAt':
        return newValue ? new Date(newValue) : null

      default:
        return newValue
    }
  })

  if (update.taskSummary) {
    newJob.finished = update.taskSummary.every((tp) => tp.pending === 0)
  }

  // Prevent backward state propagation in case of race condition
  if (!newJob.finished && prevJob.finished) {
    return prevJob
  }

  // Prevent backward progress in case of race condition
  if (newJob.taskSummary && prevJob.taskSummary) {
    for (const tp of newJob.taskSummary) {
      const prevTp = prevJob.taskSummary.find((prevTp) => prevTp.name === tp.name)
      if (prevTp) {
        if (
          ((prevTp.pending < tp.pending || prevTp.failed < tp.failed) &&
            prevTp.total === tp.total) ||
          prevTp.total > tp.total
        ) {
          return prevJob
        }
      }
    }
  }

  // Note: we might want to replace isEqual with something more efficient given our structure
  return isEqual(prevJob, newJob) ? prevJob : newJob
}

function sortJobs(jobs: Job[]) {
  jobs.sort((a, b) => {
    if (a.finished === b.finished) {
      if (a.queuedAt === b.queuedAt) {
        return 0
      } else {
        return a.queuedAt < b.queuedAt ? 1 : -1
      }
    } else {
      return a.finished ? 1 : -1
    }
  })
}

function insertJob(jobs: Job[], prevIndex: number, job: Job) {
  if (prevIndex > -1) {
    const prevJob = jobs[prevIndex]
    jobs.splice(prevIndex, 1, job)
    if (prevJob.finished !== job.finished || prevJob.queuedAt !== job.queuedAt) {
      sortJobs(jobs)
    }
  } else {
    jobs.unshift(job)
    sortJobs(jobs)
  }
}

function updateState<T extends JobsInfo>(prevState: T, jobUpdates: JobUpdate[]) {
  let updatedState = prevState

  for (const update of jobUpdates) {
    const prevJobIndex = prevState.jobs.findIndex((j) => j.id === update.id)
    const prevJob = prevJobIndex > -1 ? prevState.jobs[prevJobIndex] : undefined

    const updatedJob = updateJob(prevJob, update)
    if (updatedJob !== prevJob) {
      if (updatedState === prevState) {
        updatedState = { ...prevState }
      }

      if (updatedState.jobs === prevState.jobs) {
        updatedState.jobs = [...prevState.jobs]
      }

      insertJob(updatedState.jobs, prevJobIndex, updatedJob)

      if (updatedJob.finished !== prevJob?.finished) {
        updatedState.numUnfinished = updatedState.jobs.reduce(
          (sum, j) => (j.finished ? sum : sum + 1),
          0,
        )
      }
    }
  }

  return updatedState
}

type JobSubscriber = () => void
type UnsubscribeCallback = () => void

export interface JobManager {
  onJobFinished(jobId: string, callback: JobSubscriber): UnsubscribeCallback
}

class JobManagerImpl implements JobManager {
  private subscribers = new Map<string, Set<JobSubscriber>>()

  onJobFinished = (jobId: string, callback: JobSubscriber) => {
    const jobSubs = this.ensureJobSubcribersSet(jobId)

    let unsubscribe: UnsubscribeCallback

    const wrapped = () => {
      callback()
      unsubscribe()
    }

    unsubscribe = () => {
      jobSubs.delete(wrapped)
    }

    jobSubs.add(wrapped)

    return unsubscribe
  }

  handleJobFinished = (jobId: string) => {
    const jobSubs = this.subscribers.get(jobId)
    if (jobSubs) {
      for (const callback of jobSubs) {
        callback()
      }
    }
  }

  private ensureJobSubcribersSet(jobId: string) {
    let jobSubs = this.subscribers.get(jobId)
    if (!jobSubs) {
      jobSubs = new Set<JobSubscriber>()
      this.subscribers.set(jobId, jobSubs)
      return jobSubs
    } else {
      return jobSubs
    }
  }
}

export function useJobs() {
  const apolloClient = useApolloClient()
  const [{ jobs, numUnfinished, hasBeenLoaded }, setState] = useState<
    JobsInfo & { hasBeenLoaded: boolean }
  >({
    jobs: [],
    numUnfinished: 0,
    hasBeenLoaded: false,
  })

  const manager = useMemo(() => new JobManagerImpl(), [])

  // TODO: query error handling
  useEffect(() => {
    apolloClient
      .query<ListJobsQuery, ListJobsQueryVariables>({
        query: ListJobsDocument,
        errorPolicy: 'all',
      })
      .then((res) => {
        if (res.data) {
          setState((prev) => {
            const newState = updateState(prev, res.data.jobs)
            if (!newState.hasBeenLoaded) {
              return { ...newState, hasBeenLoaded: true }
            } else {
              return newState
            }
          })
        }
      })
      .catch((err) => {
        console.error(err)
      })

    // TODO: subscription error handling
    const sub = apolloClient
      .subscribe<JobEventsSubscription, JobEventsSubscriptionVariables>({
        query: JobEventsDocument,
        errorPolicy: 'all',
      })
      .subscribe({
        error: (error) => {
          console.error(error)
        },
        next: (payload) => {
          if (payload.data?.jobEvents.job.taskSummary) {
            const update: JobUpdate = {
              id: payload.data.jobEvents.job.id,
              taskSummary: payload.data.jobEvents.job.taskSummary,
            }

            if (payload.data.jobEvents.__typename === 'NewJobEvent') {
              update.name = payload.data.jobEvents.job.name
              update.queuedAt = payload.data.jobEvents.job.queuedAt
            } else if (payload.data.jobEvents.__typename === 'FinishedJobEvent') {
              update.finishedAt = payload.data.jobEvents.job.finishedAt
              setTimeout(() => manager.handleJobFinished(update.id))
            }

            setState((prev) => {
              return updateState(prev, [update])
            })
          }
        },
      })

    return () => {
      if (!sub.closed) {
        sub.unsubscribe()
      }
    }
  }, [apolloClient, manager])

  const jobsInfo: JobsInfo | undefined = useMemo(
    () =>
      hasBeenLoaded
        ? {
            jobs,
            numUnfinished,
          }
        : undefined,
    [jobs, numUnfinished, hasBeenLoaded],
  )

  // useEffect(() => console.log(jobsInfo), [jobsInfo])

  return {
    jobsInfo,
    jobManager: manager as JobManager,
  }
}

export const JobManagerContext = React.createContext<JobManager>(null!)
