import { useCallback, useMemo } from 'react'
import { AppRequest, Link } from '../components/AppScreenManager'
import { FACET_KINDS_BY_SUBJECT } from '../facets/constants'
import { facetDescKey, FacetKind, FacetSubject } from '../facets/FacetDescriptor'
import { FacetFilter } from '../facets/FacetFilter'
import { createLinkFactory, Navigate } from '../utils/link'
import { useScreen } from './useScreen'

export interface AssetListingScreenParams {
  indexName: string
  activeIndex?: number
  fulltextQuery?: string
  tag?: { kind: string; name: string }
  viewMode: 'gallery' | 'list' | 'preview'
  facets: FacetFilter[]
}

function paramsToLink(params: Partial<AssetListingScreenParams>): Link {
  const link = {
    name: 'AssetListing',
    params: {
      indexName: params.indexName,
      q: params.fulltextQuery,
      viewMode: params.viewMode,
    } as { [k: string]: any },
  }

  if (params.tag) {
    link.params.tagKind = params.tag.kind
    link.params.tagName = params.tag.name
  }

  if (params.activeIndex !== undefined) {
    link.params.activeIndex = params.activeIndex + 1
  }

  if (params.facets) {
    for (const facet of params.facets) {
      const key = facetDescKey(facet)
      for (const value of facet.values) {
        if (link.params[key]) {
          link.params[key] = Array.isArray(link.params[key])
            ? [...link.params[key], value]
            : [link.params[key], value]
        } else {
          link.params[key] = value
        }
      }
    }
  }

  return link
}

function reqToParams({ match }: AppRequest): AssetListingScreenParams {
  const params: AssetListingScreenParams = {
    indexName: match?.params.indexName,
    fulltextQuery: match?.queryParams.q,
    viewMode: match?.params.viewMode || 'gallery',
    facets: [],
  }

  if (match?.params.tagKind && match.params.tagName) {
    params.tag = {
      kind: match.params.tagKind,
      name: match.params.tagName,
    }
  }

  if (match?.params.activeIndex !== undefined) {
    params.activeIndex = match.params.activeIndex - 1
  }

  for (const key in match?.queryParams) {
    for (const subject in FACET_KINDS_BY_SUBJECT) {
      const subjectKinds = FACET_KINDS_BY_SUBJECT[subject as keyof typeof FACET_KINDS_BY_SUBJECT]

      for (const kind of subjectKinds) {
        const prefix = `${subject}${kind}_`

        if (key.startsWith(prefix)) {
          const name = key.substr(prefix.length)
          const values = Array.isArray(match?.queryParams[key])
            ? match?.queryParams[key]
            : [match?.queryParams[key]]

          params.facets.push({
            subject: subject as FacetSubject,
            kind: kind as FacetKind,
            name,
            values,
          })
        }
      }
    }
  }

  return params
}

export const createAssetListingLink = createLinkFactory(reqToParams, paramsToLink)

export function useAssetListingScreen() {
  const { request, navigate } = useScreen()

  const params = useMemo(() => (request ? reqToParams(request) : null), [request])
  const createLink = createAssetListingLink

  const listingNavigate: Navigate<AssetListingScreenParams> = useCallback(
    (mergeParams) => navigate(createLink(mergeParams as any)) as any,
    [navigate, createLink],
  )

  return {
    params,
    createLink,
    navigate: listingNavigate,
  }
}
