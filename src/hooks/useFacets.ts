import { useMemo } from 'react'
import { useQuery } from '@apollo/client'
import { DEFAULT_TIME_RANGES, FACET_CONFIG } from '../facets/constants'
import { FacetConfig, resolveConfig } from '../facets/FacetConfig'
import { facetDescKey, FacetDescriptor, FacetKind, FacetSubject } from '../facets/FacetDescriptor'
import { FacetFilter, facetFilterContains } from '../facets/FacetFilter'
import { buildFacetQuery, FacetQueryResult, FacetQueryVariables } from '../facets/graphql'
import { TagFacetValuesFragment, TextPropertyValuesFragment, TimeRangeBucket } from '../graphql'
import { buildQuery, translateFacetQuery, translateNonFacetQuery } from '../hooks/useAssetListing'
import { AssetListingScreenParams } from '../hooks/useAssetListingScreen'
import { isSuperRange } from '../utils/timeRange'

export interface ResolvedFacetItem {
  value: string
  label: string
  numAssets: number
  active: boolean
}

export interface ResolvedFacetsWithData {
  key: string
  desc: FacetDescriptor
  config: FacetConfig
  data: ResolvedFacetItem[] | undefined
}

/** Resolves received facet introspection of the current query against the facet config */
function resolveFacets(availableFacets: FacetDescriptor[], params: AssetListingScreenParams) {
  const resolved: Array<{ desc: FacetDescriptor; config: FacetConfig }> = []

  for (const desc of availableFacets) {
    const config = resolveConfig(desc, FACET_CONFIG)

    // Skip facets for already specified non-facet filters
    if (params.tag) {
      if (desc.kind === FacetKind.Tag && desc.subject === FacetSubject.Asset) {
        if (desc.name === params.tag.kind) {
          continue
        }
      }
    }

    if (config.show) {
      resolved.push({ desc, config })
    }
  }

  resolved.sort((a, b) => a.config.order - b.config.order)

  return resolved
}

function mapFacetData(
  desc: FacetDescriptor,
  data: any,
  activeFilters: FacetFilter[],
): Array<ResolvedFacetItem & { isRedundant?: boolean }> | undefined {
  if (!data) {
    return undefined
  }

  if (desc.kind === FacetKind.Tag) {
    const tagFacetData = data as TagFacetValuesFragment[]
    return tagFacetData.map(({ name, numAssets }) => ({
      label: name,
      value: name,
      numAssets,
      active: facetFilterContains(activeFilters, desc, name),
    }))
  } else if (desc.kind === FacetKind.TextProperty) {
    const textFacetData = data as { textValues: TextPropertyValuesFragment[] }
    return textFacetData.textValues.map(({ value, numAssets }) => ({
      label: value,
      value,
      numAssets,
      active: facetFilterContains(activeFilters, desc, value),
    }))
  } else if (desc.kind === FacetKind.TimeProperty) {
    const timeRangeFacetData = data as { timeRanges: TimeRangeBucket[] }
    return timeRangeFacetData.timeRanges.map(({ range, numAssets }, i) => ({
      value: range!.name,
      label: DEFAULT_TIME_RANGES.find((tr) => tr.name === range!.name)?.label || range!.name,
      numAssets,
      active: facetFilterContains(activeFilters, desc, range!.name),
      range,
      // Omit ranges which fully contain another, smaller range and do not have any additional assets
      isRedundant: timeRangeFacetData.timeRanges.some(
        (r, j) =>
          j !== i &&
          r.numAssets === numAssets &&
          isSuperRange(
            range?.from?.value,
            range?.to?.value,
            r.range?.from?.value,
            r.range?.to?.value,
          ),
      ),
    }))
  }
}

/** Combines resolved introspection with the data and filters the results */
function resolveFacetsWithData(
  facets: Array<{ desc: FacetDescriptor; config: FacetConfig }>,
  totalCount: number | undefined,
  data: FacetQueryResult | undefined,
  activeFilters: FacetFilter[],
): { resolvedFacets: ResolvedFacetsWithData[]; hasUsefulFacets: boolean } {
  let hasUsefulFacets = false
  const resolvedFacets = facets.map(({ desc, config }) => {
    const key = facetDescKey(desc)
    const subjectData =
      desc.subject === FacetSubject.Asset ? data?.assets.facets : data?.assets.facets.files

    const mapped = mapFacetData(desc, subjectData?.[key], activeFilters)?.filter(
      ({ numAssets, active, isRedundant }) => {
        if (active) {
          return true
        }

        if (numAssets === 0 || numAssets === totalCount) {
          return false
        }

        if (isRedundant) {
          return false
        }

        return true
      },
    )

    if (mapped && mapped.length > 0) {
      hasUsefulFacets = true
    }

    return { key, desc, config, data: mapped }
  })

  return { resolvedFacets, hasUsefulFacets }
}

export function useFacets(
  availableFacets: FacetDescriptor[],
  totalCount: number,
  params: AssetListingScreenParams,
) {
  // Resolve available facets against the config
  const resolvedFacets = useMemo(
    () => resolveFacets(availableFacets, params),
    [availableFacets, params],
  )

  // Build query for the facet data
  const facetQuery = useMemo(
    () =>
      buildFacetQuery(
        params.indexName,
        resolvedFacets.map((f) => f.desc),
      ),
    [params.indexName, resolvedFacets],
  )

  // Facet data query
  const { data } = useQuery<FacetQueryResult, FacetQueryVariables>(facetQuery.query, {
    variables: {
      query: buildQuery(translateNonFacetQuery(params), translateFacetQuery(params)),
      ...facetQuery.variables,
    },
    // We skip it if there are no facets available for the current query
    skip: resolvedFacets.length === 0,
    fetchPolicy: 'cache-first',
  })

  // Combine resolved facet configuration with the data
  const resolvedWithData = useMemo(
    () => resolveFacetsWithData(resolvedFacets, totalCount, data, params.facets),
    [resolvedFacets, totalCount, data, params],
  )

  return {
    ...resolvedWithData,
    facetDataHasBeenFetched: !!data,
  }
}
