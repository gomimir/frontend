import { Router, T } from 'chobot'
import { AssetListingScreen } from './screens/AssetListingScreen'
import { FailedAssetsScreen } from './screens/FailedAssetsScreen'
import { HomepageScreen } from './screens/HomepageScreen'
import { IndexCreateScreen } from './screens/IndexCreateScreen'
import { IndexEditScreen } from './screens/IndexEditScreen'
import { IndexListingScreen } from './screens/IndexListingScreen'
import { IndexScreen } from './screens/IndexScreen'
import { JobDetailScreen } from './screens/JobDetailScreen'
import { NonIndexScreen } from './screens/NonIndexScreen'
import { OAuthCallbackScreen } from './screens/OAuthCallbackScreen'
import { RootScreen } from './screens/RootScreen'
import { TagListingScreen } from './screens/TagListingScreen'
import { AppRoute } from './utils/AppRoute'

export const router = new Router(
  new AppRoute({
    children: [
      new AppRoute({
        name: 'OAuthCallback',
        path: 'oauth/callback',
        component: OAuthCallbackScreen,
      }),
      new AppRoute({
        component: RootScreen,
        children: [
          new AppRoute({
            name: 'Homepage',
            path: '.',
            component: HomepageScreen,
          }),
          new AppRoute({
            component: NonIndexScreen,
            children: [
              new AppRoute({
                name: 'JobDetail',
                path: 'jobs/:jobId',
                component: JobDetailScreen,
                params: {
                  jobId: T.str().rx(/[a-z0-9.~_-]+/i),
                },
              }),
              new AppRoute({
                path: 'indices[/]',
                children: [
                  new AppRoute({
                    name: 'IndexListing',
                    path: '.',
                    component: IndexListingScreen,
                  }),
                  new AppRoute({
                    name: 'IndexCreate',
                    path: 'create',
                    component: IndexCreateScreen,
                  }),
                  new AppRoute({
                    name: 'IndexEdit',
                    path: ':index/edit',
                    component: IndexEditScreen,
                  }),
                ],
              }),
            ],
          }),
          new AppRoute({
            path: ':indexName',
            params: {
              indexName: T.str().rx(/[a-z0-9.~_-]+/i),
            },
            component: IndexScreen,
            children: [
              new AppRoute({
                name: 'FailedAssets',
                path: 'failed-assets',
                component: FailedAssetsScreen,
              }),
              new AppRoute({
                name: 'AssetListing',
                path: 'assets[/:viewMode[/:activeIndex]]',
                component: AssetListingScreen,
                params: {
                  viewMode: T.dict({ list: 'list', gallery: 'gallery', preview: 'preview' }),
                  activeIndex: T.int().gt(0),
                },
              }),
              new AppRoute({
                name: 'AssetListing',
                path: 'tags/:tagKind/:tagName[/:viewMode[/:activeIndex]]',
                component: AssetListingScreen,
                params: {
                  viewMode: T.dict({ list: 'list', gallery: 'gallery', preview: 'preview' }),
                  activeIndex: T.int().gt(0),
                },
              }),
              new AppRoute({
                name: 'TagListing',
                path: 'tags/:tagKind',
                component: TagListingScreen,
              }),
            ],
          }),
        ],
      }),
    ],
  }),
)
