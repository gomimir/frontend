import { Center, Stack, Text } from '@chakra-ui/react'
import { ContentWrapper } from '../components/ContentWrapper'
import { EmptyIndex } from '../components/EmptyIndex'
import { QueryError } from '../components/QueryError'
import { TagListingGallery } from '../components/TagListingGallery'
import { getTagKind, TagKind, useIndex } from '../hooks/useIndex'
import { useTagListing } from '../hooks/useTagListing'
import { TagListingParams, useTagListingScreen } from '../hooks/useTagListingScreen'
import { useTitle } from '../hooks/useTitle'

const TagListing: React.FC<{
  tagKind: TagKind
  params: TagListingParams
}> = ({ params, tagKind }) => {
  const { data: tags, error } = useTagListing(params)

  if (error) {
    return <QueryError error={error} />
  }

  if (!tags) {
    return null
  }

  if (tags.length === 0) {
    return (
      <Text fontSize="md" mb={6}>
        {tagKind.messages.noTags} created yet
      </Text>
    )
  }

  return <TagListingGallery tags={tags} onActiveItemAction={() => {}} />
}

export const TagListingScreen: React.FC = () => {
  const index = useIndex()
  const { params } = useTagListingScreen()

  const tagKind = getTagKind(params.kind, index)
  const title = tagKind.messages.allTags

  useTitle(title)

  if (index.stats.count === 0) {
    return (
      <ContentWrapper pt={0}>
        <Center height="100vh">
          <EmptyIndex index={index} />
        </Center>
      </ContentWrapper>
    )
  }

  return (
    <ContentWrapper>
      <Stack>
        <Text textStyle="h1" pb={0}>
          {title}
        </Text>
        <TagListing key={params.kind} params={params} tagKind={tagKind} />
      </Stack>
    </ContentWrapper>
  )
}
