import React, { useState, useCallback } from 'react'
import { AuthRefresher } from '../components/AuthRefresher'
import { Login } from '../components/Login'
import { QueryError } from '../components/QueryError'
import { AuthType, useSystemInfoQuery } from '../graphql'
import { JobManagerContext, useJobs } from '../hooks/useJobs'

export const ModalContext = React.createContext(
  null! as (callback: (onClose: () => void) => React.ReactElement) => void,
)

const AuthenticatedScreenContent: React.FC = ({ children }) => {
  const { jobManager } = useJobs()
  const [modal, setModal] = useState<React.ReactElement | null>(null)
  const openModal = useCallback((callback: (onClose: () => void) => React.ReactElement) => {
    setModal(callback(() => setModal(null)))
  }, [])

  return (
    <JobManagerContext.Provider value={jobManager}>
      <ModalContext.Provider value={openModal}>
        {children}
        {modal}
      </ModalContext.Provider>
    </JobManagerContext.Provider>
  )
}

export const RootScreen: React.FC = ({ children }) => {
  const { data, error } = useSystemInfoQuery({
    fetchPolicy: 'cache-first',
  })

  if (error) {
    return <QueryError error={error} />
  }

  if (!data) {
    return null
  }

  if (data.serverInfo.auth === AuthType.Oidc) {
    if (!data.currentUser.authenticated) {
      return <Login auth={data.serverInfo.auth} />
    } else {
      return (
        <AuthRefresher auth={data.serverInfo.auth} user={data.currentUser}>
          <AuthenticatedScreenContent>{children}</AuthenticatedScreenContent>
        </AuthRefresher>
      )
    }
  }

  return <AuthenticatedScreenContent>{children}</AuthenticatedScreenContent>
}
