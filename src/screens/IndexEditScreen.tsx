import { Box, useToast } from '@chakra-ui/react'
import { isEqual } from 'lodash'
import { useCallback } from 'react'
import { ErrorIndexNotFound } from '../components/ErrorContent'
import { IndexForm } from '../components/IndexForm'
import { createFormData } from '../components/IndexForm/data'
import { QueryError } from '../components/QueryError'
import { IndexInput, useGetIndexDetailQuery, useUpdateIndexMutation } from '../graphql'
import { useScreen } from '../hooks/useScreen'
import { useTitle } from '../hooks/useTitle'
import { assert } from '../utils/assert'

export const IndexEditScreen: React.FC = () => {
  const { request, navigate } = useScreen()
  const [updateIndex] = useUpdateIndexMutation()
  const toast = useToast()

  const index = request.match?.params.index
  assert(index, 'Expected index parameter')

  const handleSubmit = useCallback(
    (data: IndexInput) => {
      updateIndex({
        variables: {
          id: index,
          index: data,
        },
        update: (cache, _result) => {
          cache.evict({
            fieldName: 'indices',
          })

          cache.evict({
            id: cache.identify({ __typename: 'Index', id: index }),
          })
        },
        errorPolicy: 'all',
      }).then((res) => {
        if (res.data?.indexUpdate.ok) {
          toast({
            status: 'success',
            title: 'Index updated',
          })
          navigate({
            name: 'IndexListing',
          })
        } else {
          toast({
            status: 'error',
            title: 'Operation failed',
          })
        }
      })
    },
    [updateIndex, toast, navigate, index],
  )

  const { data, error } = useGetIndexDetailQuery({
    variables: {
      id: index,
    },
    fetchPolicy: 'cache-first',
  })

  useTitle(`Edit ${data?.index.name}` || 'Edit index')

  if (error) {
    if (
      error.graphQLErrors.some(
        (gqlErr) => isEqual(gqlErr.path, ['index']) && gqlErr.extensions?.code === 'NOT_FOUND',
      )
    ) {
      return <ErrorIndexNotFound />
    }

    return <QueryError error={error} />
  }

  if (!data) {
    return null
  }

  return (
    <Box p={50} w="100%" maxW="1024px" mx="auto">
      <IndexForm initialData={createFormData(data.index)} onSubmit={handleSubmit} />
    </Box>
  )
}
