import React from 'react'
import { FaEllipsisV } from 'react-icons/fa'
import { FiFileText, FiImage, FiPlusCircle } from 'react-icons/fi'
import {
  Box,
  Button,
  Center,
  chakra,
  Circle,
  Flex,
  Grid,
  Icon,
  IconButton,
  Menu,
  MenuButton,
  MenuItem,
  MenuList,
  SimpleGrid,
  useColorModeValue,
  useDisclosure,
  VStack,
} from '@chakra-ui/react'
import { CrawlModal } from '../components/CrawlModal'
import { QueryError } from '../components/QueryError'
import { AssetType, IndexFragment, Operation, useListIndicesQuery } from '../graphql'
import { createAssetListingLink } from '../hooks/useAssetListingScreen'
import { useScreen } from '../hooks/useScreen'
import { createTagListingLink } from '../hooks/useTagListingScreen'
import { formatBytes } from '../utils/format'
import { PurgeAssetsModal } from '../components/PurgeAssetsModal'
import { CreateIndex } from '../components/IndexCreate'
import { DeleteIndexModal } from '../components/DeleteIndexModal'
import { UploadArchiveFromUrlModal } from '../components/UploadArchiveFromUrlModal'
import { useTitle } from '../hooks/useTitle'

function indexLandingLink(index: IndexFragment) {
  // TODO: make this configurable

  if (index.assetType === AssetType.Photo && index.tagKinds.some((k) => k.name === 'album')) {
    return createTagListingLink({
      indexName: index.id,
      kind: 'album',
    })
  } else {
    return createAssetListingLink({
      indexName: index.id,
    })
  }
}

export function indexIcon(index: { assetType: AssetType }) {
  if (index.assetType === AssetType.Photo) {
    return FiImage
  }

  return FiFileText
}

const CrawlMenuItem: React.FC<{ index: IndexFragment }> = ({ index }) => {
  const { isOpen, onOpen, onClose } = useDisclosure()

  return (
    <>
      <MenuItem onClick={onOpen}>Crawl</MenuItem>
      {isOpen && <CrawlModal index={index} onClose={onClose} />}
    </>
  )
}

const UploadArchiveMenuItem: React.FC<{ index: IndexFragment }> = ({ index }) => {
  const { isOpen, onOpen, onClose } = useDisclosure()

  return (
    <>
      <MenuItem onClick={onOpen}>Upload archive from URL</MenuItem>
      {isOpen && <UploadArchiveFromUrlModal indexId={index.id} onClose={onClose} />}
    </>
  )
}

const PurgeAssetsMenuItem: React.FC<{ index: IndexFragment }> = ({ index }) => {
  const { isOpen, onOpen, onClose } = useDisclosure()

  return (
    <>
      <MenuItem onClick={onOpen}>Purge assets</MenuItem>
      {isOpen && <PurgeAssetsModal index={index} onClose={onClose} />}
    </>
  )
}

const DeleteIndexMenuItem: React.FC<{ index: IndexFragment }> = ({ index }) => {
  const { isOpen, onOpen, onClose } = useDisclosure()

  return (
    <>
      <MenuItem onClick={onOpen}>Delete index</MenuItem>
      {isOpen && <DeleteIndexModal index={index} onClose={onClose} />}
    </>
  )
}

const IndexMenu: React.FC<{ index: IndexFragment }> = ({ index }) => {
  const { navigate } = useScreen()

  const menuItems: React.ReactElement[] = []

  if (index.permissions.assetOperations.includes(Operation.Delete)) {
    menuItems.push(
      <MenuItem onClick={() => navigate({ name: 'IndexEdit', params: { index: index.id } })}>
        Edit configuration
      </MenuItem>,
    )
  }

  if (index.permissions.indexOperations.includes(Operation.Crawl)) {
    menuItems.push(<CrawlMenuItem index={index} />)
  }

  if (index.permissions.assetCollectionOperations.includes(Operation.Upload)) {
    menuItems.push(<UploadArchiveMenuItem index={index} />)
  }

  if (index.permissions.assetOperations.includes(Operation.Delete)) {
    menuItems.push(<PurgeAssetsMenuItem index={index} />)
  }

  if (index.permissions.indexOperations.includes(Operation.Delete)) {
    menuItems.push(<DeleteIndexMenuItem index={index} />)
  }

  return menuItems.length ? (
    <>
      <Menu>
        <MenuButton
          as={IconButton}
          size="sm"
          variant="ghost"
          icon={<FaEllipsisV />}
          aria-label="More actions"
        />
        {React.createElement(MenuList, {}, ...menuItems)}
      </Menu>
    </>
  ) : null
}

const IndexCard: React.FC<{ index: IndexFragment }> = ({ index }) => {
  const { navigate } = useScreen()

  const bgColor2 = useColorModeValue('white', 'gray.700')
  const bgColorCover = useColorModeValue('gray.100', 'gray.600')
  const bgColorIcon = useColorModeValue('gray.200', 'gray.700')
  const iconColor = useColorModeValue('gray.600', 'gray.200')
  const textColor = useColorModeValue('gray.800', 'white')
  const textColor2 = useColorModeValue('gray.600', 'gray.400')

  const numAssets = index.stats?.count || 0
  const numFiles = index.stats?.files.count || 0
  const fileSize =
    (index.stats?.files.fileSize.sum || 0) + (index.stats?.files.sidecars.fileSize.sum || 0)

  return (
    <Grid
      templateColumns="1fr 2fr"
      gap={6}
      bg={bgColor2}
      shadow="lg"
      rounded="lg"
      overflow="hidden"
    >
      <Center bgColor={bgColorCover}>
        <Circle bgColor={bgColorIcon} size="64px">
          <Icon as={indexIcon(index)} w="26px" h="26px" color={iconColor} />
        </Circle>
      </Center>
      <Box p={{ base: 4, md: 4 }}>
        <Flex mt={3} alignItems="center" justifyContent="space-between">
          <chakra.h1 fontSize="2xl" fontWeight="bold" color={textColor} textTransform="capitalize">
            {index.name}
          </chakra.h1>
          <IndexMenu index={index} />
        </Flex>
        <chakra.p mt={2} fontSize="sm" color={textColor2}>
          {numAssets !== 1 ? `${numAssets}\xA0assets` : `${numAssets}\xA0asset`},{' '}
          {numFiles !== 1 ? `${numFiles}\xA0files` : `${numFiles}\xA0file`}
        </chakra.p>

        <chakra.p fontSize="sm" color={textColor2}>
          {formatBytes(fileSize, 2)}
        </chakra.p>

        <Flex mt={3} alignItems="center" justifyContent="flex-end">
          <Button
            size="sm"
            fontSize="xs"
            fontWeight="bold"
            textTransform="uppercase"
            onClick={() => navigate(indexLandingLink(index))}
          >
            Show assets
          </Button>
        </Flex>
      </Box>
    </Grid>
  )
}

const CreateIndexCard: React.FC = () => {
  const { navigate } = useScreen()
  const borderColor = useColorModeValue('gray.200', 'gray.600')
  const iconColor = useColorModeValue('gray.300', 'gray.600')

  return (
    <Center p={6} border="3px" borderStyle="dashed" borderColor={borderColor} rounded="lg">
      <VStack spacing={4}>
        <Icon as={FiPlusCircle} w="48px" h="48px" color={iconColor} />
        <Button onClick={() => navigate({ name: 'IndexCreate' })}>Create new index</Button>
      </VStack>
    </Center>
  )
}

export const IndexListingScreen: React.FC = () => {
  useTitle('Mimir')
  const bgColor = useColorModeValue('#F9FAFB', 'transparent')
  const { data, error } = useListIndicesQuery({
    fetchPolicy: 'cache-first',
  })

  const indices = data?.indices

  if (error) {
    return <QueryError error={error} />
  }

  if (!indices) {
    return null
  }

  if (indices.length === 0) {
    return <CreateIndex firstTime={true} />
  }

  // Note: there is a redirect in useEffect
  return (
    <Box minH="100vh" bg={bgColor}>
      <Box p={50} w="100%" maxW="1024px" mx="auto">
        <SimpleGrid minChildWidth="380px" spacing={10} w="100%">
          {indices.map((index) => (
            <IndexCard key={index.id} index={index} />
          ))}
          <CreateIndexCard />
        </SimpleGrid>
      </Box>
    </Box>
  )
}
