import { useEffect } from 'react'
import { useScreen } from '../hooks/useScreen'

export const HomepageScreen: React.FC = () => {
  const { navigate } = useScreen()

  useEffect(() => {
    navigate({ name: 'IndexListing' })
  }, [navigate])

  return null
}
