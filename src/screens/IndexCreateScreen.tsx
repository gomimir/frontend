import { CreateIndex } from '../components/IndexCreate'
import { useTitle } from '../hooks/useTitle'

export const IndexCreateScreen: React.FC = () => {
  useTitle(`New index`)

  return <CreateIndex />
}
