import { NavigationHome } from '../components/NavigationHome'
import { NavigationLayout } from '../components/NavigationLayout'

export const NonIndexScreen: React.FC = ({ children }) => {
  return <NavigationLayout navigationContent={<NavigationHome />}>{children}</NavigationLayout>
}
