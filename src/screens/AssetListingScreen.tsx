import React, { useCallback, useContext } from 'react'
import { useHotkeys } from 'react-hotkeys-hook'
import { Box, Center, Flex, IconButton, useColorModeValue } from '@chakra-ui/react'
import { AssetListingGallery } from '../components/AssetListingGallery'
import {
  AssetListingHeader,
  assetListingScreenTitle,
  HEADER_HEIGHT_PX,
} from '../components/AssetListingHeader'
import { PreviewPhoto } from '../components/PreviewPhoto'
import { QueryError } from '../components/QueryError'
import { EmptySearch } from '../components/EmptySearch'
import { FacetDescriptor } from '../facets/FacetDescriptor'
import { ActiveItemActionKind, ActiveItemActionUnion } from '../helpers/ActiveItem'
import { Asset, AssetListingStats, useAssetListing } from '../hooks/useAssetListing'
import { AssetListingScreenParams, useAssetListingScreen } from '../hooks/useAssetListingScreen'
import { ResolvedFacetsWithData, useFacets } from '../hooks/useFacets'
import { Index, useIndex } from '../hooks/useIndex'
import { assert } from '../utils/assert'
import { Navigate } from '../utils/link'
import { EmptyIndex } from '../components/EmptyIndex'
import { ContentWrapper } from '../components/ContentWrapper'
import { SidebarLayout, useSidebar } from '../components/SidebarLayout'
import { useTitle } from '../hooks/useTitle'
import { ContentPaddingContext } from '../components/ContentWidthProvider'
import { FiArrowLeft } from 'react-icons/fi'
import { AssetInspector } from '../components/AssetInspector'
import { AssetType } from '../graphql'
import { PreviewDocument } from '../components/PreviewDocument'
import { ItemSelectionProvider, useItemSelection } from '../hooks/useItemSelection'

interface Props {
  index: Index
  assets: Asset[]
  stats: AssetListingStats
  totalCount: number
  fetchMore: (num?: number) => Promise<void>
  params: AssetListingScreenParams
  navigate: Navigate<AssetListingScreenParams>
}

const PreviewOverlay: React.FC<{
  assetType: AssetType
  activeItemIndex?: number
  assets: Asset[]
  onActiveItemAction: (action: ActiveItemActionUnion) => void
  onClose: () => void
}> = ({ activeItemIndex, assets, onActiveItemAction, onClose, assetType }) => {
  const { left, right } = useContext(ContentPaddingContext)
  const bgColor = useColorModeValue('white', 'gray.800')

  return (
    <>
      {activeItemIndex !== undefined && (
        <Flex
          direction="column"
          position="fixed"
          top={0}
          left={0}
          width={`${left}px`}
          h={`${HEADER_HEIGHT_PX}px`}
          bgColor={bgColor}
          zIndex={1}
          alignItems="center"
          justifyContent="center"
        >
          <IconButton
            icon={<FiArrowLeft size={22} />}
            variant="ghost"
            color="gray.500"
            aria-label="Close preview"
            onClick={onClose}
          />
        </Flex>
      )}
      <Box
        position="fixed"
        left={0}
        right={right}
        top={'44px'}
        bottom={0}
        bgColor="white"
        zIndex={1}
        visibility={activeItemIndex !== undefined ? 'visible' : 'hidden'}
        paddingRight={right > 0 ? 3 : 0}
      >
        {activeItemIndex !== undefined &&
          (assetType === AssetType.Photo ? (
            <PreviewPhoto
              assets={assets}
              activeItemIndex={activeItemIndex}
              onActiveItemAction={onActiveItemAction}
            />
          ) : (
            <PreviewDocument asset={assets[activeItemIndex]} />
          ))}
      </Box>
    </>
  )
}

const AssetListingScreenWithFacets: React.FC<
  Props & {
    resolvedFacets: ResolvedFacetsWithData[]
    hasUsefulFacets: boolean
  }
> = ({
  index,
  assets,
  stats,
  resolvedFacets,
  hasUsefulFacets,
  totalCount,
  fetchMore,
  params,
  navigate,
}) => {
  const handleActiveItemAction = useCallback(
    (action: ActiveItemActionUnion) => {
      switch (action.kind) {
        case ActiveItemActionKind.SetIndex:
          navigate((params) => ({ ...params, activeIndex: action.index }))
          break
        case ActiveItemActionKind.UnsetIndex:
          navigate(({ activeIndex, ...params }) => params)
          break
        case ActiveItemActionKind.Preview:
          navigate(({ activeIndex, ...params }) => ({
            ...params,
            viewMode: 'preview',
            activeIndex: action.index === undefined ? activeIndex : action.index,
          }))
          break
      }
    },
    [navigate],
  )

  const handlePreviewClose = useCallback(() => {
    navigate((params) => ({ ...params, viewMode: 'gallery' }))
  }, [navigate])

  const { state: sidebarState, setState: setSidebarState } = useSidebar()
  const { toggleItemSelection } = useItemSelection()

  useHotkeys(
    'i,esc,space,enter',
    (e) => {
      if (e.key === 'i') {
        if (params?.activeIndex !== undefined) {
          if (sidebarState !== 'hidden') {
            setSidebarState('hidden')
          } else {
            setSidebarState('full')
          }
        }
      } else if (e.key === 'Escape') {
        if (sidebarState !== 'hidden') {
          setSidebarState('hidden')
        } else if (params?.viewMode === 'preview') {
          handlePreviewClose()
        } else if (params?.activeIndex !== undefined) {
          handleActiveItemAction({ kind: ActiveItemActionKind.UnsetIndex })
        }
      } else if (e.key === ' ') {
        if (params?.viewMode === 'preview') {
          handlePreviewClose()
        } else {
          handleActiveItemAction({ kind: ActiveItemActionKind.Preview })
        }
      } else if (e.key === 'Enter' && params.activeIndex !== undefined) {
        toggleItemSelection(assets[params.activeIndex].id)
      }
    },
    [
      params,
      assets,
      handleActiveItemAction,
      navigate,
      sidebarState,
      setSidebarState,
      toggleItemSelection,
    ],
  )

  return (
    <>
      <PreviewOverlay
        assetType={index.assetType}
        assets={assets}
        activeItemIndex={params?.viewMode === 'preview' ? params.activeIndex : undefined}
        onActiveItemAction={handleActiveItemAction}
        onClose={handlePreviewClose}
      />

      <ContentWrapper
        overflow={params.viewMode === 'preview' ? 'hidden' : undefined}
        h={
          params.viewMode === 'preview' || totalCount === 0
            ? [['100vh', '-webkit-fill-available'] as any]
            : undefined
        }
        pt={0}
      >
        {totalCount > 0 && (
          <AssetListingHeader
            // Force remount when changing showBigHeader to avoid render artifacts on iPad
            key={params.viewMode}
            index={index}
            params={params}
            numAssets={totalCount}
            facets={resolvedFacets}
            hasUsefulFacets={hasUsefulFacets}
            showBigHeader={params.viewMode !== 'preview'}
            assets={assets}
            activeItemIndex={params.activeIndex}
            startDate={stats.startDate}
            endDate={stats.endDate}
            onActiveItemAction={handleActiveItemAction}
          />
        )}
        {params?.viewMode !== 'preview' && (
          <Box>
            {totalCount === 0 ? (
              <Center height="100vh">
                {params.fulltextQuery ? (
                  <EmptySearch index={index} fulltextQuery={params.fulltextQuery} />
                ) : (
                  <EmptyIndex index={index} />
                )}
              </Center>
            ) : (
              <AssetListingGallery
                assets={assets}
                fetchMore={fetchMore}
                activeItemIndex={params.activeIndex}
                onActiveItemAction={handleActiveItemAction}
              />
            )}
          </Box>
        )}
      </ContentWrapper>
    </>
  )
}

const AssetListingScreenWithData: React.FC<
  Props & {
    facets: FacetDescriptor[]
  }
> = ({ facets, ...props }) => {
  const { assets, totalCount, params } = props
  const { resolvedFacets, hasUsefulFacets, facetDataHasBeenFetched } = useFacets(
    facets,
    totalCount,
    params,
  )

  if (resolvedFacets.length !== 0 && !facetDataHasBeenFetched && totalCount > 0) {
    return null
  }

  return (
    <SidebarLayout
      sidebarContent={
        params.activeIndex !== undefined ? (
          <AssetInspector asset={assets[params.activeIndex]} />
        ) : null
      }
    >
      <ItemSelectionProvider totalCount={totalCount}>
        <AssetListingScreenWithFacets
          resolvedFacets={resolvedFacets}
          hasUsefulFacets={hasUsefulFacets}
          {...props}
        />
      </ItemSelectionProvider>
    </SidebarLayout>
  )
}

export const AssetListingScreen: React.FC = () => {
  const { params, navigate } = useAssetListingScreen()
  assert(params, 'No params. Screen did not match?')

  const index = useIndex()
  const { assets, totalCount, error, fetchMore, facets, stats } = useAssetListing(params)
  useTitle(assetListingScreenTitle(index, params))

  if (error) {
    return <QueryError error={error} />
  }

  if (!assets || !facets || totalCount === undefined) {
    return null
  }

  return (
    <AssetListingScreenWithData
      key={index.id}
      index={index}
      assets={assets}
      facets={facets}
      stats={stats!}
      totalCount={totalCount}
      fetchMore={fetchMore}
      params={params}
      navigate={navigate}
    />
  )
}
