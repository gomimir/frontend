import { isEqual } from 'lodash'
import { useMemo } from 'react'
import { ErrorIndexNotFound } from '../components/ErrorContent'
import { NavigationIndex } from '../components/NavigationIndex'
import { NavigationLayout } from '../components/NavigationLayout'
import { QueryError } from '../components/QueryError'
import { useGetIndexQuery } from '../graphql'
import { Index, IndexContext } from '../hooks/useIndex'
import { useScreen } from '../hooks/useScreen'
import { useTitle } from '../hooks/useTitle'
import { assert } from '../utils/assert'
import { messagesForIndex, messagesForTagKind } from '../utils/messages'

interface IndexScreenParams {
  viewMode?: string
}

const LoadedIndexScreen: React.FC<{ index: Index }> = ({ index, children }) => {
  useTitle(index.name)

  return (
    <IndexContext.Provider value={index}>
      <NavigationLayout navigationContent={<NavigationIndex index={index} />}>
        {children}
      </NavigationLayout>
    </IndexContext.Provider>
  )
}

const IndexScreenWithID: React.FC<{ indexId: string; params: IndexScreenParams }> = ({
  indexId,
  params,
  children,
}) => {
  const { data, error } = useGetIndexQuery({
    variables: {
      id: indexId,
    },
    fetchPolicy: 'cache-first',
  })

  const index: Index | undefined = useMemo(() => {
    if (data?.index) {
      return {
        id: data.index.id,
        name: data.index.name,
        permissions: data.index.permissions,
        stats: data.index.stats,
        assetType: data.index.assetType,
        tagKinds: data.index.tagKinds.map((tkc) => ({
          name: tkc.name,
          userAssignable: tkc.userAssignable,
          messages: messagesForTagKind(tkc.name, tkc.displayName, tkc.displayNamePlural),
        })),
        fileLocations: data.index.fileLocations,
        messages: messagesForIndex(data.index.name, data.index.assetType),
      }
    }
  }, [data])

  if (error) {
    if (
      error.graphQLErrors.some(
        (gqlErr) => isEqual(gqlErr.path, ['index']) && gqlErr.extensions?.code === 'NOT_FOUND',
      )
    ) {
      return <ErrorIndexNotFound />
    }

    return <QueryError error={error} />
  }

  if (!index) {
    return null
  }

  return <LoadedIndexScreen index={index}>{children}</LoadedIndexScreen>
}

export const IndexScreen: React.FC = ({ children }) => {
  const { request } = useScreen()
  const indexId = request.match?.params.indexName
  assert(indexId, 'Expected index id')

  return (
    <IndexScreenWithID key={indexId} indexId={indexId} params={request.match!.params}>
      {children}
    </IndexScreenWithID>
  )
}
