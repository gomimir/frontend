import { useEffect } from 'react'
import { ErrorContent, ErrorMessage, ErrorTitle } from '../components/ErrorContent'
import { QueryError } from '../components/QueryError'
import { OAuthExchangeMutation, useCurrentUserQuery, useOAuthExchangeMutation } from '../graphql'
import { useScreen } from '../hooks/useScreen'
import {
  AUTH_SESSION_STORAGE_KEY,
  authCallbackUrl,
  REFRESH_SESSION_STORAGE_KEY,
} from '../utils/auth'

const CurrentUserCheck: React.FC<{
  exchangeData: Exclude<OAuthExchangeMutation['oauthExchange'], null>
}> = ({ exchangeData }) => {
  const { data, error } = useCurrentUserQuery({
    context: {
      fetchOptions: {
        auth: exchangeData.token,
      },
    },
  })

  useEffect(() => {
    if (data?.currentUser.authenticated) {
      // Note: we might possibly want to reinit the websocket connection.
      // It is not initialized at this point on this screen, but it might change in the future.
      // https://github.com/enisdenjo/graphql-ws/issues/105
      localStorage.setItem(AUTH_SESSION_STORAGE_KEY, exchangeData.token)

      if (exchangeData.refreshToken) {
        localStorage.setItem(REFRESH_SESSION_STORAGE_KEY, exchangeData.refreshToken)
      }

      window.location.replace(exchangeData.state.path)
    }
  }, [data, exchangeData])

  if (error) {
    return <QueryError error={error} />
  }

  if (data && !data.currentUser.authenticated) {
    return (
      <ErrorContent>
        <ErrorTitle>Authentication error</ErrorTitle>
        <ErrorMessage>Server is likely not configured properly.</ErrorMessage>
      </ErrorContent>
    )
  }

  // TODO: preloader
  return null
}

export const OAuthCallbackScreen: React.FC = () => {
  const { request } = useScreen()
  const [exchange, { data, error }] = useOAuthExchangeMutation()

  useEffect(() => {
    if (request.match!.queryParams.code) {
      exchange({
        errorPolicy: 'all',
        variables: {
          callbackUrl: authCallbackUrl(),
          code: request.match!.queryParams.code,
          serializedState: request.match!.queryParams.state,
        },
      })
    }
  }, [request, exchange])

  if (error) {
    return <QueryError error={error} />
  }

  if (request.match!.queryParams.error) {
    return (
      <ErrorContent>
        <ErrorTitle>Authentication error</ErrorTitle>
        <ErrorMessage>
          Authentication server responded with: {request.match!.queryParams.error}
        </ErrorMessage>
        {request.match!.queryParams.error_description && (
          <ErrorMessage>
            {/* FIXME: remove the + conversion once it is fixed in chobot */}
            {request.match!.queryParams.error_description.replace(/\+/g, ' ')}
          </ErrorMessage>
        )}
      </ErrorContent>
    )
  }

  if (!data?.oauthExchange) {
    // TODO: preloader
    return null
  }

  return <CurrentUserCheck exchangeData={data.oauthExchange} />
}
