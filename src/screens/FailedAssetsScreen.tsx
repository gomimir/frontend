import { useMemo } from 'react'
import { Text } from '@chakra-ui/react'
import { QueryError } from '../components/QueryError'
import { ListFailedAssetsQuery } from '../graphql'
import { useFailedAssets } from '../hooks/useFailedAssets'
import { useScreen } from '../hooks/useScreen'
import { getTaskDisplayName } from '../utils/jobs'

function getErrors(assets: ListFailedAssetsQuery['assets']['items']) {
  const errors = [] as Array<{
    key: string
    assetId: string
    file: { filename: string; repository: string; url: string }
    processing: {
      name: string
      error: {
        code: string
        message: string
      }
    }
  }>

  for (const asset of assets) {
    for (const file of asset.files) {
      for (const proc of file.processings) {
        if (proc.error) {
          errors.push({
            key: `${asset.id}-${file.repository}/${file.filename}-${proc.name}`,
            assetId: asset.id,
            file: {
              filename: file.filename,
              repository: file.repository,
              url: file.url,
            },
            processing: {
              name: proc.name,
              error: proc.error,
            },
          })
        }
      }
    }
  }

  return errors
}

// TODO: proper screen styling and pagination
// TODO: support for filtering by job and task
export const FailedAssetsScreen: React.FC = () => {
  const { request } = useScreen()
  const { assets, error } = useFailedAssets({
    indexNames: request.match?.params.indexName ? [request.match?.params.indexName] : undefined,
  })

  const processingErrors = useMemo(() => assets && getErrors(assets), [assets])

  if (error) {
    return <QueryError error={error} />
  }

  if (!processingErrors) {
    return null
  }

  return (
    <div>
      {processingErrors.map((err) => (
        <div key={err.key}>
          <Text>
            {err.file.filename} (repository: {err.file.repository})
          </Text>
          <Text>{getTaskDisplayName(err.processing.name)}</Text>
          <Text color="red.600">
            {err.processing.error.message} (code: {err.processing.error.code})
          </Text>
        </div>
      ))}
    </div>
  )
}
