import { Box } from '@chakra-ui/react'
import { ErrorContent, ErrorMessage, ErrorTitle } from '../components/ErrorContent'
import { JobDetail } from '../components/JobDetail'
import { useJobs } from '../hooks/useJobs'
import { useScreen } from '../hooks/useScreen'
import { useTitle } from '../hooks/useTitle'
import { assert } from '../utils/assert'

// import { Job } from '../utils/jobs'

export const JobDetailScreen: React.FC = () => {
  useTitle(`Jobs`)
  const { request } = useScreen()
  const { jobsInfo } = useJobs()

  const jobId = request.match?.params.jobId
  assert(jobId, 'Expected job id')

  if (!jobsInfo) {
    return null
  }

  const job = jobsInfo?.jobs.find((j) => j.id === jobId)
  if (!job) {
    return (
      <ErrorContent>
        <ErrorTitle>Job not found</ErrorTitle>
        <ErrorMessage>You are trying to view details of a job which no longer exists.</ErrorMessage>
      </ErrorContent>
    )
  }

  // const job: Job = {
  //   id: 'c76o2h3pc98geeare3b0',
  //   name: 'crawl',
  //   finished: false,
  //   finishedAt: null,
  //   queuedAt: new Date(),
  //   taskSummary: [
  //     {
  //       name: 'crawl',
  //       pending: 0,
  //       failed: 0,
  //       total: 1,
  //       took: 2522,
  //     },
  //     {
  //       name: 'extractExif',
  //       pending: 0,
  //       failed: 1,
  //       total: 45,
  //       took: 2281,
  //     },
  //     {
  //       name: 'generateThumbnails',
  //       failed: 0,
  //       pending: 13,
  //       took: 114510,
  //       total: 61,
  //     },
  //   ],
  // }

  return (
    <Box p={50} w="100%" maxW="1024px" mx="auto">
      <JobDetail job={job} />
    </Box>
  )
}
