import { lowerCase } from 'lodash'
import { facetDescMatch, FacetDescriptor } from './FacetDescriptor'

export interface FacetConfig {
  displayName: string
  order: number
  show: boolean
}

export interface FacetConfigAssignment extends Partial<FacetConfig> {
  match: Partial<FacetDescriptor>
}

export function resolveConfig(desc: FacetDescriptor, config: FacetConfigAssignment[]): FacetConfig {
  const result: FacetConfig = {
    displayName: lowerCase(desc.name),
    show: true,
    order: 10000,
  }

  for (const { match, ...partialConfig } of config) {
    if (facetDescMatch(match, desc)) {
      Object.assign(result, partialConfig)
    }
  }

  return result
}
