import { facetDescMatch, FacetDescriptor } from './FacetDescriptor'

export interface FacetFilter extends FacetDescriptor {
  values: string[]
}

export function facetFilterContains(
  filters: FacetFilter[],
  facetDesc: FacetDescriptor,
  facetValue: string,
) {
  return filters.some((f) => facetDescMatch(f, facetDesc) && f.values.includes(facetValue))
}

export function toggleFacetFilter(
  filters: FacetFilter[],
  facetDesc: FacetDescriptor,
  facetValue: string,
): FacetFilter[] {
  const newFacets = [...filters]

  for (let i = 0; i < newFacets.length; i++) {
    if (facetDescMatch(newFacets[i], facetDesc)) {
      const newValues = newFacets[i].values.filter((existing) => existing !== facetValue)

      if (newValues.length === newFacets[i].values.length) {
        newValues.push(facetValue)
      }

      if (newValues.length === 0) {
        newFacets.splice(i, 1)
      } else {
        newFacets[i] = {
          ...facetDesc,
          values: newValues,
        }
      }

      return newFacets
    }
  }

  newFacets.push({
    ...facetDesc,
    values: [facetValue],
  })

  return newFacets
}
