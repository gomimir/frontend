import { TimeRangeInput } from '../graphql'

export interface FacetTimeRange extends TimeRangeInput {
  label: string
}
