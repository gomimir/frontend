export enum FacetSubject {
  Asset = 'a',
  File = 'f',
}

export enum FacetKind {
  Tag = 't',
  TextProperty = 'tp',
  TimeProperty = 'dtp',
}

export interface FacetDescriptor {
  subject: FacetSubject
  kind: FacetKind
  name: string
}

export function facetDescKey(desc: FacetDescriptor) {
  return `${desc.subject}${desc.kind}_${desc.name}`
}

export function facetDescMatch(actual: Partial<FacetDescriptor>, expected: FacetDescriptor) {
  return (
    (actual.name === undefined || actual.name === expected.name) &&
    (actual.kind === undefined || actual.kind === expected.kind) &&
    (actual.subject === undefined || actual.subject === expected.subject)
  )
}
