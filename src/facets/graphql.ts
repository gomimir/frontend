import { DocumentNode, FieldNode, SelectionNode, VariableDefinitionNode } from 'graphql'
import { flatten, groupBy } from 'lodash'
import {
  AssetFilter,
  TagFacetValuesFragment,
  TagFacetValuesFragmentDoc,
  TextPropertyValuesFragment,
  TextPropertyValuesFragmentDoc,
  TimePropertyValuesFragmentDoc,
} from '../graphql'
import { gqlName, gqlSelSet, gqlVar } from '../graphql/helpers'
import { DEFAULT_TIME_RANGES } from './constants'
import { facetDescKey, FacetDescriptor, FacetKind, FacetSubject } from './FacetDescriptor'

export interface FacetQueryVariables {
  query?: AssetFilter | null
}

export interface FacetQueryResult {
  assets: {
    facets: {
      [k: string]: TextPropertyValuesFragment | TagFacetValuesFragment
    } & {
      files: {
        [k: string]: TextPropertyValuesFragment | TagFacetValuesFragment
      }
    }
  }
}

export function buildFacetQuery(
  index: string,
  facets: FacetDescriptor[],
): {
  query: DocumentNode
  variables: { [k: string]: any }
} {
  const { [FacetSubject.Asset]: assetFacets = [], [FacetSubject.File]: fileFacets = [] } = groupBy(
    facets,
    (f) => f.subject,
  )

  const fragments: Set<DocumentNode> = new Set()
  const facetSelection: FieldNode[] = []
  const fileFacetSelection: FieldNode[] = []
  const variables: { [k: string]: any } = {
    index: [index],
  }
  const variableDefinitions: VariableDefinitionNode[] = [
    {
      kind: 'VariableDefinition',
      variable: gqlVar('query'),
      type: {
        kind: 'NamedType',
        name: gqlName('AssetFilter'),
      },
    },
    {
      kind: 'VariableDefinition',
      variable: gqlVar('index'),
      type: {
        kind: 'ListType',
        type: {
          kind: 'NonNullType',
          type: {
            kind: 'NamedType',
            name: gqlName('ID'),
          },
        },
      },
    },
  ]

  const tagFacets = assetFacets.filter((f) => f.kind === FacetKind.Tag)
  if (tagFacets.length) {
    fragments.add(TagFacetValuesFragmentDoc)
    facetSelection.push(...generateTagFacetSelection(tagFacets))
  }

  for (const { properties, selection } of [
    { properties: assetFacets, selection: facetSelection },
    { properties: fileFacets, selection: fileFacetSelection },
  ]) {
    const textProperties = properties?.filter((p) => p.kind === FacetKind.TextProperty)
    if (textProperties?.length) {
      fragments.add(TextPropertyValuesFragmentDoc)
      selection.push(
        ...generatePropertyFacetSelection(
          [
            {
              kind: 'Field',
              name: gqlName('textValues'),
              selectionSet: gqlSelSet([
                {
                  kind: 'FragmentSpread',
                  name: gqlName('TextPropertyValues'),
                },
              ]),
            },
          ],
          textProperties,
        ),
      )
    }

    const timeProperties = properties?.filter((p) => p.kind === FacetKind.TimeProperty)
    if (timeProperties?.length) {
      fragments.add(TimePropertyValuesFragmentDoc)

      if (!variables.timeRanges) {
        variables.timeRanges = DEFAULT_TIME_RANGES.map(({ name, from, to }) => ({ name, from, to }))
        variableDefinitions.push({
          kind: 'VariableDefinition',
          variable: gqlVar('timeRanges'),
          type: {
            kind: 'ListType',
            type: {
              kind: 'NonNullType',
              type: {
                kind: 'NamedType',
                name: gqlName('TimeRangeInput'),
              },
            },
          },
        })
      }

      selection.push(
        ...generatePropertyFacetSelection(
          [
            {
              kind: 'Field',
              name: gqlName('timeRanges'),
              arguments: [
                { kind: 'Argument', name: gqlName('ranges'), value: gqlVar('timeRanges') },
              ],
              selectionSet: gqlSelSet([
                {
                  kind: 'FragmentSpread',
                  name: gqlName('TimePropertyValues'),
                },
              ]),
            },
          ],
          timeProperties,
        ),
      )
    }
  }

  if (fileFacetSelection.length) {
    facetSelection.push({
      kind: 'Field',
      name: gqlName('files'),
      selectionSet: gqlSelSet(fileFacetSelection),
    })
  }

  const document: DocumentNode = {
    kind: 'Document',
    definitions: [
      {
        kind: 'OperationDefinition',
        operation: 'query',
        name: gqlName('GetFacets'),
        variableDefinitions,
        selectionSet: {
          kind: 'SelectionSet',
          selections: [
            {
              kind: 'Field',
              name: gqlName('assets'),
              arguments: [
                { kind: 'Argument', name: gqlName('query'), value: gqlVar('query') },
                { kind: 'Argument', name: gqlName('index'), value: gqlVar('index') },
              ],
              selectionSet: gqlSelSet([
                { kind: 'Field', name: gqlName('facets'), selectionSet: gqlSelSet(facetSelection) },
              ]),
            },
          ],
        },
      },
      ...flatten([...fragments.values()].map((doc) => doc.definitions)),
    ],
  }

  return {
    query: document,
    variables,
  }
}

function generateTagFacetSelection(requests: FacetDescriptor[]): FieldNode[] {
  return requests.map((req) => ({
    kind: 'Field',
    alias: gqlName(facetDescKey(req)),
    name: gqlName('tags'),
    arguments: [
      {
        kind: 'Argument',
        name: gqlName('kind'),
        value: { kind: 'StringValue', value: req.name },
      },
    ],
    selectionSet: gqlSelSet([
      {
        kind: 'FragmentSpread',
        name: { kind: 'Name', value: 'TagFacetValues' },
      },
    ]),
  }))
}

function generatePropertyFacetSelection(
  selection: SelectionNode[],
  requests: FacetDescriptor[],
): FieldNode[] {
  return requests.map((req) => ({
    kind: 'Field',
    alias: gqlName(facetDescKey(req)),
    name: gqlName('property'),
    arguments: [
      {
        kind: 'Argument',
        name: gqlName('name'),
        value: { kind: 'StringValue', value: req.name },
      },
    ],
    selectionSet: gqlSelSet(selection),
  }))
}
