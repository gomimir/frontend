import { FacetConfigAssignment } from './FacetConfig'
import { FacetKind, FacetSubject } from './FacetDescriptor'
import { FacetTimeRange } from './FacetTimeRange'

export const FACET_CONFIG: FacetConfigAssignment[] = [
  {
    order: 100,
    match: {
      subject: FacetSubject.Asset,
    },
  },
  {
    order: 50,
    match: {
      kind: FacetKind.Tag,
    },
  },
  {
    order: 1000,
    match: {
      subject: FacetSubject.File,
    },
  },
  {
    order: 0,
    displayName: 'date',
    match: {
      subject: FacetSubject.File,
      kind: FacetKind.TimeProperty,
      name: 'date',
    },
  },
  {
    displayName: 'camera',
    order: 98,
    match: {
      name: 'camera_model',
    },
  },
  {
    displayName: 'lens',
    order: 99,
    match: {
      name: 'lens_model',
    },
  },
  {
    show: false,
    match: {
      name: 'name',
    },
  },
  {
    show: false,
    match: {
      name: 'lens_make',
    },
  },
  {
    show: false,
    match: {
      name: 'camera_make',
    },
  },
  // We use date instead because it is available on broader spectrum of files
  // (and gets loaded from EXIF for images anyway)
  {
    show: false,
    match: {
      name: 'authoredAt',
    },
  },
  {
    show: false,
    match: {
      name: 'extractedText',
    },
  },
  {
    show: false,
    match: {
      name: 'apple_contentId',
    },
  },
]

export const DEFAULT_TIME_RANGES: FacetTimeRange[] = [
  { name: 'today', label: 'Today', from: 'now/d' },
  { name: 'yesterday', label: 'Yesterday', from: 'now/d-1d', to: 'now/d' },
  { name: 'last7d', label: 'Last 7 days', from: 'now/d-7d' },
  { name: 'last30d', label: 'Last 30 days', from: 'now/d-30d' },
  { name: 'this-year', label: 'This year', from: 'now/y' },
  { name: 'last-year', label: 'Last year', from: 'now/y-1y', to: 'now/y' },
]

export const FACET_KINDS_BY_SUBJECT = {
  [FacetSubject.Asset]: [FacetKind.Tag, FacetKind.TextProperty, FacetKind.TimeProperty],
  [FacetSubject.File]: [FacetKind.TextProperty, FacetKind.TimeProperty],
}
