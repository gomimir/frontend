import { checkWebPSupport } from 'supports-webp-sync'
import { ThumbnailFormat } from '../graphql'

let acceptableThumbnailFormats: ThumbnailFormat[] | undefined

export function getAcceptableThumbnailFormats(): ThumbnailFormat[] {
  if (!acceptableThumbnailFormats) {
    if (checkWebPSupport()) {
      acceptableThumbnailFormats = [ThumbnailFormat.Webp, ThumbnailFormat.Jpeg]
    } else {
      acceptableThumbnailFormats = [ThumbnailFormat.Jpeg]
    }
  }

  return acceptableThumbnailFormats
}
