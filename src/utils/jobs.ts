export interface TaskSummary {
  name: string
  total: number
  pending: number
  failed: number
  took: number
}

export interface Job {
  id: string
  name: string
  queuedAt: Date
  finishedAt: Date | null
  finished: boolean
  taskSummary: TaskSummary[]
}

export function getJobProgress(job: Job) {
  const { hasDeterminateProgress, total, slices } = job.taskSummary.reduce(
    (res, taskSummary) => {
      // We do not treat reported progress as determinate until we receive at least 1 task incrementing the time
      // We do not count tasks which are already finished into this (on rare occasion something can finish in <1ms)
      if (!taskSummary.took && taskSummary.pending > 0) {
        res.hasDeterminateProgress = false
      } else {
        const numSucceeded = taskSummary.total - taskSummary.pending - taskSummary.failed
        if (numSucceeded > 0) {
          const avgTimeToSucceed = taskSummary.took / numSucceeded
          const portion = avgTimeToSucceed * taskSummary.total

          res.total += portion
          res.slices.push({
            finishedRatio: 1 - taskSummary.pending / taskSummary.total,
            portion,
          })
        }
      }

      return res
    },
    {
      hasDeterminateProgress: true,
      total: 0,
      slices: [] as Array<{ finishedRatio: number; portion: number }>,
    },
  )

  if (hasDeterminateProgress) {
    const ratio = slices.reduce(
      (ratio, { finishedRatio, portion }) => ratio + finishedRatio * (portion / total),
      0,
    )

    return ratio * 100
  }
}

const TASK_MESSAGES: { [taskName: string]: string } = {
  index: 'Indexing the files',
  crawl: 'Crawling the files',
  extractExif: 'Extracting EXIF information',
  extractText: 'Extracting text',
  generateThumbnails: 'Generating thumbnails',
  processVideo: 'Processing video files',
  uploadArchiveFromURL: 'Loading files from URL',
}

export function getTaskDisplayName(name: string) {
  return TASK_MESSAGES[name] || name
}

const JOB_MESSAGES: { [jobName: string]: string } = {
  crawl: 'Crawling for new files',
  processUploadedFiles: 'Processing uploaded files',
  reindexAssetFiles: 'Reprocessing asset',
  uploadArchiveFromURL: 'Processing uploaded files',
}

export function getJobDisplayName(name: string) {
  return JOB_MESSAGES[name] || name
}
