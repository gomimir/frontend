export class AssertionError extends Error {
  constructor(message: string) {
    super(message)

    // Maintains proper stack trace for where the assertion failed (only available on V8)
    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, assert)
    }

    // Note: The name 'AssertionError' is reserved by Jest and results in different output
    this.name = 'AssertFailure'
  }
}

/**
 * Asserts that condition is truthy, throws error otherwise
 *
 * See: https://www.typescriptlang.org/docs/handbook/release-notes/typescript-3-7.html#assertion-functions
 */
export function assert(condition: unknown, message: string): asserts condition {
  if (!condition) {
    throw new AssertionError(message)
  }
}
