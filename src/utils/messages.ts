import { AssetType } from '../graphql'

export type PluralMessageGetter = (n?: number) => string

export interface IndexMessages {
  assetName: PluralMessageGetter
  allAssets: string
}

export interface TagKindMessages {
  kindName: PluralMessageGetter
  allTags: string
  noTags: string
}

/** Basic English inflection */
export function resolveEnglishPlural(indexName: string): [string, string] {
  return indexName.endsWith('s')
    ? [indexName.substring(0, indexName.length - 1), indexName]
    : [indexName, `${indexName}s`]
}

function englishPluralGetter(singular: string, plural: string): PluralMessageGetter {
  return (n) => (n === 1 ? singular : plural)
}

function getEffectiveIndexMessages(
  indexName: string,
  config: Partial<IndexMessages> = {},
): IndexMessages {
  const assetName = config.assetName || englishPluralGetter(...resolveEnglishPlural(indexName))

  return {
    assetName,
    allAssets: config.allAssets || `All ${assetName()}`,
  }
}

function getEffectiveTagKindMessages(
  kindName: string,
  config: Partial<TagKindMessages> = {},
): TagKindMessages {
  const kindNameGetter = config.kindName || englishPluralGetter(...resolveEnglishPlural(kindName))
  return {
    kindName: kindNameGetter,
    allTags: config.allTags || `All ${kindNameGetter()}`,
    noTags: config.noTags || `No ${kindNameGetter()}`,
  }
}

/** Per-index customization */
const INDEX_MESSAGES: { [assetType in AssetType]?: Partial<IndexMessages> } = {
  [AssetType.Document]: {
    allAssets: 'All documents',
    assetName: englishPluralGetter('document', 'documents'),
  },
  [AssetType.Photo]: {
    allAssets: 'All photos',
    assetName: englishPluralGetter('photo', 'photos'),
  },
}

/** Per-tag customization */
const TAG_KIND_MESSAGES: { [kindName: string]: Partial<TagKindMessages> } = {}

export function messagesForIndex(indexName: string, assetType: AssetType): IndexMessages {
  return getEffectiveIndexMessages(indexName, INDEX_MESSAGES[assetType])
}

export function messagesForTagKind(
  kindName: string,
  displayNameSingular?: string | null,
  displayNamePlural?: string | null,
): TagKindMessages {
  const config = { ...TAG_KIND_MESSAGES[kindName] }
  if (!config.kindName && (displayNameSingular || displayNamePlural)) {
    if (displayNameSingular && displayNamePlural) {
      config.kindName = englishPluralGetter(displayNameSingular, displayNamePlural)
    } else {
      config.kindName = englishPluralGetter(
        ...resolveEnglishPlural((displayNameSingular || displayNamePlural)!),
      )
    }
  }

  return getEffectiveTagKindMessages(kindName, TAG_KIND_MESSAGES[kindName])
}
