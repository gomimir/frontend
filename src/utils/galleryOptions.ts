/**
 * Ideal number of landscape photos per row, based on that we calculate row height
 * Note: this is expected to be sorted from least to most minContainerWidth
 */
const NUM_ITEMS_FOR_WIDTH = [
  { minContainerWidth: 1600, numItemsPerRow: 4 },
  { minContainerWidth: 1000, numItemsPerRow: 3 },
  { numItemsPerRow: 2 },
]

/**
 * If the calculation is within X px from the calculated reference row height, we just adjust the height
 * In the rest of the cases the items get shuffled so that the row would contain more/less items
 */
const ACCEPTABLE_ROW_HEIGHT_DIFFERENCE = 50

/**
 * Gap between items in the grid (both vertical and horizontal)
 */
const GAP = 15

/**
 * Returns reference (ideal) row height for container of given width if we want to show N items in a row
 */
function getRefRowHeight(containerWidth: number, numItemsPerRow: number) {
  return Math.floor(((containerWidth - GAP * (numItemsPerRow - 1)) / numItemsPerRow / 3) * 2)
}

/**
 * Returns maximum possible height of a thumbnail it makes for user screen
 * if he wants to view gallery grid
 */
export function getMaxRowHeightForUserScreen() {
  const maxPossibleHeight = window.screen.height * window.devicePixelRatio

  let maxContainerWidth = maxPossibleHeight
  let maxRowHeight: number | undefined
  for (const { minContainerWidth, numItemsPerRow } of NUM_ITEMS_FOR_WIDTH) {
    const refRowHeight = getRefRowHeight(maxContainerWidth, numItemsPerRow)
    if (maxRowHeight === undefined || refRowHeight > maxRowHeight) {
      maxRowHeight = refRowHeight
    }

    // Note: ! is here because we know only the last item won't have minContainerWidth defined
    maxContainerWidth = minContainerWidth!
  }

  return maxRowHeight! + ACCEPTABLE_ROW_HEIGHT_DIFFERENCE
}

export function getGridOptions(containerWidth: number) {
  const { numItemsPerRow } = NUM_ITEMS_FOR_WIDTH.find(
    ({ minContainerWidth }) =>
      minContainerWidth === undefined || containerWidth >= minContainerWidth,
  )!

  // Reference row height
  const rowHeight = getRefRowHeight(containerWidth, numItemsPerRow)

  return {
    containerWidth,
    rowHeight,
    acceptableRowHeightDifference: ACCEPTABLE_ROW_HEIGHT_DIFFERENCE,
    gap: GAP,
  }
}
