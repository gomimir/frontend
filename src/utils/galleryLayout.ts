/** Generic item dimensions */
export interface ItemDimension {
  width: number
  height: number
}

/** Generic shape for layed out items */
export interface LayoutRect {
  x: number
  y: number
  width: number
  height: number
}

export interface GridLayoutOptions {
  containerWidth: number
  rowHeight: number
  acceptableRowHeightDifference: number
  itemBottomPadding: number
  gap: number
  topPadding: number
  bottomPadding: number
  leftPadding: number
  rightPadding: number
}

export interface GridLayout {
  /** Combined height of all layed out items */
  height: number
  /** Layed out item shapes */
  rects: LayoutRect[]
  /** Options which were used to calculate the layout */
  options: GridLayoutOptions
}

/** Calculates the layout for given items */
export function calculateGridLayout(
  items: ItemDimension[],
  options: GridLayoutOptions,
): GridLayout {
  let x = options.leftPadding
  let y = options.topPadding

  // Item index of start of the current row
  let currRowStartIndex = 0

  // Cumulative width of all items in the current row without any gaps
  let currRowItemWidth = 0

  const availableContainerWidth =
    options.containerWidth - options.rightPadding - options.leftPadding

  const rects: LayoutRect[] = []

  for (let i = 0; i < items.length; i++) {
    const itemDimensions = items[i]

    // Calculate the width of the item as scaled to reference row height
    const width = Math.min(
      itemDimensions.height > options.rowHeight
        ? Math.round((options.rowHeight / itemDimensions.height) * itemDimensions.width)
        : itemDimensions.width,
      availableContainerWidth,
    )

    // > 0 item too large to fit in the current row
    // <=0 item will fit into row
    const fit = width - (options.containerWidth - options.rightPadding - x)

    // If item won't fit
    if (fit > 0) {
      const totalRowImageWidth = currRowItemWidth + width
      const availableRowWidth = availableContainerWidth - (i - currRowStartIndex) * options.gap
      const suggestedRowHeight = (options.rowHeight / totalRowImageWidth) * availableRowWidth

      const availableRowWidth2 = availableContainerWidth - (i - currRowStartIndex - 1) * options.gap
      const suggestedRowHeight2 = (options.rowHeight / currRowItemWidth) * availableRowWidth2

      // Making the row less tall to fit this item
      if (
        options.rowHeight - suggestedRowHeight < suggestedRowHeight2 - options.rowHeight &&
        options.rowHeight - suggestedRowHeight <= options.acceptableRowHeightDifference
      ) {
        rects[i] = {
          x,
          y,
          width,
          height: options.rowHeight,
        }

        for (let j = currRowStartIndex, x = options.leftPadding; j <= i; j++) {
          rects[j].x = x
          rects[j].height = suggestedRowHeight
          rects[j].width = (rects[j].width / options.rowHeight) * suggestedRowHeight
          x += rects[j].width + options.gap
        }

        // Set for the next row
        currRowStartIndex = i + 1
        currRowItemWidth = 0
        x = options.leftPadding
        y += options.gap + suggestedRowHeight + options.itemBottomPadding

        continue
      }

      // Passing this item to the next row
      else {
        // Making the items in unfinished row larger
        if (suggestedRowHeight2 - options.rowHeight <= options.acceptableRowHeightDifference) {
          for (let j = currRowStartIndex, x = options.leftPadding; j < i; j++) {
            rects[j].x = x
            rects[j].height = suggestedRowHeight2
            rects[j].width = (rects[j].width / options.rowHeight) * suggestedRowHeight2
            x += rects[j].width + options.gap
          }

          y += options.gap + suggestedRowHeight2 + options.itemBottomPadding
        } else {
          y += options.gap + options.rowHeight + options.itemBottomPadding
        }

        // Set for the next row
        currRowStartIndex = i
        currRowItemWidth = 0
        x = options.leftPadding
      }
    }

    // Set the item
    const rect = (rects[i] = {
      x,
      y,
      width,
      height: options.rowHeight,
    })

    // Set for the next item
    x += options.gap + rect.width
    currRowItemWidth += rect.width
  }

  return {
    height: rects.length
      ? rects[rects.length - 1].y +
        rects[rects.length - 1].height +
        options.itemBottomPadding +
        options.bottomPadding
      : 0,
    rects,
    options,
  }
}

/**
 * Re-adjusts existing layout to fit within given container width.
 * No items will be moved between rows.
 * This is meant to be used as a quick responsive variant to recalculating the whole layouts
 * (usable when user is rapidly resizing the viewport).
 */
export function adjustGridLayout(layout: GridLayout, containerWidth: number): GridLayout {
  const options = { ...layout.options, containerWidth }

  if (layout.rects.length > 1) {
    const rects = new Array(layout.rects.length)
    let startIndex = 0
    let y = options.topPadding

    for (let i = 1; i < layout.rects.length; i++) {
      // Process only on the row wraps (when we find the item which has different y coordinate)
      if (layout.rects[i - 1].y !== layout.rects[i].y || i === layout.rects.length - 1) {
        // Determine index of the last item in the row
        const lastIdxInRow = i === layout.rects.length - 1 ? i : i - 1

        // Calculate total width of the row
        const rowWidthWithGaps =
          layout.rects[lastIdxInRow].x + layout.rects[lastIdxInRow].width + options.rightPadding

        // Subtract gaps so that we get clean width accommodated by the items
        const totalGapWidth = (i - startIndex - 1) * layout.options.gap

        // Calculate how much we have to scale the items
        // (how much does the new desired container width differ)
        const scaleRatio =
          (containerWidth - options.rightPadding - totalGapWidth) /
          (rowWidthWithGaps - totalGapWidth)

        // Walk through all the items in the row
        for (
          let j = startIndex, x = options.leftPadding;
          j <= lastIdxInRow;
          x += rects[j++].width + options.gap
        ) {
          // Calculate the new dimensions
          const height = Math.min(layout.rects[j].height * scaleRatio, options.rowHeight)

          const width = (height / layout.rects[j].height) * layout.rects[j].width

          rects[j] = {
            x,
            y,
            width,
            height,
          }
        }

        // Set y to start of the next row (the row height changed)
        y += rects[lastIdxInRow].height + layout.options.gap + options.itemBottomPadding
        startIndex = i
      }
    }

    return {
      ...layout,
      options,
      rects,
      height:
        rects[rects.length - 1].y +
        rects[rects.length - 1].height +
        options.bottomPadding +
        options.itemBottomPadding,
    }
  }

  return { ...layout, options }
}

export function findVisibleRange(
  rects: LayoutRect[],
  scrollTop: number,
  containerHeight: number,
): [number, number] {
  let startIndex = 0
  while (startIndex < rects.length && rects[startIndex].y + rects[startIndex].height < scrollTop) {
    startIndex++
  }

  let endIndex = startIndex
  while (endIndex < rects.length && rects[endIndex].y < scrollTop + containerHeight) {
    endIndex++
  }

  return [Math.min(startIndex, rects.length - 1), Math.min(endIndex, rects.length - 1)]
}

export function findRowStart(rects: LayoutRect[], index: number) {
  const pilot = rects[index]

  let start = index
  while (start > 0 && rects[start - 1].y === pilot.y) start--
  return start
}

export function findRowEnd(rects: LayoutRect[], index: number) {
  const pilot = rects[index]

  let end = index
  while (end < rects.length - 1 && rects[end + 1].y === pilot.y) end++

  return end
}

export function getNumRowsIn(rects: LayoutRect[], range: [number, number]) {
  let y = -1
  let numRows = 0

  for (let i = range[0]; i <= range[1]; i++) {
    if (rects[i].y > y) {
      y = rects[i].y
      numRows++
    }
  }

  return numRows
}

export function growRangeByRows(
  rects: LayoutRect[],
  range: [number, number],
  aboveRows: number,
  bottomRows: number,
) {
  let [start, end] = range

  for (let i = 0; i < aboveRows; i++) {
    start = findRowStart(rects, start)
    if (start > 0) {
      start--
    } else {
      break
    }
  }

  for (let i = 0; i < bottomRows; i++) {
    end = findRowEnd(rects, end)
    if (end < rects.length - 1) {
      end++
    } else {
      break
    }
  }

  return [start, end]
}
