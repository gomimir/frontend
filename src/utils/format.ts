import { format } from 'date-fns'

export function formatInt(n: number) {
  return n.toFixed(0).replace(/\B(?=(\d{3})+(?!\d))/g, '\xA0')
}

export function formatBytes(bytes: number, decimals = 2) {
  if (bytes === 0) {
    return '0\xA0bytes'
  }

  const k = 1024
  const dm = decimals < 0 ? 0 : decimals
  const sizes = ['bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB']

  const i = Math.floor(Math.log(bytes) / Math.log(k))

  return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + '\xA0' + sizes[i]
}

export function formatDate(value: Date): string {
  return value.toLocaleString()
}

export function formatDateRange(start: Date, end: Date): string {
  // Different month
  if (start.getFullYear() !== end.getFullYear()) {
    return `${format(start, 'MMMM\xA0uuuu')} -\xA0${format(end, 'MMMM\xA0uuuu')}`
  }

  // Same year, different month
  else if (start.getMonth() !== end.getMonth()) {
    return `${format(start, 'MMMM')} -\xA0${format(end, 'MMMM\xA0uuuu')}`
  }

  // Same year and month
  else {
    return format(end, 'MMMM\xA0uuuu')
  }
}

const TIME_UNITS_MSEC = [
  { name: 'h', value: 60 * 60 * 1000, nextValues: 1 },
  { name: 'm', value: 60 * 1000, nextValues: 1 },
  { name: 's', value: 1000 },
  { name: 'ms', value: 1 },
]

export function formatTimeTook(msec: number) {
  for (let i = 0; i <= TIME_UNITS_MSEC.length; i++) {
    const { name: unitName, value: unitValue, nextValues } = TIME_UNITS_MSEC[i]
    if (msec >= unitValue) {
      let result = Math.floor(msec / unitValue) + ' ' + unitName
      let remainder = msec % unitValue

      if (nextValues) {
        for (let j = i + 1; j < TIME_UNITS_MSEC.length && j <= i + nextValues; j++) {
          const { name: unitName, value: unitValue } = TIME_UNITS_MSEC[j]
          if (remainder >= unitValue) {
            result += ' ' + Math.floor(remainder / unitValue) + ' ' + unitName
            remainder = remainder % unitValue
          }
        }
      }

      return result
    }
  }

  return '0' + TIME_UNITS_MSEC[TIME_UNITS_MSEC.length - 1].name
}

export function formatFramerate(numerator: number, denominator: number) {
  if (denominator === 1) {
    return `${numerator} fps`
  } else {
    return `${Math.round((numerator / denominator) * 100) / 100} fps`
  }
}

export function formatModelAndMake(model?: string, make?: string): string {
  const modelWords = (model || '').toLowerCase().split(' ')
  const makeWords = (make || '').toLowerCase().split(' ')

  for (const modelW of modelWords) {
    for (const makeW of makeWords) {
      if (modelW === makeW) {
        return model!
      }
    }
  }

  return [make, model].map((_) => _).join(' ')
}
