import { PropertyPairFragment } from '../graphql'

export function isPropertyType(
  prop: PropertyPairFragment,
  typename: 'TextProperty',
): prop is Extract<PropertyPairFragment, { __typename?: 'TextProperty' }>
export function isPropertyType(
  prop: PropertyPairFragment,
  typename: 'TimeProperty',
): prop is Extract<PropertyPairFragment, { __typename?: 'TimeProperty' }>
export function isPropertyType(
  prop: PropertyPairFragment,
  typename: PropertyPairFragment['__typename'],
): boolean {
  return prop.__typename === typename
}

/** Retrieves file property by name in a type-safe manner */
export function getFileProperty(
  properties: PropertyPairFragment[],
  name: string,
  typename: 'TextProperty',
): Extract<PropertyPairFragment, { __typename?: 'TextProperty' }> | undefined
/** Retrieves file property by name in a type-safe manner */
export function getFileProperty(
  properties: PropertyPairFragment[],
  name: string,
  typename: 'NumberProperty',
): Extract<PropertyPairFragment, { __typename?: 'NumberProperty' }> | undefined
/** Retrieves file property by name in a type-safe manner */
export function getFileProperty(
  properties: PropertyPairFragment[],
  name: string,
  typename: 'FractionProperty',
): Extract<PropertyPairFragment, { __typename?: 'FractionProperty' }> | undefined
/** Retrieves file property by name in a type-safe manner */
export function getFileProperty(
  properties: PropertyPairFragment[],
  name: string,
  typename: 'TimeProperty',
): Extract<PropertyPairFragment, { __typename?: 'TimeProperty' }> | undefined
/** Retrieves file property by name in a type-safe manner */
export function getFileProperty(
  properties: PropertyPairFragment[],
  name: string,
  typename: PropertyPairFragment['__typename'],
): PropertyPairFragment | undefined {
  for (const prop of properties) {
    if (prop.name === name && prop.__typename === typename) {
      return prop
    }
  }
}
