import { router } from '../router'

export const AUTH_SESSION_STORAGE_KEY = 'auth_token'
export const REFRESH_SESSION_STORAGE_KEY = 'refresh_token'

export function authCallbackUrl() {
  return window.location.origin + router.createUrl('OAuthCallback')
}
