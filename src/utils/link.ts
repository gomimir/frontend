import { AppRequest, Link } from '../components/AppScreenManager'

interface CreateLink<T, R = Link> {
  (callback: (params: T) => T): (current: AppRequest) => R
  (params: Partial<T>): R
}

export type Navigate<T> = CreateLink<T, void>

export function createLinkFactory<T>(
  reqToParams: (req: AppRequest) => T,
  paramsToLink: (params: T) => Link,
) {
  const createLink: CreateLink<T> = (paramsOrCallback) => {
    if (typeof paramsOrCallback === 'function') {
      return (current: AppRequest) => {
        const params = reqToParams(current)
        const link = paramsToLink(paramsOrCallback(params))
        return link
      }
    } else {
      return paramsToLink(paramsOrCallback as T) as any
    }
  }

  return createLink
}
