/** Return true, if first range is fully containing the second range */
export function isSuperRange(
  aStartDateStr: string | undefined,
  aEndDateStr: string | undefined,
  bStartDateStr: string | undefined,
  bEndDateStr: string | undefined,
) {
  const parsed = [
    aStartDateStr ? new Date(aStartDateStr) : undefined,
    aEndDateStr ? new Date(aEndDateStr) : undefined,
    bStartDateStr ? new Date(bStartDateStr) : undefined,
    bEndDateStr ? new Date(bEndDateStr) : undefined,
  ]

  for (const d of parsed) {
    if (d !== undefined && isNaN(d as any)) {
      return false
    }
  }

  const [aStart, aEnd, bStart, bEnd] = parsed.map((d) => d?.getTime())

  if (aStart && (!bStart || bStart < aStart)) {
    return false
  }

  if (aEnd && (!bEnd || bEnd > aEnd)) {
    return false
  }

  return true
}
