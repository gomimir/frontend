import { FacetSubject, FacetKind } from '../facets/FacetDescriptor'
import { AssetType } from '../graphql'
import { ResolvedFacetsWithData } from '../hooks/useFacets'
import { Index } from '../hooks/useIndex'
import { formatInt } from './format'

export function formatNumAssets({
  numAssets,
  facets,
  index,
}: {
  numAssets: number
  facets: ResolvedFacetsWithData[]
  index: Index
}) {
  if (numAssets > 0) {
    const typeFacet = facets.find(
      (f) =>
        f.desc.name === 'type' &&
        f.desc.subject === FacetSubject.File &&
        f.desc.kind === FacetKind.TextProperty,
    )

    if (typeFacet?.data?.length) {
      const types = [...typeFacet.data].sort((a, b) => b.numAssets - a.numAssets)
      const info = []
      let numProcessed = 0
      for (let i = 0; i < 2 && i < types.length; i++) {
        if (types[i].value === 'image') {
          numProcessed += types[i].numAssets
          if (index.assetType === AssetType.Photo) {
            info.push(
              types[i].numAssets === 1
                ? '1\xA0photo'
                : `${formatInt(types[i].numAssets)}\xA0photos`,
            )
          } else {
            info.push(
              types[i].numAssets === 1
                ? '1\xA0image'
                : `${formatInt(types[i].numAssets)}\xA0images`,
            )
          }
        } else if (types[i].value === 'video') {
          numProcessed += types[i].numAssets
          info.push(
            types[i].numAssets === 1 ? '1\xA0video' : `${formatInt(types[i].numAssets)}\xA0videos`,
          )
        }
      }

      if (types.length > info.length) {
        if (info.length === 0) {
          info.push(numAssets === 1 ? '1\xA0file' : `${formatInt(numAssets)}\xA0files`)
        } else {
          const numOtherAssets = types.reduce((sum, t) => sum + t.numAssets, 0) - numProcessed
          if (numOtherAssets) {
            info.push(
              numOtherAssets === 1
                ? '1\xA0other\xA0file'
                : `${formatInt(numOtherAssets)}\xA0other\xA0files`,
            )
          }
        }
      }

      return info.length < 3 ? info.join(' and ') : `${info[0]}, ${info[1]} and ${info[2]}`
    }
  }

  return `${formatInt(numAssets)}\xA0${index.messages.assetName(numAssets)}`
}
