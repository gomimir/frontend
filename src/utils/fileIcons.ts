import { FaFileAlt, FaFileImage, FaFileVideo } from 'react-icons/fa'

export function getFileIcon(filename?: string) {
  if (filename) {
    const m = filename.match(/[^.]+$/)
    if (m) {
      const normalizedSuffix = m[0].toLowerCase()
      switch (normalizedSuffix) {
        case 'jpg':
        case 'jpeg':
        case 'heic':
        case 'arw':
        case 'cr2':
          return FaFileImage

        case 'mov':
          return FaFileVideo
      }
    }
  }

  return FaFileAlt
}
