export function getCommonPrefix(filenames: string[]) {
  if (filenames.length) {
    for (let i = 0; true; i++) {
      for (const filename of filenames) {
        if (i === filename.length || filename[i] !== filenames[0][i]) {
          return filenames[0].substr(0, i)
        }
      }
    }
  }

  return ''
}

export function getCommonFilenameLength(files: Array<{ filename: string }>) {
  if (files.length) {
    let strIdx = 0

    if (files.length > 1) {
      outer: for (; strIdx < files[0].filename.length; strIdx++) {
        for (let i = 1; i < files.length; i++) {
          if (
            strIdx >= files[i].filename.length ||
            files[0].filename[strIdx] !== files[i].filename[strIdx]
          ) {
            break outer
          }
        }
      }

      strIdx = Math.min(files[0].filename.lastIndexOf('/') + 1, strIdx)
    } else {
      strIdx = files[0].filename.lastIndexOf('/')
      if (strIdx > -1) {
        return strIdx + 1
      } else {
        return 0
      }
    }

    return strIdx
  }

  return 0
}
