export enum ActiveItemActionKind {
  SetIndex = 'SET_INDEX',
  UnsetIndex = 'UNSET_INDEX',
  Preview = 'PREVIEW',
}

interface ActiveItemAction {
  kind: ActiveItemActionKind
}

export interface UnsetActiveItemAction extends ActiveItemAction {
  kind: ActiveItemActionKind.UnsetIndex
}

export interface SetActiveItemAction extends ActiveItemAction {
  kind: ActiveItemActionKind.SetIndex
  index: number
}

export interface PreviewAssetAction extends ActiveItemAction {
  kind: ActiveItemActionKind.Preview
  index?: number
}

export type ActiveItemActionUnion = SetActiveItemAction | UnsetActiveItemAction | PreviewAssetAction
