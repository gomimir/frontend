#!/bin/sh

cp -R /app/build /app/public
export REACT_APP_CONFIG=`jq -nMcj --arg httpUrl "$HTTP_URL" --arg wsUrl "$WS_URL" '{"httpUrl":$httpUrl,"wsUrl":$wsUrl}'`
envsubst '$REACT_APP_CONFIG' < /app/build/index.html > /app/public/index.html

exec /goStatic --path /app/public --fallback index.html